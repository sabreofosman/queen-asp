import {FacePart} from "./face_part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {Player} from "../player/player";

declare class Iris extends FacePart {
	constructor(...data: object[]);
}


export class IrisHuman extends Iris {
	constructor(...data: object[]);

	stroke(): string;
	fill(ignore: any, ex: DrawingExports): string;
	getLineWidth(avatar: Player): number;
	clipStroke(ex: DrawingExports): DrawPoint[];
	clipFill(ex: DrawingExports): DrawPoint[];
	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
