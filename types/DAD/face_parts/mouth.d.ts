import {FacePart} from "./face_part";
import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";

declare class Mouth extends FacePart {
	constructor(...data: object[]);
}


export class MouthHuman extends Mouth {
	constructor(...data: object[]);

	fill(ignore: any, ex: DrawingExports): string;
	clipFill(): void;
	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}
