import {DrawPoint} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {ShadingPart} from "../draw/shading_part";
import {BodyPart} from "./part";

declare class GroinShading extends ShadingPart {
	constructor(...data: object[]);

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


declare class Groin extends BodyPart {
	constructor(...data: object[]);
}

export class GroinHuman extends Groin {
	constructor(...data: object[]);

	getLineWidth(): number;
	calcDrawPoints(ex: DrawingExports, mods: object, calculate: boolean): DrawPoint[];
}
