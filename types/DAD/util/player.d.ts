/**
 * Get a default properties factory
 * @param limits Descriptors of properties, each with avg, low, high, stdev, bias
 * @param discretePool List of valid values for discrete valued properties
 */
export function getDefault(limits: object, discretePool: object): () => object;

/**
 * Apply additional modifiers
 * @param sourceMods Modifiers to be modified
 * @param addMods Modifiers to add
 */
export function applyMods(sourceMods: object, addMods: object): void;

/**
 * Remove modifiers
 * @param sourceMods Modifiers to be modified
 * @param addMods Modifiers to remove
 */
export function removeMods(sourceMods: object, addMods: object): void;

// CREATE CHARACTER
/**
 * Retrieve female bias with global lookup fallback
 * @param propertyDescriptor Statistical description of property including avg, low, high
 * @param propertyName
 * @returns Female bias
 */
export function getBiasMod(propertyDescriptor: object, propertyName: string): number;

declare class Transformation {
	transform: (percent: number) => number;
	showTransformation: (percent: number) => void;
	object: any;
}

/**
 * Create a transformation object that can be passed into transformAndShow
 * @param object Any object (such as an avatar)
 * @param showTransformation Function to run at each step of the transformation; it will receive percent of completion (up to 1)
 * @param transformBy Object matching a subset of the structure of object (can miss properties, but can't have extra)
 */
export function createTransformation(object: object, showTransformation:Function, transformBy: object): Transformation;

export function transformAndShow(transformation: Transformation, duration?: number): void;
