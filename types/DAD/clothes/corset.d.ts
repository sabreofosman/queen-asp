import {DrawPoint} from "drawpoint/dist-esm";
//import {connectEndPoints} from "../draw/draw";
import {DrawingExports} from "../draw/draw";
import {SideValue} from "../parts/part";
import {ClothingPart, ClothingPartConstructor} from "./clothing";
import {Top} from "./tops";


export class CorsetBreastPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export function calcCorset(ex: DrawingExports): DrawPoint[];

export class CorsetPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

export class HalfCorsetPart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}

/* */

export class Corset extends Top { //dress?
	constructor(...data: object[]);

	botCoverage: number;
	thickness: number;
	lacing: boolean;
	knots: number;
	highlight: string;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}

export class HalfCorset extends Top { //dress?
	constructor(...data: object[]);

	topCoverage: number;
	botCoverage: number;
	thickness: number;
	lacing: boolean;
	knots: number;
	highlight: string;

	stroke(): string;
	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
