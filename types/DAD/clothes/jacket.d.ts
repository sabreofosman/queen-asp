import {
	DrawPoint,
	Point
} from "drawpoint/dist-esm";
import {DrawingExports} from "../draw/draw";
import {ShadingPart} from "../draw/shading_part";
import {SideValue} from "../parts/part";
import {Clothes, Clothing, ClothingPart, ClothingPartConstructor} from "./clothing";

export class JacketBaseShading extends ShadingPart {
	constructor(...data: object[]);

	forcedSide: SideValue;

	calcDrawPoints(ex: DrawingExports): DrawPoint[];
}


export class JacketBasePart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


/**
 * Calculate the drawpoints for a jacket
 * @this {stomachCoverage: Number, chestCoverage: Number}
 * @param ex
 * @returns {{}
 */
export function calcJacket(ex: DrawingExports): {collarbone: Point, top: DrawPoint, mid: DrawPoint, bot: {x: number, y: number}, outBot: DrawPoint, outMid: DrawPoint, breastBot: DrawPoint, breastTip: DrawPoint, outTop: DrawPoint};

/**
 * Long sleeve means between elbow and wrist
 */
export class MediumLooseSleevePart extends ClothingPart {
	constructor(...data: object[]);

	renderClothingPoints(ex: DrawingExports, ctx: CanvasRenderingContext2D): void;
}


export class Jacket extends Clothing {
	constructor(...data: object[]);

	clothingLayer: Clothes.Layer;
	/**
	 * How much of the stomach should be covered (1 means fully)
	 */
	stomachCoverage: number;
	/**
	 * How much of the chest should be covered (horizontally)
	 */
	chestCoverage: number;
	/**
	 * How far to extend the sleeve (between 0 and 1)
	 */
	sleeveLength: number;
	/**
	 * How wide the patterned lining is
	 */
	liningWidth: number;
	liningPattern: string;
	thickness: number;
	/**
	 * How tightly it clings to the body
	 */
	cling: number;
}


export class LooseJacket extends Jacket {
	constructor(...data: object[]);

	stomachCoverage: number;
	sleeveLength: number;

	fill(): string;
	get partPrototypes(): {side: SideValue, Part: ClothingPartConstructor}[];
}
