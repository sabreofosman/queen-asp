const gulp = require('gulp'),
	concat = require('gulp-concat'),
	git = require('gulp-git'),
	esbuild = require('esbuild'),
	log = require('fancy-log-levels'),
	noop = require('gulp-noop'),
	newer = require('gulp-newer'),
	postcss = require('gulp-postcss'),
	shell = require('gulp-shell'),
	sort = require('gulp-sort'),
	sourcemaps = require('gulp-sourcemaps'),
	ts = require('gulp-typescript'),
	autoprefixer = require('autoprefixer'),
	which = require('which'),
	fs = require('fs'),
	path = require('path'),
	os = require('os'),
	del = require('del'),
	yargs = require('yargs'),
	cfg = require('./build.config.json');

const args = yargs.options({
	verbosity: {type: 'number', default: 0},
	release: {type: 'boolean', default: false},
	embedsourcemaps: {type: 'boolean', default: false},
	sourcemapsincludecontent: {type: 'boolean', default: false}
}).argv;

const htmlOut = "tmp.html";
const moduleSciptsBundle = "module-bundle.js";
const moduleStylesBundle = "module-bundle.css";

log(args.verbosity);

function tweeCompilerExecutable() {
	return "tweego";
}

function buildTS(tsProjectFile, destDir, destFileName) {
	const tsOptions = {
		noEmit: false,
		outFile: destFileName,
		noEmitOnError: false
	};
	var tsProject = ts.createProject(tsProjectFile, tsOptions);
	const addSourcemaps = !args.release;
	const prefix = path.relative(destDir, '.');

	return tsProject.src()
		.pipe(addSourcemaps ? sourcemaps.init() : noop())
		.pipe(tsProject())
		.pipe(addSourcemaps ?
			sourcemaps.write(args.embedsourcemaps ? undefined : '.', {
				includeContent: args.sourcemapsincludecontent,
				sourceRoot: prefix,
				sourceMappingURLPrefix: path.relative(cfg.dirs.output, destDir)
			}) :
			noop())
		.pipe(gulp.dest(destDir));
}

function concatFiles(srcGlob, destDir, destFileName) {
	return gulp.src(srcGlob)
		.pipe(newer(destFileName))
		.pipe(sort())
		.pipe(concat(destFileName))
		.pipe(gulp.dest(destDir));
}

function bundleScriptModules(cb) {
	const prefix = path.relative(cfg.dirs.output, 'modules');
	return esbuild.build({
		entryPoints: ['modules/npm-modules.js'],
		bundle: true,
		sourcemap: !args.release,
		sourceRoot: prefix,
		outfile: path.join(cfg.dirs.intermediate, moduleSciptsBundle),
	}).then(() => cb());
}

function bundleStyleModules(cb) {
	return esbuild.build({
		entryPoints: ['modules/npm-styles.css'],
		bundle: true,
		sourcemap: !args.release,
		outfile: path.join(cfg.dirs.intermediate, moduleStylesBundle),
	}).then(() => cb());
}

function processScripts(srcGlob, destDir, destFileName) {
	const addSourcemaps = !args.release;
	const prefix = path.relative(destDir, srcGlob.substr(0, srcGlob.indexOf('*')));
	return gulp.src(srcGlob)
		.pipe(sort())
		.pipe(addSourcemaps ? sourcemaps.init() : noop())
		.pipe(concat(destFileName))
		.pipe(addSourcemaps ?
			sourcemaps.write(args.embedsourcemaps ? undefined : '.', {
				includeContent: args.sourcemapsincludecontent,
				sourceRoot: prefix,
				sourceMappingURLPrefix: path.relative(cfg.dirs.output, destDir)
			}) :
			noop())
		.pipe(gulp.dest(destDir));
}

function processStylesheets(srcGlob, destDir, destFileName) {
	const addSourcemaps = !args.release;
	const prefix = path.relative(destDir, srcGlob.substr(0, srcGlob.indexOf('*')));
	return gulp.src(srcGlob)
		.pipe(newer(path.join(destDir, destFileName)))
		.pipe(sort())
		.pipe(addSourcemaps ? sourcemaps.init() : noop())
		.pipe(concat(destFileName))
		.pipe(cfg.options.css.autoprefix ?
			postcss([autoprefixer({overrideBrowserslist: ['last 2 versions']})]) :
			noop())
		.pipe(addSourcemaps ?
			sourcemaps.write(args.embedsourcemaps ? undefined : '.', {
				includeContent: args.sourcemapsincludecontent,
				sourceRoot: prefix,
				sourceMappingURLPrefix: path.relative(cfg.dirs.output, destDir)
			}) :
			noop())
		.pipe(gulp.dest(path.normalize(destDir)));
}

function processSrc(name, processorFunc, globs, destDir, destFileName, ...args) {
	let tasks = [];
	if (!Array.isArray(globs) || globs.length === 1) {
		const src = Array.isArray(globs) ? globs[0] : globs;
		tasks.push(() => processorFunc(src, destDir, destFileName, args));
		tasks[tasks.length - 1].displayName = "process-" + name;
	} else { // many globs
		const ext = path.extname(destFileName);
		const bn = path.basename(destFileName, ext);
		for (let i = 0; i < globs.length; ++i) {
			tasks.push(() => processorFunc(globs[i], destDir, `${bn}-${i}${ext}`, args));
			tasks[tasks.length - 1].displayName = `process-${name}-${i}`;
		}
	}
	const res = gulp.parallel(...tasks);
	res.displayName = name;
	return res;
}

function injectGitCommit(cb) {
	cb();
/*
	git.revParse({args: '--short HEAD'}, function(err, hash) {
		if (!err) {
			log.info('current git hash: ', hash);
			fs.writeFile(cfg.gitVersionFile, `App.Version.commitHash = '${hash}';\n`, cb);
		} else {
			cb();
		}
	});
*/
}

function cleanupGit(cb) {
	cb();
//	fs.unlink(cfg.gitVersionFile, cb);
}

function resolveExternalModuleJs(name) {
	if (fs.existsSync(name)) {
		return name;
	}

	// then check in "modules" dir
	const probe = `modules/${name}.js`;
	if (fs.existsSync(probe)) {
		return path.relative(process.cwd(), probe);
	}

	// the last case is a node module
	let mp = require.resolve(name);
	const minMp = mp.replace(".js", ".min.js");
	if (fs.existsSync(minMp)) {
		return path.relative(process.cwd(), minMp);
	}

	return path.relative(process.cwd(), mp);
}

function resolveExternalModuleCss(name) {
	if (fs.existsSync(name)) {
		return name;
	}
	const moduleDir = `./node_modules/${name}`;
	const pkgJson = require(`${moduleDir}/package.json`);
	return path.relative(process.cwd(), path.join(moduleDir, pkgJson.style));
}


function tweeCompileCommand() {
	fs.mkdirSync(cfg.dirs.output, { recursive: true });
	let srcs = [path.join(path.normalize(cfg.dirs.intermediate), "story"), ...cfg.sources.story.media]

	let modules = [];
	modules = modules.concat(cfg.sources.externalModule.js.map(m => resolveExternalModuleJs(m)));
	modules = modules.concat(cfg.sources.externalModule.css.map(m => resolveExternalModuleCss(m)));
	modules.push(path.join(cfg.dirs.intermediate, moduleSciptsBundle));
	modules.push(path.join(cfg.dirs.intermediate, moduleStylesBundle));

	log.debug(modules);

	let moduleArgs = modules.map(fn => `--module=${fn}`);
	return `${tweeCompilerExecutable()} --head=${cfg.sources.head} -o ${path.join(cfg.dirs.intermediate, htmlOut)} ${moduleArgs.join(' ')} ${srcs.join(' ')}`;
}

gulp.task('compileStory', shell.task(tweeCompileCommand(), { env: { ...process.env, ...cfg.options.twee.environment }, verbose: args.verbosity >= 3 }));

function prepareComponent(name) {
	const processors = {
		"css": {
			func: processStylesheets,
			output: "-styles.css"
		},
		"js": {
			func: processScripts,
			output: "-js-script.js"
		},
		"ts": {
			func: buildTS,
			output: "-ts.script.js"
		},
		"twee": {
			func: concatFiles,
			output: ".twee"
		},
		"media": {
			func: null
		}
	};

	const c = cfg.sources[name];
	const subTasks = [];
	for (const srcType in c) {
		const proc = processors[srcType];
		if (proc.func) {
			const outFile = `${name}${proc.output}`;
			subTasks.push(processSrc(`${name}-${srcType}`, proc.func, c[srcType], path.join(cfg.dirs.intermediate, name), outFile, cfg.options[srcType]));
		}
	}
	let r = gulp.parallel(subTasks);
	r.displayName = "prepare-" + name;
	return r;
}

function makeThemeCompilationTask(themeName) {
	const taskName = `make-theme-${themeName}`;
	gulp.task(taskName, function() {
		return processStylesheets(`${cfg.sources.themes.baseDir}/${themeName}/**/*.css`,
			path.join(cfg.dirs.output, cfg.sources.themes.outSubDir), `theme-${themeName}.css`);
	});
	return taskName;
}

const themeTasks = fs.readdirSync(cfg.sources.themes.baseDir)
	.filter(entry => fs.statSync(path.join(cfg.sources.themes.baseDir, entry)).isDirectory())
	.map(entry => makeThemeCompilationTask(entry));

function moveHTMLInPlace(cb) {
	fs.rename(path.join(cfg.dirs.intermediate, htmlOut), path.join(cfg.dirs.output, cfg.product), cb);
}

function clean() {
	if (fs.existsSync(cfg.gitVersionFile)) {
		fs.unlinkSync(cfg.gitVersionFile);
	}

	return del(
		[`${cfg.dirs.intermediate}/**`]
	);
}

function prepare(cb) {
	return gulp.series(clean, injectGitCommit,
		gulp.parallel(bundleScriptModules, bundleStyleModules, prepareComponent("story")))(cb);
}

if (fs.existsSync('.git') && fs.statSync('.git', ).isDirectory()) {
	gulp.task('buildHTML', gulp.series(prepare, 'compileStory', cleanupGit));
} else {
	gulp.task('buildHTML', gulp.series(prepare, 'compileStory'));
}

exports.clean = clean;
exports.html = gulp.series('buildHTML', moveHTMLInPlace);
exports.themes = gulp.parallel(themeTasks);
exports.default = exports.html
exports.all = gulp.parallel(exports.html, exports.themes);
