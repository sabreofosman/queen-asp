// Weak Goblin
namespace App.Combat {
	const defaultLoseHandler = (p: Entity.Player): void => {p.adjustStat(CoreStat.Energy, -1)};

	Object.append(encounterData, {
		'abamond goblin weak': {
			enemies: ["weak goblin"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 1,
					max: 20
				},
				{
					chance: 20,
					type: 'Random',
					min: 50,
					max: 100
				},
				{
					chance: 10,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Weak Slime
		'abamond slime weak': {
			enemies: ["weak slime"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 1,
					max: 20
				},
				{
					chance: 20,
					type: 'Random',
					min: 50,
					max: 100
				},
				{
					chance: 10,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Goblin Scout
		'abamond goblin scout': {
			enemies: ["goblin scout"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 5,
					max: 35
				},
				{
					chance: 30,
					type: 'Random',
					min: 50,
					max: 125
				},
				{
					chance: 20,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Blue Slime
		'abamond blue slime': {
			enemies: ["blue slime"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 5,
					max: 35
				},
				{
					chance: 30,
					type: 'Random',
					min: 50,
					max: 125
				},
				{
					chance: 20,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Goblin Hunter
		'abamond goblin hunter': {
			enemies: ["goblin hunter"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 5,
					max: 55
				},
				{
					chance: 30,
					type: 'Random',
					min: 50,
					max: 160
				},
				{
					chance: 30,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Brown Slime
		'abamond brown slime': {
			enemies: ["brown slime"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 5,
					max: 55
				},
				{
					chance: 30,
					type: 'Random',
					min: 50,
					max: 160
				},
				{
					chance: 30,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Goblin Warrior
		'abamond goblin warrior': {
			enemies: ["goblin warrior"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 15,
					max: 75
				},
				{
					chance: 30,
					type: 'Random',
					min: 80,
					max: 180
				},
				{
					chance: 35,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Pink Slime
		'abamond pink slime': {
			enemies: ["pink slime"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 15,
					max: 75
				},
				{
					chance: 30,
					type: 'Random',
					min: 80,
					max: 180
				},
				{
					chance: 35,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Cave Crawler
		'abamond crawler': {
			enemies: ["cave crawler"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 15,
					max: 75
				},
				{
					chance: 30,
					type: 'Random',
					min: 80,
					max: 180
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 10,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Zombie
		'abamond zombie': {
			enemies: ["zombie"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 15,
					max: 75
				},
				{
					chance: 30,
					type: 'Random',
					min: 80,
					max: 180
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 10,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Dark Cave Crawler
		'abamond dark crawler': {
			enemies: ["dark cave crawler"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 15,
					max: 75
				},
				{
					chance: 30,
					type: 'Random',
					min: 100,
					max: 200
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 20,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Dark Zombie
		'abamond dark zombie': {
			enemies: ["dark zombie"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 15,
					max: 75
				},
				{
					chance: 30,
					type: 'Random',
					min: 100,
					max: 200
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 20,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Deep Crawler
		'abamond deep crawler': {
			enemies: ["deep cave crawler"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 15,
					max: 75
				},
				{
					chance: 30,
					type: 'Random',
					min: 100,
					max: 200
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 30,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				},
				{
					chance: 10,
					type: Items.Category.MiscLoot,
					tag: 'glowing crystal',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Deep Zombie
		'abamond deep zombie': {
			enemies: ["deep zombie"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 15,
					max: 75
				},
				{
					chance: 30,
					type: 'Random',
					min: 100,
					max: 200
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 30,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				},
				{
					chance: 10,
					type: Items.Category.MiscLoot,
					tag: 'glowing crystal',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Hobgoblin Chief
		'abamond hobgoblin chief': {
			enemies: ["hobgoblin chief"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 25,
					max: 100
				},
				{
					chance: 30,
					type: 'Random',
					min: 120,
					max: 240
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 35,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				},
				{
					chance: 15,
					type: Items.Category.MiscLoot,
					tag: 'glowing crystal',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Gelatinous Cube
		'abamond gelatinous cube': {
			enemies: ["gelatinous cube"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 25,
					max: 100
				},
				{
					chance: 30,
					type: 'Random',
					min: 120,
					max: 240
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 35,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				},
				{
					chance: 15,
					type: Items.Category.MiscLoot,
					tag: 'glowing crystal',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Hobgoblin Sorcerer
		'abamond hobgoblin sorcerer': {
			enemies: ["hobgoblin sorcerer"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 25,
					max: 110
				},
				{
					chance: 30,
					type: 'Random',
					min: 120,
					max: 240
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				},
				{
					chance: 20,
					type: Items.Category.MiscLoot,
					tag: 'glowing crystal',
					min: 1,
					max: 1,
				},
				{
					chance: 5,
					type: Items.Category.MiscLoot,
					tag: 'stone tablet',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Death Jelly
		'abamond death jelly': {
			enemies: ["death jelly"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 25,
					max: 110
				},
				{
					chance: 30,
					type: 'Random',
					min: 120,
					max: 240
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				},
				{
					chance: 20,
					type: Items.Category.MiscLoot,
					tag: 'glowing crystal',
					min: 1,
					max: 1,
				},
				{
					chance: 5,
					type: Items.Category.MiscLoot,
					tag: 'stone tablet',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Succubus
		'abamond succubus': {
			enemies: ["succubus"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 25,
					max: 110
				},
				{
					chance: 30,
					type: 'Random',
					min: 120,
					max: 240
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				},
				{
					chance: 20,
					type: Items.Category.MiscLoot,
					tag: 'glowing crystal',
					min: 1,
					max: 1,
				},
				{
					chance: 10,
					type: Items.Category.MiscLoot,
					tag: 'stone tablet',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		},

		// Incubus
		'abamond incubus': {
			enemies: ["incubus"],
			fatal: false,
			winPassage: "CombatAbamondGenericWin",
			losePassage: "CombatAbamondGenericLoseNonFatal",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 25,
					max: 110
				},
				{
					chance: 30,
					type: 'Random',
					min: 120,
					max: 240
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'broken rune',
					min: 1,
					max: 2,
				},
				{
					chance: 40,
					type: Items.Category.MiscLoot,
					tag: 'old arrowhead',
					min: 1,
					max: 1,
				},
				{
					chance: 20,
					type: Items.Category.MiscLoot,
					tag: 'glowing crystal',
					min: 1,
					max: 1,
				},
				{
					chance: 10,
					type: Items.Category.MiscLoot,
					tag: 'stone tablet',
					min: 1,
					max: 1,
				}
			],
			loseHandler: defaultLoseHandler
		}
	});
}
