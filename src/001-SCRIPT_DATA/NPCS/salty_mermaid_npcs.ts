App.Data.npcs["Crew"] = {
	name: "Crew", // This is a place holder and not meant to be used as an actual NPC.
	location: 'Salty Mermaid',
	mood: 0,
	dailyMood: -2,
	lust: 100,
	dailyLust: 20,
	gender: 1,
	title: "Crew of the Salty Mermaid",
	longDesc: "\
        The crew of the Salty Mermaid are a murderous and ruthless bunch interested in killing… and plugging your sissy ass."
};

App.Data.npcs["Cook"] = {
	name: "Cookie",
	location: 'Salty Mermaid',
	mood: 20,
	dailyMood: -2,
	lust: 50,
	dailyLust: 10,
	gender: 1,
	title: "NPC_NAME, the Ship's Cook",
	longDesc: "\
        NPC_NAME is a pasty old-timer with a massive paunch. Sweat plasters the sparse hair on his head to his pallid pate. \
        The bristling thicket of peppery grey hair blanketing the rest of him is lush and glistens with grease. \
        His soiled blouse hangs open to showcase his bear-like beer-belly and his fat furry feet are bare and blackened.\n\n\
        The apron dangling next to him weeps food and other mysterious substances in various states of spoilage onto the galley \
        floor.\n\n\
        He leers at you with an unsavory grin and slops his thick lips with his puffy tongue.",
	briefDesc: "The cook at the Salty Mermaid",
	store: "galley"
};

App.Data.npcs["Captain"] = {
	name: "Captain Reginald",
	location: 'Salty Mermaid',
	mood: 20,
	dailyMood: -2,
	lust: 50,
	dailyLust: 10,
	gender: 1,
	title: "NPC_NAME, plunderer of the Lost Coast",
	longDesc: "\
        NPC_NAME is an older, elegantly dressed man with well coiffed mane of long black hair and an almost aristocratic \
        demeanor. He certainly doesn't look the part of a blood thirsty pirate, which is probably why he was so easily able \
        to fool you.",
	briefDesc: "The captain of the Salty Mermaid"
};

App.Data.npcs["FirstMate"] = {
	name: "Kipler",
	location: 'Salty Mermaid',
	mood: 20,
	dailyMood: -2,
	lust: 60,
	dailyLust: 10,
	gender: 1,
	title: "First Mate NPC_NAME",
	longDesc: "\
        First Mate NPC_NAME is a fearsome bruiser in his early 30's. A living statue of sleek ebony skin burnished over \
        slabs of furrowed muscle, he stands marvellous and still, fit to challenge even the majesty and poise of the very \
        figurehead on the prow. He is clean-shaven of both face and head and wears naught but bleached breeches, clearly \
        concerned with neither adornments nor attachments.\n\n\
        His dark eyes look straight through you as you approach, as if you were only another earthly entanglement he \
        would prefer to neglect.",
	briefDesc: "NPC_NAME is the captain's right hand at the Salty Mermaid"
};

App.Data.npcs["Quartermaster"] = {
	name: "Julius",
	location: 'Salty Mermaid',
	mood: 20,
	dailyMood: -2,
	lust: 50,
	dailyLust: 10,
	gender: 1,
	title: "Quartermaster NPC_NAME",
	longDesc: "\
        NPC_NAME is a whelp in both age and appearance. Bucked teeth and excitable eyes betray his rodent-like \
        nature even before he begins acting skittish, as he is now. His taupe skin is blotchy with freckles and a \
        discoloration like a splat of old oatmeal covers the right side of his inverted triangle of a face. A shock \
        of woolly red hair sits atop his head like a big bloody dandelion.\n\n\
        You expect that he weaseled out of serving as cabin girl himself by dint of looking very much like a weasel.\n\n\
        He eyes you with both delight and disdain as you approach, obviously viewing you as one of the very few onboard \
        to whom he is superior.",
	store: "cargo"
};

App.Data.npcs["Dummy"] = {
	name: 'Practice Dummy',
	location: 'Salty Mermaid',
	mood: 100,
	dailyMood: 0,
	lust: 100,
	dailyLust: 0,
	gender: 2,
	title: 'NPC_NAME',
	longDesc: "\
        Just a practice dummy to be used by code. Not to ever be seen by players.\
        ",
}

App.Data.npcGroups["SaltyMermaidCrew"] = ["Crew", "Cook", "Quartermaster", "FirstMate", "Captain"];
