App.Data.npcs["Paola"] = {
	name: "Paola",
	location: 'Abamond',
	mood: 40,
	dailyMood: 0,
	lust: 40,
	dailyLust: 10,
	gender: 0,
	title: "NPC_NAME St. Marie, Voodoo Priestess",
	longDesc: "\
        This intimidatingly tall black woman is dressed in an odd assortment of coloured cloth and garish jewelry. Her \
        pendulous breasts jiggle and shake in their attempt to free themselves from the confines of her loose tube-top \
        whenever she breaks out into a hearty laugh.",
	briefDesc: "NPC_NAME is the Voodoo Priestess at the @@.location-name;Abamond@@"
};

App.Data.npcs["Papa Baba"] = {
	name: "Papa Baba",
	location: 'Abamond',
	mood: 40,
	dailyMood: 0,
	lust: 40,
	dailyLust: 10,
	gender: 1,
	title: "NPC_NAME Houngan",
	longDesc: "\
        This lean, dusky voodoo priest is dressed in a threadbare black coat with stained, off-white breeches \
        and shoeless feet. On his head is an old capotain hat adorned with a spray of feathers and a crudely-painted \
        death's head. He also wears a necklace of alternating beads and crocodile teeth, a hemp cord bracelet with a \
        carved, wooden charm, and an ivory-colored sash tied around his waist. His solemn stare is slightly unsettling, \
        his eyes distant as if he is seeing the spirit and material worlds all at once.",
	briefDesc: "NPC_NAME is the Voodoo Priest at the @@.location-name;Abamond@@"
};

App.Data.npcs["Bobola Tree"] = {
	name: "Bobola Tree",
	location: 'Abamond',
	mood: 20,
	dailyMood: 0,
	lust: 100,
	dailyLust: 100,
	gender: 1,
	title: "An Ancient NPC_NAME",
	longDesc: "\
        This ancient tree stands alone in the dank swamp, it's roots forming a twisting network of tendrils that expand \
        outwards for meters. It's trunk is large and stout, adorned with long spindly limbs that stretch out the sky, \
        festooned with bright purple leaves. It might be a trick of the light, but every now and then you swear you \
        see one of the trees large roots move, just slightly. You have an uneasy feeling about this.",
	briefDesc: "An ancient tree at the @@.location-name;Abamond@@"
};

// Mamazons
App.Data.npcs["Bustilla"] = {
	name: "Bustilla",
	location: 'Abamond',
	mood: 50,
	dailyMood: 0,
	lust: 50,
	dailyLust: 0,
	gender: 0,
	title: "NPC_NAME, Queen of the Mamazons",
	longDesc: "\
        Bustilla is an statuesque blonde woman with a kind face. She stands at an imposing six feet tall \
        and is 'gifted' with breasts that can only be \
        described as impossibly improbable. You are left speechless at the ease of which she is able to manuever \
        her mammoth breasts and you wonder if all Mamazons are as strong as her, or if it is part of their \
        particular tribal training that allows them to handle such incredible hooters with almost no difficulty \
        at all.",
	briefDesc: "Queen of the Mamazons at the @@.location-name;Abamond@@"
};

App.Data.npcs["Emi Bigwuns"] = {
	name: "Emi",
	location: 'Abamond',
	mood: 50,
	dailyMood: 0,
	lust: 50,
	dailyLust: 0,
	gender: 0,
	title: "NPC_NAME <span class='npc'>Bigwuns</span>, Mamazon Trader",
	longDesc: "\
        Emi is a young girl, barely out of her teens, with a long mane of somewhat mousy brown hair that \
        attractively frames her plump face and green eyes. Despite being a Mamazon and coming from a \
        prestigious family, her breasts can only be described as 'utterly amazing', which is to say \
        completely substandard for Mamazon society. She reluctantly admits that this is primarily why \
        she became a merchant.",
	briefDesc: "Trader at the Mamazon village at the @@.location-name;Abamond@@",
	store: "emi"
};

App.Data.npcGroups["MamazonsVillageCitizens"] = ['Bustilla', 'Emi Bigwuns'];

// Glutezons
App.Data.npcs["Badonkadonk"] = {
	name: "Badonka-Donk",
	location: 'Abamond',
	mood: 50,
	dailyMood: 0,
	lust: 50,
	dailyLust: 0,
	gender: 0,
	title: "NPC_NAME, Queen of the Glutezons",
	longDesc: "\
        Badonka-Donk is a woman of average height with a pleasant face that looks far too young \
        on someone who leads a tribe of deadly jungle women. Her skin is a soft nutty brown color and \
        her body is remarkably toned, clearly she is someone who enjoys a great deal of physical activity. \
        Despite that, she looks quite comfortable on her throne - her absolutely enormous buttocks spilling \
        over the sides of her chair, with her well muscled thighs crossed on top of each other.",
	briefDesc: "Queen of the Gluetezons at the @@.location-name;Abamond@@"
};

App.Data.npcs["Didi"] = {
	name: "Didi",
	location: 'Abamond',
	mood: 50,
	dailyMood: 0,
	lust: 50,
	dailyLust: 0,
	gender: 0,
	title: "NPC_NAME de Bootay",
	longDesc: "\
        Didi is an older woman in her mid to late 30's with a shock of red hair and a figure like an overripe \
        pair. Her enormous buttocks wiggle and shake as she walks, barely contained by her voluminous loin \
        cloth. Didi is a descendant of a pirate that was shipwrecked on this island two generations ago, \
        which explains her unusual coloring and appreciation for items from more civilied parts.",
	briefDesc: "Trader at the Gluetezons village at the @@.location-name;Abamond@@",
	store: "didi"
};
