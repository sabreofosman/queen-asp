/* eslint-disable @typescript-eslint/no-unused-vars */
declare namespace App{
	const enum Gender {
		It = -1,
		Female = 0,
		Male = 1,
		Shemale = 2
	}

	type DynamicValue<T> = T | ((player: Entity.Player) => T);

	type DynamicText = DynamicValue<string>;
}

declare namespace App.Data {

	type SkillBonusData = Partial<Record<Skills.Any, [number, number, number]>>;

	const enum CosmeticsType {
		HairTool = "hair tool",
		HairProducts = "hair products",
		HairTreatment = "hair treatment",
		BasicMakeup = "basic makeup",
		ExpensiveMakeup = "expensive makeup"
	}

	//#region Items
	const enum ItemTypeConsumable {
		Food = "food",
		Potion = "potion",
		MiscConsumable = "misc",
		LootBox = "lootBox",
		MiscLoot = "miscLoot"
	}

	const enum ItemTypeSpecial {
		Quest = "quest",
		Reel = "reel"
	}

	type ItemTypeInventory = ItemTypeConsumable | ItemTypeSpecial | CosmeticsType;

	type ItemTypeEquipment = Items.ClothingType | Items.Category.Weapon;
	type ItemType = ItemTypeInventory | ItemTypeEquipment;
	type ItemTypesShop = Exclude<ItemType, ItemTypeSpecial.Quest>;

	interface ItemDesc {
		name: string;
		inMarket?: boolean;
		quest?: boolean;
		value?: number;
		priceCoefficient?: number;
	}

	interface InventoryItemDesc<TType extends ItemType> extends ItemDesc {
		shortDesc: string;
		longDesc: string;
		type: TType;
	}

	interface ConsumableItemDesc<TType extends ItemType> extends InventoryItemDesc<TType> {
		message?: string;
		/**
		 * Number of uses that are sold in a single item
		 * @default 1
		 */
		charges?: number;
		effects?: string[];
	}

	type DrugItemDesc = ConsumableItemDesc<ItemTypeConsumable.Potion>;
	type FoodItemDesc = ConsumableItemDesc<ItemTypeConsumable.Food>;
	type MiscConsumableItemDesc = ConsumableItemDesc<ItemTypeConsumable.MiscConsumable>;
	type MiscLootItemDesc = ConsumableItemDesc<ItemTypeConsumable.MiscLoot>;
	type LootBoxItemDesc = ConsumableItemDesc<ItemTypeConsumable.LootBox>;
	type QuestItemDesc = InventoryItemDesc<ItemTypeSpecial.Quest>;

	interface CosmeticsItemDesc extends ConsumableItemDesc<CosmeticsType> {
		skillBonus: SkillBonusData;
	}

	type HairColor = "black" | "blond" | "brown" | "red" | "lavender";

	interface ClothingItemDesc extends InventoryItemDesc<Items.ClothingType> {
		slot: ClothingSlot;
		restrict?: ClothingSlot[];
		color: string;
		rarity: Items.Rarity;
		style?: Fashion.Style[];
		wearEffect?: string[];
		activeEffect?: string[];
		inMarket?: boolean;
		locked?: boolean;
		meta?: string[];
	}

	interface WigItemDesc extends ClothingItemDesc {
		slot: ClothingSlot.Wig;
		hairLength?: number; // for wigs
		hairStyle?: string; // for wigs
		hairBonus?: number; // for wigs
	}

	type ReelWildcard = "beauty" | "fem" | "perv";
	type ReelAction = Whoring.SexActStr | ReelWildcard;

	interface ReelDesc extends ItemDesc {
		rank: Items.Rarity;
		css: string;
		data: Partial<Record<ReelAction, number>>;
	}

	type InventoryItemDescAny = DrugItemDesc | FoodItemDesc |
		MiscConsumableItemDesc | LootBoxItemDesc | QuestItemDesc |
		CosmeticsItemDesc;

	type EquipmentItemDescAny = ClothingItemDesc | WigItemDesc;
	type ItemDescAny = InventoryItemDescAny | EquipmentItemDescAny;
	//#endregion

	interface BeautyProcedureDesc {
		name: string;
		difficulty: number;
		style: number;
		short: string;
	}

	interface HairStyleDesc extends BeautyProcedureDesc {
		resource1: number; // RES1 = ACCESSORIES, RES2 = PRODUCT
		resource2: number;
		min: number;
		max: number;
	}

	interface MakeupStylesDesc extends BeautyProcedureDesc { // RES1 = BASIC MAKEUP, RES2 = EXPENSIVE MAKEUP
		resource1: number;
		resource2: number;
	}

	interface NPCDesc {
		name: string;
		mood: number;
		dailyMood: number;
		lust: number;
		dailyLust: number;
		gender:  0 | 1 | 2;
		title: string;
		longDesc: string;
		location: DynamicText;
		briefDesc?: string;
		store?: string;
	}

	interface StoreInventoryItem {
		category: Items.Rarity.Common | Items.Rarity.Rare;
		type: Items.Category;
		qty: number;
		max: number;
		price: number;
		mood: number;
		lock: 0 | 1;
		tag: string;
	}

	interface StoreDesc {
		name: string;
		open: DayPhase[];
		restock: number;
		maxRares?: number;
		unlockFlag?: string;
		unlockFlagValue?: string | number;
		inventory: StoreInventoryItem[];
	}

	interface ItemTypeDescMap {
		[Items.Category.Drugs]: DrugItemDesc;
		[Items.Category.Food]: FoodItemDesc;
		[Items.Category.Cosmetics]: CosmeticsItemDesc;
		[Items.Category.MiscConsumable]: MiscConsumableItemDesc
		[Items.Category.MiscLoot]: MiscLootItemDesc;
		[Items.Category.Clothes]: ClothingItemDesc | WigItemDesc;
		[Items.Category.Weapon]: ClothingItemDesc;
		[Items.Category.Quest]: QuestItemDesc;
		[Items.Category.LootBox]: LootBoxItemDesc;
		[Items.Category.Reel]: ReelDesc;
	}

	type ItemCategoryAny = keyof ItemTypeDescMap;
	type ItemNameTemplate<T extends keyof ItemTypeDescMap> = `${T}/${string}`;
	type ItemNameTemplateAny = ItemNameTemplate<ItemCategoryAny>;
	type ItemNameTemplateConsumable = ItemNameTemplate<keyof Items.ConsumableItemTypeMap>;
	type ItemNameTemplateEquipment = ItemNameTemplate<Items.Category.Clothes | Items.Category.Weapon>;
	type ItemNameTemplateInventory = ItemNameTemplate<keyof Items.InventoryItemTypeMap>;

	//#region Effects

	type EffectAction = (player: Entity.Player) => string | void;
	type ItemEffectAction<T extends Items.BaseItem> = (p: Entity.Player, o: T) => string | void;

	interface EffectDescBase {
		value: number;
		knowledge: string[];
	}

	interface Effect<TAction> extends EffectDescBase {
		fun: TAction;
	}

	type EffectDesc = Effect<EffectAction>;
	type ClothingWearEffectDesc = Effect<ItemEffectAction<Items.Clothing>>;

	type VirtualSkillStr = 'sharpness' | 'damageResistance';
	type WearSkill = Skills.AnyStr | VirtualSkillStr | CoreStatStr | BodyStatStr | DerivedBodyStatStr;
	type WearEffectAction = (s: WearSkill, o: Items.Clothing) => number;
	type ClothingActiveEffectDesc = Effect<WearEffectAction>;
	//#endregion

	//#region Loot boxes
	type ItemFilter<T extends keyof Data.ItemTypeDescMap> = (ds: Items.ItemDescDictionary<T>, category: T, pool: number) => Items.ItemDescDictionary<T>;
	interface LootTableItem {
		type: Items.Category | "coins";
		chance: number;
		min?: number;
		max?: number;
		maxCount?: number;
		free?: boolean;
		filter?: ItemFilter<Items.Category>;
	}

	interface LootItem {
		category: Items.Category;
		tag: string;
		min: number;
		max: number;
	}
	//#endregion

	namespace Tasks {
		class CheckResult {
			result: number;
			value: number;
			mod: number;
		}

		type CheckResults = Record<string, CheckResult>;

		type TaskCheckFunc = (player: Entity.Player, task: Scene, checks: CheckResults) => number;

		namespace Checks {
			interface Base<TType extends string> {
				type: TType;
				tag: string;
				opt?: "noScaling" | "random" | "raw";
			}

			interface BaseNumeric<TType extends string> extends Base<TType> {
				value?: number;
				difficulty: number;
			}

			interface StatOrSKill<TType extends string, TName extends string> extends BaseNumeric<TType> {
				name: TName;
			}

			type Stat = StatOrSKill<Stat.Core, CoreStatStr>;
			type Skill = StatOrSKill<Stat.Skill, Skills.AnyStr>;
			type Body = StatOrSKill<Stat.Body, BodyStatStr>;
			type Meta = StatOrSKill<"meta", "beauty" | "danceStyle">;
			interface Func extends Base<"func"> {
				value: number;
				func: TaskCheckFunc;
			}

			type Any = Stat | Skill | Body | Meta | Func;
		}

		namespace Posts {
			interface Base<T extends string> {
				type: T;
			}

			interface Option<T> {
				opt?: T;
			}

			interface Name<T> {
				name: T;
			}

			interface WearableOption {
				wear?: "wear";
			}

			interface Value<Type> {
				value: Type;
			}

			interface NumericValue extends Value<number> {
				/** check name to read factor value from */
				factor?: string;
				opt?: 'random';
			}

			type GenericValue<T> = Exclude<T, undefined> extends void ? Record<string, never> : T extends number ? NumericValue : Value<T>;

			type BaseValue<TType extends string, Value = number> = Base<TType> & GenericValue<Value>;
			interface BaseFlag<Type extends string > extends Base<Type> {
				name: string;
				value: TaskFlag | undefined;
				op?: "set" | "add"
			}

			type Named<TType extends string, N, V = number> = BaseValue<TType, V> & Name<N>;

			type Consumable = Named<Items.Category.Food | Items.Category.Drugs | Items.Category.Cosmetics, string>;
			type Slot = Base<"slot">;
			type Clothing = Named<Items.Category.Clothes | Items.Category.Weapon, string> & WearableOption;
			type Item = Named<"item", ItemNameTemplateAny>;
			type PickItem = Named<"pickItem", ItemCategoryAny, {price: number; metaKey?: string;}> & WearableOption;
			type ItemChoice = Named<"itemChoice", ItemNameTemplateAny>;
			type BodyXp = Named<"bodyXp", BodyStatStr>;
			type StatXp = Named<"statXp", CoreStatStr>;
			type SkillXp = Named<"skillXp", Skills.AnyStr>;
			type Stat = Named<Stat.Core, CoreStatStr>;
			type Body = Named<Stat.Body, BodyStatStr>;
			type Skill = Named<Stat.Skill, Skills.AnyStr>;

			interface NpcStat extends Named<"npcStat", App.NpcStat> {
				npc?: string | string[];
			}

			type JobFlag = BaseFlag<"jobFlag">;
			type QuestFlag = BaseFlag<"questFlag">;
			type Quest = Named<"quest", string, "start" | "complete">;
			type Counter = Named<"counter", string, number | undefined>;
			type LootBox = Named<Items.Category.LootBox, string>;
			type Money = BaseValue<"money">;
			type Tokens = BaseValue<"tokens">;
			type Store = Named<"store", string, string> & Option<"lock" | "unlock">;
			type ResetStore = Base<"resetShop"> & Name<string>;
			type CorruptWillpower = Named<"corruptWillpower", "low" | "medium" | "high">;// & Option<number>;
			type SailDays = Base<"sailDays"> & Value<number>;
			type TrackCustomers = Base<"trackCustomers"> & Name<string>;
			type SaveDate = Base<"saveDate"> & Name<string>;

			interface ProgressOp {
				op?: "set" | "add" | "multiply"
			}
			type TrackProgress = Named<"trackProgress", string, number | undefined> & ProgressOp;
			interface SetClothingLock extends Base<"setClothingLock"> {
				slot: ClothingSlot;
				value: boolean;
			}

			type GameState = Named<"gameState", "dayPhase">;
			type BodyEffect = Named<"bodyEffect", string, boolean>;

			/** Apply an effect from EffectLib */
			type Effect = Base<"effect"> & Name<string>;

			type Any = BodyXp | SkillXp | StatXp | Body | Skill | Stat | Slot | Consumable | Clothing | Item |
				ItemChoice | PickItem | NpcStat | Counter | SaveDate |
				JobFlag | QuestFlag | Quest | Store | ResetStore | LootBox | Money | Tokens | CorruptWillpower | SailDays |
				TrackCustomers | TrackProgress | SetClothingLock | GameState | BodyEffect | Effect;
		}

		type EqualityCondition = "eq" | "ne" | "==" | "!=" | boolean;
		type Condition = EqualityCondition | "gte" | "lte" | "gt" | "lt" |
			">=" | "<=" | ">" | "<" ;

		type ValueComparison<T> = T extends number ? Condition : T extends boolean ? never : EqualityCondition;
		//#region Requirements
		namespace Requirements {
			interface Base<Type extends string> {
				type: Type;
				altTitle?: string;
				group?: string;
			}

			interface BaseValue<Type extends string, T> extends Base<Type> {
				value: T;
				condition?: ValueComparison<T>;
			}

			interface Named<TType extends string, TName extends string> extends Base<TType> {
				name: TName;
			}

			interface Conditional<TType extends string, TName extends string> extends Base<TType> {
				name: TName;
				value?: boolean;
			}

			type NamedValue<Type extends string, TName extends string, T> = BaseValue<Type, T> & Named<Type, TName>;
			type NamedNumeric<Type extends string, TName extends string> = NamedValue<Type, TName, number>;
			type FlagValue = number | string | boolean | number[] | string[];

			type BaseFlag<TType extends string, TValue> = NamedValue<TType, string, TValue>;

			type Stat = NamedNumeric<Stat.Core, CoreStat>;
			type Skill = NamedNumeric<Stat.Skill, Skills.Any>;
			type Body = NamedNumeric<Stat.Body, BodyStat>;
			type Meta = NamedNumeric<'meta', "beauty">;
			type Money = BaseValue<"money", number>;
			type Tokens = BaseValue<"tokens", number>;

			interface NPCStat extends NamedNumeric<"npcStat", NpcStat> {
				option?: string;
			}
			interface DaysPassed extends BaseValue<"daysPassed", number> {
				name?: string;
			}

			type QuestFlag = BaseFlag<"questFlag", FlagValue>;
			type Quest = BaseFlag<"quest", QuestState>;
			type Job = BaseFlag<"jobFlag", FlagValue>;

			interface Item extends NamedNumeric<"item", ItemNameTemplateAny> {
				/** Do not show this in the quest requirements until player finds the item */
				hidden?: boolean;
			}

			type Equipped = Conditional<"equipped", ItemNameTemplateEquipment>;

			interface NameOrSLot {
				slot?: ClothingSlot;
				name?: ItemNameTemplateEquipment;
			}
			interface IsWearingBase extends BaseValue<"isWearing", boolean> {
				isLocked?: boolean;
			}

			type IsWearing = IsWearingBase & RequireAtLeastOne<NameOrSLot>;

			type StyleCategory = NamedNumeric<"styleCategory", keyof typeof Data.Fashion.Style>;
			type Style = BaseValue<"style", number>;
			type HairStyle = NamedValue<"hairStyle", string, boolean>;
			type HairColor = NamedValue<"hairColor", string, boolean>;
			type PortName = NamedValue<"portName", string, boolean>;
			type InPort = BaseValue<"inPort", number>;
			type TrackProgress = NamedNumeric<"trackCustomers" | "trackProgress", string>;

			type Any = Body | Stat | Skill | Meta | Money | Tokens |
				NPCStat | DaysPassed | Quest | QuestFlag | Job |
				Item | Equipped | IsWearing | StyleCategory |
				Style | HairStyle | HairColor |
				PortName | InPort | TrackProgress;
		}
		//#endregion

		//#region Triggers
		namespace Triggers {

			interface BooleanValueBase {
				value?: boolean;
			}

			interface GeneralValueBase<T> {
				value: T;
				condition?: Condition;
			}

			type ValueBase<T> = T extends boolean ?
				BooleanValueBase :
				GeneralValueBase<T>;

			interface Base<TType extends string>  {
				type: TType;
			}

			interface Named<T extends string> {
				name: T;
			}

			type BaseValue<TTYpe extends string, T> = Base<TTYpe> & ValueBase<T>;

			type NamedValue<TType extends string, NType extends string, Value> = BaseValue<TType, Value> & Named<NType>;
			type NamedNumeric<TType extends string, NType extends string> = NamedValue<TType, NType, number>;
			interface Random {
				opt?: "random";
			}

			type NPCStat = NamedNumeric<"npcStat", NpcStat>;
			type Random100 = BaseValue<"random100", number>;
			type Counter = NamedNumeric<"counter", string> & Random;
			type StatCore = NamedNumeric<Stat.Core, CoreStatStr> & Random;
			type StatBody = NamedNumeric<Stat.Body, BodyStatStr> & Random;
			type StatSkill = NamedNumeric<Stat.Skill, Skills.AnyStr> & Random;
			type FlagSet = NamedValue<"flagSet", string, boolean>;

			type Tag = NamedNumeric<"tag", string>;

			type HasItem = NamedValue<"hasItem", ItemNameTemplateAny, boolean>;

			type TrackProgress = NamedNumeric<"trackProgress", string>;

			type Any = NPCStat | NPCStat | Random100 | Counter |
				StatCore | StatBody | StatSkill | FlagSet | Tag | HasItem | TrackProgress;
		}
		//#endregion

		namespace Costs {
			interface Base<TType extends string> {
				type: TType;
				value: number;
			}

			interface Named<TType extends string, TName> extends Base<TType> {
				name: TName;
			}

			type Stat = Named<Stat.Core, CoreStatStr>;
			type Skill = Named<Stat.Skill, Skills.AnyStr>;
			type Body = Named<Stat.Body, BodyStatStr>;
			type Money = Base<"money">;
			type Tokens = Base<"tokens">;
			type Item = Named<"item", ItemNameTemplateAny>;
			type Time = Base<"time">;

			type Any = Stat | Skill | Body | Money | Tokens | Item | Time;
			type CostType = Any['type'];
		}

		interface Task {
			/** Job or quest identifier */
			id: string;
			title: string;
			/** Id of the NPC who gives the task */
			giver: string;
			/**
			 *  Is job hidden until requirements are met.
			 * @default false
			*/
			hidden?: boolean;
		}

		class ConditionalTextFragment {
			text: string;
			[key: string]: any;
		}

		interface JobSceneContent {
			post?: NonEmptyArray<Posts.Any>;
			text?: string | NonEmptyArray<ConditionalTextFragment | string>;
		}

		interface StaticJobSceneData {
			id: string;
			triggers?: NonEmptyArray<Triggers.Any>;
			triggersAny?: NonEmptyArray<Triggers.Any>;
			checks?: NonEmptyArray<Checks.Any>;
		}

		type JobScene = StaticJobSceneData & RequireAtLeastOne<JobSceneContent>;

		interface Job extends Task {
			pay?: number;
			tokens?: number;
			rating: 1 | 2 | 3 | 4 | 5;
			phases: NonEmptyArray<DayPhase>;
			/** Minimal number of days until the job can be repeated, can be 0 which means the same day */
			days: number;
			cost?: NonEmptyArray<Costs.Any>;
			requirements?: NonEmptyArray<Requirements.Any>;
			intro: string;
			start?: string;
			scenes: NonEmptyArray<JobScene>;
			jobResults?: NonEmptyArray<ConditionalTextFragment>;
			end?: string;
		}

		interface Quest extends Task {
			/** Flags that are required to trigger quest. */
			pre?: NonEmptyArray<Requirements.Any>;
			/** Flags that are set when quest is completed. */
			post?: NonEmptyArray<Posts.Any>;
			checks?: NonEmptyArray<Requirements.Any>;
			onAccept?: NonEmptyArray<Posts.Any>;
			onDayPassed?: (this: App.Quest, player: Entity.Player) => void;
			reward?: NonEmptyArray<Posts.Any>;
			intro: string;
			middle: string;
			finish: string;
			journalEntry: string;
			journalComplete: string;
		}
	}

	// #region Synergy
	interface SynergyElementChoice<T extends StatTypeStr> {
		type: T;
		name: StatTypeStrMap[T];
		bonus: number;
	}

	type BodySynergyElement = SynergyElementChoice<Stat.Body>;

	type SynergyElement = SynergyElementChoice<"body"> |
		SynergyElementChoice<"skill"> |
		SynergyElementChoice<"core">;

	type SkillSynergyData = Partial<Record<Skills.Any | CoreStat, SynergyElement[]>>;

	//#endregion

	interface StatLevelingRecord {
		cost: number;
		step: number;
	}
	interface StatLevelingRecordAdjective {
		adjective: string;
	}

	type MinimalStatLevelingData = Record<number, StatLevelingRecordAdjective>;

	interface NoneStatLevelingData {
		none: {cost: 0, step: 0}
	}

	interface FixedStatLevelingRecord {
		cost: number;
		step: number;
	}

	interface FixedStatLevelingData {
		fixed: FixedStatLevelingRecord;
	}

	type ExplicitStatLevelingRecord = StatLevelingRecord & StatLevelingRecordAdjective;

	type ExplicitStatLevelingData = Record<number, ExplicitStatLevelingRecord>;

	type LevelingCostFunction = (level: number) => number;
	type LevelingDataAny = MinimalStatLevelingData | ExplicitStatLevelingData | FixedStatLevelingData | NoneStatLevelingData

	type StatLevelingRecordAny = StatLevelingRecord | ExplicitStatLevelingRecord | StatLevelingRecordAdjective;

	export interface StatConfig {
		min: number;
		max: number;
		levelingCost?: (level: number) => number;
		leveling: LevelingDataAny;
	}

	export interface BodyStatConfig extends StatConfig {
		cmMin: number;
		cmMax: number;
	}

	export interface SkillStatConfig extends StatConfig {
		altName?: string;
	}

	export interface CoreStatConfig extends StatConfig {
	}

	interface StatConfigMap {
		[Stat.Core]: CoreStatConfig;
		[Stat.Skill]: SkillStatConfig;
		[Stat.Body]: BodyStatConfig;
	}

	interface StatConfigStrMap {
		'core': CoreStatConfig;
		'skill': SkillStatConfig;
		'body': BodyStatConfig;
	}

	namespace Whoring {
		type SexActStr = "hand" | "ass" | "bj" | "tits";
		type SexualBodyPart = BodyPart.Ass | BodyPart.Bust | BodyPart.Face | BodyPart.Lips | BodyPart.Penis;
		type BonusSource = Fashion.StyleId | SexualBodyPart;
		type BonusCategory = CoreStat.Perversion | CoreStat.Femininity | "beauty";

		interface Bonus {
			body?: Partial<Record<SexualBodyPart, number>>,
			style?: Partial<Record<Fashion.StyleId, number>>,
			stat: Partial<Record<BonusCategory, number>>
		}

		export interface WhoringLocationDesc {
			/** Shows in the UI as location. */
			desc: string;
			/** Sex act weighted table. */
			wants: Partial<Record<SexActStr, number>>;
			/** Minimum payout rank. */
			minPay: number;
			/** Maximum payout rank. */
			maxPay: number;
			/** Bonus payout, weighted table */
			bonus: Bonus;
			names: string[];
			title: string | null;
			rares: string[];
			npcTag: string;
			marquee?: string;
		}
	}

	//#region Events

	type EventCheckFunc = (player: Entity.Player) => boolean;

	interface EventDesc {
		/** A unique ID for the event. */
		id: string;
		/** The passage you are traversing from */
		from: string;
		force?: boolean;
		/** Number of times the event can repeat, 0 means forever. */
		maxRepeat: number;
		/** Minimum day the event shows up on */
		minDay: number;
		/** Maximum day the event shows up on, 0 means forever */
		maxDay: number;
		/** Interval between repeats on this event. */
		cool: number;
		/** Time phases the event is valid for */
		phase: DayPhase[];
		/** Override passage that the player is routed to. */
		passage: string;
		/** Condition function that is called to check if the event fires. */
		check: EventCheckFunc;
	}
	//#endregion

	namespace Combat {
		interface Enemy {
			name: string;
			title: string;
			health: number;
			maxHealth: number;
			energy: number;
			attack: number;
			defense: number;
			maxStamina: number;
			stamina: number;
			speed: number;
			moves: string;
			gender: Gender;
			portraits?: string[];
			bust?: number;
			ass?: number;
		}

		type MoveUnlockTest = (player: App.Combat.Combatant) => boolean;

		interface Move {
			name: string;
			description: string;
			icon: string;
			stamina: number;
			combo: number;
			speed: number;
			damage: number;
			unlock: MoveUnlockTest;
			miss: string[][];
			hit: string[][];
		}

		interface Style {
			engine: App.Combat.Engines.EngineConstructor;
			moves: Record<string, Move>;
		}

		namespace EncounterLoots {
			interface Base {
				chance: number;
				min: number;
				max: number;
			}

			interface Coins extends Base {
				type: 'Coins';
			}

			interface Random extends Base {
				type: 'Random';
			}

			interface Item extends Base {
				type: Items.Category;
				tag: string;
			}

			type Any = Coins | Random | Item;
		}

		interface EncounterDesc {
			enemies: string[],
			fatal: boolean;
			winPassage: string;
			losePassage: string;
			intro?: string;
			lootMessage?: string;
			loot?: EncounterLoots.Any[];
			winHandler?: (player: Entity.Player, encounterData: EncounterDesc) => void;
			loseHandler?: (player: Entity.Player, encounterData: EncounterDesc) => void;
			fleeHandler?: (player: Entity.Player, encounterData: EncounterDesc) => void;
		}

		interface ClubEncounter {
			title: string;
			winsRequired: number;
			maxWins: number;
			encounter: string;
		}
	}

	interface ModDesc {
		level: number;
		encounter: string;
		name: string;
		symbol: string;
		color: string;
	}

	interface TarotCardDesc {
		name: string;
		css: string;
		chat: string;
		msg: string;
		effects: string[];
	}

	//#region Ratings
	type FullStatStr<Type extends Stat, Values extends string> = `${Type}/${Values}`;

	type FullBodyStatStr = FullStatStr<Stat.Body, BodyStat | DerivedBodyStat>;
	type FullCoreStatStr = FullStatStr<Stat.Core, CoreStat>;
	type FullSkillStatStr = FullStatStr<Stat.Skill, Skills.Any>;
	type FullStatStrAny = FullBodyStatStr | FullCoreStatStr | FullSkillStatStr;

	type RatingFinalValue = string | NonEmptyArray<string>;
	type RatingValue = RatingFinalValue | {[key: number]: RatingValue};
	type Rating = Record<number, RatingValue>;

	interface SimpleRated {
		leveling: Record<number, string>;
	}
	interface FullRated {
		index: (FullBodyStatStr|FullCoreStatStr)[];
		leveling: Rating;
	}

	interface BodyAdjectiveReference {
		rating: (FullBodyStatStr|FullCoreStatStr)[];
		index?: (FullBodyStatStr|FullCoreStatStr)[];
		applicableLevel?: {min: number, max: number}[];
	}

	interface BodyPartConfig {
		adjective: BodyAdjectiveReference;
		noun: RatingFinalValue | SimpleRated | FullRated;
	}
	//#endregion

	type NamingAspect = "adjective" | "noun";
	interface NamingConfig {
		bodyConfig: Record<BodyPart, BodyPartConfig>;
	}

	namespace Travel {
		type ConditionCheckResult = boolean | string;

		interface BasicConditionalDestination {
			/**
			 * The destination is completely hidden from the player if false
			 * @default true
			 */
			available?: (player: Entity.Player) => boolean;
		}
		interface ConditionalDestination extends BasicConditionalDestination {
			destination: DynamicText;
			text?: DynamicText;
			action?: (player: Entity.Player, passageName: string) => void;

			enabled?: (player: Entity.Player) => boolean;
		}

		interface LockedDestinationWithNote extends BasicConditionalDestination {
			text: DynamicText;
			note: DynamicText;
		}

		type Destination = string | ConditionalDestination | LockedDestinationWithNote;

		export type Destinations = NonEmptyArray<Destination>;
	}
}

/* eslint-enable @typescript-eslint/no-unused-vars */
