namespace App.Data {
	Object.append(jobs, {
		ISLA_WAITRESS: {
			id: "ISLA_WAITRESS",
			title: "Wench For Hire",
			giver: "IslaTavernKeeper",
			pay: 40,
			rating: 3,
			phases: [1],
			days: 1,
			cost: [
				{type: "time", value: 3},
				{type: Stat.Core, name: CoreStat.Energy, value: 2}
			],
			requirements: [{type: "style", value: 50, condition: "gte"}],
			start: "\
        NPC_NAME eyes you up and down, muttering to himself and then nods slowly in your direction.\n\n\
        s(The job's easy enough lass, so don't worry if you don't have much experience at wenching. So long as \
        you don't screw up too badly, you'll get paid), he says.\n\n\
        That's a relief. For once it seems you can get paid some coin without having to suck a dick… or five.\n\n\
        s(Just a word of warning…) he adds, s(The boys can get pretty 'hands on' if you know what I mean, \
        especially after they've had a few. It shouldn't go too far… not like last time anyway.)\n\n\
        Aaaaaand… there it is.\
        ",
			intro: "\
        NPC_NAME says, s(I'm always looking for help on the floor. If you can sling beers and don't mind a \
        hand or two on your arse, we're hiring.)\
        ",
			scenes: [
				{
					id: "SCENE01",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Domestic.Serving, difficulty: 30, value: 100}],
					post: [
						{type: "npcStat", name: NpcStat.Mood, value: 5, factor: "A"},
						{type: "money", value: 20, factor: "A"}
					],
					text: [
						"You start your shift. The Tavern is mostly quiet this time of day and the customers are \
                relatively orderly. ",
						{
							A: 33,
							text: "However, despite this, you find it @@difficult@@ to manage all of your orders and \
                    keep them happy."
						},
						{
							A: 66,
							text: "You start off slowly and make a few miss-steps, but eventually @@you get the hang of it@@ \
                    and fall into a natural rhythm of slinging ale and grub."
						},
						{
							A: 500,
							text: "All in all, it's @@not very difficult@@ to keep everyone happy - plied with drinks and food."
						},
						"Eventually the rush ends and you get a few moments to catch your breath and prepare for the \
                next bustle of customers.\n"
					]
				},
				{
					id: "SCENE02",
					checks: [{tag: "B", type: Stat.Skill, name: Skills.Domestic.Serving, difficulty: 30, value: 100}],
					post: [
						{type: "npcStat", name: NpcStat.Mood, value: 5, factor: "B"},
						{type: "money", value: 20, factor: "B"}
					],
					text: [
						"The sky outside starts to darken and before long new customers start to arrive and you steel yourself \
                for the dinner rush. ",
						{
							B: 33,
							text: "However, despite this, you find it @@difficult@@ to manage all of your orders and \
                    keep them happy."
						},
						{
							B: 66,
							text: "You start off slowly and make a few miss-steps, but eventually @@you get the hang of it@@ \
                    and fall into a natural rhythm of slinging ale and grub."
						},
						{
							B: 500,
							text: "All in all, it's @@not very difficult@@ to keep everyone happy - plied with drinks and food."
						}
					]
				},
				{
					id: "SCENE02a",
					triggers: [{type: Stat.Body, name: BodyPart.Ass, value: 40, condition: "gte"}],
					checks: [{tag: "B1", type: Stat.Skill, name: Skills.Charisma.Seduction, difficulty: 100, value: 50}],
					post: [{type: "money", value: 50, factor: "B1"}],
					text: [
						"Your pASS arse attracts a lot of attention from the customers. Several times during your \
                shift you find yourself being groped or otherwise fondled by smelly labourers or fishermen.\
                ",
						{
							B1: 33,
							text: "You do your best to ignore the constant molestation and smile through it, \
                    but @@your tips suffer for it@@."
						},
						{
							B1: 66,
							text: "You do your best at flirting and smiling while being molested and @@mostly it \
                    seems to work@@, getting you some extra coin in tips."
						},
						{
							B1: 500,
							text: "You use the opportunity to flirt and grin at the obviously sex starved customers, \
                    who are more than happy to @@tip you generously@@ for a feel of your pASS cheeks."
						}
					],
				},
				{
					id: "SCENE03",
					checks: [{tag: "C", type: Stat.Skill, name: Skills.Domestic.Serving, difficulty: 100, value: 100}],
					post: [
						{type: "npcStat", name: NpcStat.Mood, value: 5, factor: "C"},
						{type: "money", value: 20, factor: "C"}
					],
					text: [
						"Eventually the dinner rush comes to an end, leaving behind only the customers who are interested in \
            some late night drinking and entertainment. You take a short break and then with renewed determination \
            head back into the growing crowd. \
            ",
						{
							C: 33,
							text: "However, despite this, you find it @@difficult@@ to manage all of your orders and keep \
                    them happy."
						},
						{
							C: 66,
							text: "You start off slowly and make a few miss-steps, but eventually \
                    @@you get the hang of it@@ and fall into a natural rhythm of slinging ale and grub."
						},
						{
							C: 500,
							text: "All in all, it's @@not very difficult@@ to keep everyone happy - plied with \
                    drinks and food."
						}
					],
				},
				{
					id: "SCENE03a",
					triggers: [{type: Stat.Skill, name: Skills.Charisma.Dancing, value: 30, condition: "gte"}],
					checks: [{tag: "C1", type: Stat.Skill, name: Skills.Charisma.Dancing, difficulty: 60, value: 60}],
					post: [{type: "money", value: 60, factor: "C1"}],
					text: [
						"During your shift one of the locals begins to entertain the crowd by playing on his fiddle. \
                The tune is quite catchy and you find yourself tapping your feet in time with the music. \
                Some of the patrons catch on and with a little encouragement start to dance with you. Soon \
                they are encouraging you as you twirl and twist away on the floor and the incessant clapping \
                starts to get to your head. Caught up in the moment you quickly climb on top of the bar and \
                start dancing for the crowd.\
                ",
						{
							C1: 33,
							text: "Your dancing is @@competent, if not inspired@@ and while the crowd seems to enjoy \
                    it, they don't really loosen their purse strings for you either."
						},
						{
							C1: 66,
							text: "You mix in a few sexy moves with your dancing, exposing your pBUST tits in brief \
                    flashes and winking saucily at the crowd and you are @@rewarded with a good amount of tips@@ \
                    for your effort."
						},
						{
							C1: 500,
							text: "Using everything you know, you prance and strut your stuff across the bar. You \
                    tease and flirt with the crowd, exposing brief glimpses of your pBUST tits and nASS to \
                    them and riling them up into a sexual frenzy. By the end of your routine they are \
                    @@practically showering you with coins@@."
						}
					],
				}
			],
			end: "\
        Finally, your shift in the tavern is over. NPC_NAME comes up to you and hands you your pay for the night.\
        ",
		}
	});
}
