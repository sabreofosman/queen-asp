namespace App.Data {
	Object.append(jobs, {
		YVONNE_TRAINING: {
			id: "YVONNE_TRAINING", title: "Hair and Makeup Training", giver: "Yvonne", pay: 0,
			rating: 3, // of 5
			phases: [0, 1],
			days: 1,
			cost: [
				{type: "time", value: 2},
				{type: Stat.Core, name: CoreStat.Energy, value: 2},
				{type: "money", value: 250}
			],
			intro:
				"NPC_NAME says s(Looking for some style tutoring? Sure, I can help you, for a price…)",
			start:
				"NPC_NAME ushers you into her inner working area and sits you in front of a large vanity situated in front of a wide \
        picture window. She explains that the natural light is good for exposing flaws that need to be addressed. With a bit \
        of flourish, she sits you in front of a pristine glass mirror and begins to explain to you many concepts and techniques \
        related to applying pigments and powder to 'accentuate your natural beauty'.",
			scenes: [
				{
					// No requirements.
					id: "MAKEUP",
					checks: [{tag: "A", type: Stat.Skill, name: Skills.Charisma.Styling, difficulty: 70, value: 100}],
					post: [{type: "skillXp", name: Skills.Charisma.Styling, value: 50}],
					// Just print widget
					text: [
						"You spend some time, deeply enraptured by her lecture and then attempt to apply the principles that she taught you to your own face.",
						{A: 85, text: "It's @@more difficult@@ than you thought and NPC_NAME has to constantly correct your technique or choices."},
						{A: 120, text: "With only minor commentary from NPC_NAME, you manage to do @@a decent job@@. "},
						{A: 500, text: "You take your time and manage to @@skillfully recreate the techniques@@ that you were shown. NPC_NAME looks at you and then nods in approval."}
					],
				},
				{
					// No requirements.
					id: "HAIR",
					checks: [{tag: "B", type: Stat.Skill, name: Skills.Charisma.Styling, difficulty: 70, value: 100}],
					post: [{type: "skillXp", name: Skills.Charisma.Styling, value: 50}],
					// Just print widget
					text: [
						"Next, comes a lecture and practice session related to hair care and styling. NPC_NAME spends quite a long time \
                going into detail about how you should care for and maintain your hair, even recommending several products that \
                (of course) she happens to sell. On the back of that she segues into some practical examples of styling - taking \
                your hair and demonstrating how to craft various popular styles. Finally, it becomes your turn to try and attempt \
                it, and you bite your lip with concentration as you get to work.",
						{B: 85, text: "It's @@more difficult@@ than you thought and NPC_NAME has to constantly correct your technique or choices."},
						{B: 120, text: "With only minor commentary from NPC_NAME, you manage to do @@a decent job@@. "},
						{B: 500, text: "You take your time and manage to @@skillfully recreate the techniques@@ that you were shown. NPC_NAME looks at you and then nods in approval."}
					],
				}

			],
			end:
				"Finally, finished for the day, NPC_NAME happily takes your payment and ushers you to the door.\n\n\
        s(You're welcome to come back any time you want, PLAYER_NAME,) she says, s(Just come early enough in the day and we'll \
        work something out.)\n\n\
        Despite your misgivings about makeup in general, the experience was good enough that you might just take her up on that offer.",
		}
	});
}
