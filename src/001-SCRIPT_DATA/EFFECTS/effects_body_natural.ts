// This file lists effects that happens with body on their own with time
namespace App.Data {
	Object.append(effectLib, {
		/** HEALTH */
		/** ENERGY */
		// Overnight healing.
		NATURAL_HEALING: {
			fun: (p) => {
				const health = 5 + Math.ceil(p.getStat(Stat.Core, CoreStat.Energy) * 2) +
					Math.ceil(p.getStat(Stat.Core, CoreStat.Fitness) / 10); // 5 + 0-20 + 0-10
				const energy = Math.floor((p.getStat(Stat.Core, CoreStat.Nutrition) / 25) + (p.getStat(Stat.Core, CoreStat.Fitness) / 25)); // 1 - 8
				const mod = 1 - Math.clamp(p.getStat(Stat.Core, CoreStat.Toxicity) / 150, 0, 1); // 0 - 1

				p.adjustStat(CoreStat.Health, Math.clamp(Math.ceil(health * mod), 5, health)); // Always give 5 health.
				p.adjustStat(CoreStat.Energy, Math.clamp(Math.ceil(energy * mod), 1, energy)); // Always give 1 energy.
			},
			value: 0, knowledge: ["Healing++"]
		},
		// Resting healing.
		NATURAL_RESTING: {
			fun: (p) => {
				const heal = Math.clamp(Math.ceil((p.getStat(Stat.Core, CoreStat.Nutrition) / 20) +
					(p.getStat(Stat.Core, CoreStat.Fitness) / 20)), 1, 10); // 1 - 10
				const mod = 1 - Math.clamp(p.getStat(Stat.Core, CoreStat.Toxicity) / 150, 0, 1); // 0 - 1
				p.adjustStat(CoreStat.Health, Math.ceil(heal * mod));
				p.adjustStat(CoreStat.Energy, 1);
				p.adjustStat(CoreStat.Toxicity, -5);
			},
			value: 0,
			knowledge: ["Healing+"]
		},
		NATURAL_QUEST_HOOKS: {
			fun: (p) => {
				if (Quest.isCompleted(p, "BETTER_BED")) p.adjustStat(CoreStat.Energy, 1);
				if (Quest.isCompleted(p, "CABIN_DECORATION")) p.adjustStatXP(CoreStat.Willpower, 5);
				if (Quest.isCompleted(p, "CABIN_RUG")) p.adjustStatXP(CoreStat.Willpower, 5);
			},
			value: 0, knowledge: ["Cabin Accessories++"]
		},
		/** NUTRITION */
		NATURAL_NUTRITION: {
			fun: (p) => {
				p.adjustStat(CoreStat.Nutrition, -5);
				let nutrition = p.getStat(Stat.Core, CoreStat.Nutrition);
				const nutritionXP = p.getStatXP(Stat.Core, CoreStat.Nutrition);
				if (nutrition >= 90 && nutritionXP > 150) {
					p.adjustBodyXP(BodyPart.Waist, nutritionXP - 150); // Get Fatter!?
					setup.notifications.addMessage(Notifications.MessageCategory.StatusChange, setup.world.day + 1, "@@.state-neutral;You feel as if you ate too much yesterday.@@");
					nutrition = p.getStat(Stat.Core, CoreStat.Nutrition);
				}
				// Going hungry, lose some belly fat.
				if (nutrition <= 40) {
					p.adjustBodyXP(BodyPart.Waist, -25);
				}
				// Starving. Yikes.
				if (nutrition <= 20) {
					setup.notifications.addMessage(Notifications.MessageCategory.StatusChange, setup.world.day + 1, "@@.state-negative;You are starving!@@");
					p.adjustStat(CoreStat.Energy, -1); // Reduce Energy.
					p.adjustStat(CoreStat.Health, -15);
					p.adjustBodyXP(BodyPart.Waist, -50);
				}
			},
			value: 0, knowledge: ["Hunger+"]
		},
		/** TOXICITY */
		NATURAL_DETOXIFICATION: {
			fun: (p) => {p.adjustStat(CoreStat.Toxicity, -((5 + (p.getStat(Stat.Core, CoreStat.Fitness) / 10))) * 2);},
			value: 0, knowledge: ["Detoxification+"]
		},
		NATURAL_TOXIC_DAMAGE: {
			fun: (p) => {
				const tox = p.getStat(Stat.Core, CoreStat.Toxicity);
				const dmg = tox <= 100 ? 0 : Math.ceil(10 * (tox / 300));

				if (dmg > 0) {
					p.adjustStat(CoreStat.Health, (dmg * -1.0));
					setup.notifications.addMessage(Notifications.MessageCategory.StatusChange, setup.world.day + 1, "@@.state-negative;&dArr;You feel slightly sick@@… your current " +
						PR.colorizeString(p.getStatPercent(Stat.Core, CoreStat.Toxicity), CoreStat.Toxicity) +
						" is probably to blame.");
				}
			},
			value: 0,
			knowledge: ["Poisoned+"]
		},
		/** HORMONES */
		NATURAL_HORMONE_SHIFT: {
			value: 0, knowledge: ["Hormonal processes+"],
			fun: (p) => {
				const hormones = p.getStat(Stat.Core, CoreStat.Hormones);
				const hormonesXp = p.getStatXP(Stat.Core, CoreStat.Hormones);
				// Adjust physical characteristics based on hormone balance. Only shift body if there is XP
				// related to the hormone shift stored in the player object.
				let hormoneShift = 0;
				// The "Futa" stat blocks penis, bust, and balls shrinking if their stats are lower or equal to the futa stat.
				// Also, if the balls stat percent is lower than the futa one, balls do not produce hormonal shift.
				// If, however, the futa stat is higher than the penis or bust stat, it results in willpower drain down to 15
				const futaPercent = p.getStatPercent(Stat.Core, CoreStat.Futa);

				if (hormones > 100 && hormonesXp > 0) {
					hormoneShift = hormones - 100;
					p.adjustBodyXP(BodyPart.Face, hormoneShift, 40);
					p.adjustBodyXP(BodyPart.Bust, hormoneShift, 5);
					p.adjustBodyXP(BodyPart.Lips, hormoneShift, 40);
					p.adjustBodyXP(BodyPart.Ass, hormoneShift, 10);
					p.adjustBodyXP(BodyPart.Hips, hormoneShift, 10);
					const penisPercent = p.getStatPercent(Stat.Body, BodyPart.Penis);
					if (penisPercent > futaPercent) {
						p.adjustBodyXP(BodyPart.Penis, (hormoneShift * -1.0), 1);
					} else if (futaPercent > penisPercent) {
						p.adjustStatXP(CoreStat.Willpower, futaPercent - penisPercent, 15);
					}
					if (p.getStatPercent(Stat.Body, BodyPart.Balls) > futaPercent) {
						p.adjustBodyXP(BodyPart.Balls, (hormoneShift * -1.0), 0);
					}
				} else if (hormones < 100 && hormonesXp < 0) {
					hormoneShift = (100 - hormones);
					const getTargetValue = (min: number, max: number): number => {
						return max - (hormoneShift / 100) ** 2 * (max - min);
					}

					p.adjustBodyXP(BodyPart.Face, (hormoneShift * -1.0), getTargetValue(15, 50));
					p.adjustBodyXP(BodyPart.Bust, (hormoneShift * -1.0), getTargetValue(0, 15));
					p.adjustBodyXP(BodyPart.Lips, (hormoneShift * -1.0), getTargetValue(15, 50));
					p.adjustBodyXP(BodyPart.Ass, (hormoneShift * -1.0), getTargetValue(10, 30));
					p.adjustBodyXP(BodyPart.Hips, (hormoneShift * -1.0), getTargetValue(7, 9));
					p.adjustBodyXP(BodyPart.Penis, hormoneShift, getTargetValue(44, 34));
					p.adjustBodyXP(BodyPart.Balls, hormoneShift, getTargetValue(44, 34));
				}
				// Decrease the players hormone XP relative to the size of their balls.
				if (p.getStatPercent(Stat.Body, BodyPart.Balls) > futaPercent) {
					p.adjustStatXP(CoreStat.Hormones, ((p.getStat(Stat.Body, BodyPart.Balls) / 5) * -1.0)); // Bigger balls add more male hormones.
				}
			}
		},
		/** FITNESS */
		NATURAL_FITNESS_DECREASE: {
			fun: (p) => {p.adjustStatXP(CoreStat.Fitness, -5);},
			value: 0, knowledge: ["Flabbiness+"]
		},
		/** HEIGHT */
		/** HAIR */
		NATURAL_HAIR_GROW: {
			fun: (p) => {p.adjustBodyXP(BodyPart.Hair, 4, 0); /* Hair grows about 0.05 cm per day. */},
			value: 0, knowledge: ["Hair grow+"]
		},
		/** FACE */
		/** BUST */
		/** BUST FIRMNESS */
		NATURAL_BUST_SAGGINESS: {
			fun: (p) => {p.adjustBodyXP(BodyProperty.BustFirmness, -1 * Math.pow(p.getStat(Stat.Body, BodyPart.Bust), 2) / 200);},
			value: 0, knowledge: ["Bust Sag+"]
		},
		/** LACTATION */
		NATURAL_LACTATION_DECREASE: {
			fun: (p) => {p.adjustBodyXP(BodyProperty.Lactation, -100, 0);},
			value: 0,
			knowledge: ["Milk production-"]
		}
	});
	/** ASS */
	/** HIPS */
	/** LIPS */
	/** PENIS */
	/** BALLS */
	/** WAIST */
	export const naturalBodyEffects = Object.keys(effectLib).filter(s => s.startsWith("NATURAL_"));
}
