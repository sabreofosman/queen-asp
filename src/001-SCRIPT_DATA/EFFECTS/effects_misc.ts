namespace App.Data {
	Object.append(effectLib, {
		BLACK_HAIR_DYE: {
			fun: (p) => {p.hairColor = 'black'; setup.avatar.drawPortrait();},
			value: 100,
			knowledge: ["Dye Hair Black"]
		},
		BROWN_HAIR_DYE: {
			fun: (p) => {p.hairColor = 'brown'; setup.avatar.drawPortrait();},
			value: 100,
			knowledge: ["Dye Hair Brown"]
		},
		RED_HAIR_DYE: {
			fun: (p) => {p.hairColor = 'red'; setup.avatar.drawPortrait();},
			value: 100,
			knowledge: ["Dye Hair Red"]
		},
		BLOND_HAIR_DYE: {
			fun: (p) => {p.hairColor = 'blond'; setup.avatar.drawPortrait();},
			value: 100,
			knowledge: ["Dye Hair Blond"]
		},
		/** THE LOVERS - TAROT CARD */
		THE_LOVERS: {
			fun: (p) => {p.addItem(Items.Category.Drugs, "siren elixir", 0);},
			value: 500, knowledge: ["Add Item++++"]
		},
		/** THE EMPRESS - TAROT CARD */
		THE_EMPRESS: {
			fun: (p) => {p.addItem(Items.Category.LootBox, "common food loot box", 0);},
			value: 500, knowledge: ["Add Item++++"]
		},

		VOODOO_LACTATION: {
			fun: (p) => {p.adjustBodyXP(BodyProperty.Lactation, 300, 75);},
			value: 0,
			knowledge: ["Voodoo induced lactation+"]
		},

		VOODOO_BUST_FIRMNESS_NORMAL: {
			fun: (p) => {
				const curXp = p.getStatXP(Stat.Body, BodyProperty.BustFirmness);
				if (curXp < 0) {
					p.adjustBodyXP(BodyProperty.BustFirmness, 1 - curXp, 70);
				} else {
					p.adjustBodyXP(BodyProperty.BustFirmness, curXp, 70);
				}
			},
			value: 0,
			knowledge: ["Voodoo induced breast firmness+"]
		},

		VOODOO_BUST_FIRMNESS_FULL: {
			fun: (p) => {
				const curXp = p.getStatXP(Stat.Body, BodyProperty.BustFirmness);
				if (curXp < 0) {
					p.adjustBodyXP(BodyProperty.BustFirmness, 10 - curXp);
				} else {
					p.adjustBodyXP(BodyProperty.BustFirmness, curXp);
				}
			},
			value: 0,
			knowledge: ["Voodoo induced breast firmness++"]
		},
	});
}
