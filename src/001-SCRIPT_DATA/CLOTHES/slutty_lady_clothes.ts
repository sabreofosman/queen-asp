// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT

		// NECK SLOT

		// NIPPLES SLOT

		// BRA SLOT
		"luxurious black bra": { // +20
			name: "luxurious black bra",
			shortDesc: "{COLOR} strapless lace bra",
			longDesc: "Besides having expertly crafted sexy lace, this bra can support quite a generous bust without straps and without covering cleavage area. It is ideal for wearing with dresses with uncovered shoulders and deep cleavage.",
			slot: ClothingSlot.Bra,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "SEXY_CLOTHING"],
			style: [Fashion.Style.SluttyLady]
		},

		// CORSET SLOT

		// PANTY SLOT
		"luxurious black panties": { // +20
			name: "luxurious black panties",
			shortDesc: "{COLOR} satin lace panties",
			longDesc: "These panties are a perfect combination of conservatism and sexiness. They are not panties that are pushed aside because the man is too horny to bother to take them of. These panties are removed in a richly decorated room, illuminated by candles, while orchestra can be faintly heard from the ball room.",
			slot: ClothingSlot.Panty,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "SEXY_CLOTHING"],
			style: [Fashion.Style.SluttyLady]
		},

		// STOCKINGS SLOT
		"luxurious black stockings": { // +20
			name: "luxurious black stockings",
			shortDesc: "pair of {COLOR} stockings with an intricate pattern",
			longDesc: "These stockings end halfway between knee and crotch, leaving enough skin to expose under a miniskirt or through a dress cut. They seem to have combination of several patterns of different scale, first coarse, then finer, then even finer, you aren't even sure you can see the finest. This creates almost hypnotic effect, drawing eyes to the legs of the wearer.",
			slot: ClothingSlot.Stockings,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING", "SEXY_CLOTHING"],
			style: [Fashion.Style.SluttyLady]
		},

		// SHIRT SLOT

		// PANTS SLOT

		// DRESS SLOT
		"luxurious black evening gown": { // +40
			name: "luxurious black evening gown",
			shortDesc: "{COLOR} full-length silk evening gown",
			longDesc: "\
	This dress looks worthy of a queen. It's strapless with a large plunging neckline, requiring a very specific type of bra \
	to look its best. It's low hem almost touches the floor, but one deep slit in the side, almost to panties, ensures that \
	the wearer's legs are quite on display. A dozen small gemstones decorate the area around cleavage, completing feeling of luxury.\
	",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING", "SEXY_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.SluttyLady],
			inMarket: false
		},

		// COSTUME SLOT
		"sexy showgirl outfit": { // +40
			name: "sexy showgirl outfit", shortDesc: "sexy {COLOR} showgirl outfit",
			longDesc: "\
	A classic showgirl costume, albiet one designed to be taken off quickly and in pieces. It comes complete \
	with a pair of large fans for dancing with.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "red", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.SluttyLady, Fashion.Style.SexyDancer],
			inMarket: false
		},

		// SHOES SLOT
		"luxurious black high heels": { // +20
			name: "luxurious black high heels",
			shortDesc: "pair of {COLOR} designer heels",
			longDesc: "\
	The heels of these shoes seem to be of ideal height: catching eyes, but far from ridiculous heels of 'bimbo' shoes. \
	The craftsmanship is immaculate, and several small diamonds are an excellent finishing touch.",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FANCY_MOVES"],
			style: [Fashion.Style.SluttyLady, Fashion.Style.SexyDancer],
			inMarket: false
		},

		// BUTT SLOT
		"luxurious butt plug": { // +12
			name: "luxurious butt plug",
			shortDesc: "thick {COLOR} butt plug decorated with gems",
			longDesc: "\
	The perfect accessory for a kinky lady. Wear it to a ball, to a wedding, to a casual promenade. No-one will guess! \
	Well, depending on your reputation, they may guess, but they will never know for sure. And when the time comes, \
	your butt is ready for sex! This one is made of material unknown to you, but it feels unbelievably nice. \
	The base is encrusted with a large gemstone to add an extra eye-catching appeal.\
	",
			slot: ClothingSlot.Butt,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING", "GAPE_TRAINING"],
			style: [Fashion.Style.SluttyLady],
			inMarket: false
		},

		// PENIS SLOT
		"luxurious chastity cage": { // +12
			name: "luxurious chastity cage",
			shortDesc: "high-quality metal chastity cage",
			longDesc: "This chastity cage is made from the finest steel by the finest craftsman. Its constructions ensures not only that the wearer will not escape, but also that it will not damage him. The sheer value of this thing means that whoever locked the wearer did that consensually, and the cage itself is a loving gift.",
			slot: ClothingSlot.Penis,
			color: "silvery", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory, locked: false,
			wearEffect: ["PERVERTED_CLOTHING"],
			style: [Fashion.Style.SluttyLady, Fashion.Style.Bdsm],
			inMarket: false
		},

		// WEAPON SLOT (huh?)
	});
}
