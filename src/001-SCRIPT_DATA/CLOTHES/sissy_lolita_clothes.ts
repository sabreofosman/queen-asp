// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT
		"pink head bow": { // +6
			name: "pink head bow", shortDesc: "a cute {COLOR} bow",
			longDesc: "This pink bow is a cute accessory that makes you feel like a little girl.",
			slot: ClothingSlot.Hat, color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita, Fashion.Style.DaddyGirl]
		},

		// NECK SLOT
		"sissy collar": { // +9
			name: "sissy collar", shortDesc: "a {COLOR} fur-lined leather collar emblazoned with the word 'SISSY' in rhinestones.",
			longDesc: "This thick collar is made of sturdy leather and lined with soft fur, making it somewhat tolerable to wear… if it wasn't for the word 'SISSY' writ large in shiny faux gems across it.",
			slot: ClothingSlot.Neck, color: "pink", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita]
		},

		// NIPPLES SLOT

		// BRA SLOT
		"striped bra": { // +10
			name: "striped bra", shortDesc: "a {COLOR} and white striped bra",
			longDesc: "This cute little number is exceptionally girly, perfect for a little sissy baby.",
			slot: ClothingSlot.Bra, color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita]
		},

		// CORSET SLOT

		// PANTY SLOT
		"striped panties": { // +10
			name: "striped panties", shortDesc: "a pair of {COLOR} and white striped panties",
			longDesc: "This cute little number is exceptionally girly, perfect for a little sissy baby.",
			slot: ClothingSlot.Panty, color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita]
		},

		"frilly bloomers": { // +15
			name: "frilly bloomers", shortDesc: "a pair of frilly {COLOR} cotton bloomers",
			longDesc: "These bloomers are cut short and have a frilly bottom, perfect for a little sissy girl.",
			slot: ClothingSlot.Panty, color: "pink", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita]
		},

		// STOCKINGS SLOT
		"striped stockings": { // +10
			name: "striped stockings", shortDesc: "a pair of {COLOR} and white striped stockings",
			longDesc: "These cute striped stockings have adorable little pink bows at the top.",
			slot: ClothingSlot.Stockings, color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita, Fashion.Style.Bimbo]
		},

		"sweet cherry socks": { // +20
			name: "sweet cherry socks", shortDesc: "SweetCherry &trade, brand knee socks",
			longDesc: "\
    These knee socks are feel smooth to the touch, a testament to the high quality materials \
    that they are made from. They are light pink with white lacing around the top tied off with \
    delicate pink silk. Each sock is embroidered on the side with white stiching that reads \
    'I &#9825, Cum'\
    ",
			slot: ClothingSlot.Stockings,
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.SissyLolita],
			inMarket: false
		},

		// SHIRT SLOT

		// PANTS SLOT

		// DRESS SLOT
		"frilly dress": { // +20
			name: "frilly dress", shortDesc: "a cute frilly {COLOR} dress",
			longDesc: "This dress is all ruffles and lace. Perfect for attire for a dedicated sissy girl.",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita]
		},

		"sweet cherry dress": { // +40
			name: "sweet cherry dress", shortDesc: "SweetCherry &trade, brand lolita dress",
			longDesc: "\
    This extravagant dress is made from the finest cottons and silks. The brand is known for catering to \
    the sissy lolita fetish and the outfit shows this off fabulously in the selection of print - \
    an assortment of cute cakes, sweets and throbbing dicks.\
    ",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.SissyLolita],
			inMarket: false
		},

		// COSTUME SLOT
		"sissy maid outfit": { // +40
			name: "sissy maid outfit", shortDesc: "bright {COLOR} sissy maid outfit",
			longDesc: "\
    This frilly outfit looks like a sugary sweet cartoon version of a maid outfit. It's highwaisted skirt is packed with \
    crinole and is so sort it exposes your arse no matter what you do. The top is white with pink piping with puffy \
    sleeves and cute little pink bows. The entire outfit is cinched with an almost useless 'maid apron' around your \
    middle.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.SissyLolita, Fashion.Style.SexyDancer],
			inMarket: false
		},
		// SHOES SLOT
		"pink mary janes": { // +10
			name: "pink mary janes", shortDesc: "a pair of low heeled {COLOR} mary janes",
			longDesc: "These girly shoes have bows on the ankles and a short heel making them easy to walk in.",
			slot: ClothingSlot.Shoes, color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita, Fashion.Style.DaddyGirl]
		},

		"pink platform mary janes": { // +15
			name: "pink platform mary janes", shortDesc: "a pair of {COLOR} platform mary janes",
			longDesc: "These girly shoes have bows on the ankles and a chunky platform heel. Difficult to walk in but both sexy and girly.",
			slot: ClothingSlot.Shoes, color: "pink", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.SissyLolita, Fashion.Style.DaddyGirl]
		},

		"sweet cherry shoes": { // +20
			name: "sweet cherry shoes", shortDesc: "SweetCherry &trade, brand platform shoes",
			longDesc: "\
    These shoes are made in the popular 'maryjane' style, commplete with toestrap, buckle and a chunk heel. The \
    platform is tall and the shoe is heavy causing you to walk in an exagerated prancing gait. They are extremely \
    well made and durable and despite everything incredibly comfortable to walk in.\
    ",
			slot: ClothingSlot.Shoes,
			color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: [],
			style: [Fashion.Style.SissyLolita, Fashion.Style.SexyDancer],
			inMarket: false
		},
		// BUTT SLOT

		// PENIS SLOT

		// WEAPON SLOT (huh?)
	});
}
