// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT
		"cheap wig": {
			name: "cheap wig", shortDesc: "a short {COLOR} wig",
			longDesc: "This cheap wig is made out of badly dyed horse hair.",
			slot: ClothingSlot.Wig, color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Accessory,
			hairLength: 30, hairStyle: "a short bob", hairBonus: 30,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		wig: {
			name: "wig", shortDesc: "a long {COLOR} wig",
			longDesc: "This wig is made from human hair and of quite a fine quality.",
			slot: ClothingSlot.Wig, color: "blond", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Accessory,
			hairLength: 60, hairStyle: "loose curls", hairBonus: 60,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		// HAT SLOT
		bonnet: {
			name: "bonnet", shortDesc: "a frilly {COLOR} bonnet",
			longDesc: "This frilly little bonnet is a dainty accessory sure to give off a girlish demeanor.",
			slot: ClothingSlot.Hat, color: "pink", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		// NECK SLOT
		choker: {
			name: "choker", shortDesc: "a {COLOR} silk choker",
			longDesc: "This thin choker is made of silk and designed to accentuate a delicate neck.",
			slot: ClothingSlot.Neck, color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		"ancient metal collar": {
			name: "ancient metal collar",
			shortDesc: "an ancient metal collar",
			longDesc: "An ancient collar made of mat metal inlaid with a few big gems of various colours.",
			slot: ClothingSlot.Neck, color: "darkgrey",
			rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["FUTA_COLLAR"],
			inMarket: false
		},

		// NIPPLES SLOT

		// BRA SLOT
		chemise: {
			name: "chemise", shortDesc: "a plain {COLOR} cotton chemise",
			longDesc: "A brief slip designed to cover the breasts under clothes. Not very supportive at all.",
			slot: ClothingSlot.Bra, color: "white", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		"cotton bra": {
			name: "cotton bra", shortDesc: "a {COLOR} cotton bra trimmed with lace",
			longDesc: "A regular sized bra designed to contain and support female breasts. It offers modest support and a small amount of sexual appeal.",
			slot: ClothingSlot.Bra, color: "white", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		"silk bra": {
			name: "silk bra", shortDesc: "a sexy {COLOR} silk bra with lace trim",
			longDesc: "This expensive undergarment is designed to accentuate and display a woman's chest with lustful appeal.",
			slot: ClothingSlot.Bra, color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		// CORSET SLOT
		"fashion corset": {
			name: "fashion corset", shortDesc: "a tightly bound {COLOR} silk and lace corset",
			longDesc: "This corset is made from silk and lace, more fashionable than functional, but it still tightly binds your waist and lifts your breasts.",
			slot: ClothingSlot.Corset, color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING", "WAIST_CINCHING"]
		},

		// PANTY SLOT
		"cotton bloomers": {
			name: "cotton bloomers", shortDesc: "a pair of {COLOR} cotton bloomers",
			longDesc: "The most basic of undergarments. No frills and almost zero sex appeal.",
			slot: ClothingSlot.Panty, color: "white", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		"sheer bloomers": {
			name: "sheer bloomers", shortDesc: "a pair of sheer {COLOR} silk and lace bloomers",
			longDesc: "Made from sheer silk, they're smooth against your skin and decidedly sexy.",
			slot: ClothingSlot.Panty, color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		// STOCKINGS SLOT
		"cotton stockings": {
			name: "cotton stockings", shortDesc: "a pair of {COLOR} cotton stockings",
			longDesc: "Basic stockings, held up with string ties.",
			slot: ClothingSlot.Stockings, color: "white", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		// SHIRT SLOT
		"cotton blouse": {
			name: "cotton blouse", shortDesc: "a {COLOR} cotton blouse",
			longDesc: "This simple cotton blouse is made from rough cotton.",
			slot: ClothingSlot.Shirt, restrict: [ClothingSlot.Shirt, ClothingSlot.Dress, ClothingSlot.Costume], color: "white", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		"silk blouse": {
			name: "silk blouse", shortDesc: "a low cut {COLOR} silk blouse",
			longDesc: "This elegant silk blouse is cut daringly low.",
			slot: ClothingSlot.Shirt, restrict: [ClothingSlot.Shirt, ClothingSlot.Dress, ClothingSlot.Costume], color: "white", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		"grey tshirt": {
			name: "grey tshirt",
			shortDesc: "a {COLOR} t-shirt",
			longDesc: "This simple t-shirt is made from rough cotton.",
			slot: ClothingSlot.Shirt,
			restrict: [ClothingSlot.Shirt, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "grey",
			rarity: Items.Rarity.Common,
			type: Items.ClothingType.Clothing,
			wearEffect: []
		},

		// PANTS SLOT
		"leather pants": {
			name: "leather pants", shortDesc: "a pair of {COLOR} leather pants",
			longDesc: "These leather pants are incredibly tight and designed to show off your ass.",
			slot: ClothingSlot.Pants, restrict: [ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing
		},

		"leather shorts": {
			name: "leather shorts", shortDesc: "a pair of low rise {COLOR} leather shorts",
			longDesc: "These tight leather shorts ride up in all the right places.",
			slot: ClothingSlot.Pants, restrict: [ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing
		},

		// DRESS SLOT
		"cotton dress": {
			name: "cotton dress", shortDesc: "a {COLOR} cotton dress",
			longDesc: "This simple cotton dress is made from rough cotton.",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "blue", rarity: Items.Rarity.Common, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"]
		},
		sundress: {
			name: "sundress", shortDesc: "a cute {COLOR} floral sundress",
			longDesc: "This soft sundress has a cute floral pattern that is exceptionally girly.",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "pink", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"]
		},
		"sexy dress": {
			name: "sexy dress", shortDesc: "a sexy {COLOR} mini dress",
			longDesc: "This daringly short dress hugs your body and lets everyone know it's for sale.",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING"], activeEffect: ["FLIRTY"]
		},

		// COSTUME SLOT

		"landlubber costume": {
			name: "landlubber costume", shortDesc: "a perverted {COLOR} school girl outfit",
			longDesc: "This costume consists of a two-piece outfit with accessories that is designed to mimic a perverts dream of a school uniform. It has a skirt that has holes where the anus and penis are " +
				"as well as on the shirt where the cutouts are over the nipples. And the material of it goes invisible when it becomes wet, which is a favourite of the crew.",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "white", rarity: Items.Rarity.Common, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "PERVERTED_CLOTHING"]
		},

		"pathetic loser costume": {
			name: "pathetic loser costume", shortDesc: "a perverted {COLOR} clown outfit",
			longDesc: "This costume consists of a full-blown clown outfit including red glue on nose. It makes one look ridiculous but also sexy since it is tight at the right places so that it shows of " +
				"one's assets. Also, there are undoubtedly some people that have always dreamed of fucking a clown, those sick bastards.",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "red", rarity: Items.Rarity.Common, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "PERVERTED_CLOTHING"]
		},

		"maid costume": {
			name: "maid costume", shortDesc: "a frilly {COLOR} maid uniform",
			longDesc: "This is a typical maid costume that has been altered to showcase your ass and chest.",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.OnePiece,
			wearEffect: ["FEMININE_CLOTHING", "KINKY_CLOTHING"], activeEffect: ["MAIDS_PROWESS"]
		},

		// SHOES SLOT
		"worn boots": {
			name: "worn boots", shortDesc: "a worn pair of {COLOR} boots",
			longDesc: "These boots are decidedly ugly. More functional than fashionable.",
			slot: ClothingSlot.Shoes, color: "brown", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing
		},

		"short heels": {
			name: "short heels", shortDesc: "a pair of short {COLOR} heels",
			longDesc: "These dainty shoes have a small kitten heel. Not too hard to walk in.",
			slot: ClothingSlot.Shoes, color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		"medium heels": {
			name: "medium heels", shortDesc: "a pair of medium {COLOR} heels",
			longDesc: "These shoes have a medium height heel that causes you to roll your ass as you walk.",
			slot: ClothingSlot.Shoes, color: "black", rarity: Items.Rarity.Common, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"]
		},

		// BUTT SLOT

		// PENIS SLOT
		"cock ribbon": {
			name: "cock ribbon", shortDesc: "a cute little {COLOR} ribbon",
			longDesc: "This dainty little ribbon is meant to be tied on a sissy clit for decoration.",
			slot: ClothingSlot.Penis, color: "pink", rarity: Items.Rarity.Common, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING"]
		},

		// WEAPON SLOT (huh?)
		"worn machete": {
			name: "worn machete", shortDesc: "<span style='color:purple'>a well worn machete</span>",
			longDesc: "This large blade is 32cm long and has a slightly curved angle to it. It's not really a knife, but more like an axe. In a fight you could use it similar to a short sword.",
			slot: ClothingSlot.Weapon, color: "steel", rarity: Items.Rarity.Common, type: Items.ClothingType.Weapon,
			wearEffect: [], inMarket: false
		}
	});
}
