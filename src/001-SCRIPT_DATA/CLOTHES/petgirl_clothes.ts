// Cow girl clothing descriptions provided by sirwolffe @ the tfgamessite.com forums.

// STYLE TABLE
// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
// ACCESSORY    3       6           9       12
// CLOTHING     5       10          15      20
// ONE PIECE    10      20          30      40
namespace App.Data {
	Object.append(App.Data.clothes, {
		// WIG SLOT

		// HAT SLOT
		"cow headband": { // +9
			name: "cow headband", shortDesc: "a well crafted cowgirl headband",
			longDesc: "A sturdy black hair band with cute little scrimshawed cow horns and fuzzy ears.",
			slot: ClothingSlot.Hat, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.PetGirl], meta: ['cow outfit', 'job reward']
		},

		"cat ears": {
			name: "cat ears", shortDesc: "a pair of fluffy {COLOR} cat ears",
			longDesc: "These large furry cat ears are attached by a headband to be worn.",
			slot: ClothingSlot.Hat, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			style: [Fashion.Style.PetGirl]
		},

		// NECK SLOT
		"cow collar": { // +9
			name: "cow collar", shortDesc: " a cute gold cowbell and collar",
			longDesc: "A shiny cowbell gently clangs from from a soft leather collar decorated with a black lace ribbon.",
			slot: ClothingSlot.Neck, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.PetGirl], meta: ['cow outfit', 'job reward']
		},

		// NIPPLES SLOT
		"nipple bells": { // +9
			name: "nipple bells", shortDesc: "a pair of belled gold nipple rings",
			longDesc: "These large diameter and heavy gage rings have little bells hanging from them. Their weight ceaselessly stimulates your nipples to \"full mast\".",
			slot: ClothingSlot.Nipples, color: "gold", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING"], style: [Fashion.Style.PetGirl, Fashion.Style.Bdsm], meta: ['cow outfit', 'job reward']
		},

		// BRA SLOT
		"cow bra": { // +10
			name: "cow bra", shortDesc: "a reinforced {COLOR} comfy maternity bra",
			longDesc: "In white on the right cup it says, \"got milk under spot X\". In white on the left cup is a horned bull skull over a pair of crossed milk cans, making an X.",
			slot: ClothingSlot.Bra, color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.PetGirl], meta: ['cow outfit', 'job reward']
		},

		// CORSET SLOT
		"cow corset": { // +10
			name: "cow corset", shortDesc: "a holstein patterned cup-less corset in velvet",
			longDesc: "This ingeniously designed piece is built to be let out as the wearer become plumper, or safely carry a baby, while providing substantial back support.",
			slot: ClothingSlot.Corset, color: "white", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["FEMININE_CLOTHING", "WAIST_CINCHING"], style: [Fashion.Style.PetGirl], meta: ['cow outfit', 'job reward']
		},

		// PANTY SLOT
		"cow panties": { // +10
			name: "cow panties", shortDesc: "a high cut brief of {COLOR} silk",
			longDesc: "It is trimmed in elegant black lace. The back panel proudly proclaims,\"Moo Matey!\". The front has little white satin bows. Has a tail hole.",
			slot: ClothingSlot.Panty, color: "black", rarity: Items.Rarity.Uncommon, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.PetGirl], meta: ['cow outfit', 'job reward']
		},

		// STOCKINGS SLOT
		"cow stockings": { // +15
			name: "cow stockings", shortDesc: "a pair of cow print silk stockings",
			longDesc: "They reach to mid-thigh, are topped off by black lace and tied with a wide white ribbon in a bow.",
			slot: ClothingSlot.Stockings, color: "black", rarity: Items.Rarity.Rare, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"], style: [Fashion.Style.PetGirl], meta: ['cow outfit', 'job reward']
		},

		// SHIRT SLOT

		// PANTS SLOT
		"cow skirt": { // +20
			name: "cow skirt", shortDesc: "a fine wool holstein patterned full skirt",
			longDesc: "Hangs from the hips and just barely covers your ass when you bend forward. It doesn't if you hug your knees. Sways enticingly and flares out on spins.",
			slot: ClothingSlot.Pants, restrict: [ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume], color: "pink", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.PetGirl, Fashion.Style.Bimbo], meta: ['cow outfit', 'job reward']
		},

		// DRESS SLOT
		"fuzzy black bikini": { // +40
			name: "fuzzy black bikini", shortDesc: "fuzzy {COLOR} bikini",
			longDesc: "\
    This two piece ensemble consists of a strapless bikini top and high cut briefs that are covered with a soft \
    fur like material that is soft to the touch and has a brilliant sheen. \
    ",
			slot: ClothingSlot.Dress, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FLIRTY"],
			style: [Fashion.Style.PetGirl],
			inMarket: false
		},

		// COSTUME SLOT
		"sexy kitten costume": { // +40
			name: "sexy kitten costume", shortDesc: "{COLOR} sexy kitten costume",
			longDesc: "\
    A furry one piece costume that comes with some accessories such as a pair of kitten ears and a tail. It's \
    obviously designed for either a costume ball or private 'play'.\
    ",
			slot: ClothingSlot.Costume, restrict: [ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume],
			color: "white", rarity: Items.Rarity.Legendary, type: Items.ClothingType.OnePiece,
			wearEffect: ["KINKY_CLOTHING"],
			activeEffect: ["MAJOR_STRIPPERS_ALLURE"],
			style: [Fashion.Style.PetGirl, Fashion.Style.SexyDancer],
			inMarket: false
		},

		// SHOES SLOT
		"cow boots": { // +20
			name: "cow boots", shortDesc: "a pair of shiny black calf length cavalier boots",
			longDesc: "These 3 inch heels sport a sleek in design with 4\" fold over on the sides and front. They have white laces up from the back of ankle to top, white felt liner and large mirrored steel buckles with ankle support strap. The front sports a hoof shaped mirrored steel toe piece.",
			slot: ClothingSlot.Shoes, color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["FEMININE_CLOTHING"],
			activeEffect: ["FANCY_MOVES"],
			style: [Fashion.Style.PetGirl, Fashion.Style.SexyDancer], meta: ['cow outfit', 'job reward']
		},

		"fuzzy paws": { // +20
			name: "fuzzy paws", shortDesc: "pair of {COLOR} fuzzy paws",
			longDesc: "\
    These shoes are designed to look like animal paws, right down to having an exaggerated footpad imprint. They are \
    made of some sort of high quality material and covered with a layer of luxuriously soft fur.\
    ",
			slot: ClothingSlot.Shoes,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Clothing,
			wearEffect: ["SEXY_CLOTHING"],
			activeEffect: ["FANCY_MOVES"],
			style: [Fashion.Style.PetGirl, Fashion.Style.SexyDancer],
			inMarket: false
		},

		// BUTT SLOT
		"cow tail": { // +9
			name: "cow tail", shortDesc: "a butt plug with a {COLOR} cow tail attached ",
			longDesc: "This anal plug is attached to a faux cow tail, sporting a cute little ribbon. It's warm to the touch, and sways on its own or when you walk. It feels real, is it magical? And it’s very comfortable, never accidentally slips out, and is always clean. It’s white with a black bush tip.",
			slot: ClothingSlot.Butt, color: "white", rarity: Items.Rarity.Rare, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING", "GAPE_TRAINING"], style: [Fashion.Style.PetGirl, Fashion.Style.Bdsm], meta: ['cow outfit', 'job reward']
		},

		"fuzzy anal tail": { // +12
			name: "fuzzy anal tail",
			shortDesc: "{COLOR} fuzzy tail ending in a thick butt plug",
			longDesc: "\
    The perfect accessory for any kitten on the prowl. The tail has been enchanted so that as long as the plug part \
    is nestled within your sphincter you can move it freely with but a mere thought!\
	",
			slot: ClothingSlot.Butt,
			color: "black", rarity: Items.Rarity.Legendary, type: Items.ClothingType.Accessory,
			wearEffect: ["PERVERTED_CLOTHING", "GAPE_TRAINING"],
			style: [Fashion.Style.PetGirl],
			inMarket: false
		},

		// PENIS SLOT

		// WEAPON SLOT (huh?)
	});
}
