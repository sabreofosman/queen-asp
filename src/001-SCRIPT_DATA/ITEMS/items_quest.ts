namespace App.Data {
	Object.append(App.Data.questItems, {
		'isla harbor mansion key': {
			name: "isla harbor mansion key",
			shortDesc: "<span class='item-quest'>Isla Harbor mansion key</span>",
			longDesc: "You obtained this backdoor key by letting Jarvis the butler pillage YOUR backdoor. It was hard work and your poor abused asshole leaked cum for hours afterwards, but now you can sneak into the mansion after dark.",
			type: ItemTypeSpecial.Quest
		},

		'14 inch purple dildo': {
			name: "14 inch purple dildo",
			shortDesc: "<span class='item-quest'>a 14 inch purple dildo</span>",
			longDesc: "This giant dildo is surprisingly rubbery with realistic veins and an equally impressive set of fake balls attached to it. Somewhat surprisingly you found this in the closet of your supposedly virginal girlfriend.",
			type: ItemTypeSpecial.Quest
		},

		"porno portfolio": {
			name: "porno portfolio",
			shortDesc: "<span class='item-quest'>a portfolio of pornographic drawings</span>",
			longDesc: "This leather portfolio is bound with string and contains numerous drawings of naked women engaged in various acts of fornication using all sorts of implements. You wonder why this was at the bottom of your girlfriends closet?",
			type: ItemTypeSpecial.Quest
		},

		"smugglers rum cask": {
			name: "smugglers rum cask",
			shortDesc: "<span class='item-quest'>a cask of Smuggler's Rum</span>",
			longDesc: "This small cask is filled with to the brim with the popular rum from the Smuggler's Den on Golden Isle.",
			type: ItemTypeSpecial.Quest
		},

		"bag of mojo": {
			name: "bag of mojo",
			shortDesc: "<span class='item-quest'>a bag of mojo</span>",
			longDesc: "You have no idea what's inside this bag and at this point you're too afraid to ask.",
			type: ItemTypeSpecial.Quest
		},

		"lolita book": {
			name: "lolita book",
			shortDesc: "<span class='item-quest'>a dirty novel entitled \"Daddy's Little Pet\"</span>",
			longDesc: "Evidently this book is contraband in seven countries and it's easy to see why. Along with the provocative incest story-line, it also has several drawings depicting several illegal acts with a minor. What kind of monster would get off on this stuff?",
			type: ItemTypeSpecial.Quest
		},

		"strange chemicals": {
			name: "strange chemicals",
			shortDesc: "<span class='item-quest'>a crate of strange chemicals</span>",
			longDesc: "This ratty wooden crate is filled to the brim with toxic powders and chemicals. You shudder to think what would happen if you dropped it or gave it a good bump.",
			type: ItemTypeSpecial.Quest
		},

		"rare ore": {
			name: "rare ore",
			shortDesc: "<span class='item-quest'>a cluster of rare magical ore</span>",
			longDesc: "\
	This sample of rare ore came from the depths of the caves on @@.location-name;Abamond@@. It feels warm to the touch \
	and causes a slight tingly sensation to crawl up your arm while you hold it. It's obviously magical to some degree.\
	",
			type: ItemTypeSpecial.Quest
		},

		"bucket of bobola sap": {
			name: "bucket of bobola sap",
			shortDesc: "<span class='item-quest'>bucket of bobola sap</span>",
			longDesc: "\
	This bucket is overflowing with the viscous purple sap of the bobola tree. It gives off a pungent smell that \
	reminds you of semen. In this state it's mostly useless, but a skilled alchemist could transform it into \
	someting else. \
	",
			type: ItemTypeSpecial.Quest
		},

		"bucket of refined bobola sap": {
			name: "bucket of refined bobola sap",
			shortDesc: "<span class='item-quest'>bucket of refined bobola sap</span>",
			longDesc: "\
	This bucket is overflowing with the viscous purple sap of the bobola tree. It gives off a pungent smell that \
	reminds you of semen. It has been chemically refined and treated so that it can be processed into goods by \
	a skilled craftsman. \
	",
			type: ItemTypeSpecial.Quest
		},

		"letter of introduction": {
			name: "letter of introduction",
			shortDesc: "<span class='item-quest'>letter of introduction</span>",
			longDesc: "\
	This letter is an absolute fraud, but it cost you a fair amount of coins to procure so it should hopefully \
	work. \
	",
			type: ItemTypeSpecial.Quest
		},

		"strange parcel": {
			name: "strange parcel",
			shortDesc: "<span class='item-quest'>strange parcel</span>",
			longDesc: "\
	This parcel was given to you by <span class='npc'>Petey O'Bannon</span> and is to be \
	delivered to <span class='npc'>Constable Jonah Blythe</span> on \
	<span class='location-name'>Port Royale</span>.\
	",
			type: ItemTypeSpecial.Quest
		},
	});
}
