// BOX TYPE     MINIMUM     BONUS ROLL  COLOR
// COMMON       0           0           grey
// UNCOMMON     20          10          lime
// RARE         30          20          cyan
// LEGENDARY    50          30          orange
namespace App.Data {
	Object.append(lootBoxes, {
		"common food loot box": {
			name: "common food chest",
			shortDesc: "@@.item-common;A small crate used for storing foodstuff@@",
			longDesc: "This small crate looks rather shoddy and the contents rattle when you shake it.",
			message: "With unrestrained curiosity you tear open the treasure chest to find…\n\n",
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["FOOD_LOOT_BOX_COMMON"]
		},

		"uncommon food loot box": {
			name: "uncommon food chest",
			shortDesc: "@@.item-uncommon;A crate used for storing foodstuff@@",
			longDesc: "This crate looks rather shoddy and the contents rattle when you shake it.",
			message: "With unrestrained curiosity you tear open the treasure chest to find…\n\n",
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["FOOD_LOOT_BOX_UNCOMMON"]
		},

		"rare food loot box": {
			name: "rare food chest",
			shortDesc: "@@.item-rare;A large crate used for storing foodstuff@@",
			longDesc: "This large crate looks rather shoddy and the contents rattle when you shake it.",
			message: "With unrestrained curiosity you tear open the treasure chest to find…\n\n",
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["FOOD_LOOT_BOX_RARE"]
		},

		"legendary food loot box": {
			name: "legendary food chest",
			shortDesc: "@@.item-legendary;A large crate used for storing foodstuff@@",
			longDesc: "This large crate looks rather shoddy and the contents rattle when you shake it.",
			message: "With unrestrained curiosity you tear open the treasure chest to find…\n\n",
			type: ItemTypeConsumable.LootBox,
			// Effect : [ TABLE, Minimum Roll, Bonus to roll
			effects: ["FOOD_LOOT_BOX_LEGENDARY"]
		}
	});

	lootTables["FOOD"] = [
		{
			type: Items.Category.Food,
			chance: 100,
		}
	];
}
