App.Combat.moves["KRAKEN"] = {
	engine: App.Combat.Engines.Kraken,
	moves: {
		Grab: {
			name: "Grab",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 5,
			combo: 0, // Costs no combo points
			speed: 5,
			damage: 0.8,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME grabs at you, but misses!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME grabs you in a slimy grip!"
				]
			]
		},
		Strangle: {
			name: "Strangle",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.2,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME tries to strangle you, but you break free!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME wraps around your neck and squeezes!"
				]
			]
		},
		Mouth: {
			name: "Mouth",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 0, // Costs no combo points
			speed: 20,
			damage: 1.5,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME tries to jam into your mouth, but you break free!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME slides into your mouth and starts to throat fuck you!"
				]
			]
		},
		Ejaculate1: {
			name: "Ejaculate1",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 0,
			speed: 20,
			damage: 0,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME starts to ejaculate into your throat, but you move your head and break free!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME tenses up and starts to ejaculate into your mouth, pumping a thick stream of goo into your stomach!"
				]
			]
		},
		Ass: {
			name: "Ass",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 0, // Costs no combo points
			speed: 20,
			damage: 1.5,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME tries to jam into your asshole, but you break free!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME slides into your asshole and starts to violently fuck you!"
				]
			]
		},
		Ejaculate2: {
			name: "Ejaculate2",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 0,
			speed: 20,
			damage: 0,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME starts to ejaculate into your ass, but you shale your hips and break free!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME tenses up and starts to ejaculate into your ass, pumping a thick stream of goo into your bowels!"
				]
			]
		}
	}
}
