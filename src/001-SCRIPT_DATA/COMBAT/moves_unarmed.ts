App.Combat.moves["UNARMED"] = {
	engine: App.Combat.Engines.Unarmed,
	moves: {
		Punch: {
			name: "Punch",
			description: "\
            A basic strike with the unarmed fist. No fancy technique.<br>\
            <span style='color:darkred'>DMG LOW</span> \
            <span style='color:darkgoldenrod'>STA LOW</span> \
            <span style='color:cyan'>SPD FAST</span><br>\
            <span style='color:deeppink'>Combo Builder</span>\
            ",
			icon: "punch_icon",
			stamina: 5,
			combo: 0, // Costs no combo points
			speed: 5,
			damage: 1.0,
			unlock: () => true,
			miss: [
				[
					"You swing at NPC_NAME with your fist, but miss!",
					"NPC_NAME swings at you, but misses!"
				],
				[
					"You strike at NPC_NAME with your fist, but the attack is deflected!",
					"NPC_NAME strikes at you with NPC_PRONOUN fist, but you deflect the attack!"
				],
				[
					"NPC_NAME dodges your fist!",
					"You dodge NPC_NAME's fist!"
				]
			],
			hit: [
				[
					"You strike NPC_NAME with a light blow!",
					"NPC_NAME strikes you with a light blow!"
				],
				[
					"You punch NPC_NAME with a solid strike!",
					"NPC_NAME punches you with a solid strike!"
				],
				[
					"You punch NPC_NAME with a brutal strike!",
					"NPC_NAME punches you with a brutal strike!"
				],
				[
					"You hit NPC_NAME with a bone crushing punch!",
					"NPC_NAME hits you with a bone crushing punch!"
				]
			]
		},
		Kick: {
			name: "Kick",
			description: "\
            A basic kick. Solid damage, but slow.<br>\
            <span style='color:darkred'>DMG LOW</span> \
            <span style='color:darkgoldenrod'>STA LOW</span> \
            <span style='color:cyan'>SPD AVERAGE</span><br>\
            <span style='color:deeppink'>Combo Builder</span>\
            ",
			icon: "kick_icon",
			stamina: 5,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.5,
			unlock: () => true,
			miss: [
				[
					"You kick at NPC_NAME, but miss!",
					"NPC_NAME kicks at you, but misses!"
				],
				[
					"You kick at NPC_NAME, but the attack is deflected!",
					"NPC_NAME kicks at you, but you deflect the attack!"
				],
				[
					"NPC_NAME dodges your kick!",
					"You dodge NPC_NAME's kick!"
				]
			],
			hit: [
				[
					"You strike NPC_NAME with a light kick!",
					"NPC_NAME strikes you with a light kick!"
				],
				[
					"You hit NPC_NAME with a solid kick!",
					"NPC_NAME hits you with a solid kick!"
				],
				[
					"You hit NPC_NAME with a brutal kick!",
					"NPC_NAME hits you with a brutal kick!"
				],
				[
					"You hit NPC_NAME with a bone crushing kick!",
					"NPC_NAME hits you with a bone crushing kick!"
				]
			]
		},
		Haymaker: {
			name: "Haymaker",
			description: "\
            A powerful uppercut that has a chance of stunning an opponent.<br>\
            <span style='color:darkred'>DMG LOW</span> \
            <span style='color:darkgoldenrod'>STA AVERAGE</span> \
            <span style='color:cyan'>SPD AVERAGE</span><br>\
            <span style='color:gold'>Combo Consumer (Low)</span> - \
            <span style='color:yellow'>STUN</span>\
            ",
			icon: "haymaker_icon",
			stamina: 10,
			combo: 2,
			speed: 10,
			damage: 2.0,
			unlock: () => true,
			miss: [
				[
					"You swing at NPC_NAME with your fist, but miss!",
					"NPC_NAME swings at you, but misses!"
				],
				[
					"You strike at NPC_NAME with your fist, but the attack is deflected!",
					"NPC_NAME strikes at you with NPC_PRONOUN fist, but you deflect the attack!"
				],
				[
					"NPC_NAME dodges your fist!",
					"You dodge NPC_NAME's fist!"
				]
			],
			hit: [
				[
					"You strike NPC_NAME with a glancing haymaker!",
					"NPC_NAME strikes you with a glancing haymaker!"
				],
				[
					"You punch NPC_NAME with a solid haymaker!",
					"NPC_NAME punches you with a solid haymaker!"
				],
				[
					"You punch NPC_NAME with a brutal haymaker!",
					"NPC_NAME punches you with a brutal haymaker!"
				],
				[
					"You hit NPC_NAME with a bone crushing haymaker!",
					"NPC_NAME hits you with a bone crushing haymaker!"
				]
			]
		},
		Knee: {
			name: "Knee",
			description: "\
            A knee to the groin. Classic move. More effective on males.<br>\
            <span style='color:darkred'>DMG ???</span> \
            <span style='color:darkgoldenrod'>STA LOW</span> \
            <span style='color:cyan'>SPD SLOW</span><br>\
            <span style='color:gold'>Combo Consumer (Medium)</span> - \
            <span style='color:yellow'>??? Effect</span>\
            ",
			icon: "knee_icon",
			stamina: 5,
			combo: 3,
			speed: 20,
			damage: 1.0,
			unlock: () => true,
			miss: [
				[
					"You kick at NPC_NAME, but miss!",
					"NPC_NAME kicks at you, but misses!"
				],
				[
					"You kick at NPC_NAME, but the attack is deflected!",
					"NPC_NAME kicks at you, but you deflect the attack!"
				],
				[
					"NPC_NAME dodges your kick!",
					"You dodge NPC_NAME's kick!"
				]
			],
			hit: [
				[
					"You strike NPC_NAME with a light knee to the privates!",
					"NPC_NAME strikes you with a light knee to the privates!"
				],
				[
					"You hit NPC_NAME with a solid knee to the privates!",
					"NPC_NAME hits you with a solid knee to the privates!"
				],
				[
					"You hit NPC_NAME with a brutal knee to the privates!",
					"NPC_NAME hits you with a brutal knee to the privates!"
				],
				[
					"You hit NPC_NAME with a bone crushing knee to the privates!",
					"NPC_NAME hits you with a bone crushing knee to the privates!"
				]
			]
		},
	}
};
