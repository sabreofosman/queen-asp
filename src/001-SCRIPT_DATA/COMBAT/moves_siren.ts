App.Combat.moves["SIREN"] = {
	engine: App.Combat.Engines.Siren,
	moves: {
		Touch: {
			name: "Touch",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 5,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.0,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME reaches for you, but misses!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME lands an icy touch on you, so cold it burns!"
				]
			]
		},
		Toss: {
			name: "Toss",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 0, // Costs no combo points
			speed: 10,
			damage: 1.2,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME tries to grab you, but you break free!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME grabs you and tosses you to the ground!"
				]
			]
		},
		Scream: {
			name: "Scream",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 10,
			combo: 4,
			speed: 20,
			damage: 1.5,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME lets loose an ear splitting scream, but it has no effect!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME lets loosean ear splitting scream, causing immense pain!"
				]
			]
		},
		Drown: {
			name: "Drown",
			description: "\
        Monster Attack - No Info<br>\
        ",
			icon: "slash_icon",
			stamina: 20,
			combo: 6,
			speed: 20,
			damage: 2.5,
			unlock: () => true,
			miss: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER",
					"NPC_NAME summons a large tidal wave, but you narrowly avoid it!"
				]
			],
			hit: [
				[
					"BUG - ATTACK NOT USABLE BY PLAYER!",
					"NPC_NAME summons a large tidal wave that smothers and drowns you!"
				]
			]
		}
	}
}
