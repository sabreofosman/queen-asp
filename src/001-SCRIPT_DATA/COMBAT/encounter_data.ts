namespace App.Combat {
	Object.append(encounterData, {
		TEST_FIGHT_1: {
			enemies: ["Champion Siren"],
			fatal: false,
			winPassage: "CombatWinTest",
			losePassage: "Deck",
			intro: "You step up to the ring, ready to fight. ENEMY_0 scoffs at you."
		},

		TEST_FIGHT_2: {
			enemies: ["Kraken Tentacle", "Kraken Tentacle", "Kraken Tentacle"],
			fatal: false,
			winPassage: "CombatWinTest",
			losePassage: "Deck",
			intro: "You are surrounded!",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 10,
					max: 50
				},
				{
					chance: 100,
					type: 'Random',
					min: 50,
					max: 100
				},
				{
					chance: 80,
					type: Items.Category.Food,
					tag: 'purple tentacle',
					min: 1,
					max: 1,
				},
				{
					chance: 60,
					type: Items.Category.Food,
					tag: 'purple tentacle',
					min: 1,
					max: 1,
				},
				{
					chance: 40,
					type: Items.Category.Food,
					tag: 'purple tentacle',
					min: 1,
					max: 1,
				}
			]
		},

		// Kraken Event on the Salty Mermaid
		KrakenAttack: {
			enemies: ["Kraken Tentacle", "Kraken Tentacle", "Kraken Tentacle"],
			fatal: false,
			winPassage: "CombatWinKraken",
			losePassage: "CombatLoseKraken",
			intro: "You are surrounded!",
			lootMessage: "Scattered on the deck you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 10,
					max: 50
				},
				{
					chance: 100,
					type: 'Random',
					min: 50,
					max: 100
				},
				{
					chance: 20,
					type: 'Random',
					min: 50,
					max: 300
				},
				{
					chance: 80,
					type: Items.Category.Food,
					tag: 'purple tentacle',
					min: 1,
					max: 1,
				},
				{
					chance: 60,
					type: Items.Category.Food,
					tag: 'purple tentacle',
					min: 1,
					max: 1,
				},
				{
					chance: 40,
					type: Items.Category.Food,
					tag: 'purple tentacle',
					min: 1,
					max: 1,
				}
			],
			winHandler: () => {
				setup.world.advancePhaseTo(Phase.Afternoon);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, 10));
			},
			loseHandler: () => { // The crew is slightly happy you helped them out.
				setup.world.advancePhaseTo(Phase.Night);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, 5));
			},
			fleeHandler: () => {
				setup.world.advancePhaseTo(Phase.Afternoon);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, -5));
			}
		},

		// Siren attack on the Salty Mermaid
		SirenAttack: {
			enemies: ["Siren", "Siren"],
			fatal: false,
			winPassage: "CombatWinSiren",
			losePassage: "CombatLoseSiren",
			intro: "You are surrounded!",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 50,
					max: 175
				},
				{
					chance: 100,
					type: 'Random',
					min: 100,
					max: 200
				},
				{
					chance: 80,
					type: 'Random',
					min: 100,
					max: 200
				},
				{
					chance: 50,
					type: Items.Category.Drugs,
					tag: 'siren elixir',
					min: 1,
					max: 2
				},

			],
			winHandler: () => {
				setup.world.advancePhaseTo(Phase.LateNight);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, 10));
			},
			loseHandler: () => {
				setup.world.advancePhaseTo(Phase.LateNight);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, 5));
			},
			fleeHandler: () => {
				setup.world.advancePhaseTo(Phase.LateNight);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, -5));
			}
		},

		// Pirate Attack on the Salty Mermaid
		PirateAttack: {
			enemies: ["Weak Pirate", "Weak Pirate"],
			fatal: false,
			winPassage: "CombatWinPirate",
			losePassage: "CombatLosePirate",
			intro: "You are surrounded! ENEMY_0 jeers at you, ready for a fight!",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 50,
					max: 125
				},
				{
					chance: 100,
					type: 'Random',
					min: 100,
					max: 200
				},
				{
					chance: 80,
					type: 'Random',
					min: 100,
					max: 200
				},
				{
					chance: 60,
					type: 'Random',
					min: 100,
					max: 200
				}

			],
			winHandler: () => {
				setup.world.advancePhaseTo(Phase.Afternoon);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, 10));
			},
			loseHandler: () => { // The crew is slightly happy you helped them out.
				setup.world.advancePhaseTo(Phase.Night);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, 5));
			},
			fleeHandler: () => {
				setup.world.advancePhaseTo(Phase.Afternoon);
				setup.world.npcGroup('SaltyMermaidCrew').forEach(npc => npc.adjustStat(NpcStat.Mood, -5));
			}
		},

		// Kipler Duel
		KIPLER_DUEL: {
			enemies: ["Kipler"],
			fatal: false,
			winPassage: "CombatWinKiplerDuel",
			losePassage: "CombatLoseKiplerDuel",
			intro: "You follow <span class='npc'>Kipler</span> out onto the deck, a small crowd assembles to watch your duel",
			winHandler: (p) => {
				setup.world.nextPhase(1);
				Quest.virtualById('KIPLER_DEFEATED_SUB_QUEST').markAsCompleted(p);
				setup.world.npc('Crew').adjustStat(NpcStat.Mood, 20);
			},
			loseHandler: () => {
				setup.world.nextPhase(1);
			},
			fleeHandler: () => {
				setup.world.nextPhase(1);
			}
		},

		// Mamazon Quest Encounter
		GlutezonAmbush: {
			enemies: ["Weak Glutezon", "Weak Glutezon", "Weak Glutezon"],
			fatal: true,
			winPassage: "CombatWinMamazonQuest",
			losePassage: "CombatLoseMamazonQuest",
			intro: "You leap from your hiding spot and engage with the raiders!",
			winHandler: (p) => {
				setup.world.nextPhase(1);
				Quest.virtualById('MAMAZON_CHAMP_SUB1').markAsCompleted(p);
				setup.world.npcGroup('MamazonsVillageCitizens').forEach(npc => npc.adjustStat(NpcStat.Mood, 20));
			},
			loseHandler: () => {
				setup.world.nextPhase(1);
			},
			fleeHandler: () => {
				setup.world.nextPhase(1);
			}
		},

		// Glutezon Quest Encounter
		MamazonAmbush: {
			enemies: ["Weak Mamazon", "Weak Mamazon"],
			fatal: true,
			winPassage: "CombatWinGlutezonQuest",
			losePassage: "CombatLoseGlutezonQuest",
			intro: "You leap from your hiding spot and engage with the seductresses!",
			winHandler: () => {
				setup.world.nextPhase(1);
			},
			loseHandler: () => {
				setup.world.nextPhase(1);
			},
			fleeHandler: () => {
				setup.world.nextPhase(1);
			}
		},

		// Boobpire street encounter
		BoobpireStreets: {
			enemies: ["Boobpire"],
			fatal: false,
			winPassage: "CombatWinBoobpireStreets",
			losePassage: "CombatLoseBoobpireStreets",
			intro: "Suddenly, the woman corners you in an alley and reveals herself to be a dreaded <span class='state-sexiness'>Boobpire</span>!",
			lootMessage: "Scattered on the ground you find the following items…",
			loot: [
				{
					chance: 100,
					type: 'Coins',
					min: 50,
					max: 250
				},
				{
					chance: 100,
					type: 'Random',
					min: 150,
					max: 250
				},
				{
					chance: 80,
					type: Items.Category.Drugs,
					tag: 'siren elixir',
					min: 1,
					max: 1,
				}
			],
			winHandler: () => {
				setup.world.nextPhase(1);
			},
			loseHandler: (p) => {
				p.adjustBodyXP(BodyPart.Bust, -200);
				setup.world.nextPhase(1);
			},
			fleeHandler: () => {
				setup.world.nextPhase(1);
			}
		},

		// Bertie Quest Encounter - Queen's Favor Part 2
		QueenFavorAmbush: {
			enemies: ["HoodedAssassain"],
			fatal: true,
			winPassage: "CombatWinQueenFavorAmbush",
			losePassage: "CombatLoseQueenFavorAmbush",
			intro: "The hooded man leaps to attack you!"
		}
	});
}
