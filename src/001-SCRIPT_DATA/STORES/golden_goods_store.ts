namespace App.Data {
	stores.goldenGoods = {
		name: "Golden Goods", open: [0, 1, 2, 3], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 12, max: 12, price: 1.0, mood: 0, lock: 0, tag: "beer"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "rum"},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "basic makeup"},
			{category: Items.Rarity.Common, type: Items.Category.Cosmetics, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "hair products"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 8, max: 8, price: 1.0, mood: 0, lock: 0, tag: "medicinal herbs"},
			{category: Items.Rarity.Common, type: Items.Category.MiscConsumable, qty: 20, max: 20, price: 1.0, mood: 0, lock: 0, tag: "torch"},
			{category: Items.Rarity.Common, type: Items.Category.MiscConsumable, qty: 10, max: 10, price: 1.0, mood: 0, lock: 0, tag: "shovel"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "black hair dye"},
			{category: Items.Rarity.Common, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pirate boots"},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pirate bra"},
			{category: Items.Rarity.Rare, type: Items.Category.Clothes, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "pirate thong"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "milkdew melon"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "mighty banana"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "honey mead"},
			{category: Items.Rarity.Rare, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "butter gourd"}
		]
	};
}
