namespace App.Data {
	stores.isabella = {
		name: "Isabella Ioveanu", open: [0, 1, 2, 3], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "apElderberry"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "apPersico"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apRose"},
			{category: Items.Rarity.Common, type: Items.Category.Drugs, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apSulfur"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "apTanzy"},
			{category: Items.Rarity.Rare, type: Items.Category.Drugs, qty: 1, max: 1, price: 1.0, mood: 0, lock: 0, tag: "apThorn"}
		]
	};
}
