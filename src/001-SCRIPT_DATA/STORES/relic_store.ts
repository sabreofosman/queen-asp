// Special store
namespace App.Data {
	export interface RelicStoreEntry {
		category: Items.CategoryConsumable;
		type: ItemTypeConsumable;
		tag: string;
		cost: {type: ItemNameTemplateConsumable, amount: number}[];
	}
	export const relicStore: RelicStoreEntry[] = [
		// Common Face, Lips, Hair
		{
			category: Items.Category.Drugs, type: ItemTypeConsumable.Potion, tag: "relic enhancer a", cost: [
				{
					type: "drugs/fairy dust",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 3
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer b", // Bust, Perversion, Femininity, Seduction
			cost: [
				{
					type: "food/milkdew melon",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 8
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer c", // Ass, Hips, Perversion, Seduction
			cost: [
				{
					type: "food/butter gourd",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 8
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer d", // Fitness, Waist, Dancing
			cost: [
				{
					type: "food/cheap wine",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 3
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer e", // Swashbuckling, Navigating, Sailing
			cost: [
				{
					type: "food/rum",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer f", // Seduction, Singing, Dancing, Styling
			cost: [
				{
					type: "food/orgeat",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer g", // Seduction, Singing, Dancing, Styling
			cost: [
				{
					type: "food/bumbo",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer h", // Penis, Balls, Perversion
			cost: [
				{
					type: "food/mighty banana",
					amount: 2
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				},
				{
					type: "miscLoot/glowing crystal",
					amount: 1
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer i", // Fitness, Waist Up
			cost: [
				{
					type: "drugs/khafkir",
					amount: 2
				},
				{
					type: "miscLoot/broken rune",
					amount: 4
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 2
				},
				{
					type: "miscLoot/glowing crystal",
					amount: 1
				}
			]
		},
		{
			category: Items.Category.Drugs,
			type: ItemTypeConsumable.Potion,
			tag: "relic enhancer j", // TitFucking, AssFucking, BlowJobs, HandJobs
			cost: [
				{
					type: "drugs/siren elixir",
					amount: 1
				},
				{
					type: "miscLoot/broken rune",
					amount: 6
				},
				{
					type: "miscLoot/old arrowhead",
					amount: 4
				},
				{
					type: "miscLoot/glowing crystal",
					amount: 2
				},
				{
					type: "miscLoot/stone tablet",
					amount: 1
				}
			]
		},
	];
}
