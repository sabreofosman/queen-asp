namespace App.Data {
	stores.barnabyLong = {
		name: "Long's Public House", open: [0, 1, 2, 3], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "cider"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "beer"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "cordial waters"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "bumbo"}
		]
	};

	stores.meghanLong = {
		name: "Long's Public House", open: [0, 1, 2, 3], restock: 7,
		inventory: [
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "mystery stew"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 4, max: 4, price: 1.0, mood: 0, lock: 0, tag: "roast fish"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "apple tart"},
			{category: Items.Rarity.Common, type: Items.Category.Food, qty: 2, max: 2, price: 1.0, mood: 0, lock: 0, tag: "jojo nut pie"}
		]
	};
}
