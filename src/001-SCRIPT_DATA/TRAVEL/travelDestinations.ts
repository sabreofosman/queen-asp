namespace App.Data.Travel {
	export function disableAtLateNight(): boolean {
		return setup.world.phase >= Phase.LateNight ? false : true;
	}
}
