namespace App.Entity {

	interface StatTypeObjectMap {
		[Stat.Core]: Record<CoreStat, number>;
		[Stat.Skill]: Record<Skills.Any, number>;
		[Stat.Body]: Record<BodyStat, number>;
	}

	export const enum FlagType {
		Job = 'job',
		Quest = 'quest'
	}

	export type ItemCallback = (charges: number, name: string, itemClass: keyof Items.InventoryItemTypeMap) => void;
	export type ItemPredicate = (charges: number, name: string, itemClass: keyof Items.InventoryItemTypeMap) => boolean;
	/**
	 * Provides convenient methods for inspecting an inventory objects (@see Entity.PlayerState.inventory
	 * and below the Player class).
	 *
	 * Might be used by NPCs in a future version, thus is not part of the Player class.
	 *
	 * This object is not meant to be serialized by SugarCube
	 *
	 */
	export class InventoryManager {
		static readonly MAX_ITEM_CHARGES = 100;

		private _items: Record<string, Items.InventoryItem>;
		private _reelInSlots: Record<number, Items.Reel | null>;

		private get _state(): GameState.Player {
			return World.instance.state.character.pc as GameState.Player;
		}

		constructor() {
			// create item objects for each record
			this._items = {};
			this.forEachItemRecord(undefined, undefined, (charges, tag, itemClass) => {
				this._ensureWrapObjectExists(itemClass, tag, charges);
			});

			this._reelInSlots = {};
			for (const slot in this._state.slots) {
				if (!this._state.slots.hasOwnProperty(slot)) continue;
				const reelTag = this._state.slots[slot];
				this._reelInSlots[slot] = reelTag == null ? null : Items.factory(Items.Category.Reel, reelTag, this)
			}
		}

		/**
		 * Looks through all classes for item with the given tag and returns the item class
		 * @param Tag
		 * @param FindAll return all matching items (the first one otherwise)
		 * @private
		 * TODO refactor the loop to exit early if FindAll = false
		 */
		private _findItemClass(tag: string): Items.CategoryInventory;
		private _findItemClass(tag: string, findAll: false): Items.CategoryInventory;
		private _findItemClass(tag: string, findAll: true): Items.CategoryInventory[];
		private _findItemClass(tag: string, findAll = false): Items.CategoryInventory | Items.CategoryInventory[] | null {
			const res: Items.CategoryInventory[] = [];
			Object.entries(this._state.inventory).forEach((e) => {
				if (e[1].hasOwnProperty(tag)) {
					res.push(e[0]);
				}
			});
			if (findAll) {
				return res;
			}
			if (res.length == 0) return null;
			return res[0];
		}

		forEachItemRecord<Callback extends ItemCallback>(tag: string | undefined, itemClass: Items.CategoryInventory | undefined,
			func: Callback, thisObj?: ThisParameterType<Callback>): void {
			const types: Items.CategoryInventory[] = itemClass == undefined ? Object.keys(this._state.inventory) : [itemClass];
			types.forEach(function (this: InventoryManager, type) {
				const val = this._state.inventory[type];
				if (!val) return;
				if (tag != undefined) {
					if (val.hasOwnProperty(tag)) {
						func.call(thisObj, val[tag], tag, type);
					}
				} else {
					for (const n in val) {
						if (!val.hasOwnProperty(n)) continue;
						func.call(thisObj, val[n], n, type);
					}
				}
			}, this);
		}

		everyItemRecord<Predicate extends ItemPredicate>(tag: string | undefined, itemClass: Items.CategoryInventory | undefined, func: Predicate, thisObj?: ThisParameterType<Predicate>): void {
			const types: Items.CategoryInventory[] = itemClass == undefined ? Object.keys(this._state.inventory) : [itemClass];
			types.every(function (this: InventoryManager, type) {
				const val = this._state.inventory[type];
				if (!val) return;
				if (tag !== undefined) {
					if (val.hasOwnProperty(tag)) {
						return func.call(thisObj, val[tag], tag, type);
					}
				} else {
					for (const n in val) {
						if (!val.hasOwnProperty(n)) continue;
						if (!func.call(thisObj, val[n], n, type)) return false;
					}
					return true;
				}
			}, this);
		}

		private _ensureWrapObjectExists<T extends  keyof Items.InventoryItemTypeMap>(itemClass: T, tag: string, _charges: number, id?: string): Items.InventoryItem {
			if (id == undefined) id = Items.makeId(itemClass, tag);
			if (!this._items.hasOwnProperty(id)) {
				// charges is stored already, hence '0' to prevent stack overflowing
				this._items[id] = Items.factory(itemClass, tag, this, 0);
			}
			return this._items[id];
		}

		filter(func: (item: Items.InventoryItem) => boolean, thisObj?: unknown): Items.InventoryItem[] {
			const res: Items.InventoryItem[] = [];
			this.forEachItemRecord(undefined, undefined, function (this: InventoryManager, ch, nm, cl) {
				const itemId = Items.makeId(cl, nm);
				const itemWrapObj = this._ensureWrapObjectExists(cl, nm, ch, itemId);
				if (func.call(thisObj, itemWrapObj) == true) res.push(itemWrapObj);
			}, this);
			return res;
		}

		charges(itemClass: Items.CategoryInventory | undefined, tag: string): number {
			let res = 0;
			this.forEachItemRecord(tag, itemClass, (n) => {res += n;});
			return res;
		}

		setCharges(itemClass: Items.CategoryInventory, tag: string, count: number): number {
			const cl = (itemClass == undefined) ? this._findItemClass(tag, true)[0] : itemClass;
			const clamped = Math.clamp(Math.floor(count), 0, InventoryManager.MAX_ITEM_CHARGES);
			const clValues = this._state.inventory[cl];
			if (clValues) {
				if (clamped == 0) {
					if (this._state.inventory.hasOwnProperty(cl) && clValues.hasOwnProperty(tag)) {
						delete clValues[tag];
						delete this._items[Items.makeId(cl, tag)];
					}
				} else {
					if (!this._state.inventory.hasOwnProperty(cl)) this._state.inventory[cl] = {};
					clValues[tag] = clamped;
					this._ensureWrapObjectExists(itemClass, tag, clamped);
				}
			}
			return clamped;
		}

		/**
		 * Adds (or removes) charges to a given item. If item is not in the inventory and Amount > 0, item record
		 * is created.
		 * If Amount < 0, resulting number of charges does not go below zero.
		  * @returns new number of charges
		 */
		addCharges(itemClass: Items.CategoryInventory, tag: string, amount: number): number {
			const cl = (itemClass == undefined) ? this._findItemClass(tag, true)[0] : itemClass;
			if (cl == undefined) throw Error(`No item tagged '${tag}'`);
			// slight hack to not allow items that don't exist to be added to the inventory.
			const data = Items.tryGetItemsDictionary(cl);
			if (data == null) throw Error("No item class '" + cl + "' exists.");
			if (!data.hasOwnProperty(tag)) throw Error(`No item tagged '${tag}' exists in class '${cl}'`);
			if (!this._state.inventory.hasOwnProperty(cl)) this._state.inventory[cl] = {};

			const clInv = this._state.inventory[cl]!;
			if (!clInv.hasOwnProperty(tag)) {
				clInv[tag] = 0;
			}

			// Items with Charges in their data grant that many charges when added to the inventory.
			// All usage however only subtracts 1 charge.
			if (amount > 0) {
				amount = amount * Items.getCharges(cl, tag);
			} else {
				amount = amount == 0 ? Items.getCharges(cl, tag) : amount; // add charges if no charge specified?
			}

			return this.setCharges(cl, tag, clInv[tag] + amount);
		}

		addItem<T extends keyof Items.InventoryItemTypeMap>(itemClass: T, tag: string, charges: number): Items.InventoryItemTypeMap[T] {
			type R = Items.InventoryItemTypeMap[T];
			this.addCharges(itemClass, tag, charges == undefined ? 1 : charges);
			return this._items[Items.makeId(itemClass, tag)] as R;
		}

		removeItem(id: Data.ItemNameTemplateInventory): void {
			const n = Items.splitId(id);
			this.setCharges(n.category, n.tag, 0);
		}

		/**
		* Attempt to pick a reel from inventory by Id and then equip it to a slot. It will remove any reel
		* equipped in that slot and place it back in the inventory.
		*/
		equipReel(toEquipID: Data.ItemNameTemplate<Items.Category.Reel>, reelSlot: number): void {
			const nm = Items.splitId(toEquipID);
			this.addCharges(Items.Category.Reel, nm.tag, -1);

			const reelTag = this._state.slots[reelSlot];
			if (reelTag != null) {
				this.addCharges(Items.Category.Reel, reelTag, 1);
			}

			this._state.slots[reelSlot] = nm.tag;
			this._reelInSlots[reelSlot] = Items.factory(Items.Category.Reel, nm.tag, this);
		}

		/**
		 * Remove an equipped reel and place it in the inventory.
		 */
		removeReel(slotID: number): void {
			const reelTag = this._state.slots[slotID];
			if (reelTag) {
				this.addCharges(Items.Category.Reel, reelTag, 1);
				this._state.slots[slotID] = null;
				this._reelInSlots[slotID] = null;
			}
		}

		/**
		* Turn the equipped reels into an array to iterate/read.
		*/
		equippedReelItems(): Items.Reel[] {
			const arr = Object.values(this._reelInSlots).filter(o => !_.isNil(o));
			return (typeof arr === 'undefined') ? [] : arr as Items.Reel[];
		}

		reelSlots(): Record<number, Items.Reel | null> {
			return this._reelInSlots;
		}

		/**
		 * Adds item to the set of favorites
		 */
		addFavorite(id: string): void {
			this._state.inventoryFavorites.add(id);
		}

		/**
		 * Removes item from the favorites set
		 */
		deleteFavorite(id: string): void {
			this._state.inventoryFavorites.delete(id);
		}

		/**
		 * Tests whether the item is in the favorites set
		 */
		isFavorite(id: string): boolean {
			return this._state.inventoryFavorites.has(id);
		}
	}

	export class ClothingManager {
		private readonly _wardrobeItems: Items.Clothing[] = []; // TODO make it a Set
		private _equippedItems: Partial<Record<ClothingSlot, Items.Clothing | null>> = {};

		private get _state(): GameState.Player {
			return World.instance.state.character.pc as GameState.Player;
		}

		private get _wardrobe() {
			return this._state.wardrobe;
		}

		private get _equipment() {
			return this._state.equipment;
		}

		/**
		 * Creates object for tracking equipment state
		 */
		static equipmentRecord(id: Data.ItemNameTemplateEquipment, isLocked = false): {id: Data.ItemNameTemplateEquipment; locked: boolean;} {
			return {id, locked: isLocked ?? false};
		}

		constructor() {
			for (const id of this._wardrobe) {
				// wardrobe lists item ids
				const t = Items.splitId(id);
				this._ensureWrapObjectExists(t.tag, id);
			}

			for (const [prop, eq] of Object.entries(this._equipment)) {
				if (eq === undefined) continue;
				if (eq === null) {this._equippedItems[prop] = null; continue;}
				const n = Items.splitId(eq.id);
				this._equippedItems[prop] = Items.factory(n.category, n.tag, this);
			}
		}

		private _ensureWrapObjectExists(tag: string, id?: string): Items.Clothing {
			if (id == undefined) id = Items.makeId(Items.Category.Clothes, tag);
			for (let i = 0; i < this._wardrobeItems.length; ++i) {
				if (this._wardrobeItems[i].id == id) return this._wardrobeItems[i];
			}
			this._wardrobeItems.push(Items.factory(Items.Category.Clothes, tag, this));
			return this._wardrobeItems[this._wardrobeItems.length - 1];
		}

		/**
		 * Finds slot in which item with the given Id is worn
		 */
		private _findWornSlot(id: string): ClothingSlot | null {
			for (const [prop, eq] of Object.entries(this._equippedItems)) {
				if (eq?.id == id) return prop;
			}
			return null;
		}

		/**
		 * Returns true an item with the given Id is worn in the given slot. If slot param is omitted,
		 * every slot is checked.
		 */
		isWorn(id: string, slot?: ClothingSlot): boolean {
			if (slot == undefined) return this._findWornSlot(id) != null;
			const eq = this._equipment[slot];
			return (this._equipment.hasOwnProperty(slot) && eq?.id == id);
		}

		/**
		 * Is item in the named slot is locked?
		 * @returns True is there is an item in the slot and it's locked, false otherwise
		 */
		isLocked(slot: ClothingSlot): boolean {
			return this._equipment[slot]?.locked === true;
		}

		/**
		 * Useful helper method.
		 */
		setLock(slot: ClothingSlot, lock: boolean): void {
			const eq = this._equipment[slot];
			if (eq) {
				eq.locked = lock;
			}
		}

		/**
		 * Wear item and optionally set its 'locked' state.
		 * @param id
		 * @param lock lock or unlock item. Leaves locked state as is if the parameter is omitted.
		 */
		wear(id: string, lock?: boolean): void {
			let slot = this._findWornSlot(id);
			if (slot === null) { // currently the item is not worn
				for (let i = 0; i < this._wardrobeItems.length; ++i) {
					if (this._wardrobeItems[i].id != id) continue;
					const itm = this._wardrobeItems[i];
					slot = itm.slot;

					const slotsToUndress: ClothingSlot[] = itm.restrict;
					slotsToUndress.push(slot);
					// handle restriction by removing items from the restricted slots
					for (const sl of slotsToUndress) {
						const ei = this._equippedItems[sl] ?? null;
						if (ei) { // move worn item into the wardrobe
							this._wardrobeItems.push(ei);
							this._wardrobe.push(ei.id);
							delete this._equippedItems[sl];
							delete this._equipment[sl];
						}
					}
					this._equippedItems[slot] = itm;
					this._wardrobeItems.splice(i, 1);
					this._equipment[slot] = ClothingManager.equipmentRecord(itm.id, itm.isLocked);
					this._wardrobe.splice(i, 1);
					break;
				}
			}
			if (slot != null && lock !== undefined) {
				this.setLock(slot, lock);
			}
		}

		takeOffSlot(slot: ClothingSlot): void {
			const itm = this._equippedItems[slot];
			if (itm) {
				this._equippedItems[slot] = null;
				this._equipment[slot] = null;
				this._wardrobeItems.push(itm);
				this._wardrobe.push(itm.id); // push(Id) ?
			}
		}

		takeOff(id: string): void {
			for (const [slot, itm] of Object.entries(this._equippedItems)) {
				if (itm && itm.id === id) {
					this.takeOffSlot(slot);
					break;
				}
			}
		}

		addItem(name: string, wear = false): Items.Clothing {
			const item = Items.factory(Items.Category.Clothes, name, this);
			this._wardrobe.push(item.id);
			this._wardrobeItems.push(item);
			if (wear == true) {
				this.wear(item.id);
			}
			return item;
		}

		get wardrobe(): Items.Clothing[] {
			return this._wardrobeItems;
		}

		get equipment(): Partial<Record<ClothingSlot, Items.Clothing | null>> {
			return this._equippedItems;
		}

		//#region Equipment sets
		get storedSets(): GameState.EquipmentSet[] {
			return this._state.equipmentSets ?? [];
		}

		saveCurrentSet(name: string): void {
			const items: Partial<Record<ClothingSlot, Data.ItemNameTemplateEquipment>> = {};
			for (const [s, eq] of Object.entries(this.equipment)) {
				if (eq && !eq.isLocked) {
					items[s] = eq.id;
				}
			}
			const sets = this.storedSets;
			sets.push({name, items});
			this._state.equipmentSets = sets;
		}

		deleteSet(indexOrName: number | string): void {
			if (typeof indexOrName === "number") {
				if (indexOrName >= 0 && indexOrName < this.storedSets.length) {
					this._state.equipmentSets.splice(indexOrName, 1);
				}
			} else {
				this._state.equipmentSets = this.storedSets.filter(set => set.name !== indexOrName);
			}
		}

		wearSet(indexOrName: number | string): void {
			if (typeof indexOrName === "number") {
				if (indexOrName >= 0 && indexOrName < this.storedSets.length) {
					const set = this.storedSets[indexOrName];
					for (const [s, id] of Object.entries(set.items)) {
						if (!this.isLocked(s) && !this.isWorn(id, s)) {
							this.wear(id);
						}
					}
					setup.avatar.drawPortrait();
				}
			} else {
				const index = this.storedSets.findIndex(s => s.name === indexOrName);
				if (index >= 0) {
					this.wearSet(index);
				}
			}
		}

		//#endregion
	}

	export class Human {
		constructor(id: string) {
			this._id = id;
		}

		protected get state(): GameState.Human {
			return World.instance.state.character[this._id];
		}

		private readonly _id: string;
	}

	export class Player extends Human {
		#clothing: Entity.ClothingManager| null = null;
		#inventory: Entity.InventoryManager | null = null;

		constructor() {
			super("pc");
		}

		protected override get state(): GameState.Player {
			return super.state as GameState.Player;
		}

		/**
		 * Moved this here to use it in a couple of different place.
		 */
		private static _modFormula(roll: number, target: number): number { // TODO free function?
			return Math.clamp(roll / target, 0.1, 2.0);
		}

		/**
		 * Moved this here to use it in a couple of different places.
		 */
		private static _targetFormula(val: number, difficulty: number): number { // TODO free function?
			return (100 - Math.clamp(50 + val - difficulty, 5, 95));
		}

		/**
		 * Sort of a generic skill roll that just uses the base formulas and doesn't inherently
		 * grant xp. Used for arbitrary checks that are derived from meta statistics, etc.
		 */
		static genericRoll(skillVal: number, difficulty: number, amount: number, scaling = false): number {
			scaling = scaling || false;
			const target = Player._targetFormula(skillVal, difficulty);
			const diceRoll = Math.floor(Math.random() * 100);
			const mod = Player._modFormula(diceRoll, target);
			if (World.instance.state.debugMode)
				console.debug(
					`GenericRoll(${skillVal},${difficulty},${amount},${scaling}):  Target=${target}, DiceRoll=${diceRoll}, Mod=${mod}`);
			if (scaling == true) return (amount * mod);
			return diceRoll >= target ? 0 : 1;
		}

		acknowledgeSkillUse(skill: Skills.Any, xpEarned: number): void {
			// Corrupt player for performing sex skill checks.
			switch (skill) {
				case Skills.Sexual.HandJobs:
					this.corruptWillPower(xpEarned, 40);
					break;
				case Skills.Sexual.TitFucking:
					this.corruptWillPower(xpEarned, 50);
					break;
				case Skills.Sexual.BlowJobs:
					this.corruptWillPower(xpEarned, 60);
					break;
				case Skills.Sexual.AssFucking:
					this.corruptWillPower(xpEarned, 70);
					break;
			}
		}

		/**
		 * Performs a skill roll.
		 * @param SkillName - Skill to check.
		 * @param Difficulty - Test difficulty.
		 * @param Scaling - Return value is always the XpMod and never 0.
		 * @returns result of check.
		 */

		// TODO: Is this called from anywhere other than SkillRoll? Should I just simplify and move to that method?
		private _skillRoll(skillName: Skills.Any, difficulty: number, scaling = false): number {
			const target = this.calculateSkillTarget(skillName, difficulty);
			let diceRoll = Math.floor(Math.random() * 100);
			const synergy = this.getSynergyBonus(skillName);

			diceRoll += Math.clamp(synergy, 0, 100); // Cap 100
			diceRoll += this.rollBonus(Stat.Skill, skillName);

			const baseXp = Math.clamp(difficulty - this.getStat(Stat.Skill, skillName), 10, 50);
			const xpMod = Player._modFormula(diceRoll, target);
			const xpEarned = Math.ceil(baseXp * xpMod);

			this.adjustSkillXP(skillName, xpEarned);

			this.acknowledgeSkillUse(skillName, xpEarned);

			if (World.instance.state.debugMode) {
				console.debug("skillRoll(%s,%d): Target = %d, DiceRoll = %d XPMod = %f\n", skillName, difficulty, target, diceRoll, xpMod);
			}

			if (this.state.gameStats.skills[skillName] === undefined) {
				this.state.gameStats.skills[skillName] = {success: 0, failure: 0};
			}

			if (diceRoll >= target) {
				(this.state.gameStats.skills as Required<GameState.SkillUseStats>)[skillName].success += 1;
			} else {
				(this.state.gameStats.skills as Required<GameState.SkillUseStats>)[skillName].failure += 1;
			}

			if (scaling == true) return xpMod;
			if (diceRoll >= target) return 1;
			return 0;
		}

		corruptWillPower(xp: number, difficulty = 50): void {
			xp = Math.abs(xp); // make sure negatives become positives.

			console.debug("corruptWillPower(%d, %d) called", xp, difficulty);

			const perv = this.getStat(Stat.Core, CoreStat.Perversion);
			const mod = 1 - Math.max(0, Math.min(perv / difficulty, 1)); // 0 - 1
			let toWillPowerXP = Math.ceil(xp * mod) * -1.0;
			const toPerversionXP = Math.ceil((xp * mod) / 2);

			if (World.instance.state.difficultySetting === GameState.Difficulty.Landlubber) {
				toWillPowerXP = Math.ceil(Math.abs(toWillPowerXP) * 0.8) * -1.0;
			} else if (World.instance.state.difficultySetting === GameState.Difficulty.PatheticLoser) {
				toWillPowerXP = Math.ceil(Math.abs(toWillPowerXP) * 0.7) * -1.0;
			}

			this.adjustXP(Stat.Core, CoreStat.Willpower, toWillPowerXP, 0, true);
			this.adjustXP(Stat.Core, CoreStat.Perversion, toPerversionXP, 0, true);
		}

		/**
		 *
		 * @param type SKILL or STAT
		 * @param name Attribute to check
		 */
		rollBonus<T extends keyof StatTypeStrMap>(type: T, name: StatTypeStrMap[T]): number {
			let bonus = 0;

			if (this.state.voodooEffects.hasOwnProperty("PIRATES_PROWESS") && type == Stat.Skill) {
				bonus += this.state.voodooEffects["PIRATES_PROWESS"] as number;
			}
			if (World.instance.state.difficultySetting === GameState.Difficulty.Landlubber) bonus += 5;
			if (World.instance.state.difficultySetting === GameState.Difficulty.PatheticLoser) bonus += 10;
			bonus += this.getWornSkillBonus(name);

			return bonus;
		}

		/**
		 * Performs a skill roll with a value amount which is modified when returned. Only works on skills
		 * and will auto generate and apply XP to characters.
		 */
		skillRoll(skillName: Skills.Any, difficulty: number, amount: number, scaling?: boolean): number;
		skillRoll(skillName: Skills.AnyStr, difficulty: number, amount: number, scaling?: boolean): number;
		skillRoll(skillName: Skills.Any, difficulty: number, amount: number, scaling?: boolean): number {
			const mod = this._skillRoll(skillName, difficulty, scaling);
			const ret = Math.ceil(amount * mod);
			if (World.instance.state.debugMode) console.debug(`skillRoll: Mod=${mod},Amount=${amount},Scaling=${scaling},Ret=${ret}`);
			return Math.ceil(amount * mod);
		}

		/**
		 * Like a skill roll, but doesn't grant xp. Can roll against other stats as well.
		 */
		statRoll<T extends keyof StatTypeMap>(type: T, name: StatTypeMap[T], difficulty: number, amount: number, scaling?: boolean): number;
		statRoll<T extends keyof StatTypeStrMap>(type: T, name: StatTypeStrMap[T], difficulty: number, amount: number, scaling?: boolean): number;
		statRoll<T extends keyof StatTypeMap>(type: T, name: StatTypeMap[T], difficulty: number, amount: number, scaling = false): number {
			const target = this.calculateSkillTarget(name, difficulty, type);
			let diceRoll = (Math.floor(Math.random() * 100) + 1);

			if (type === Stat.Skill) {
				diceRoll += Math.clamp(this.getSynergyBonus(name as Skills.Any), 0, 100); // Cap 100
			}
			diceRoll += this.rollBonus(type, name);

			const mod = Player._modFormula(diceRoll, target);

			if (World.instance.state.debugMode) {
				console.debug(`StatRoll(${name},${difficulty}):  Target = ${target}, DiceRoll = ${diceRoll} Mod=${mod}\n`);
			}

			// Kludge because some "skills" are called from here due to not granting XP, such as slot machine skills.

			if (scaling) {
				if (this.state.gameStats.skills.hasOwnProperty(name))
					(this.state.gameStats.skills as Required<GameState.SkillUseStats>)[name as Skills.Any].success += 1;

				return (amount * mod);
			}

			if (diceRoll >= target) {
				if (this.state.gameStats.skills.hasOwnProperty(name))
					(this.state.gameStats.skills as Required<GameState.SkillUseStats>)[name as Skills.Any].success += 1;
				return 1;
			} else {
				if (this.state.gameStats.skills.hasOwnProperty(name))
					(this.state.gameStats.skills as Required<GameState.SkillUseStats>)[name as Skills.Any].failure += 1;
				return 0;
			}
		}

		/**
		 * Calculate the target of the 1-100 dice roll for a skill check. Always a 5% chance of success/failure.
		 */
		calculateSkillTarget<T extends keyof StatTypeMap>(skillName: StatTypeMap[T], difficulty: number, alternate: T): number;
		calculateSkillTarget(skillName: Skills.Any, difficulty: number): number;
		calculateSkillTarget<T extends keyof StatTypeMap>(skillName: StatTypeMap[T], difficulty: number, alternate: T | Stat.Skill = Stat.Skill): number {
			const skillVal = this.getStat(alternate as T, skillName);
			return Player._targetFormula(skillVal, difficulty);
		}

		/**
		 * Checks the SkillSynergy dictionary and adds any skill bonuses to dice rolls on
		 * skill checks. For example, TitFucking gets a bonus for the size of the Players Bust score.
		 */
		getSynergyBonus(skillName: Skills.Any | CoreStat): number{
			if (!Data.Lists.skillSynergy.hasOwnProperty(skillName)) return 0;
			let bonus = 0;
			const synergy = Data.Lists.skillSynergy[skillName] ?? [];
			for (const s of synergy) {
				bonus += Math.ceil(this.getStatPercent(s.type, s.name) * s.bonus);
			}
			return bonus;
		}

		/**
		 * Restyle hair.
		 */
		reStyle(): void {
			if (this.canReStyle() == false) return;
			const lm = this.state.lastUsedMakeup;
			const makeup = Data.Lists.makeupStyles.filter(item => item.short == lm)[0].name;
			const lh = this.state.lastUsedHair;
			const hair = Data.Lists.hairStyles.filter(item => item.short == lh)[0].name;

			const wig = this.getEquipmentInSlot(ClothingSlot.Wig);
			this.doStyling(wig?.id ?? hair, makeup);
			this.adjustStat(CoreStat.Energy, -1);
		}

		/**
		 * Simple routine to check if the player can reapply their style.
		 */
		canReStyle(): boolean {
			if (this.state.core.value[CoreStat.Energy] < 1) return false;
			if (this.state.lastUsedMakeup == this.state.makeup.style) return false;
			const m1 = this.inventory.charges(Items.Category.Cosmetics, 'basic makeup');
			const m2 = this.inventory.charges(Items.Category.Cosmetics, 'expensive makeup');
			const h1 = this.inventory.charges(Items.Category.Cosmetics, 'hair accessories');
			const h2 = this.inventory.charges(Items.Category.Cosmetics, 'hair products');
			const lm = this.state.lastUsedMakeup;
			const makeup = Data.Lists.makeupStyles.filter(item => item.short == lm)[0];
			if ((m1 < makeup.resource1) || (m2 < makeup.resource2)) return false;

			if (this.getEquipmentInSlot(ClothingSlot.Wig) !== null) return true;
			const lh = this.state.lastUsedHair;
			const hair = Data.Lists.hairStyles.filter(item => item.short == lh)[0];
			return ((h1 >= hair.resource1) && (h2 >= hair.resource2));
		}

		/** TODO: THIS ENTIRE AREA IS GARBAGE. REFACTOR IT AND REDO MAKEUP AND HAIRSTYLE STUFF **/

		/**
		 * Style hair and makeup.
		 * @param hairID
		 * @param makeupID
		 */
		doStyling(hairID: string, makeupID: string): void {
			const obj = this.getItemById(hairID as Data.ItemNameTemplateEquipment);
			const wig = this.getEquipmentInSlot(ClothingSlot.Wig);
			if (typeof obj !== 'undefined') { // We passed an Item Id and found an item.
				if ((wig === null) || (wig.id != hairID))
					this.wear(this.wardrobeItem(hairID));
			} else {
				if (wig != null) this.remove(wig);

				const hair = Data.Lists.hairStyles.filter(item => item.name == hairID)[0];

				if (this.getItemCharges("hair tool") >= hair.resource1 && this.getItemCharges("hair treatment") >= hair.resource2) {
					this.state.hair.style = hair.short;
					this.state.hair.bonus = this.skillRoll(Skills.Charisma.Styling, hair.difficulty, hair.style, true);
					this.state.lastUsedHair = hair.short;
					this.useItemCharges("hair tool", hair.resource1);
					this.useItemCharges("hair treatment", hair.resource2);
				}
			}

			const makeup = Data.Lists.makeupStyles.filter(item => item.name == makeupID)[0];

			if (this.getItemCharges("basic makeup") >= makeup.resource1 && this.getItemCharges("expensive makeup") >= makeup.resource2) {
				this.state.makeup.style = makeup.short;
				this.state.makeup.bonus = this.skillRoll(Skills.Charisma.Styling, makeup.difficulty, makeup.style, true);
				this.state.lastUsedMakeup = makeup.short;
				this.useItemCharges("basic makeup", makeup.resource1);
				this.useItemCharges("expensive makeup", makeup.resource2);
			}

			setup.avatar.drawPortrait();
		}

		/**
		 * Calculates the Players "Beauty" based on other statistics.
		 */
		get beauty(): number {
			const cBeauty = Math.round(
				(this.getStat(Stat.Body, BodyPart.Face) * 0.4) +
				(this.figure * 0.4) +
				(this.getStat(Stat.Core, CoreStat.Fitness) * 0.3));
			return Math.clamp(cBeauty, 0, 100);
		}

		/**
		 * Fetish rating is derived from the enlarged (or minimized) size of the Players body parts.
		 * Generally however, bigger (what) means more points. Used to calculate whoring pay and some
		 * other activities.
		 */
		get fetish(): number {
			let score = 0;
			// 5 - 15 for boobs and ass each   (30 pts)
			if (this.getStatPercent(Stat.Body, BodyPart.Bust) >= 30) {
				score += Math.round((5 + (this.getStatPercent(Stat.Body, BodyPart.Bust) / 10)));
			}
			if (this.getStatPercent(Stat.Body, BodyPart.Ass) >= 30) {
				score += Math.round((5 + (this.getStatPercent(Stat.Body, BodyPart.Ass) / 10)));
			}
			// 10 pts for extra firm or extra sagging boobs
			if (this.getStatPercent(Stat.Body, BodyProperty.BustFirmness) > 90) {
				score += this.getStatPercent(Stat.Body, BodyProperty.BustFirmness) - 90;
			}
			if (this.getStatPercent(Stat.Body, BodyProperty.BustFirmness) < 10) {
				score += 10 - this.getStatPercent(Stat.Body, BodyProperty.BustFirmness);
			}

			// up to 10 each for lips and waist and hips (35 pts)
			if (this.getStatPercent(Stat.Body, BodyPart.Lips) >= 80) {
				score += Math.round((this.getStatPercent(Stat.Body, BodyPart.Lips) / 10));
			}
			if (this.getStatPercent(Stat.Body, BodyPart.Hips) >= 80) {
				score += Math.round((this.getStatPercent(Stat.Body, BodyPart.Hips) / 10));
			}
			if (this.getStatPercent(Stat.Body, BodyPart.Waist) <= 30) score += 5;
			if (this.getStatPercent(Stat.Body, BodyPart.Waist) <= 15) score += 5;
			if (this.getStatPercent(Stat.Body, BodyPart.Waist) <= 1) score += 5;

			// Penis and Balls, 10 each. (10 pts)
			if ((this.getStatPercent(Stat.Body, BodyPart.Penis) <= 10) && (this.getStatPercent(Stat.Body, BodyPart.Balls) <= 10)) {
				score += 10; // tiny genitals!
			}
			if ((this.getStatPercent(Stat.Body, BodyPart.Penis) >= 90) && (this.getStatPercent(Stat.Body, BodyPart.Balls) >= 90)) {
				score += 10; // big genitals
			}

			return Math.clamp(Math.round((score / 75) * 100), 0, 100);
		}

		/**
		 * Derived statistic that reports "style" made up out of hair, makeup and clothing.
		 */
		get style(): number {
			const cStyle = Math.round((this.hairRating * 0.25) +
				(this.makeupRating * 0.25) + (this.clothesRating * 0.5));
			return Math.clamp(cStyle, 0, 100);
		}

		/**
		 * shim function that returns the "hair rating" of the player, but checks first if they are wearing
		 * a wig and then reports that number instead.
		 */
		get hairRating(): number {
			return Math.clamp(this.getEquipmentInSlot(ClothingSlot.Wig)?.hairBonus ?? this.state.hair.bonus, 0, 100);
		}

		getHairStyle(): string {
			return this.getEquipmentInSlot(ClothingSlot.Wig)?.hairStyle ?? this.state.hair.style;
		}

		getHairColor(): Data.HairColor {
			return this.getEquipmentInSlot(ClothingSlot.Wig)?.hairColor ?? this.state.hair.color;
		}

		/**
		 * The makeup rating of the player.
		 */
		get makeupRating(): number {
			return Math.clamp(this.state.makeup.bonus, 0, 100);
		}

		/**
		 * Derived statistic (face + makeup). Bonus payout for hand jobs if you have a good face.
		 */
		get faceRating(): number {
			return Math.ceil(Math.max(0, Math.min(((this.makeupRating + this.getStat(Stat.Body, BodyPart.Face)) / 2), 100)));
		}

		/**
		 * Iterates through players worn items and sums .Style property.
		 */
		get clothesRating(): number {
			const cStyle = Object.values(this.clothing.equipment).reduce((prev, cur) => {
				return cur ? prev + cur.styleBonus : prev;
			}, 0);
			return Math.clamp(Math.round(((cStyle / 100) * 100)), 1, 100); // 1 - 100 rating
		}

		getStyleSpecRating(spec: Data.Fashion.Style): number {
			return Object.values(this.clothing.equipment).reduce((prev, cur) => {
				return cur ? prev + cur.categoryBonus(spec) : prev;
			}, 0);
		}

		/**
		 * Derived statistic, lends itself to Beauty. WaistRating, BustRating, HipsRating and AssRating contribute.
		 */
		get figure(): number {
			const tFig = Math.round((this.waistRating + this.bustRating +
				this.hipsRating + this.assRating) / 4);
			return Math.clamp(tFig, 1, 100); // Normalize between 1 - 100
		}

		/**
		 * Calculates "golden ratio" for waist @ Player's height and then returns a score relative their current waist.
		 */
		get waistRating(): number {
			const goldenWaist = Math.round((PR.statToCm(this, "height") * 0.375)); // 54cm to 78cm
			return Math.round(((goldenWaist / PR.waistInCM(this)) / 1.8) * 100);
		}

		/**
		 * Calculates "golden ratio" for bust @ Player's height and then returns a score relative their current bust.
		 */
		get bustRating(): number {
			const goldenBust = (Math.round((PR.statToCm(this, "height") * 0.375)) * 1.5);
			return Math.round(((PR.busInCM(this) / goldenBust) / 1.6) * 100);
		}

		/**
		 * Calculates "golden ratio" for hips @ Player's height and then returns a score relative their current bust.
		 */
		get hipsRating(): number {
			const goldenHips = (Math.round((PR.statToCm(this, "height") * 0.375)) * 1.5);
			return Math.round(((PR.hipsInCM(this) / goldenHips) / 1.6) * 100);
		}

		/**
		 * Combination of Ass + Hips.
		 */
		get assRating(): number {
			return Math.round((this.getStatPercent(Stat.Body, BodyPart.Ass) + this.getStatPercent(Stat.Body, BodyPart.Hips)) / 2);
		}

		/**
		 * For now just the percentage of the lips 1-100.
		 */
		get lipsRating(): number {
			return this.getStatPercent(Stat.Body, BodyPart.Lips);
		}

		/**
		 * Returns the config settings for a statistic type.
		 */
		getStatConfig<T extends keyof Data.StatConfigMap>(type: T): Record<StatTypeMap[T], Data.StatConfigMap[T]>;
		getStatConfig<T extends keyof Data.StatConfigStrMap>(type: T): Record<StatTypeStrMap[T], Data.StatConfigStrMap[T]>;
		getStatConfig(type: Stat): any {
			return PR.getStatConfig(type);
		}

		/**
		 * Returns object that holds stat values
		 */
		getStatObject<T extends keyof StatTypeObjectMap>(type: T): StatTypeObjectMap[T] {
			if (type == Stat.Core) return this.state.core.value as StatTypeObjectMap[T];
			if (type == Stat.Skill) return this.state.skill.value as StatTypeObjectMap[T];
			if (type == Stat.Body) return this.state.body.value as StatTypeObjectMap[T];
			throw "WTF TS?!"
		}

		/**
		 * Returns object that holds stat XP values
		 */
		getStatXPObject<T extends keyof StatTypeObjectMap>(type: T): StatTypeObjectMap[T] {
			if (type == Stat.Core) return this.state.core.xp as StatTypeObjectMap[T];
			if (type == Stat.Skill) return this.state.skill.xp as StatTypeObjectMap[T];
			if (type == Stat.Body) return this.state.body.xp as StatTypeObjectMap[T];
			throw "WTF TS?!"
		}

		/**
		 * Return a statistic value (raw)
		 */
		getStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number;
		getStat<T extends keyof StatTypeObjectMap>(type: T, statName: StatTypeObjectMap[T]): number;
		getStat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T]): number;
		getStat<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): StatTypeObjectMap[T][S] | number {
			const statObj = this.getStatObject(type);
			const res = statObj[statName];
			return res ?? 0;
		}

		/**
		 * Returns the current XP of a statistic.
		 */
		getStatXP<T extends keyof StatTypeMap, S extends StatTypeMap[T]>(type: T, statName: S): number;
		getStatXP<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): number;
		getStatXP<T extends keyof StatTypeStrMap, S extends StatTypeStrMap[T]>(type: T, statName: S): number;
		getStatXP<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S): StatTypeObjectMap[T][S] | number {
			const xpObj = this.getStatXPObject(type);
			const res = xpObj[statName];
			return res ?? 0;
		}

		getMaxStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number {
			return this.getStatConfig(type)[statName].max;
		}

		getMinStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): number {
			return this.getStatConfig(type)[statName].min;
		}

		getStatPercent<T extends StatTypeStr>(type: T, statName: StatTypeStrMap[T], statValue?: number): number;
		getStatPercent<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], statValue?: number): number {
			const minStatValue = this.getMinStat(type, statName);
			const value = (statValue === undefined ? this.getStat(type, statName) : statValue);
			return Math.floor(((value - minStatValue) / (this.getMaxStat(type, statName) - minStatValue)) * 100);
		}

		/**
		 * Set raw statistic value
		 */
		setStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], value: number): void;
		setStat<T extends keyof StatTypeObjectMap>(type: T, statName: StatTypeObjectMap[T], value: number): void;
		setStat<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], value: number): void;
		setStat<T extends keyof StatTypeObjectMap, S extends keyof StatTypeObjectMap[T]>(type: T, statName: S, value: StatTypeObjectMap[T][S]): void {
			this.getStatObject(type)[statName] = value;
		}

		/**
		 * Returns XP cost for increasing stat level by one to @see targetScore
		 * @param type Stat name (e.g. Stat.Body)
		 * @param statName Stat name (e.g. "lips")
		 * @param targetScore Target level (e.g. 13)
		 * @returns XP cost
		 */
		getLeveling<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], targetScore: number): {cost: number, step: number};
		getLeveling<T extends keyof Data.StatConfigMap>(type: T, statName: StatTypeMap[T], targetScore: number): {cost: number, step: number} {
			const statCfg = this.getStatConfig(type)[statName];
			const levels = statCfg.levelingCost ?? statCfg.leveling;
			// var Percent = Math.round(( (( TargetScore - this.getMinStat(Type, StatName)) / ( this.getMaxStat(Type, StatName) - this.getMinStat(Type,StatName))) * 100));
			const percent = this.getStatPercent(type, statName, this.getStat(type, statName) + targetScore);
			if (levels instanceof Function) {
				return {cost: levels(percent) - levels(percent - 1), step: 1};
			}
			let level = {cost: 100, step: 1};

			for (const [prop, val] of Object.entries(levels)) {
				if ((val === 'fixed') || (val === 'none') || (percent <= parseInt(prop))) {
					level = {
						cost: val.cost,
						step: ((this.getMaxStat(type, statName) / 100) * val.step)
					};
					break;
				}
			}

			return level;
		}

		getCapStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): number {
			return Math.round((Math.max(this.getMinStat(type, statName), Math.min(amount, this.getMaxStat(type, statName)))) * 100) / 100;
		}

		adjustStat(statName: CoreStat, amount: number): void;
		adjustStat(statName: CoreStatStr, amount: number): void;
		adjustStat(statName: CoreStat, amount: number): void {
			if (World.instance.debugMode) console.log(`AdjustStat: name=${statName}, amount=${amount}`);
			this.state.core.value[statName] = this.getCapStat(Stat.Core, statName, (this.getStat(Stat.Core, statName) + amount));
		}

		adjustBody(statName: BodyStat, amount: number): void;
		adjustBody(statName: BodyStatStr, amount: number): void;
		adjustBody(statName: BodyStat, amount: number): void {
			if (World.instance.debugMode) console.log(`AdjustBody: name=${statName}, amount=${amount}`);
			this.state.body.value[statName] = this.getCapStat(Stat.Body, statName, (this.getStat(Stat.Body, statName) + amount));
		}

		adjustSkill(statName: Skills.Any, amount: number): void;
		adjustSkill(statName: Skills.AnyStr, amount: number): void;
		adjustSkill(statName: Skills.Any, amount: number): void {
			if (World.instance.debugMode) console.log(`AdjustSkill: name=${statName}, amount=${amount}`);
			this.state.skill.value[statName] = this.getCapStat(Stat.Skill, statName, (this.getStat(Stat.Skill, statName) + amount));
		}

		// Used to directly set XP to a specific amount.
		setXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): void;
		setXP<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], amount: number): void;
		setXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number): void {
			switch (type) {
				case Stat.Core:
					this.state.core.xp[statName as CoreStat] = amount;
					break;
				case Stat.Skill:
					this.state.skill.xp[statName as Skills.Any] = amount;
					break;
				case Stat.Body:
					this.state.body.xp[statName as BodyStat] = amount;
					break;
			}
			if (World.instance.debugMode)
				console.debug("SetXP: set to %d", amount);
		}

		adjustXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number, limiter?: number, noCap?: boolean): void;
		adjustXP<T extends keyof StatTypeStrMap>(type: T, statName: StatTypeStrMap[T], amount: number, limiter?: number, moCap?: boolean): void;
		adjustXP<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T], amount: number, limiter = 0, noCap = false): void {
			amount = Math.ceil(amount); // No floats.
			if (World.instance.debugMode)
				console.debug(`AdjustXP: type=${type},stat=${statName},amount=${amount},limit=${limiter},noCap=${noCap}`);

			if ((amount > 0) && (this.getStat(type, statName) >= limiter) && (limiter != 0)) return;
			if ((amount < 0) && (this.getStat(type, statName) <= limiter) && (limiter != 0)) return;
			if ((amount > 0) && (this.getStat(type, statName) >= this.getMaxStat(type, statName))) return;
			if ((amount < 0) && (this.getStat(type, statName) <= this.getMinStat(type, statName))) return;

			if (noCap == false && Math.abs(this.getStatXP(type, statName)) >= 1000) {
				amount = Math.ceil(amount / 10);
			} else if (noCap == false && Math.abs(this.getStatXP(type, statName)) >= 500) {
				amount = Math.ceil(amount / 4);
			} else if (noCap == false && Math.abs(this.getStatXP(type, statName)) >= 250) {
				amount = Math.ceil(amount / 2);
			}

			if (type === Stat.Core) {
				this.state.core.xp[statName as CoreStat] += amount;
			} else if (type === Stat.Skill) {
				this.state.skill.xp[statName as Skills.Any] += amount;
			} else if (type === Stat.Body) {
				this.state.body.xp[statName as BodyStat] += amount;
			}
			if (World.instance.debugMode)
				console.debug(`AdjustXP: Adjusted by ${amount}`);
		}

		adjustStatXP(statName: CoreStatStr, amount: number, limiter = 0): void {
			return this.adjustXP(Stat.Core, statName, amount, limiter);
		}

		adjustBodyXP(statName: BodyStatStr, amount: number, limiter = 0): void {
			return this.adjustXP(Stat.Body, statName, amount, limiter);
		}

		adjustSkillXP(statName: Skills.Any, amount: number, limiter = 0): void {
			return this.adjustXP(Stat.Skill, statName, amount, limiter);
		}

		levelStat<T extends keyof StatTypeMap>(type: T, statName: StatTypeMap[T]): void {
			const targetScore = this.getStatXP(type, statName) < 0 ? -1 : 1;
			const leveling = this.getLeveling(type, statName, targetScore);
			if ((Math.abs(this.getStatXP(type, statName))) < leveling.cost) return;

			if (targetScore < 0 && this.getStat(type, statName) == this.getMinStat(type, statName)) return;
			if (targetScore > 0 && this.getStat(type, statName) == this.getMaxStat(type, statName)) return;

			console.log(`Leveling Stat:${statName}(${this.getStat(type, statName)})`);

			const cost = (targetScore < 0) ? leveling.cost : (leveling.cost * -1.0);
			const step = (targetScore < 0) ? (leveling.step * -1.0) : leveling.step;

			switch (type) {
				case Stat.Skill:
					const statTitle = PR.pSkillName(statName as Skills.Any);
					this.adjustSkill(statName as Skills.Any, step);
					const adjective = step < 0 ? " skill decreases! How did this happen!?" : " skill improves!";
					setup.notifications.addMessage(Notifications.MessageCategory.Knowledge, setup.world.day + 1,
						"Your " + PR.colorizeString(this.getStatPercent(Stat.Skill, statName as Skills.Any), statTitle) + adjective);
					break;

				case Stat.Core:
					this.adjustStat(statName as CoreStat, step);
					Player.notifyBodyChange(statName as CoreStatStr, step);
					break;

				case Stat.Body:
					this.adjustBody(statName as BodyStat, step);
					Player.notifyBodyChange(statName as BodyStatStr, step);
					break;
			}

			this.adjustXP(type, statName, cost, 0, true);
		}

		levelStatGroup(type: Stat): void {
			let keys: string[] = [];
			if (type == Stat.Core) keys = Object.keys(this.state.core.value);
			if (type == Stat.Skill) keys = Object.keys(this.state.skill.value);
			if (type == Stat.Body) keys = Object.keys(this.state.body.value);

			for (const k of keys) this.levelStat(type, k as StatTypeMap[typeof type]);
		}

		earnMoney(m: number, cat: GameState.AnyIncome): void {
			const mi = Math.ceil(m);
			this.state.gameStats.moneyEarned[cat] += mi;
			this.state.money = Math.max(0, (this.state.money + mi));
		}

		spendMoney(m: number, cat: GameState.CommercialActivity): void;
		spendMoney(m: number, cat: GameState.SpendingTarget.Shopping, subcat: Items.Category): void;
		spendMoney(m: number, cat: GameState.AnySpending, subCat?: Items.Category): void {
			const mi = Math.ceil(m);
			if (cat === GameState.SpendingTarget.Shopping) {
				this.state.gameStats.moneySpendings.shopping[subCat!] += mi;
			} else {
				this.state.gameStats.moneySpendings[cat] += mi;
			}
			this.state.money = Math.max(0, (this.state.money - mi));
		}

		adjustTokens(m: number): void {
			const mi = Math.ceil(m);
			console.debug(`AdjustTokens: ${mi}`);
			if (mi > 0) {this.state.gameStats.tokensEarned += mi;}
			console.debug(`state.Tokens=${this.state.tokens}`);
			this.state.tokens = Math.max(0, (this.state.tokens + mi));
			console.debug(`state.Tokens=${this.state.tokens}`);
		}

		randomAdjustBodyXP(amount: number): void {
			this.adjustBodyXP(Object.keys(this.state.body.xp).randomElement(), amount, 0);
		}

		get bodyEffects(): string[] {
			return this.state.bodyEffects ?
				Data.naturalBodyEffects.concat(this.state.bodyEffects) :
				Data.naturalBodyEffects;
		}

		/**
		 * Add named effect to the list of active body effects
		 */
		addBodyEffect(effectName: string): void {
			if (this.state.bodyEffects.indexOf(effectName) === -1) {
				this.state.bodyEffects.push(effectName);
			}
		}

		/**
		 * Removes named effect from the list of active body effects
		 */
		removeBodyEffect(effectName: string): void {
			if (this.state.bodyEffects.indexOf(effectName) !== -1) {
				this.state.bodyEffects = this.state.bodyEffects.filter(s => s !== effectName);
			}
		}

		// Resting and Sleeping functions.
		nextDay(): void {
			// Gain 'Knowledge' about worn clothes, log days worn.
			// Apply passive effects on worn items.
			for (const eqItem of Object.values(this.clothing.equipment)) {
				if (!(eqItem instanceof Items.Clothing)) continue;

				if (Math.random() > 0.8)
					this.addHistory('clothingKnowledge', eqItem.name, 1); // tracking effect knowledge
				this.addHistory("daysWorn", eqItem.name, 1); // tracking just days worn
				eqItem.applyEffects(this);
				const logMsg = eqItem.learnKnowledge(this);
				if ((typeof logMsg != 'undefined') && logMsg != "") {
					setup.notifications.addMessage(Notifications.MessageCategory.Knowledge, setup.world.day + 1, logMsg);
				}
			}

			// Tarot card effects and dream.
			const tarotCard = this.jobFlags.TAROT_CARD as string;
			if (tarotCard) {
				const tarot = Data.tarot[tarotCard];
				this.applyEffects(tarot.effects);
				setup.notifications.addMessage(Notifications.MessageCategory.Dreams, setup.world.day + 1, tarot.msg);
				delete this.jobFlags.TAROT_CARD;
			}

			this.applyEffects(this.bodyEffects);

			this.levelStatGroup(Stat.Core);
			this.levelStatGroup(Stat.Body);
			this.levelStatGroup(Stat.Skill);

			this.state.hair.bonus = 0;
			this.state.makeup.bonus = 0;
			this.state.hair.style = "bed head";
			this.state.makeup.style = "plain faced";

			// Decrease voodoo effects
			this.endHexDuration();

			// What day are we on our current voyage.
			this.state.sailDays = ((this.state.sailDays + 1) >= Data.Lists.shipRoute.length) ? 0 : (this.state.sailDays + 1);
		} // NextDay

		/**
		 * Move time counter to next phase of day.
		 * @param phases - Number of phases to increment.
		 */
		nextPhase(): void {
			this.adjustStat(CoreStat.Nutrition, -5);
			this.levelStat(Stat.Core, CoreStat.Nutrition);
		}

		rest(): void {
			setup.world.nextPhase(1);
			this.applyEffects(["NATURAL_RESTING"]);
			this.levelStatGroup(Stat.Skill);
		}

		getShipLocation(): {x: number; y: number; passage: string; title: string;} {
			const routes = Data.Lists.shipRoute;
			if (this.state.sailDays >= routes.length) {
				this.state.sailDays = 0; // Shouldn't happen, but fix it if it does.
			}

			const dict = {
				x: routes[this.state.sailDays].left,
				y: routes[this.state.sailDays].top,
				passage: "",
				title: ""
			};

			const titles: {[x: string]: {passage: string, title: string}} = {
				/* eslint-disable @typescript-eslint/naming-convention */
				IslaHarbor: {passage: "IslaHarbor", title: "Isla Harbor"},
				GoldenIsle: {passage: "GoldenIsle", title: "Golden Isle"},
				Abamond: {passage: "Abamond", title: "Abamond"},
				PortRoyale: {passage: "PortRoyale", title: "Port Royale"},
				/* eslint-enable @typescript-eslint/naming-convention */
			};

			const pos = routes[this.state.sailDays].P;
			if (titles.hasOwnProperty(pos)) {
				dict.passage = titles[pos].passage;
				dict.title = titles[pos].title;
			}

			return dict;
		}

		/**
		 * @param n Number of days to look ahead.
		 * @returns
		 */
		isInPort(n = 0): boolean {
			const routes = Data.Lists.shipRoute;
			const days = (this.state.sailDays + n >= routes.length ? 0 : this.state.sailDays + n);
			return (routes[days].P != "AtSea");
		}

		/**
		 *
		 * @param n Days to advance
		 */
		advanceSailDays(n = 1): boolean {
			if (this.isInPort(0) == true || this.isInPort(n) == true) return false;
			const routes = Data.Lists.shipRoute;
			this.state.sailDays = (this.state.sailDays >= routes.length ? n : this.state.sailDays + n);
			return true;
		}

		// Equipment and Inventory Related Functions

		/**
		 * Does the character own the item in question
		 * @param name
		 */
		ownsWardrobeItem(name: string): boolean {
			if (this.clothing.wardrobe.filter(o => o.name == name).length > 0) return true;
			const slot = Data.clothes[name].slot;
			const equipmentInSlot = this.clothing.equipment[slot];
			if (equipmentInSlot == null || !(equipmentInSlot instanceof Items.Clothing)) return false;
			return equipmentInSlot.name == name;
		}

		maxItemCapacity(itemDict: {tag: string}): boolean {
			const o = this.getItemByName(itemDict.tag);

			if (o != undefined) {
				if (o.charges >= 100) return true;
			}
			return false;
		}

		wardrobeItem(id: string): Items.Clothing {
			return this.clothing.wardrobe.filter(o => o.id == id)[0];
		}

		wardrobeItemsBySlot(slot: ClothingSlot): Items.Clothing[] {
			const res = this.clothing.wardrobe.filter(item => item.slot == slot);
			res.sort((a, b) => a.name.localeCompare(b.name));
			return res;
		}

		/* currently unused
		PrintEquipment(Slot: ClothingSlot): string {
			const eq = this.Clothing.Equipment[Slot];
			if (!eq) return "@@.state-disabled;Nothing@@";
			return eq.Description;
		}
		*/

		getEquipmentInSlot(slot: ClothingSlot): Items.Clothing | null {
			const eq = this.clothing.equipment[slot];
			return eq ? eq : null;
		}

		/**
		 * Search equipped items
		 * @param Name
		 * @param SlotFlag Search by slot instead of item name.
		 */
		isEquipped(slot: ClothingSlot | ClothingSlot[], slotFlag: true): boolean;
		isEquipped(name: string | string[], slotFlag?: false): boolean;
		isEquipped(nameOrSlot: string | string[], slotFlag = false): boolean {
			const isEquipped = (name: string, flag: boolean): boolean => {
				if (flag) {
					return this.clothing.equipment[nameOrSlot as ClothingSlot] != null;
				}

				for (const itm of Object.values(this.clothing.equipment)) {
					if (itm && (itm.name === name || itm.id === name)) return true;
				}
				return false;
			};

			if (Array.isArray(nameOrSlot)) {
				return nameOrSlot.some(n => isEquipped(n, slotFlag));
			}

			return isEquipped(nameOrSlot, slotFlag);
		}

		isItemEquipped(item: Items.Clothing): boolean {
			return this.equipment[item.slot]?.id === item.id;
		}

		wear(item: Items.Clothing | string, lock = false): void {
			if (typeof item === "string") {
				this.clothing.wear(item, lock);
			} else {
				this.clothing.wear(item.id, lock);
			}
			setup.avatar.drawPortrait();
		}

		autoWearStyle(style: Data.Fashion.Style): void {
			for (const [slot, currentlyWorn] of Object.entries(this.clothing.equipment)) {
				if (!this.clothing.equipment.hasOwnProperty(slot)) continue;

				if (currentlyWorn?.isLocked) continue;

				// Get all matching items by Category and Slot.
				const matchingItems = $.grep(this.clothing.wardrobe,
					c => c.slot == slot && $.inArray(style, c.style) >= 0
				);

				if (matchingItems.length < 1) continue; // Nothing matching

				// Sorting by style descending
				matchingItems.sort((a, b) => b.styleBonus - a.styleBonus);

				const wear = currentlyWorn == null // Nothing being worn, so wear it.
					|| $.inArray(style, currentlyWorn.style) < 0 // Item in slot is not of the right category, so swap them.
					|| currentlyWorn.style < matchingItems[0].style; // Item in slot has less style, so swap them.

				// if (wear) this.Wear(matchingItems[0]);

				if (wear) this.clothing.wear(matchingItems[0].id);
			}
			setup.avatar.drawPortrait();
		}

		strip(): void {
			for (const item of Object.values(this.clothing.equipment)) {
				if (item && !item.isLocked) {
					this.clothing.takeOff(item.id);
				}
			}
			setup.avatar.drawPortrait();
		}

		/**
		 * Tests is any of the named clothes worn in the given slot
		 * @returns True is any of the items is worn, false otherwise
		 */
		isAnyClothingWorn(tags: string[], slot: ClothingSlot): boolean {
			return tags.some(t => this.clothing.isWorn(Items.makeId(Items.Category.Clothes, t), slot));
		}

		remove(item: Items.Clothing): void {
			if (!item) return;
			this.clothing.takeOff(item.id);
			setup.avatar.drawPortrait();
		}

		/** @deprecated */
		getItemByName(name: string): Items.InventoryItem {
			return this.inventory.filter(o => o.tag == name)[0];
		}

		getItemById(id: Data.ItemNameTemplateConsumable): Items.Consumable | undefined;
		getItemById(id: Data.ItemNameTemplateAny): Items.AnyItem | undefined;
		getItemById(id: Data.ItemNameTemplateAny): Items.AnyItem | undefined {
			let result: Items.AnyItem | undefined | null = undefined;

			let itemList: Items.AnyItem[] = this.inventory.filter(o => o.id == id); // Look in items first.

			if (itemList.length < 1) { // Now check wardrobe
				itemList = this.clothing.wardrobe.filter(o => o.id == id);
			}

			if (itemList.length < 1) { // Check worn stuff.
				for (const itm of Object.values(this.clothing.equipment)) {
					if (itm && itm.id == id) {
						result = itm;
						break;
					}
				}
			}

			if (!result) result = undefined;
			if (!result && itemList.length > 0) result = itemList[0];
			return result;
		}

		getItemByTypes(types: string[], sort = false): Items.InventoryItem[] {
			const res = this.inventory.filter(o => types.contains(o.type));

			if (!sort) return res;

			res.sort((a, b) => {
				const af = this.inventory.isFavorite(a.id);
				if (af != this.inventory.isFavorite(b.id)) {return af ? -1 : 1;}
				return a.name.localeCompare(b.name);
			});
			return res;
		}

		/**
		 * Returns the total number of charges across all items belonging to a certain type.
		 */
		getItemCharges(type: string): number {
			return this.inventory
				.filter(o => o.type == type)
				.reduce((acc, cur) => {return acc + cur.charges}, 0);
		}

		/**
		 * Returns number of charges for item with the given name and type (if provided) or among all types otherwise.
		 * @param name Item name
		 * @param Item type (category) name
		 * @returns Number of charges available or zero.
		 */
		itemCharges(name: string, type?: Items.CategoryInventory): number {
			return this.inventory.charges(type, name);
		}

		/**
		 * Attempt to iterate through all items of same type and consume charges from them until Amount has been
		 * satisfied. It will delete items if it consumes all their charges.
		 */
		useItemCharges(type: string, amount: number): void {
			if (amount <= 0) return;
			const items = this.inventory.filter(o => o.type == type);
			let i = 0;

			while (amount > 0 && i < items.length) {
				const item = items[i];
				if (item instanceof Items.Consumable) {
					const usableCharges = Math.min(amount, item.charges);
					if (usableCharges > 0) {
						item.removeCharges(usableCharges);
						amount -= usableCharges;
						i++;
					}
				}
			}
		}

		deleteItem(item: Items.InventoryItem): void {
			this.inventory.removeItem(item.id);
		}

		/**
		 * Create and add an item to the player.
		 */
		addItem(category: Items.CategoryEquipment, name: string, unused: undefined, opt?: "wear"): Items.Clothing | null;
		addItem<T extends keyof Items.InventoryItemTypeMap>(category: T, name: string, count?: number): Items.InventoryItemTypeMap[T];
		addItem<T extends keyof Items.ItemTypeMap>(category: T, name: string, count?: number, opt?: "wear"): Items.ItemTypeMap[T] | null;
		addItem<T extends keyof Items.ItemTypeMap>(category: T, name: string, count?: number, opt?: "wear"): Items.ItemTypeMap[T] | null {
			if (category === Items.Category.Clothes || category === Items.Category.Weapon) {
				if (this.ownsWardrobeItem(name)) return null; // No duplicate equipment allowed.
				return this.clothing.addItem(name, opt == "wear") as Items.ItemTypeMap[T];
			} else {
				return this.inventory.addItem(category, name, count ?? 1) as Items.ItemTypeMap[T];
			}
		}

		getWornSkillBonus(skill: Data.WearSkill): number {
			let bonus = 0;
			for (const eq of Object.values(this.clothing.equipment)) {
				if (!eq) {continue;}
				const tBonus = eq.getBonus(skill);
				if (tBonus > 0) {
					bonus += tBonus;
					if (World.instance.debugMode == true) console.log("Found skill bonus : " + skill + " on" + eq.name);
				}
			}
			return bonus;
		}

		/**
		 * Find item and reduce charges. Delete from inventory if out of charges.
		 * @param itemId
		 * @param charges to consume
		 * @returns The item object
		 */
		takeItem(itemId: Data.ItemNameTemplateAny, charges = 1): Items.AnyItem | undefined {
			const o = this.getItemById(itemId);
			if (o instanceof Items.Clothing || o instanceof Items.Reel) {
				return o;
			}
			if (o) {
				o.removeCharges(charges); // will remove the item from inventory if charges reaches 0
			}
			return o;
		}

		/**
		 * Use an item. Apply effects. Delete from inventory if out of charges.
		 */
		useItem(itemId: Data.ItemNameTemplateConsumable): string {
			const o = this.takeItem(itemId);
			if (o instanceof Items.Consumable) {
				this.addHistory("items", o.tag, 1);
				o.applyEffects(this);
				return o.message(this);
			}
			return "";
		}

		/**
		 * Applies named effects on the player
		 */
		applyEffects(effectNames: string[]): void {
			for (const eName of effectNames) {
				console.group(`Effect ${eName}`);
				Data.effectLib[eName].fun(this);
				console.groupEnd();
			}
		}

		static notifyBodyChange(bodyPart: BodyStatStr | CoreStatStr, step: number): void {
			const c = Data.Lists.bodyChanges[bodyPart];
			if (c) {
				setup.notifications.addMessage(Notifications.MessageCategory.StatChange, setup.world.day + 1, (step < 0 ? c.shrink : c.grow));
			}
		}

		getHistory(type: string, flag: string): number {
			if ((typeof this.state.history[type] === 'undefined')) return 0;
			if ((typeof this.state.history[type][flag] === 'undefined')) return 0;

			return this.state.history[type][flag];
		}

		addHistory(type: string, flag: string, amount: number): void {
			if ((typeof this.state.history[type] === 'undefined')) this.state.history[type] = {};
			if ((typeof this.state.history[type][flag] === 'undefined')) this.state.history[type][flag] = 0;

			const t = this.getHistory(type, flag);
			this.state.history[type][flag] = (t + amount);
		}

		removeHistory(type: string, flag: string): void {
			if ((typeof this.state.history[type][flag] !== 'undefined')) delete this.state.history[type][flag];
		}

		// Voodoo
		hasHex(hex: string): boolean {
			return this.state.voodooEffects.hasOwnProperty(hex);
		}

		setHex(hex: string, value: string | number): void {
			this.state.voodooEffects[hex] = value;
		}

		removeHex(hex: string): void {
			delete this.state.voodooEffects[hex];
		}

		endHexDuration(): void {
			for (const prop in this.state.voodooEffects) {
				if (!this.state.voodooEffects.hasOwnProperty(prop)) continue;
				switch (prop) {
					case "PIRATES_PROWESS_DURATION":
						(<number> this.state.voodooEffects[prop])--;
						if (this.state.voodooEffects[prop] <= 0) {
							delete this.state.voodooEffects["PIRATES_PROWESS"];
							delete this.state.voodooEffects["PIRATES_PROWESS_DURATION"];
							setup.notifications.addMessage(Notifications.MessageCategory.StatusChange, setup.world.day + 1, "You feel the effects of your pirates skill leave you…");
						}
				}
			}
		}

		// Acquire everything for debug purposes
		acquireAllItems(): void {
			console.group("AcquireAllItems");
			for (const prop in Data.clothes) {
				if (Data.clothes.hasOwnProperty(prop)) {
					if (!this.ownsWardrobeItem(prop)) {
						console.log("Adding \"" + prop + "\" (clothes)");
						this.addItem(Items.Category.Clothes, prop, 1);
					} else {
						console.log("\"" + prop + "\" (clothes) already owned");
					}
				}
			}
			console.groupEnd();
		}

		/**
		 * Returns number that represents how high-class the PC looks.
		 * 0 is obvious commoner and 100 is a rich noble. Can be higher or lower.
		 */
		get highClassPresentability(): number {
			// For now just consider "Slutty Lady" the proper attire,
			// while other clothes contribute only part of their style.
			// Do not count parts that are not visible in a "proper" situation.
			const getBonus = (slot: ClothingSlot, equipmentItem: Items.Clothing) => {
				// The proper attire
				let bonus = equipmentItem.categoryBonus(Data.Fashion.Style.SluttyLady);
				if (bonus > 0) {
					console.log(`Slot: ${slot}, equipment name: "${equipmentItem.name}", bonus: ${bonus}`);
					return bonus;
				}

				// Not so proper, but closer than others
				bonus = equipmentItem.categoryBonus(Data.Fashion.Style.HighClassWhore) / 2;
				if (bonus > 0) {
					console.log(`Slot: ${slot}, equipment name: "${equipmentItem.name}", bonus: ${bonus}`);
					return bonus;
				}

				// Oh well
				bonus = equipmentItem.styleBonus / 4;
				console.log(`Slot: ${slot}, equipment name: "${equipmentItem.name}", bonus: ${bonus}`);
				return bonus;
			};

			console.group("HighClassPresentability");
			let result = 0;

			const underwearSlots = [ClothingSlot.Bra, ClothingSlot.Nipples, ClothingSlot.Panty, ClothingSlot.Butt, ClothingSlot.Penis];
			for (const [slot, equipmentItem] of Object.entries(this.clothing.equipment)) {
				if (equipmentItem != null && !underwearSlots.includes(slot)) {
					result += getBonus(slot, equipmentItem);
				}
			}

			console.log(`Total for clothes: ${result}`);

			const forHair = this.hairRating * 0.25;
			const forMakeup = this.makeupRating * 0.25;
			console.log(`For hair: ${forHair}`);
			console.log(`For makeup: ${forMakeup}`);

			result += forHair + forMakeup;
			console.log(`Total: ${result}`);
			console.groupEnd();
			return result;
		}

		// TODO: include other factors and maybe calibrate.
		/**
		 * Returns number that represents how obvious it is that the PC is male.
		 *  100 means completely impassable and 0 means completely passable. Can be higher or lower.
		 */
		get obviousTrappiness(): number {
			console.group("ObviousTrappiness");
			// console.log(this.state_.body.value);
			// console.log(this.state_.core.value);

			// Huge penis makes trappiness very, very obvious
			const penisOversizing = Math.max(this.state.body.value.penis - 75, 0);
			const penisContribution = penisOversizing * penisOversizing;
			console.log(`penisContribution: ${penisContribution}`);

			// Let's say DD breasts give -50, and we have diminishing returns
			const bustContribution = -Math.sqrt(this.state.body.value.bust * 50 * 50 / 11);
			console.log(`bustContribution: ${bustContribution}`);

			// Style gives a small contribution, but synergizes with femininity
			const styleContribution = - this.clothesRating * .1;
			console.log(`styleContribution: ${styleContribution}`);

			// Femininity is important
			const femininityContribution = - this.state.core.value.femininity * .3;
			console.log(`femininityContribution: ${femininityContribution}`);

			// Full femininity and full style give -50 together
			const femininityStyleSynergy = - this.state.core.value.femininity * this.clothesRating * .005;
			console.log(`femininityStyleSynergy: ${femininityStyleSynergy}`);

			// Base value is full
			let result = 100 + penisContribution + bustContribution + styleContribution +
				femininityContribution + femininityStyleSynergy;
			// Let's make sure result is not too far from soft borders
			if (result > 100) result = 100 + Math.sqrt(result - 100);
			if (result < 0) result = -Math.sqrt(-result);

			console.log(`result: ${result}`);
			console.groupEnd();

			return result;
		}

		// region SLOT wheel stuff

		/**
		 * Unlock a slot.
		 */
		unlockSlot(): boolean {
			if (this.state.currentSlots + 1 > this.maxSlots) return false;
			this.state.currentSlots++;
			return true;
		}

		/**
		 * Fetch all reels in the players inventory.
		 */
		getReelsInInventory(): Items.Reel[] {
			return this.inventory.filter(o => o instanceof Items.Reel) as Items.Reel[];
		}

		/**
		 * Fetch a single reel by ID.
		 */
		getReelByID(filterID: string): Items.Reel | null {
			let arr = this.getReelsInInventory();
			arr = arr.filter(o => o.id == filterID);
			return (arr.length > 0) ? arr[0] : null; // huh? At least grab the first one.
		}

		/**
		 * Attempt to pick a reel from inventory by Id and then equip it to a slot. It will remove any reel
		 * equipped in that slot and place it back in the inventory.
		 */
		equipReel(toEquipID: Data.ItemNameTemplate<Items.Category.Reel>, reelSlot: number): void {
			this.inventory.equipReel(toEquipID, reelSlot);
		}

		/**
		 * Remove an equipped reel and place it in the inventory.
		 */
		removeReel(slotID: number): void {
			this.inventory.removeReel(slotID);
		}

		/**
		 * Turn the equipped reels into an array to iterate/read.
		 */
		getReels(): Items.Reel[] {
			return this.inventory.equippedReelItems();
		}

		get isFuta(): boolean {
			return this.state.core.value.futa > 0;
		}

		// endregion

		get inventoryManager(): InventoryManager {
			if (!this.#inventory) {
				this.#inventory = new Entity.InventoryManager();
			}
			return this.#inventory;
		}

		get clothing(): Entity.ClothingManager {
			if (!this.#clothing) {
				this.#clothing = new Entity.ClothingManager();
			}
			return this.#clothing;
		}

		/**
		 * Returns total number of item charges in inventory
		 */
		inventoryItemsCount(): number {
			let res = 0;
			this.inventoryManager.forEachItemRecord(undefined, undefined, (n /* , tag, itemClass */) => {res += n;});
			return res;
		}

		/**
		 * Returns total number of different item types in inventory
		 */
		inventoryItemsTypes(): number {
			let res = 0;
			this.inventoryManager.forEachItemRecord(undefined, undefined, (/* n, tag, itemClass */) => {res += 1;});
			return res;
		}

		// redirections for the state properties
		// TODO extract into a Human class
		get originalName(): string {return this.state.name.original;}
		set originalName(n: string) {this.state.name.original = n;}
		get slaveName(): string {return this.state.name.slave;}
		set slaveName(n: string) {this.state.name.slave = n;}
		get girlfriendName(): string {return World.instance.state.character.girlfriend.name.original;}
		set girlfriendName(n: string) {World.instance.state.character.girlfriend.name.original = n;}
		get nickName(): string {return this.state.name.nick;}
		get hairColor(): Data.HairColor {return this.state.hair.color;}
		set hairColor(c: Data.HairColor) {this.state.hair.color = c;}
		get hairStyle(): string {return this.state.hair.style;}
		set hairStyle(s: string) {this.state.hair.style = s;}
		get hairBonus(): number {return this.state.hair.bonus;}
		set hairBonus(v: number) {this.state.hair.bonus = v;}
		get makeupStyle(): string {return this.state.makeup.style;}
		set makeupStyle(s: string) {this.state.makeup.style = s;}
		get makeupBonus(): number {return this.state.makeup.bonus;}
		set makeupBonus(v: number) {this.state.makeup.bonus = v;}
		get eyeColor(): string {return this.state.eyeColor;}
		get money(): number {return this.state.money;}
		/**
		 * @deprecated
		 * Use earnMoney() / spendMoney() instead
		 */
		set money(c: number) {this.state.money = c;}
		get tokens(): number {return this.state.tokens;}
		set tokens(c: number) {this.state.tokens = c;}
		get sailDays(): number {return this.state.sailDays;}
		get lastUsedMakeup(): string {return this.state.lastUsedMakeup;}
		get lastUsedHair(): string {return this.state.lastUsedHair;}
		get lastQuickWardrobe(): string {return this.state.lastQuickWardrobe;}

		get jobFlags(): Record<string, TaskFlag> {return this.state.jobFlags;}
		get voodooEffects(): Record<string, string | number> {return this.state.voodooEffects;}
		get questFlags(): Record<string, TaskFlag> {return this.state.questFlags;}

		flagValue(type: FlagType, name: string, defaultValue?: TaskFlag): TaskFlag {
			return this._flagsObject(type)[name] ?? defaultValue;
		}

		setFlagValue(type: FlagType, name: string, value: TaskFlag): void {
			this._flagsObject(type)[name] = value;
		}

		increaseFlagValue(type: FlagType, name: string, value: number): number {
			const flagsObj = this._flagsObject(type);
			const newVal = (flagsObj[name] ?? 0) as number + value;
			flagsObj[name] = newVal;
			return newVal;
		}

		deleteFlag(type: FlagType, name: string): void {
			const flagsObj = this._flagsObject(type);
			delete flagsObj[name];
		}

		get history(): Record<string, Record<string, number>> {return this.state.history;}

		// Player Statistic Variables
		get coreStats(): Record<CoreStat, number> {return this.state.core.value;}
		get coreStatsXP(): Record<CoreStat, number> {return this.state.core.xp;}

		get bodyStats(): Record<BodyStat, number> {return this.state.body.value;}
		get bodyXP(): Record<BodyStat, number> {return this.state.body.xp;}

		get skills(): Record<Skills.Any, number> {return this.state.skill.value;}
		get skillsXP(): Record<Skills.Any, number> {return this.state.skill.xp;}

		get wardrobe(): Items.Clothing[] {return this.clothing.wardrobe;}

		get inventory(): InventoryManager {return this.inventoryManager;}
		get equipment(): Partial<Record<ClothingSlot, Items.Clothing | null>> {return this.clothing.equipment;}

		get slots(): Record<number, Items.Reel | null> {return this.inventory.reelSlots();}
		get currentSlots(): number {return this.state.currentSlots;} // Starting allocation of whoring

		// eslint-disable-next-line class-methods-use-this
		get maxSlots(): number {return 9;} // YOU SHALL NOT PASS

		saveLoaded(): void {
			this.#inventory = null;
			this.#clothing = null;
		}

		get gameStats(): GameState.GameStats {return this.state.gameStats;}

		get faceData(): DaBodyPreset {return this.state.faceData;}

		setFace(presetID: string): void {
			// TODO copy data
			this.state.faceData = Data.DAD.facePresets[presetID];
		}

		get isAlive(): boolean {
			return this.coreStats.health > 0;
		}

		static get instance(): Player {
			return setup.player;
		}

		private _flagsObject(type: FlagType): Record<string, TaskFlag> {
			switch (type) {
				case FlagType.Job:
					return this.state.jobFlags;
				case FlagType.Quest:
					return this.state.questFlags;
			}
		}
	}
}
