namespace App {
	type BonusFunction<T> = (player: Entity.Player, arg: T) => number;
	type DistributeBonusFunction<U> = U extends unknown ? BonusFunction<U> : never;
	type SexualitySource = Data.Whoring.BonusCategory;

	interface Payout {
		slot: number;
		payout: number;
		xp: number;
		mood: number;
		lust: number;
		sat: number;
		type: Data.ReelAction;
		want: number;
	}
	interface MoneyEarning {
		money: number;
		want: number;
		type: Data.ReelAction;
	}

	type BonusFunctions<T extends PropertyKey, V> = Record<T, DistributeBonusFunction<V>>;
	type BonusFunctionRecord<T extends PropertyKey> = {[K in T]: BonusFunction<K>};

	/**
	 * Main engine for slot machine mini game.
	 */
	export class SlotEngine {
		#customers: Customer[] = [];
		#rares: string[] = [];
		#maxSlots = 9;
		#element = "#WhoreUI";
		#inventoryElement = "#SlotInventoryUI";
		#dataKey = "";
		#spins = 5;
		#spinning = false;
		#selectedCustomer = -1;
		#player: Entity.Player;
		#moneyEarned: MoneyEarning[] = [];
		#xpEarned: Partial<Record<Data.ReelAction, number>> = {};
		#misses = 0;
		#desperationSpins = 0;
		#returnPassage = "Deck";
		#endStatus: string[] = [];
		#title = "A BACK ALLEY";
		#marquee: string | null = null;
		#selectedSlot = -1;
		#reels: EZSlots | undefined;

		static readonly #REEL_BONUS: BonusFunctions<Data.ReelAction, number> = {
			ass: (player, d) => player.statRoll(Stat.Skill, Skills.Sexual.AssFucking, d, 1, true),
			hand: (player, d) => player.statRoll(Stat.Skill, Skills.Sexual.HandJobs, d, 1, true),
			tits: (player, d) => player.statRoll(Stat.Skill, Skills.Sexual.TitFucking, d, 1, true),
			bj: (player, d) => player.statRoll(Stat.Skill, Skills.Sexual.BlowJobs, d, 1, true),
			beauty: SlotEngine._styleBonus,
			fem: SlotEngine._femBonus,
			perv: SlotEngine._pervBonus,
		};

		static readonly #BONUS_CAT_BONUS: BonusFunctionRecord<Data.Whoring.BonusCategory> = {
			[CoreStat.Perversion]: SlotEngine._statBonus,
			[CoreStat.Femininity]: SlotEngine._statBonus,
			beauty: SlotEngine._styleBonus,
		};

		static readonly #BONUS_SOURCE_BONUS: BonusFunctionRecord<Data.Whoring.BonusSource> = {
			bust: SlotEngine._bodyBonus,
			ass: SlotEngine._bodyBonus,
			face: SlotEngine._bodyBonus,
			lips: SlotEngine._bodyBonus,
			penis: SlotEngine._bodyBonus,

			/* eslint-disable @typescript-eslint/naming-convention */
			PirateSlut: SlotEngine._fashionBonus,
			Bimbo: SlotEngine._fashionBonus,
			SissyLolita: SlotEngine._fashionBonus,
			GothicLolita: SlotEngine._fashionBonus,
			Bdsm: SlotEngine._fashionBonus,
			DaddyGirl: SlotEngine._fashionBonus,
			NaughtyNun: SlotEngine._fashionBonus,
			PetGirl: SlotEngine._fashionBonus,
			HighClassWhore: SlotEngine._fashionBonus,
			SluttyLady: SlotEngine._fashionBonus,
			SexyDancer: SlotEngine._fashionBonus,
			Ordinary: SlotEngine._fashionBonus,
			/* eslint-enable @typescript-eslint/naming-convention */
		};

		private static _instance: SlotEngine;
		/**
		 * Dictionary of string tokens to css class names.
		 */
		static readonly #SLOT_TYPES: Record<Data.ReelAction, string> = {
			ass: "AssSlotReel",
			tits: "TitsSlotReel",
			bj: "BJSlotReel",
			hand: "HandSlotReel",
			fem: "FemSlotReel",
			perv: "PervSlotReel",
			beauty: "BeautySlotReel",
		};

		static readonly #SLOT_TYPES_TO_NAMES: Record<Data.ReelAction, string> = {
			ass: "Ass Fucking",
			tits: "Tit Fucking",
			bj: "BlowJobs",
			hand: "HandJobs",
			fem: "Femininity",
			perv: "Perversion",
			beauty: "Beauty",
		};

		static readonly #SLOT_TYPES_TO_SKILL_NAMES: Record<Data.ReelAction, Skills.Sexual | SexualitySource> = {
			ass: Skills.Sexual.AssFucking,
			tits: Skills.Sexual.TitFucking,
			bj: Skills.Sexual.BlowJobs,
			hand: Skills.Sexual.HandJobs,
			fem: CoreStat.Femininity,
			perv: CoreStat.Perversion,
			beauty: "beauty",
		};

		static readonly #WILD_CARDS: Data.ReelWildcard[] = ["fem", "perv", "beauty"];

		constructor(player: Entity.Player) {
			this.#player = player;
		}

		// region PUBLIC FUNCTIONS

		static get instance(): SlotEngine {
			if (!SlotEngine._instance) {
				SlotEngine._instance = new SlotEngine(setup.player);
			}
			return SlotEngine._instance;
		}

		loadScene(dataKey: string, returnPassage: string, elementID?: string): void {
			if (typeof elementID !== 'undefined') this.#element = elementID;
			this.#dataKey = dataKey;
			this.#returnPassage = returnPassage;

			// Load the data dictionary for this scene.
			const dict = Data.whoring[dataKey];

			// You can have up to 6 customers. This is largely dictated by the overall lust of the NPC container object.
			// const npc = setup.world.npc(dict.NPC_TAG);
			// var maxCustomers = Math.round(Math.max(1, Math.min( (6 * (npc.Lust()/100)), 6)));
			this.#title = dict.desc;
			this.#customers = []; // Clear data.
			this.#rares = [];
			this.#moneyEarned = []; //
			this.#xpEarned = {};
			this.#spins = 5;
			this.#desperationSpins = 0;
			this.#misses = 0;
			this.#endStatus = [];
			this.#selectedCustomer = -1;
			this.#marquee = dict.marquee ?? null;

			while (this.#customers.length < 6) {
				// We could generate a rare customer.
				if (dict.rares.length > 0) {
					if (Math.floor(Math.random() * 100) >= 90) {
						const customerObject = new Customer(dict.rares.randomElement());
						// Check to see if we already drew this rare.
						if (!this.#rares.includes(customerObject.name)) {
							this.#customers.push(customerObject);
							this.#rares.push(customerObject.name);
							continue;
						}
					}
				}

				this.#customers.push(new Customer(this.#dataKey));
			}
		}

		get returnPassage(): string {
			return this.#returnPassage;
		}

		/**
		 * Called from the Twine passage. Set the player object in the engine and attach an event to listen
		 * for when the passage finishes rendering to fire off jquery.
		 */
		drawUI(player: Entity.Player): void {
			if (typeof player !== 'undefined') this.#player = player;
			$(document).one(":passageend", this._drawUICB.bind(this));
		}

		/**
		 * Fetch the equipped slots on the player.
		 */
		getSlots(): Items.Reel[] {
			if (this.#player === undefined) return [];
			return this._player.getReels();
		}

		/**
		 * Called from the skills panel to draw the interface for slot management.
		 */
		drawSlotInventory(player?: Entity.Player): void {
			if (typeof player !== 'undefined') this.#player = player;
			$(document).one(":passageend", this._drawSlotInventoryCB.bind(this));
		}

		private static _addXXX(ob: Customer, root: JQuery) {
			const num = random(1, 10);
			let pic = `ss_anal_${num}`;
			switch (ob.mostSatisfied().want) {
				case "ass": pic = `ss_anal_${num}`; break;
				case "hand": pic = `ss_hand_${num}`; break;
				case "bj": pic = `ss_bj_${num}`; break;
				case "tits": pic = `ss_tit_${num}`; break;
			}

			root.addClass(pic);
		}

		/**
		 * Called from the 'WhoreEnd' passage to give out loot/rewards and to tell the player about it.
		 */
		printResults(player: Entity.Player): string {
			if (this.#endStatus.length > 0) return this.#endStatus.join('\n');

			this.#player = player;
			this.#endStatus = [];

			setup.world.nextPhase();

			let tmp = "";
			let bonus = 0;
			const oSatisfied = this.#customers.filter(o => o.satisfaction >= 100);

			player.addHistory("customers", this.#dataKey, oSatisfied.length); // Keep track of satisfied customers.

			switch (oSatisfied.length) {
				case 0: tmp = "You satisfied no customers, this is going to catch up with you later."; bonus = 1.0; break;
				case 1:
				case 2: tmp = `You satisfied ${oSatisfied.length} customers, earning you a slight boost in overall mood.`; bonus = 1.1; break;
				case 3:
				case 4:
				case 5: tmp = `You satisfied ${oSatisfied.length} customers, earning you a healthy boost in overall mood.`; bonus = 1.2; break;
				case 6: tmp = `You satisfied ${oSatisfied.length} customers, earning you a large boost in overall mood.`; bonus = 1.5; break;
			}

			this.#endStatus.push(tmp); // Add mood chat.

			const npcTags = this.#customers.map((o) => {return o.tag;}).filter((a, b, s) => {return s.indexOf(a) == b;}); // All available tags.

			const oMood = this.#customers.filter((o) => {return (o.mood != o.oMood);}); // Get all affected customers.
			const mood = Math.floor(Math.floor(Math.floor((oMood.reduce((a, o) => {
				return (a + (o.mood - o.oMood))
			}, 0) / 6)) * bonus) * 0.8); // Set mood adjustment
			let affTags = oMood.map((o) => {return o.tag;}).filter((a, b, s) => {return s.indexOf(a) == b;}); // Tags we touched their mood.

			for (let i = 0; i < npcTags.length; i++) // Iterate through and adjust mood.
				if (affTags.contains(npcTags[i]))
					setup.world.npc(npcTags[i]).adjustStat( NpcStat.Mood, mood);

			// Do the same for lust.

			const oLust = this.#customers.filter((o) => {return (o.lust != o.oLust);}); // Get all affected customers.
			const lust = (Math.floor(Math.floor((oLust.reduce((a, o) => (a + (o.oLust - o.lust)), 0) / 6)) * 0.5) * -1.0); // Set mood adjustment
			affTags = oLust
				.map(o => o.tag)
				.filter((a, b, s) => s.indexOf(a) == b); // Tags we touched their mood.

			for (let i = 0; i < npcTags.length; i++) // Iterate through and adjust mood.
				if (affTags.contains(npcTags[i]))
					setup.world.npc(npcTags[i]).adjustStat(NpcStat.Lust, lust);

			// Show XP gain.
			// Sort gain by XP points via a temporary array
			const xpArray = Object.entries(this.#xpEarned).map((e) => {
				return {key: e[0], value: e[1]}
			});
			// Sort the array based on the "value" element property
			xpArray.sort((first, second) => {return second.value - first.value;});

			for (let i = 0; i < xpArray.length; ++i) {
				const key = xpArray[i].key;
				const statName = SlotEngine.#SLOT_TYPES_TO_SKILL_NAMES[key];
				if (statName == 'beauty') continue;

				const name = SlotEngine.#SLOT_TYPES_TO_NAMES[key];

				const xp = xpArray[i].value;
				if (statName === CoreStat.Perversion || statName === CoreStat.Femininity) {
					this.#player.adjustStatXP(statName, xp);
				} else {
					this.#player.acknowledgeSkillUse(statName, xp);
					this.#player.adjustSkillXP(statName, xp);
				}

				this.#endStatus.push(`You gained @@.item-xp;${xp}@@ ${name} XP.`);
			}

			// Show Money Gain.
			const money = this.#customers.reduce((a, o) => {return a + o.spent}, 0);
			this.#player.earnMoney(money, GameState.IncomeSource.Whoring);
			this.#endStatus.push(`You gained @@.item-money;${money}@@ coins in payment for services rendered.`);

			// Check for rare loot drops.
			for (let i = 0; i < oSatisfied.length; i++) {
				if ((1 + Math.floor(Math.random() * 100)) <= (oSatisfied[i].mood - 60)) {
					const tipTotal = oSatisfied[i].spent;
					const item = Items.pickItem([Items.Category.Food, Items.Category.Drugs, Items.Category.Cosmetics], {price: tipTotal});
					if (item != null) {
						this.#player.addItem(item.cat, item.tag, 0);
						this.#endStatus.push(oSatisfied[i].name + " gave you an extra tip: " + item.desc);
					}
				}
			}

			const desperation = this.#desperationSpins * 20;
			if (desperation > 0) {
				this.#player.adjustStatXP(CoreStat.Willpower, (desperation * -1.0));
				this.#endStatus.push(`You lost @@.state-negative;${desperation} willpower XP@@. This is a dangerous situation.`);
			}
			return this.#endStatus.join("\n");
		}

		private get _player() {
			if (!this.#player) {throw "Player is undefined";}
			return this.#player;
		}

		private get _reels() {
			if (!this.#reels) {throw "Reels are undefined";}
			return this.#reels;
		}

		// UI SPECIFIC FUNCTIONS

		private _drawCustomers() {
			for (let i = 0; i < 6; i++)
				$(`#WhoreCustomer${i}`).remove();

			for (let i = 0; i < this.#customers.length; i++)
				$(this.#element).append(this._getCustomerUI(i));
		}

		private _redrawCustomerUI(index: number) {
			const mood = $(`#WhoreMood${index}`);
			mood.width(this.#customers[index].mood);
			const lust = $(`#WhoreLust${index}`);
			lust.width(this.#customers[index].lust);
			const sat = $(`#WhoreSatisfaction${index}`);
			sat.width(this.#customers[index].satisfaction);
			const root = $(`#WhoreCustomer${index}`);
			if (this.#selectedCustomer == index) {
				root.css('border-color', 'lime');
			} else {
				root.css('border-color', 'saddlebrown');
			}
		}

		private static _bonusSourceIsFashionStyle(b: Data.Whoring.BonusSource): b is Data.Fashion.StyleId {
			return Data.Fashion.Style.hasOwnProperty(b);
		}

		private _getCustomerUI(index: number) {
			const ob = this.#customers[index];

			const root = $('<div>').addClass('WhoreCustomerGUI').attr('id', `WhoreCustomer${index}`);
			// Assign select customer callback
			if (ob.satisfaction < 100) {
				root.on("click", {customer: index}, this._customerSelectCB.bind(this));
			} else {
				root.empty();
				root.addClass('WhoreCustomer');
				root.css('opacity', '0.4');
				root.css('cursor', 'not-allowed');
				SlotEngine._addXXX(ob, root);
				const completed = $('<div>').addClass('CustomerComplete').text('SATISFIED');
				completed.css('opacity', '0');
				completed.animate({opacity: 1.0}, 2000);
				root.append(completed);
				return root;
			}

			if (this.#selectedCustomer == index) {
				root.css('border-color', 'lime');
			} else {
				root.css('border-color', 'saddlebrown');
			}

			// Customer Name and Rank
			const name = $('<span>').addClass('WhoreLabel').text(ob.name);
			root.append(name);

			const rank = $('<span>').addClass('WhoreRank').addClass(`Pay${ob.payout}`).text('$');
			root.append(rank);

			// Mood, Lust and Satisfaction labels and bars.
			const mood = $('<span>').addClass('WhoreMood').text('Mood');
			root.append(mood);
			const moodBar = $('<div>').addClass('WhoreMood').attr('id', `WhoreMood${index}`).width(ob.mood);
			root.append(moodBar);

			const lust = $('<span>').addClass('WhoreLust').text('Lust ');
			root.append(lust);
			const lustBar = $('<div>').addClass('WhoreLust').attr('id', `WhoreLust${index}`).width(ob.lust);
			root.append(lustBar);

			const satisfaction = $('<span>').addClass('WhoreSatisfaction').text('Sat.');
			root.append(satisfaction);
			const satisfactionBar = $('<div>').addClass('WhoreSatisfaction').attr('id', `WhoreSatisfaction${index}`).width(ob.satisfaction);

			root.append(satisfactionBar);

			// Wants and bonus
			const wantLabel = $('<span>').addClass('WhoreWant').text('Wants');
			root.append(wantLabel);

			for (let i = 0; i < ob.wants.length; i++) {
				const wantClass = `WhoreWant${i + 1} WhoreWantColor${i + 1}`;
				const want = $('<span>').addClass(wantClass).text(_.pascalCase(ob.wants[i]));
				root.append(want);
			}

			const bonusLabel = $('<span>').addClass('WhoreBonus').text('Bonus');
			root.append(bonusLabel);

			const bonusSlotClass = "WhoreBonusSlot " + "BonusCat" + ob.bonusCat;
			const bonusText = SlotEngine._bonusSourceIsFashionStyle(ob.bonus) ? Data.Fashion.Style[ob.bonus] : _.pascalCase(ob.bonus);
			const bonusSlot = $('<span>').addClass(bonusSlotClass).text(bonusText);
			root.append(bonusSlot);

			return root;
		}

		private static _getLockedSlots(lockedCount: number): [number, number] {
			switch (lockedCount) {
				case 1: return [0, 1]
				case 2: return [1, 1];
				case 3: return [1, 2];
				case 4: return [2, 2];
				case 5: return [3, 2];
				case 6: return [3, 3];
				case 7: return [3, 4];
				case 8: return [4, 4];
				default: return [4, 5];
			}
		}

		private _drawSlots() {
			$('#SlotContainer').remove();
			// Make slot container div.
			const root = $('<div>').attr('id', "SlotContainer");
			$(this.#element).append(root);

			// Calculate locked slots.
			const lockedSlots = this.#maxSlots - this.getSlots().length;

			const [before, after] = SlotEngine._getLockedSlots(lockedSlots);

			// Draw 'before' locked slots.
			for (let i = 0; i < before; i++) {
				const lockedSlot = $('<div class="LockedSlot"></div>');
				root.append(lockedSlot);
			}

			// Get slot reels.
			const slots: Data.ReelAction[][] = [];
			const starting: number[] = [];
			const winning: number[] = [];

			for (const slot of this.getSlots()) {
				slots.push(slot.reelsArray);
				starting.push(0);
				winning.push(0);
			}
			if (slots.length >= 3) {
				this.#reels = new EZSlots(this, "SlotContainer",
					{
						reelCount: slots.length, startingSet: starting, winningSet: winning, time: 2.0,
						symbols: slots, height: 90, width: 60, callback: this._slotCB.bind(this),
					});
			}

			// Draw 'after' locked slots.
			for (let i = 0; i < after; i++) {
				const lockedSlot = $('<div style="background-image:[img[locked_slot_icon]]" class="LockedSlot"></div>');
				root.append(lockedSlot);
			}
			// Add Spin button, hook up click event.
			const spinButton = $('<button>').addClass("WhoreSpinButton").text("SPIN AHOY!");
			spinButton.on("click", this._spinEH.bind(this));
			const statusPanel = $('#WhoreStatusPanel');
			statusPanel.append(spinButton);

			// Add cash out button, hook up click event.
			const cashOutButton = $('<button>').addClass("WhoreCashOutButton").text("CASH OUT!");
			cashOutButton.on("click", this._cashOutEH.bind(this));
			statusPanel.append(cashOutButton);
		}

		/**
		 * Draw the slot inventory management screen.
		 */
		private _drawSlotInventory() {
			// Get the root panel in the screen.
			const root = $(this.#inventoryElement);
			// Find the container div and empty it
			root.empty();

			// Calculate locked slots.
			const lockedSlots = this.#player.maxSlots - this.#player.currentSlots;
			const [before, after] = SlotEngine._getLockedSlots(lockedSlots);

			const makeEmptySlot = (locked: boolean, key: string): JQuery => {
				if (locked) {
					// Empty slot that is not unlocked. Add a place holder.
					return $('<div class="LockedSlot2"></div>');
				}
				// Slot is empty AND unlocked.
				const slot = $('<div>').attr('id', `SlotInventory_${key}`).addClass('OpenSlot');
				slot.on("click", {slot: key}, this._selectSlotCB.bind(this));
				if (this.#selectedSlot == null) this.#selectedSlot = parseInt(key);
				return slot;
			};

			const makeUsedSlot = (reel: Items.Reel, key: string): JQuery => {
				// Slot is not empty and it's unlocked.
				const slot = $('<div>').attr('id', `SlotInventory_${key}`).addClass('SlottedSlot');
				slot.addClass(reel.css);
				slot.on("click", {slot: key}, this._selectSlotCB.bind(this));
				if (this.#selectedSlot == null) this.#selectedSlot = parseInt(key);
				return slot;
			};

			// Draw active.
			let i = 0;
			const maxNonLocked = this.#player.maxSlots - after;
			for (const e of Object.entries(this.#player.slots)) {
				i++;
				const reel = e[1];
				const key = e[0];

				const slot = reel ? makeUsedSlot(reel, key) : makeEmptySlot(i <= before || i > maxNonLocked, key);
				if (this.#selectedSlot != null && key == this.#selectedSlot.toString()) slot.css('border', 'solid 1px lime');

				root.append(slot);
			}

			this._drawSlotInventoryList();
			this._drawInventoryCurrent();
		}

		private _drawSlotInventoryList() {
			const root = $(this.#inventoryElement);
			// Draw slot inventory
			const inventory = $('<div>').addClass('SlotInventory');
			root.append(inventory);

			const reels = this.#player.getReelsInInventory();

			for (let i = 0; i < reels.length; i++) {
				const item = $('<div>').addClass('SlotInventoryItem');
				const head = $('<span>').addClass('SlotHeader').html(`${reels[i].charges} x (${reels[i].rank}) ${reels[i].name}`);

				switch (reels[i].rank) {
					case Items.Rarity.Common: head.css('color', 'grey'); break;
					case Items.Rarity.Uncommon: head.css('color', 'lime'); break;
					case Items.Rarity.Rare: head.css('color', 'cyan'); break;
					case Items.Rarity.Legendary: head.css('color', 'orange'); break;
				}

				item.append(head);
				item.append($('<br>'));
				for (const e of Object.entries(SlotEngine.#SLOT_TYPES_TO_NAMES)) {
					const percent = reels[i].calcPercent(e[0]);
					if (percent <= 0) continue;
					const span = $('<span>').addClass('SlotAttribute').text(`${e[1]} ${percent}%`);
					item.append(span);
				}

				item.append($('<br>'));
				const button = $('<button>').addClass('EquipSlotButton').text('EQUIP');
				button.on('click', {id: reels[i].id}, this._equipSlotCB.bind(this));

				item.append(button);
				inventory.append(item);
			}
		}

		private _drawInventoryCurrent() {
			const root = $(this.#inventoryElement);
			// Draw current slot item info.
			const current = $('<div>').addClass('SlotCurrent');
			root.append(current);

			const reel = this.#player.slots[this.#selectedSlot];

			if (!reel) {
				const tempHeader = $('<div>').addClass('SlotCurrentHeader').text("EMPTY SLOT SELECTED");
				current.append(tempHeader);
			} else {
				const header = $('<div>').addClass('SlotCurrentHeader').html("(" + reel.rank + ") " + reel.name);

				switch (reel.rank) {
					case Items.Rarity.Common: header.css('color', 'grey'); break;
					case Items.Rarity.Uncommon: header.css('color', 'lime'); break;
					case Items.Rarity.Rare: header.css('color', 'cyan'); break;
					case Items.Rarity.Legendary: header.css('color', 'orange'); break;
				}

				current.append(header);
				const buttonDiv = $('<div>').addClass('SlotCurrentButton');
				const button = $('<button>').addClass('RemoveSlotButton').text('REMOVE');
				button.on('click', this._removeSelectedSlotCB.bind(this));

				buttonDiv.append(button);
				current.append(buttonDiv);
				current.append($('<hr>').addClass('SlotCurrent'));

				for (const e of Object.entries(SlotEngine.#SLOT_TYPES_TO_NAMES)) {
					const percent = reel.calcPercent(e[0]);
					if (percent <= 0) continue;
					const span = $('<span>').addClass('SlotAttribute').text(`${e[1]} ${percent}%`);
					current.append(span);
				}

				current.append($('<hr>').addClass('SlotCurrent'));
			}
		}

		private _drawStatus() {
			$('#WhoreStatusPanel').remove();

			const root = $('<div>').addClass('WhoreStatus').attr('id', "WhoreStatusPanel");
			if (this.#marquee != null) {
				const marquee = $('<div>').addClass('WhoreMarquee');
				marquee.addClass(this.#marquee);
				root.append(marquee);
			} else {
				const title = $('<div>').addClass(('WhoreStatusTitle')).text(this.#title);
				root.append(title);
				const spinTitle = $('<span>').addClass('WhoreSpinsLeftTitle').text('SPINS LEFT');
				root.append(spinTitle);
			}

			const buyButton = $('<button>').addClass("WhoreBuySpinButton").text("BUY 5 SPINS FOR 1 ENERGY");
			buyButton.on("click", this._buyEnergyCB.bind(this));
			root.append(buyButton);

			const wildcardTitle = $('<span>').addClass('WhoreWildcardTitle').text('WILDCARD POWER');
			root.append(wildcardTitle);

			const wildcardBox = $('<div>').addClass('WhoreWildcardBox');
			const style = $('<span>').addClass('WhoreWildcardStyle').text('Beauty');
			wildcardBox.append(style);
			const styleBar = $('<div>').addClass('WhoreWildcardStyle').width(this.#player.beauty);
			wildcardBox.append(styleBar);
			const perversion = $('<span>').addClass('WhoreWildcardPerversion').text('Perversion ');
			wildcardBox.append(perversion);
			const perversionBar = $('<div>').addClass('WhoreWildcardPerversion').width(this.#player.getStat(Stat.Core, CoreStat.Perversion));
			wildcardBox.append(perversionBar);
			const femininity = $('<span>').addClass('WhoreWildcardFemininity').text('Femininity');
			wildcardBox.append(femininity);
			const femininityBar = $('<div>').addClass('WhoreWildcardFemininity').width(this.#player.getStat(Stat.Core, CoreStat.Femininity));
			wildcardBox.append(femininityBar);
			root.append(wildcardBox);

			const lootTitle = $('<span>').addClass('WhoreLootTitle').text('CURRENT EARNINGS');
			root.append(lootTitle);

			const lootBox = $('<div>').addClass('WhoreLootBox').attr('id', 'WhoreLootBox');
			for (let i = 0; i < this.#moneyEarned.length; i++) {
				const wantClass = $('<span>').addClass(`WhoreWantColor${this.#moneyEarned[i].want}`)
					.html(SlotEngine._typeAcronym(this.#moneyEarned[i].type));
				lootBox.append(wantClass);
			}

			root.append(lootBox);

			// If we reloaded page or came back from another passage, reset the bar to it's last known position.
			const width = Math.max(0, Math.min(116 * this.#misses, 696));
			const bar = $('#DesperationFiller');
			bar.width(width);

			// Draw Desperation button

			$('#DesperationButton').remove();
			const desperationButton = $('<button>').addClass("DesperationButtonDeactivated").attr('id', 'DesperationButton').text('DESPERATION SPIN');
			desperationButton.on("click", this._desperationButtonCB.bind(this));
			$('#DesperationContainer').append(desperationButton);

			$(this.#element).append(root);

			this._redrawSpins();
		}

		private _drawDesperationBar() {
			if (this.#misses > 5) return; // Don't keep drawing past the maximum of 5 misses.
			const width = Math.max(0, Math.min(116 * this.#misses, 696))
			const bar = $('#DesperationFiller');
			if (width == 0) {
				bar.animate({width: width, easing: "linear"}, 1000);
			} else {
				bar.animate({width: width, easing: "swing"}, 1000);
			}
		}

		private static _typeAcronym(type: Data.ReelAction) {
			return type === "bj" ? "Bj" : type[0].toUpperCase();
		}

		private static _addMoneyToEarningsBox(want: number, type: Data.ReelAction) {
			const wantClass = $('<span>').addClass(`WhoreWantColor${want}`).html(SlotEngine._typeAcronym(type) + ' ');
			$('#WhoreLootBox').append(wantClass);
		}

		private _redrawSpins() {
			let spinText = "";
			const root = $('#WhoreStatusPanel');

			for (let i = 0; i < this.#spins; i++) spinText += "&#10026; "
			const spinBox = $('<div>').addClass(('WhoreSpinsLeftBox')).attr('id', "WhoreSpinsLeftBox").html(spinText);

			root.append(spinBox);
		}

		/** Make a cool looking dialog. */
		private _dialog(message: string) {
			PR.dialogBox(this.#element, message, {fgColor: "white"});
		}

		/** Rising message */
		private _risingDialog(message: string, color: string) {
			PR.risingDialog(this.#element, message, color);
		}

		/** Draw a big circle with a bang on it.
			 <h1 class="ml8">
			 <span class="letters-container">
			 <span class="letters letters-left">Hi</span>
			 <span class="letters bang">!</span>
			 </span>
			 <span class="circle circle-white"></span>
			 <span class="circle circle-dark"></span>
			 <span class="circle circle-container"><span class="circle circle-dark-dashed"></span></span>
			 </h1>
		 */
		private _whackPlayer() {
			this.#player.adjustStat(CoreStat.Health, -10);
			// Redraw Health Bar
			$('#Health').html(PR.pStatMeter(CoreStat.Health, this.#player, false));
			const root = $(this.#element);
			$('#WhoreDialogDiv').remove();

			const div = $('<div>').addClass('WhoreDialog').attr('id', 'WhoreDialogDiv');
			const header = $('<h1>').addClass('ml8');
			const lContainer = $('<span>').addClass('letters-container');

			header.append(lContainer);

			// Word
			const words = ['POW', 'BANG', 'SLAP', 'WHACK', 'KICK', 'SMACK', 'THUD'];
			const word = words.randomElement();
			lContainer.append($('<span>').addClass('letters letters-left').text(word));
			lContainer.append($('<span>').addClass('letters bang').text('!'));

			// Circle
			header.append($('<span>').addClass('circle circle-white'));
			header.append($('<span>').addClass('circle circle-dark'));

			const cContainer = $('<span>').addClass('circle circle-container');
			cContainer.append($('<span>').addClass('circle circle-dark-dashed'));
			header.append(cContainer);

			// finish up.
			div.append(header);
			root.append(div);

			anime.timeline({loop: false})
				.add({
					targets: '.ml8 .circle-white',
					scale: [0, 3],
					opacity: [1, 0],
					easing: "easeInOutExpo",
					rotateZ: 360,
					duration: 1100,
				}).add({
					targets: '.ml8 .circle-container',
					scale: [0, 1],
					duration: 1100,
					easing: "easeInOutExpo",
					offset: '-=1000',
				}).add({
					targets: '.ml8 .circle-dark',
					scale: [0, 1],
					duration: 1100,
					easing: "easeOutExpo",
					offset: '-=600',
				}).add({
					targets: '.ml8 .letters-left',
					scale: [0, 1],
					duration: 1200,
					offset: '-=550',
				}).add({
					targets: '.ml8 .bang',
					scale: [0, 1],
					rotateZ: [45, 15],
					duration: 1200,
					offset: '-=1000',
				}).add({
					targets: '.ml8',
					opacity: 0,
					duration: 1000,
					easing: "easeOutExpo",
					delay: 1400,
				});

			anime({
				targets: '.ml8 .circle-dark-dashed',
				rotateZ: 360,
				duration: 8000,
				easing: "linear",
				loop: false,
			});

			setTimeout(this._checkIfDead.bind(this), 3000);
		}

		private _slotWinDiv(slot: number, content: string) {
			const child = $(`#SlotWindow_${slot}`);
			const parent = child.parent();
			const pxy = parent.position();
			const cxy = child.position();
			const startTop = pxy.top - 60;
			const startLeft = cxy.left + 5;
			const endTop = pxy.top - 400;

			const newDiv = $('<div>').addClass("SlotWinner").attr('id', `SlotWinner_${slot}`).html(content);
			newDiv.css('top', startTop);
			newDiv.css('left', startLeft);
			newDiv.animate({opacity: 0.8, top: endTop}, 4000, function () {$(this).remove();});
			$(this.#element).append(newDiv);
		}

		private _removeCustomer(index: number) {
			this.#selectedCustomer = -1;
			$(`#WhoreCustomer${index}`).remove();
			$(this.#element).append(this._getCustomerUI(index));
		}

		private _checkIfDead() {
			if (!this._player.isAlive) Engine.play('DeathEnd');
		}

		// HELPER FUNCTIONS
		static getSlotClass(type: Data.ReelAction): string {
			return SlotEngine.#SLOT_TYPES[type];
		}

		private static _isWild(token: Data.ReelAction | 'wild'): boolean;
		private static _isWild(token: number, map: Record<Data.ReelAction | 'wild', number[]>): boolean
		private static _isWild(token: Data.ReelAction  | 'wild' | number, map?: Record<Data.ReelAction | 'wild', number[]>): boolean {
			if (typeof map !== 'undefined') return map.wild.contains(token);
			return SlotEngine.#WILD_CARDS.contains(token);
		}

		private static _isPerv(num: number, map: Record<Data.ReelAction | 'wild', number[]>) {return map.perv.contains(num);}
		private static _isFem(num: number, map: Record<Data.ReelAction | 'wild', number[]>) {return map.fem.contains(num);}
		private static _isBeauty(num: number, map: Record<Data.ReelAction | 'wild', number[]>) {return map.beauty.contains(num);}
		private static _splitReel(a: number[]) {
			const r: number[][] = [];
			let t: number[] = [];

			for (let i = 0; i < a.length; ++i) {
				if (i == 0) {
					t.push(a[i]);
					continue;
				}

				if (a[i - 1] != (a[i] - 1)) {
					r.push(t);
					t = [];
				}

				t.push(a[i]);
			}
			r.push(t);

			return r;
		}

		// Bonus modifiers
		private static _skillBonus(player: Entity.Player, skill: Skills.Any) {return player.statRoll(Stat.Skill, skill, 100, 1, true);}
		private static _bodyBonus(player: Entity.Player, body: BodyStat) {return player.statRoll(Stat.Body, body, 100, 1, true);}
		private static _statBonus(player: Entity.Player, stat: CoreStat) {return player.statRoll(Stat.Core, stat, 100, 1, true);}
		private static _fashionBonus(player: Entity.Player, fashion: Data.Fashion.StyleId) {
			return player.getStyleSpecRating(Data.Fashion.Style[fashion]) / 100;
		}

		// 0.0 to 1.0 mods.
		private static _styleBonus(player: Entity.Player) {return (player.beauty / 100);}
		private static _femBonus(player: Entity.Player) {return player.getStat(Stat.Core, CoreStat.Femininity) / 100;}
		private static _pervBonus(player: Entity.Player) {return player.getStat(Stat.Core, CoreStat.Perversion) / 100;}

		// an intermidiate function required because of TS bug #46476
		private _getBonus<BonusCat extends PropertyKey>(bonusDict: BonusFunctionRecord<BonusCat>, bonus: BonusCat): number {
			return bonusDict[bonus](this._player, bonus);
		}

		private _calculateJackpot(slotMap: Record<Data.ReelAction | "wild", number[]>, key: Data.ReelAction, slots: number[]): Payout[] {
			const c = this.#customers[this.#selectedCustomer];
			const mood = Math.floor((c.mood / 2));
			const lust = Math.floor((c.lust / 2));

			let basePay = 2 + (c.payout * 2);

			// See if the customer even WANTS this.
			let wantMod = 0;
			let wantPos = 0;
			// Sex match
			if (!SlotEngine._isWild(key)) {
				for (let i = 0; i < c.wants.length; i++)
					if (c.wants[i] == key) {
						wantPos = (i + 1);
						wantMod = (i == 0) ? 1.0 : (i == 1) ? 0.75 : 0.5;
					}
			} else { // This was a wildcard match
				wantMod = SlotEngine.#REEL_BONUS[key](this._player, 0);
			}

			if (wantMod == 0) return []; // What? We didn't even WANT this. It's not a payout.

			basePay = (basePay * wantMod); // 25, 18.75, 12.5

			// This is the overall modifier for the pay.
			switch (slots.length) {
				case 9: basePay = (basePay * 10); break;
				case 8: basePay = (basePay * 5); break;
				case 7: basePay = (basePay * 4); break;
				case 6: basePay = (basePay * 3); break;
				case 5: basePay = (basePay * 2); break;
				case 4: basePay = (basePay * 1.5); break;
			}

			// Build results for the spin.
			// ========  BASE PAYOUT =========
			// Payout is equal to the Pay * the modifier returned by the skill check for sex acts
			// The difficulty is equal to the c.PayOut attribute of the customer * 20
			// For wildcard matches it's just the default pay.
			// All results for pay are multiplied by the jackpot modifier. Add result as "PAYOUT"
			// ========  BONUS PAYOUT ========
			// Take the 1/2 the payout for that slot and apply the Category as a wildcard mod, add this to "PAYOUT"
			// Check Repeat the process for the bonus mod from the customer, add this to "PAYOUT"
			// ======== SKILL XP GAIN ========
			// You can gain skill in sex and perversion and femininity. The default XP is equal to c.PayOut * 20, modified by the result mod
			// This is checked once for each matching slot (including wild cards, but not beauty). Bonus does not grant XP.
			// ======== RAISING MOOD, LUST and SATISFACTION =========
			// Satisfaction is raised by 20 * skill check mod for every reel in the hit. When it reaches max, the customer is removed (as satisfied).
			// Mood is raised by 5 * skill check mod for every reel hit. There is no maximum.
			// Lust is raised only by Perversion wildcards, it is raised by 10 flat for each one hit.
			// ======== MOOD and LUST PAYOUT ======
			// After positive adjustments the base payout is modified by 0.5 + (50/(c.Mood/2)), for a 0.5 to 1.0 modifier.
			// Bonus pay is then added equal to 1 + (50/(c.Lust/2))
			// ======== MAXIMUM SATISFACTION ======
			// When satisfaction reaches 100 or higher, the customer is removed from the pool of available customers. The
			// game ends (no more spins) when there are no customers left.

			const results: Payout[] = [];

			for (let i = 0; i < slots.length; i++) {
				const result = {slot: slots[i], payout: 0, xp: 0, mood: 0, lust: 0, sat: 0, type: key, want: wantPos};
				let checkmod = 1.0;

				// Reset the type if this is a wildcard match
				if (SlotEngine._isWild(slots[i], slotMap)) {
					if (SlotEngine._isPerv(slots[i], slotMap)) result.type = 'perv';
					if (SlotEngine._isFem(slots[i], slotMap)) result.type = 'fem';
					if (SlotEngine._isBeauty(slots[i], slotMap)) result.type = 'beauty';

					checkmod = SlotEngine.#REEL_BONUS[result.type](this._player, 0); // wildcard bonus function need no value
				} else {
					checkmod = SlotEngine.#REEL_BONUS[key](this._player, (c.payout * 20));
				}

				// Lets clamp the mod to 0.2 to 1.6
				checkmod = Math.max(0.2, Math.min(checkmod, 1.6));

				result.payout = basePay * checkmod;
				// Add bonus category for style/fem/perv
				result.payout += ((basePay / 3) * this._getBonus(SlotEngine.#BONUS_CAT_BONUS, c.bonusCat));
				// Add bonus cat for body parts / styles
				result.payout += ((basePay / 3) * this._getBonus(SlotEngine.#BONUS_SOURCE_BONUS, c.bonus));
				// Add XP bonus for everything but BEAUTY
				result.xp = (SlotEngine._isBeauty(slots[i], slotMap) != true) ? Math.round((c.payout * 7.5) * wantMod) : 0;
				// Add Mood
				result.mood = Math.round(5 * checkmod);
				// Add Lust
				result.lust = (SlotEngine._isPerv(slots[i], slotMap)) ? 20 : -5;
				// Add Satisfaction
				result.sat = Math.round(15 * checkmod);
				// Modify pay by mood formula
				result.payout = (result.payout * (0.5 + (mood / 100)));
				// Add a bonus due to high lust.
				result.payout += (basePay * (lust / 100));

				result.payout = Math.round(result.payout);
				this._addMoneyToCustomer(result.payout);
				results.push(result);
			}

			return results;
		}

		private _addMoneyToPlayer(money: number, want: number, type: Data.ReelAction) {
			this.#moneyEarned.push({money: money, want: want, type: type});
			SlotEngine._addMoneyToEarningsBox(want, type);
		}

		private _addXPToPlayer(type: Data.ReelAction, xp: number) {
			this.#xpEarned[type] = (this.#xpEarned[type] ?? 0) + xp;
		}

		private _addToCustomer(prop: "satisfaction" | "mood" | "lust", value: number) {
			this.#customers[this.#selectedCustomer][prop] =
				Math.clamp(this.#customers[this.#selectedCustomer][prop] + value, 0, 100);
			const div = $(`#Whore${_.capitalize(prop)}${this.#selectedCustomer}`)
			div.animate({width: this.#customers[this.#selectedCustomer][prop], easing: 'linear'}, 500);
			// div.width(this._Customers[this._SelectedCustomer].Mood);
		}

		private _addMoneyToCustomer(money: number) {
			this.#customers[this.#selectedCustomer].spent += money;
		}

		// endregion

		// region CALLBACKS AND EVENT HANDLERS

		/**
		 * Called by the Reel object when the slot animation has finished playing. Passes the results as an array of strings.
		 * @param results the slot wheel results.
		 */
		private _slotCB(results: number[]) {
			// Convert array positions back into tokens.
			const spinResult = results.map((r, ind) => this.getSlots()[ind].symbol(r));

			const slotMap: Record<Data.ReelAction | "wild", number[]> = {
				ass: [],
				hand: [],
				bj: [],
				tits: [],
				wild: [],
				fem: [],
				perv: [],
				beauty: [],
			};

			// Create mapping.

			for (const key of Object.keys(slotMap)) {
				for (let i = 0; i < spinResult.length; i++) {
					if (spinResult[i] == key) slotMap[key].push(i);
					// track wilds separately
					if (SlotEngine._isWild(spinResult[i]) && slotMap[key].includes(i) == false && !SlotEngine._isWild(key)) slotMap[key].push(i);
				}
			}

			// Transform map into an array of arrays representing sequences.
			const sequences: Record<string, number[][]> = {};
			for (const key of Object.keys(slotMap)) {
				// We have a minimum of 3 sequential numbers in a map category
				if (slotMap[key].length >= 3) sequences[key] = SlotEngine._splitReel((slotMap[key]));
			}

			// Iterate through sequence map to calculate payout.
			let payout: Payout[] = [];
			for (const skey of Object.keys(sequences)) {
				const key = skey as Data.ReelAction;
				const seq = sequences[key];

				for (let i = 0; i < seq.length; i++) {
					if (seq[i].length >= 3) {
						payout = payout.concat(this._calculateJackpot(slotMap, key, seq[i]));
					}
				}
			}

			let timeout = 500;
			if (payout.length > 0) {
				// Let's calculate all of the satisfaction, mood and lust at one go.
				this._addToCustomer('satisfaction', payout.reduce((a, o) => {return (a + o.sat)}, 0));
				this._addToCustomer('mood', payout.reduce((a, o) => {return (a + o.mood)}, 0));
				this._addToCustomer('lust', payout.reduce((a, o) => {return (a + o.lust)}, 0));

				// sneak peak
				const keys = Object.keys(payout[0]);
				for (let i = 0; i < keys.length; i++) {
					if (keys[i] == 'slot' || keys[i] == 'type' || keys[i] == 'want') continue;
					timeout = 300 * i;
					setTimeout(this._doSlotResults.bind(this, payout, keys[i]), timeout);
				}

				timeout += 500; // For finishing up spin.
				this.#misses = 0;

				const jackpot = ['JACKPOT', 'JACKPOT', 'JACKPOT', 'DOUBLE JACKPOT', 'TRIPLE JACKPOT', 'QUADRUPLE JACKPOT', 'SUPER JACKPOT', 'MEGA JACKPOT', 'ULTRA JACKPOT'];
				const colors = ['lime', 'lime', 'lime', 'gold', 'gold', 'orange', 'orange', 'cyan', 'purple'];
				this._risingDialog(jackpot[payout.length - 1], colors[payout.length - 1]);
				$('#DesperationButton').removeClass("DesperationButtonActivated").addClass("DesperationButtonDeactivated");
			} else {
				// We missed.
				this.#misses++;
				if (this.#misses == 5) {
					this._risingDialog('DESPERATION MODE UNLOCKED', 'deepPink');
					$('#DesperationButton').addClass("DesperationButtonActivated").removeClass("DesperationButtonDeactivated");
				} else {
					if (this.#misses < 5) { // No desperation mode yet.
						this._risingDialog('MISS', 'red');
					} else {
						this._whackPlayer();
					}
				}
			}

			setTimeout(this._drawDesperationBar.bind(this), 100);
			setTimeout(this._unlockSpinnerCB.bind(this), timeout);
		}

		/**
		 * Called by timeout after a spin has resolved. All animation events should be in process before this
		 * fires off. Check to see if customer is satisfied and remove him from available selection.
		 */
		private _unlockSpinnerCB() {
			if (this.#selectedCustomer != -1 && typeof this.#selectedCustomer !== 'undefined' && this.#customers.length > this.#selectedCustomer) {
				const oCustomer = this.#customers[this.#selectedCustomer];
				if (typeof oCustomer !== 'undefined' && oCustomer.satisfaction >= 100) this._removeCustomer(this.#selectedCustomer);
			}
			this.#spinning = false;
		}

		/**
		 * Print and animate the results of the slot spin. Modify the earnings and customer objects.
		 * @param results Array of payout objects
		 * @param key The key we are checking on the payout object.
		 */
		private _doSlotResults(results: Payout[], key: string) {
			for (let i = 0; i < results.length; i++) {
				let html = "";
				switch (key) {
					case 'payout':
						html = `<span class="SlotPayout">$${results[i][key]}</span>`;
						this._addMoneyToPlayer(results[i][key], results[i].want, results[i].type);
						break;
					case 'xp':
						html = `<span class="SlotXp">${results[i][key]} XP</span>`;
						this._addXPToPlayer(results[i].type, results[i][key]);
						break;
					case 'mood':
						html = `<span class="SlotMood">${results[i][key]} MOOD</span>`;
						break;
					case 'lust':
						html = `<span class="SlotLust">${results[i][key]} LUST</span>`;
						break;
					case 'sat':
						const wantType = results[i].type;
						const ob = this.#customers[this.#selectedCustomer];
						if (ob.satisfiedWants.hasOwnProperty(wantType)) ob.satisfiedWants[wantType]! += 1;
						break;
				}

				if (html == "") continue;
				this._slotWinDiv(results[i].slot, html);
			}
		}

		/**
		 * Called when you click the 'spin' button.
		 */
		private _spinEH() {
			if (this.#spins <= 0) return this._dialog("NO SPINS LEFT");
			if (this.#spinning == true) return; // No effect if spin in process. Maybe make it play a sound later?
			if (this.getSlots().length < 3) return this._dialog("EQUIP 3 OR MORE SLOTS");
			if (this.#selectedCustomer == -1) return this._dialog("SELECT CUSTOMER");
			this.#spinning = true;
			this.#spins--;
			this._redrawSpins();
			this._reels.spin();
		}

		private _cashOutEH() {
			if (this.#spinning == true) return;
			SugarCube.Engine.play("WhoreEnd");
		}

		/**
		 * Attached to the Customer's UI at creation.
		 * @param e event object
		 */
		private _customerSelectCB(e: {data: {customer: number}}) {
			if (this.#spinning == true) return; // Can't switch customers in the middle of a spin.
			$(`#WhoreCustomer${this.#selectedCustomer}`).css('border-color', 'saddlebrown'); // old customer
			$(`#WhoreCustomer${e.data.customer}`).css('border-color', 'lime'); // new customer

			this.#selectedCustomer = e.data.customer;
		}

		/**
		 * Attached to the 'Buy more spins' button.
		 */
		private _buyEnergyCB() {
			if (this.getSlots().length < 3) return this._dialog("EQUIP 3 SLOTS");

			if (this.#spins >= 20 || this._player.getStat(Stat.Core, CoreStat.Energy) < 1) return;
			this.#spins += 5;
			this._player.adjustStat(CoreStat.Energy, -1);

			// Redraw Energy bar
			$('#Energy').html(PR.pStatMeter(CoreStat.Energy, this._player, false));
			this._redrawSpins();
		}

		private _desperationButtonCB() {
			if (this.#misses < 5 || this.#spinning) return;
			this.#desperationSpins++;
			this.#spinning = true;
			this._reels.win(this.#customers[this.#selectedCustomer].wants[0]);
			$('#DesperationButton').removeClass("DesperationButtonActivated").addClass("DesperationButtonDeactivated");
		}

		private _drawUICB() {
			this.#spinning = false;
			this._drawCustomers();
			this._drawStatus();
			this._drawSlots();

			if (this.getSlots().length < 3) {
				this._dialog("EQUIP 3 SLOTS");
			} else {
				if (this.#selectedCustomer == -1) this._dialog("SELECT CUSTOMER");
			}
		}

		private _drawSlotInventoryCB() {
			this._drawSlotInventory();
		}

		private _selectSlotCB(e: {data: {slot: number}}) {
			this.#selectedSlot = e.data.slot;
			this._drawSlotInventory();
		}

		private _equipSlotCB(e: JQuery.ClickEvent<unknown, {id: Data.ItemNameTemplate<Items.Category.Reel>}>) {
			this.#player.equipReel(e.data.id, this.#selectedSlot);
			this._drawSlotInventory();
		}

		private _removeSelectedSlotCB() {
			this.#player.removeReel(this.#selectedSlot);
			this._drawSlotInventory();
		}

		// endregion
	}

	/**
	* This is the customer object for the game. It just holds data.
	* @param {string} dataKey
	*/
	class Customer {
		/** Name of the customer. */
		name = "Pirate Pete";
		/** Current Lust. */
		lust = 1;
		/** Original Lust. */
		oLust = 1;
		/** Current Mood. */
		mood = 1;
		/** Original Mood. */
		oMood = 1;
		/** Current Satisfaction. */
		satisfaction = 1;
		/** Payout on scale of 1 to 5 */
		payout = 5;
		/** Bonus Reward Category */
		bonus: Data.Whoring.BonusSource = "PirateSlut";
		/** Wildcard category on bonus */
		bonusCat: Data.Whoring.BonusCategory = CoreStat.Perversion;
		wants: Data.Whoring.SexActStr[] = [];
		/** How much we've paid out. */
		spent = 0;
		/** NPC tag. */
		tag = "";
		satisfiedWants: Partial<Record<Data.ReelAction, number>> = {};

		constructor(dataKey: string) {
			if (typeof Data.whoring === 'undefined' || Data.whoring.hasOwnProperty(dataKey) == false) {
				alert("Bad call to new Customer() : dataKey '" + dataKey + "' is undefined or missing.");
				return;
			}

			const dict = Data.whoring[dataKey];

			// Set Name.
			this.name = dict.names.randomElement();
			if (typeof dict.title !== 'undefined' && dict.title != null) {this.name = dict.title + " " + this.name;}

			// Set the Payout
			this.payout = (dict.minPay >= dict.maxPay) ? dict.minPay : dict.minPay + Math.round(Math.random() * (dict.maxPay - dict.minPay));

			// Get the NPC from the Player.
			this.tag = dict.npcTag;
			const npc = setup.world.npc(this.tag);

			if (typeof npc !== 'undefined') {
				// Adjust the mood and lust of the customer by the mood of the stored associated NPC object.
				this.oMood = Math.floor((npc.mood / 2)) + Math.floor(Math.random() * (npc.mood / 2));
				this.mood = this.oMood;
				this.oLust = Math.floor((npc.lust / 2)) + Math.floor(Math.random() * (npc.lust / 2));
				this.lust = this.oLust;
			}

			// Set the Wants array of the character.
			let count = 0;
			while (this.wants.length < 3) {
				++count;
				const tmp = randomProperty(dict.wants);
				if (!this.wants.contains(tmp)) {
					this.satisfiedWants[tmp] = 0;
					this.wants.push(tmp);
				}
				if (count >= 30) break;
			}

			// Set the bonus and bonus category.
			this.bonus = randomProperty(_.merge({}, dict.bonus.body, dict.bonus.style));
			this.bonusCat = randomProperty(dict.bonus.stat);
		}

		mostSatisfied(): {want: Data.ReelAction, val: number} {
			const sortable: {want: Data.ReelAction, val: number}[] = [];
			for (const e of Object.entries(this.satisfiedWants)) {
				sortable.push({want: e[0], val: e[1]});
			}

			sortable.sort((a, b) => {
				return b.val - a.val;
			});

			return sortable[0];
		}
	}

	type EZSlotsResultCallback = (res: number[]) => void;
	interface EZSlotsOptions {
		reelCount?: number;
		symbols: Data.ReelAction[] | Data.ReelAction[][]
		startingSet?: number[];
		winningSet: number[];
		width?: number;
		height?: number;
		time?: number;
		callback?: EZSlotsResultCallback;
	}
	class EZSlots {
		private readonly _reelCount: number;
		private readonly _symbols: Data.ReelAction[] | Data.ReelAction[][];
		private _sameSymbolsEachSlot = true;
		private readonly _startingSet: number[] | undefined;
		private _winningSet: number[];
		private readonly _width: number;
		private readonly _height: number;
		/** time in millis for a spin to take */
		private readonly _time: number;
		private readonly _howManySymbolsToAppend: number;
		/** location for selected symbol... needs to be a few smaller than howManySymbolsToAppend */
		private readonly _endingLocation = 7;
		/** jquery object reference to main wrapper */
		private readonly _jqo: JQuery<HTMLElement>;
		/** callback function to be called once slots animation is finished */
		private readonly _callback: EZSlotsResultCallback | undefined;

		/** jquery object reference to strips sliding up and down */
		private readonly _jqoSliders: JQuery<HTMLElement>[] = [];

		#engine: SlotEngine;
		/**
		 * Gratuitously stolen from Kirk Israel and modified for this game.
		 */
		constructor(engine: SlotEngine, id: string, options: EZSlotsOptions) {
			this.#engine = engine;
			// set some variables from the options, or with defaults.
			this._reelCount = options.reelCount ?? 3; // how many reels, assume 3
			this._symbols = options.symbols;
			this._sameSymbolsEachSlot = true;
			this._startingSet = options.startingSet;
			this._winningSet = options.winningSet;
			this._width = options.width ?? 100;
			this._height = options.height ?? 100;
			this._time = options.time ? (options.time * 1000) : 6500; //
			// this.howManySymbolsToAppend = Math.round(this.time/325); //how many symbols each spin adds
			this._howManySymbolsToAppend = 30;
			this._endingLocation = 7; //
			this._jqo = $("#" + id); //
			this._callback = options.callback; //

			this._init();
		}

		spin() {
			return this._spinAll();
		}

		win(symbol: Data.ReelAction) {
			return this._spinWin(symbol);
		}

		// to initialize we construct the correct number of slot windows
		// and then populate each strip once
		private _init() {
			this._jqo.addClass("ezslots"); // to get the css goodness
			// figure out if we are using the same of symbols for each window - assume if the first
			// entry of the symbols is not a string we have an array of arrays
			if (typeof this._symbols[0] != 'string') {
				this._sameSymbolsEachSlot = false;
			}
			// make each slot window
			for (let i = 0; i < this._reelCount; i++) {
				const jqoSlider = $('<div class="slider"></div>');
				const jqoWindow = $(`<div class="window" id="SlotWindow_${i}"></div>`);
				this._scaleJqo(jqoWindow).append(jqoSlider); // make window right size and put slider in it
				this._jqo.append(jqoWindow); // add window to main div
				this._jqoSliders.push(jqoSlider); // keep reference to jqo of slider
				this._addSymbolsToStrip(jqoSlider, i, false, true); // and add the initial set
			}
		}

		// convenience function since we need to apply width and height to multiple parts
		private _scaleJqo(jqo: JQuery<HTMLElement>) {
			jqo.css("height", `${this._height}px`).css("width", `${this._width}px`);
			return jqo;
		}

		// add the various symbols - but take care to possibly add the "winner" as the symbol chosen
		private _addSymbolsToStrip(jqoSlider: JQuery<HTMLElement>, whichReel: number, shouldWin: boolean, isInitialCall = false) {
			const symbolsToUse = this._sameSymbolsEachSlot ? this._symbols as Data.ReelAction[] : this._symbols[whichReel] as Data.ReelAction[];
			const chosen = shouldWin ? this._winningSet[whichReel] : Math.floor(Math.random() * symbolsToUse.length);
			for (let i = 0; i < this._howManySymbolsToAppend; i++) {
				let ctr = (i == this._endingLocation) ? chosen : Math.floor(Math.random() * symbolsToUse.length);
				if (i == 0 && isInitialCall && this._startingSet) {
					ctr = this._startingSet[whichReel];
				}
				// we nest "content" inside of "symbol" so we can do vertical and horizontal centering more easily
				// var jqoContent = $("<div class='content'>"+symbolsToUse[ctr]+"</div>");
				const contentDiv = "<div class='content " + SlotEngine.getSlotClass(symbolsToUse[ctr]) + "'></div>";
				const jqoContent = $(contentDiv);
				this._scaleJqo(jqoContent);
				const jqoSymbol = $("<div class='symbol'></div>");
				this._scaleJqo(jqoSymbol);
				jqoSymbol.append(jqoContent);
				jqoSlider.append(jqoSymbol);
			}
			return chosen;
		}

		// to spin, we add symbols to a strip, and then bounce it down
		private _spinOne(jqoSlider: JQuery<HTMLElement>, whichReel: number, shouldWin: boolean) {
			const heightBefore = parseInt(jqoSlider.css("height"), 10);
			const chosen = this._addSymbolsToStrip(jqoSlider, whichReel, shouldWin);
			const marginTop = -(heightBefore + ((this._endingLocation) * this._height));
			if (SugarCube.settings.fastAnimations == true) {
				jqoSlider.css('margin-top', `${marginTop}px`);
			} else {
				jqoSlider.stop(true, true).animate(
					{"margin-top": `${marginTop}px`},
					{duration: this._time + Math.round(Math.random() * 1000), easing: "easeOutElastic"});
			}
			return chosen;
		}

		private _spinAll(shouldWin = false) {
			const results: number[] = [];
			for (let i = 0; i < this._reelCount; i++) {
				results.push(this._spinOne(this._jqoSliders[i], i, shouldWin));
			}

			if (this._callback) {
				if (SugarCube.settings.fastAnimations == true) {
					this._callback(results);
				} else {
					setTimeout(this._callback.bind(this, results), this._time);
				}
			}

			return results;
		}

		/**
		 * Hack by me to force a specific symbol.
		 */
		private _spinWin(symbol: Data.ReelAction) {
			const winningSet: number[] = [];
			for (const arr of this._symbols) { // the array of options on the reel.
				if (arr.indexOf(symbol) != -1) {
					winningSet.push(arr.indexOf(symbol));
				} else if (arr.indexOf('perv') != -1) {
					winningSet.push(arr.indexOf('perv'));
				} else if (arr.indexOf('fem') != -1) {
					winningSet.push(arr.indexOf('fem'));
				} else if (arr.indexOf('beauty') != -1) {
					winningSet.push(arr.indexOf('beauty'));
				} else {
					winningSet.push(0);
				}
			}

			this._winningSet = winningSet;
			this._spinAll(true);
		}
	}
}
