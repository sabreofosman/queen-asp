/**
* Unit system class
*/
namespace App.UnitSystems {
	class Facet {
		/**
		 * Conversion from CGS
		 */
		baseFactor: number;
		scaleFactors: number[];
		symbols: string[];
		units: string[];
		unitsPlural: string[];
		delimeters: string;

		constructor(baseFactor: number, scaleFactors: number[], symbols: string[], units: string[], unitsPlural: string[], delimeters: string) {
			this.baseFactor = baseFactor;
			this.scaleFactors = scaleFactors;
			this.symbols = symbols;
			this.units = units;
			this.unitsPlural = unitsPlural;
			this.delimeters = delimeters;
		}
	}

	export class AbstractUnitSystem {
		braCups: Record<number, string>;
		length: Facet;
		mass: Facet;

		constructor(lengthFacet: Facet, massFacet: Facet, braCups: Record<number, string>,) {
			this.braCups = braCups;
			this.length = lengthFacet;
			this.mass = massFacet;
		}

		private static _biggestUnit(facet: Facet, x: number): number {
			let v = AbstractUnitSystem.value(facet, x);
			const scaleFactors = facet.scaleFactors;
			let i = 0;
			for (i = 1; i < scaleFactors.length; ++i) {
				const nextFactor = scaleFactors[i - 1];
				if (v < nextFactor) {
					break;
				}
				const realUnits = v / nextFactor;
				const roundedCount = Math.floor(realUnits);
				v -= roundedCount * nextFactor;
			}
			return i - 1;
		}

		static value(facet: Facet, x: number): number {
			return Math.round(facet.baseFactor * x);
		}

		static valueString(facet: Facet, x: number, compact = false, sample?: number): string {
			const v = AbstractUnitSystem.value(facet, x);
			const compts = [v];
			const units: string[] = facet.units;
			const scaleFactors: number[] = facet.scaleFactors;
			let maxUnitIndex = 0;
			if (compact) {
				if (sample !== undefined) {
					maxUnitIndex = AbstractUnitSystem._biggestUnit(facet, sample);
				} else {
					maxUnitIndex = units.length - 1;
				}
			}
			for (let i = 1; i <= maxUnitIndex; ++i) {
				const nextFactor = scaleFactors[i - 1];
				if (compts[i - 1] < nextFactor) {
					break;
				}
				const realUnits = compts[i - 1] / nextFactor;
				const roundedCount = Math.floor(realUnits);
				compts[i - 1] = compts[i - 1] - roundedCount * nextFactor;
				compts.push(roundedCount);
			}
			let res = "";
			const symbols = facet.symbols;

			for (let j = compts.length - 1; j >= 0; --j) {
				if (compts[j] !== 0) {
					if (res.length > 0) res += facet.delimeters;
					res += `${compts[j]}${facet.delimeters}${symbols[j]}`;
				}
			}
			return res;
		}

		lengthValue(x: number): number {
			return AbstractUnitSystem.value(this.length, x);
		}

		lengthString(x: number, compact= false, sample?: number): string {
			return AbstractUnitSystem.valueString(this.length, x, compact, sample);
		}

		massValue(x: number): number {
			return AbstractUnitSystem.value(this.mass, x);
		}

		massString(x: number, compact = false, sample?: number): string {
			return AbstractUnitSystem.valueString(this.mass, x, compact, sample);
		}

		cupString(bustStatVal: number): string {
			// stat val is the cup size in EU system multiplied by 3
			const cupSizeEU = bustStatVal / 3.;
			// cup size is bust - underbust difference in cm divided by 2
			const diff = this.lengthValue(cupSizeEU * 2 + 4);
			const cups = this.braCups;
			const cupStr = "";
			let lastSmallerCup= 0;
			for (const cup of Object.keys(cups)) {
				const nCup = parseInt(cup);
				if (diff < nCup) break;
				lastSmallerCup = nCup;
			}
			return cupStr + cups[lastSmallerCup];
		}
	}

	export class Metric extends AbstractUnitSystem {
		constructor() {
			super(new Facet(
				1.0, [100, 1000],
				["cm", "m", "km"],
				["centimeter", "meter", "kilometer"], ["centimetres", "meters", "kilometers"],
				"&thinsp;"),
			new Facet(
				1.0, [1000, 1000],
				["g", "kg", "t"],
				["gram", "kilogram", "tonne"], ["grams", "kilograms", "tonnes"],
				"&thinsp;"),
			{ // difference in cm, taken from http://brasizecalculator.eu/ for underbust of  80 cm
				0: "0",
				5: "AA",
				7: "A",
				9: "B",
				11: "C",
				13: "D",
				15: "E",
				17: "F",
				19: "G",
				21: "H",
				23: "I",
				25: "J",
				27: "K",
				29: "L",
				31: "M",
				33: "N",
				35: "O",
				37: "P",
				39: "Q",
				41: "R",
				43: "S",
				45: "T",
				47: "U",
				49: "V",
				51: "W",
				53: "X",
				55: "Y",
				57: "Z",
				59: "ZA",
				61: "ZB",
				63: "ZC",
				65: "ZD",
				67: "ZE",
				69: "ZF",
				71: "ZG",
				73: "ZH"
			});
		}
	}

	export class Imperial extends AbstractUnitSystem {
		constructor() {
			super(new Facet(
				1.0 / 2.54, [12, 5280],
				["&prime;", "&Prime;", "m"],
				["inch", "foot", "mile"], ["inches", "feet", "miles"],
				""),
			new Facet(
				1. / 28.3495, [16, 14, 160], // 28.349523125
				["oz", "lb", "st", "t"],
				["ounce", "pound", "stone", "ton"], ["ounces", "pounds", "stones", "tons"],
				"&thinsp;"),

			{ // difference in inches, taken from http://brasizecalculator.eu/
				0: "0",
				1: "AA",
				2: "A",
				3: "B",
				4: "C",
				5: "D",
				6: "DD",
				7: "E",
				8: "F",
				9: "FF",
				10: "G",
				11: "GG",
				12: "H",
				13: "HH",
				14: "J",
				15: "JJ",
				16: "K",
				17: "KK",
				18: "L",
				19: "LL",
				20: "M",
				21: "MM",
				22: "N",
				23: "NN",
				24: "O",
				25: "OO",
				26: "P",
				27: "PP",
				28: "Q",
				29: "QQ",
				30: "R",
				31: "RR",
				32: "S",
				33: "SS",
				34: "T",
				35: "TT",
				36: "U",
				37: "UU",
				38: "V",
				39: "VV",
				40: "W",
				41: "WW",
				42: "X",
				43: "XX",
				44: "Y",
				45: "YY",
				46: "Z",
				47: "ZZ"
			});
		}
	}
}

namespace App {
	/**
	* Unit system class
	*/
	export const unitSystem = (function () {
		let system: UnitSystems.AbstractUnitSystem = new UnitSystems.Imperial();

		function lengthValue(x: number): number {
			return system.lengthValue(x);
		}

		function lengthString(x: number, compact?: boolean, sample?: number): string {
			return system.lengthString(x, compact, sample);
		}

		function massValue(x: number): number {
			return system.massValue(x);
		}

		function massString(x: number, compact?: boolean, sample?: number): string {
			return system.massString(x, compact, sample);
		}

		function cupString(bustStatValue: number): string {
			return system.cupString(bustStatValue);
		}

		function unitSettingChangedHandler() {
			switch (SugarCube.settings.units) {
				case "Imperial":
					system = new UnitSystems.Imperial();
					break;
				case "Metric":
					system = new UnitSystems.Metric();
					break;
			}
		}

		return {
			lengthValue: lengthValue,
			lengthString: lengthString,
			massValue: massValue,
			massString: massString,
			cupString: cupString,
			unitSettingChangedHandler: unitSettingChangedHandler
		};
	}())
}
