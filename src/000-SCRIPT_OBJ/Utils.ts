namespace App.Utils {
	/**
	 * replaces special HTML characters with their '&xxx' forms
	 */
	export function escapeHtml(text: string): string {
		const map: Record<string, string> = {
			'&': '&amp;',
			'<': '&lt;',
			'>': '&gt;',
			'"': '&quot;',
			"'": '&#039;'
		};
		return text.replace(/[&<>"']/g, m => map[m]);
	}

	export function gaussianPair(mu: number, sigma: number): [number, number] {
		let u = Math.random();
		let v = Math.random();
		let s = u * u + v * v;
		while (s === 0 || s > 1) {
			u = Math.random();
			v = Math.random();
			s = u * u + v * v;
		}

		const f = Math.sqrt(-2 * Math.log(s) / s) * sigma;
		return [u * s + mu, v * f + mu];
	}

	// eslint-disable-next-line @typescript-eslint/ban-types
	export function dirObject(o: object): string {
		const res: string[] = [];
		for (const e of Object.entries(o)) {
			res.push(`${String(e[0])} = ${e[1]}`); // eslint-disable-line @typescript-eslint/restrict-template-expressions
		}
		return res.join(', ');
	}
}

namespace App {
	export function isDefined<T>(v: T): v is NonNullable <T> {
		return !_.isNil(v);
	}

	export function assertIsDefined<T>(val: T): asserts val is NonNullable<T> {
		if (!isDefined(val)) {
			throw `Expected 'val' to be defined, but received ${val}`; // eslint-disable-line @typescript-eslint/restrict-template-expressions
		}
	}

	export function assertIsFinalRating(val: Data.RatingValue): asserts val is Data.RatingFinalValue {
		if (!(typeof val === "string" || Array.isArray(val))) {
			throw Error("rating value is not final");
		}
	}

	export function valueOrDefault<T>(v: T, def: NonNullable<T>): NonNullable<T> {
		return isDefined(v) ? v : def;
	}

	export function registerTravelDestination(destinations: Record<string, Data.Travel.Destinations>): Record<string, Data.Travel.Destinations> {
		return Object.assign(App.Data.Travel.destinations, destinations);
	}

	export function registerPassageDisplayNames(names: Record<string, DynamicText>): Record<string, DynamicText> {
		return Object.assign(App.Data.Travel.passageDisplayNames, names);
	}

	interface PassageLink {
		link: string;
		text?: string;
		setter?: string;
	}

	const sc2LinkSimple = /\[\[([^\|^(?:\->)]+)(?:\||(?:\->))?(?:([^\]]+))?\](?:\[(.+)\])?\]/;

	export function parseSCPassageLink(s: string): PassageLink {
		const m = sc2LinkSimple.exec(s);
		if (!m) {
			throw `Link parsing error: '${s} does not look like a passage link to me`;
		}
		return {link: m[1], text: m[2], setter: m[3]};
	}

	/**
	 * Returns random element from a weighted table
	 */
	export function randomProperty<T extends PropertyKey>(choices: Partial<Record<T, number>>): EnumerablePropertyKey<T> {
		const totalWeight = _.sum(Object.values(choices));
		let rnd = _.random(totalWeight, false);
		const res = Object.entries(choices).find((e) => {
			rnd -= e[1];
			return rnd <= 0;
		})
		if (!res) {
			throw Error("Could not find anything in the choices object! Perhaps it's empty?")
		}
		return res[0];
	}

	export function splitSlashed<T1 extends string, T2 extends string>(id: `${T1}/${T2}`): [T1, T2] {
		const parts = id.split('/');
		return [parts[0] as T1, parts[1] as T2];
	}

	export function splitUnderscored<T1 extends string, T2 extends string>(id: `${T1}_${T2}`): [T1, T2] {
		const parts = id.split('_');
		return [parts[0] as T1, parts[1] as T2];
	}

	export namespace Effects {
		export type TemplateAction = (player: Entity.Player, amount: number) => void;
		export type TemplateData = Record<string, {points: number, value?: number, suffixCount: number}>;
		export interface ActionSet {
			knowledge: string;
			knowledgeSuffix: string;
			pointsFactor?: number;
			valueFactor?: number;
			data: TemplateData;
		}
		export function makeEffects(action: TemplateAction, data: Record<string, ActionSet>): Record<string, Data.EffectDesc> {
			const res: Record<string, Data.EffectDesc> = {};
			for (const baseName in data) {
				const set = data[baseName];
				for (const [act, d] of Object.entries(set.data)) {
					res[_.snakeCase(`${baseName}_${act}`).toUpperCase()] = {
						fun: p => action(p, d.points * (set.pointsFactor ?? 1)),
						value: (d.value ?? d.points) * (set.valueFactor ?? 1),
						knowledge: [`${set.knowledge}${(set.knowledgeSuffix ?? "").repeat(d.suffixCount)}`]
					};
				}
			}
			return res;
		}
	}
}

Object.defineProperty(Array.prototype, 'randomElement', {
	configurable: true,
	writable: true,

	value(this: unknown[]) {
		if (this.length === 0) {
			throw "randomElement: Array is empty";
		}

		return this.random();
	}
});

Object.defineProperty(Object.prototype, 'append', {
	value(target: Record<PropertyKey, unknown>, source: Record<PropertyKey, unknown>): Record<PropertyKey, unknown> {
		for (const p in source) {
			if (target.hasOwnProperty(p)) {
				throw Error(`Property ${p} already defined`);
			}
			target[p] = source[p];
		}
		return target;
	}
})

Object.defineProperty(Math, 'mean', {
	configurable: true,
	writable: true,

	value(v0: number, ...vn: number[]): number {
		let sum = v0;
		for (const v of vn) {
			sum += v;
		}
		return sum / (vn.length + 1);
	}
});

Object.defineProperty(_, 'pascalCase', {
	value(this: typeof _, s: string): string {
		return s.length ? s[0].toUpperCase() + this.camelCase(s.slice(1)) : s;
	}
});
