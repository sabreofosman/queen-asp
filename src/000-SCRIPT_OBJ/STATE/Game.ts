namespace App.GameState {
	export const enum Difficulty {
		Normal,
		Landlubber,
		PatheticLoser
	}

	export interface StoreInventory {
		lastStocked: number;
		inventory: Data.StoreInventoryItem[];
		rare: Data.StoreInventoryItem[];
	}

	export type NPCQuestFlagValue = string | number;
	export interface NPCQuestFlag {
		value: NPCQuestFlagValue;
		tmp: number;
	}

	export interface NPC extends NPCStats {
		questFlags: {[x: string]: NPCQuestFlag};
	}

	export interface IGame {
		gameBookmark: string;
		menuAction: string | Entity.NPC | Job;
		whore: {
			link: string;
			passage?: string;
		}
		difficultySetting: Difficulty;
		inIntro?: 1;
		tutorial: {
			whoring: boolean;
		}
		// Game/Environment Variables
		day: number;
		phase: DayPhase; // 0 morning, 1 afternoon, 2 evening, 3 night, 4 late night
		storeInventory: Record<string, StoreInventory>;
		character: Record<string, Human>;
		npc: Record<string, NPC>;
		debugMode?: boolean;
	}

	export class Game implements IGame {
		gameBookmark = "";
		menuAction = "";
		whore = {link: ""};
		difficultySetting = Difficulty.Landlubber;
		inIntro?: 1 = 1;
		tutorial = {whoring: false};
		// Game/Environment Variables
		day = 1;
		phase = Phase.Morning;
		storeInventory = {};
		npc = {};
		character: Record<string, Human> = {};
	}

	function makePC(): Player {
		const pc = new Player();
		pc.age = 18;
		pc.name.given = Data.names.Male.randomElement();
		pc.name.original = "Smithe";
		pc.name.slave = Data.names.Sissy.randomElement();
		pc.gender = Gender.Shemale;

		$.extend(true, pc.faceData, Data.DAD.facePresets['Default 1']);
		for (const skill of Object.keys(pc.skill.value)) {
			pc.gameStats.skills[skill] = {failure: 0, success: 0};
		}

		pc.body.value = {
			face: 20,
			lips: 20,
			bust: 0,
			bustFirmness: 30,
			lactation: 0,
			ass: 2,
			waist: 46,
			hips: 2,
			penis: 44,
			balls: 44,
			hair: 5,
			height: 60,
		};
		pc.skill.value.dancing = 5;
		pc.skill.value.singing = 5;
		pc.skill.value.cooking = 5;
		pc.skill.value.cleaning = 5;
		pc.skill.value.serving = 5;
		pc.core.value = {
			energy: 3,
			femininity: 0,
			fitness: 20,
			futa: 0,
			health: 80,
			hormones: 20,
			nutrition: 100,
			perversion: 0,
			toxicity: 30,
			willpower: 100
		};

		pc.inventory = {
			cosmetics: {
				"hair accessories": 10,
				"hair products": 10,
				"basic makeup": 10
			},
			reel: {
				commonWhore: 2,
				commonWildcard: 1
			}
		};

		function makeEquipRecord(name: string) {
			const tmp = Data.clothes[name];
			return Entity.ClothingManager.equipmentRecord(Items.makeId(Items.Category.Clothes, name), tmp.locked ?? false);
		}

		pc.equipment = {
			wig: makeEquipRecord("cheap wig"),
			neck: makeEquipRecord("collar"),
			bra: makeEquipRecord("chemise"),
			panty: makeEquipRecord("cotton bloomers"),
			stockings: makeEquipRecord("cotton stockings"),
			dress: makeEquipRecord("cotton dress"),
			shoes: makeEquipRecord("worn boots"),
			penis: makeEquipRecord("chastity cage"),
		};

		return pc;
	}

	function makeGirlfriend(pcSlaveName: string): Human {
		const gf = new Human();

		gf.age = 18;
		gf.gender = Gender.Female;
		gf.name.original = "Rowe";
		do {
			gf.name.given = Data.names.Female.randomElement();
		} while (gf.name.given === pcSlaveName);

		gf.body.value = {
			face: 70,
			lips: 50,
			bust: 17,
			bustFirmness: 30,
			lactation: 0,
			ass: 20,
			waist: 28,
			hips: 23,
			penis: 0,
			balls: 0,
			hair: 45,
			height: 36,
		};
		gf.skill.value.dancing = 15;
		gf.skill.value.singing = 15;
		gf.skill.value.cleaning = 5;
		gf.skill.value.serving = 25;
		gf.skill.value.blowJobs = 15;
		gf.skill.value.handJobs = 20;
		gf.core.value = {
			energy: 3,
			femininity: 80,
			fitness: 15,
			futa: 0,
			health: 80,
			hormones: 90,
			nutrition: 100,
			perversion: 20,
			toxicity: 0,
			willpower: 100
		};
		return gf;
	}

	function makeNewGame(): Game {
		const res = new Game();

		res.character.pc = makePC();
		res.character.girlfriend = makeGirlfriend(res.character.pc.name.slave);

		res.npc = {};

		res.storeInventory = {
			galley: {lastStocked: 0, inventory: [], rare: []},
			cargo: {lastStocked: 0, inventory: [], rare: []},
			islaTavern: {lastStocked: 0, inventory: [], rare: []},
			islaStore: {lastStocked: 0, inventory: [], rare: []},
			smugglers: {lastStocked: 0, inventory: [], rare: []},
			peacock: {lastStocked: 0, inventory: [], rare: []},
			goldenGoods: {lastStocked: 0, inventory: [], rare: []},
			lustyLass: {lastStocked: 0, inventory: [], rare: []},
			saucySlattern: {lastStocked: 0, inventory: [], rare: []},
			yvonneStore: {lastStocked: 0, inventory: [], rare: []},
			marketStore: {lastStocked: 0, inventory: [], rare: []},
			bazaarStore: {lastStocked: 0, inventory: [], rare: []},
			emily: {lastStocked: 0, inventory: [], rare: []},
			bradshaw: {lastStocked: 0, inventory: [], rare: []},
			barnabyLong: {lastStocked: 0, inventory: [], rare: []},
			meghanLong: {lastStocked: 0, inventory: [], rare: []},
			isabella: {lastStocked: 0, inventory: [], rare: []},
			fineas: {lastStocked: 0, inventory: [], rare: []},
			bella: {lastStocked: 0, inventory: [], rare: []}
		};

		return res;
	}

	export function setNewGameVariables(): void {
		Object.assign(State.variables, makeNewGame());
	}
}
