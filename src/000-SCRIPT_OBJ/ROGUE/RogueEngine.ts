
namespace App.Rogue {
	export class Engine {
		// Internal Variables, most of them are initialized in loadScene()
		#scheduler!: InstanceType<typeof ROT.Scheduler.Speed>;
		#engine!: ROT.Engine;
		#player!: Player;
		#level!: Level;
		#display!: ROT.Display;
		#textBuffer!: TextBuffer;
		#sideBar!: Sidebar;
		#elementSelector!: string;
		#passage!: string;
		#depth = 1;
		#maxDepth = 100;
		#lastDrawnCells: number[] = [];
		#currentDrawnCells: number[] = [];
		#title = "Dungeon";

		private static _instance: Engine;

		static get instance(): Engine {
			if (Engine._instance === undefined) {
				Engine._instance = new Engine();
			}
			return Engine._instance;
		}

		loadScene(elementSelector: string, exitPassage: string, depth: number): void {
			this.#elementSelector = elementSelector;

			this.#title = "Abamond Caves";
			this.#passage = exitPassage;
			this.#display = new ROT.Display({width: 100, height: 40, fontSize: 12});
			this.#scheduler = new ROT.Scheduler.Speed();
			this.#engine = new ROT.Engine(this.#scheduler);
			this.#depth = depth;
			this.#maxDepth = 100;
			this.#lastDrawnCells = [];

			this.#player = new Rogue.Player({ch: "@", fg: "hotpink", bg: null});

			this.#level = Engine._genLevel(this.#depth);
			const bufferSize = 5;
			const size = this.#level.getSize();
			// side info panel
			this.#sideBar = new Rogue.Sidebar({
				display: this.#display,
				position: new Rogue.XY(size.x + 1, 0),
				size: new Rogue.XY(20, size.y + bufferSize)
			});
			// Bottom chat window
			this.#textBuffer = new Rogue.TextBuffer({
				display: this.#display,
				position: new Rogue.XY(0, size.y),
				size: new Rogue.XY(size.x, bufferSize),
				lines: bufferSize
			});

			this._switchLevel();
		}

		getDepth(): number {return this.#depth;}

		descend(): void {
			this.#depth += 1;
			const savedLevel = JobEngine.getJobFlag<number>(setup.player, "ABAMOND_CAVE_LEVEL");
			if (!savedLevel || savedLevel < this.#depth) {
				JobEngine.setJobFlag(setup.player, "ABAMOND_CAVE_LEVEL", this.#depth);
			}
			this.#lastDrawnCells = []; // Clear buffer
			this.#level = Engine._genLevel(this.#depth);
			this._switchLevel();
		}

		ascend(): void {
			console.log(`Current depth: ${this.#depth}`);
			this.#depth -= 1;
			console.log(`Moving to depth: ${this.#depth}`);
			this.#lastDrawnCells = [];
			if (this.#depth <= 0) {
				console.debug("Exiting dungeon…");
				setup.world.phase = Phase.Night;
				SugarCube.Engine.play(this.#passage);
			} else {
				this.#level = Engine._genLevel(this.#depth);
				this._switchLevel(true);
			}
		}

		drawUI(): void {
			$(document).one(":passageend", () => this._drawUICB());
		}

		draw(xy: XY): void {
			const entity = this.#level.getEntityAt(xy);
			if (entity) {
				const visual = entity.visual;
				this.#display.draw(xy.x, xy.y, visual.ch, visual.fg, visual.bg);
			}
		}

		redraw(xy: XY): void {
			this.#lastDrawnCells = this.#currentDrawnCells;
			this.drawWithLight(xy);
		}

		over(): void {
			this.#engine.lock();
			/* FIXME show something */
		}

		private static _genLevel(depth: number) {
			const level = new Rogue.Level(depth);
			level.generateMap(80, 40);
			level.fillBorders(80, 40);
			level.genEntrance();
			level.genExit();
			level.genTreasure();
			level.genMonsters();
			return level;
		}

		refreshStatus(): void {
			this.#sideBar.clear();
			this.#sideBar.flush();
			// Draw GUI.
			this.#sideBar.title(this.#title);
			this.#sideBar.level(`Level: ${this.#depth}/${this.#maxDepth}`);
			this.#sideBar.torches(`Torches: ${Player.torches}`);
			this.#sideBar.shovels(`Shovels: ${Player.shovels}`);
			this.#sideBar.help();
		}

		get scheduler(): InstanceType<typeof ROT.Scheduler.Speed> {
			return this.#scheduler;
		}

		get textBuffer(): TextBuffer {
			return this.#textBuffer;
		}

		get engine() {
			return this.#engine;
		}

		get level() {
			return this.#level;
		}

		get player() {
			return this.#player;
		}

		private _switchLevel(opt = false) {
			this.#scheduler.clear();
			this.#scheduler.add(this.#player, true);

			if (opt) {
				this.#level.setEntity(this.#player, this.#level.exitXY);
			} else {
				this.#level.setEntity(this.#player, this.#level.entranceXY);
			}
			const size = this.#level.getSize();

			const bufferSize = 5;
			this.#display.setOptions({width: size.x + 20, height: size.y + bufferSize});

			this.#textBuffer.clear();

			this.#display.clear();
			this.drawWithLight(this.#player.xy);

			this.refreshStatus();

			/* add new beings to the scheduler */
			const beings = this.#level.getBeings();
			for (const b of beings.values()) {
				if (b instanceof Being) {
					this.#scheduler.add(b, true);
				}
			}
		}

		drawWithLight(pxy: XY): void {
			/** light level */
			const ll = this.#player.lightLevel;
			this.#currentDrawnCells = this.#level.cellsAtRadius(pxy.x, pxy.y, ll, false).map(c => xyToNumber(c));

			const walls = new Array<Array<number>>(ll * 2 + 1);
			const visibilityMap = new Array<Array<boolean>>(ll * 2 + 1);
			for (let i = 0; i < walls.length; ++i) {
				walls[i] = new Array<number>(ll * 2 + 1);
				walls[i].fill(0); // starting with no walls
				visibilityMap[i] = new Array<boolean>(ll * 2 + 1);
				visibilityMap[i].fill(false); // everything is invisible
			}

			const x0 = pxy.x - ll;
			const y0 = pxy.y - ll;

			const wallXY = this.#level.getWalls();
			for (const xy of this.#currentDrawnCells) {
				if (wallXY.get(xy)) {
					const xyVal = numberToXY(xy);
					walls[xyVal.x - x0][xyVal.y - y0] = 1; // put a wall
				}
			}

			// ray tracing to detect visible walls
			function traceLine(i: number, j: number) { // i and j are orts
				for (let l = 1; l <= ll; ++l) {
					const x = Math.round(ll + l * i);
					const y = Math.round(ll + l * j);
					visibilityMap[x][y] = true;
					if (walls[x][y] === 1) { // wall found
						break;
					}
				}
			}

			// player's position is always visible
			visibilityMap[ll][ll] = true;

			traceLine(1, 0);
			traceLine(-1, 0);
			traceLine(0, 1);
			traceLine(0, -1);
			for (let t = 1; t <= ll; ++t) {
				const vX = ll;
				const vY = t;
				const vL = Math.sqrt(vX * vX + vY * vY);
				const iX = vX / vL;
				const iY = vY / vL;

				traceLine(iX, iY);
				traceLine(-iX, iY);
				traceLine(iX, -iY);
				traceLine(-iX, -iY);

				traceLine(iY, iX);
				traceLine(-iY, iX);
				traceLine(iY, -iX);
				traceLine(-iY, -iX);
			}

			for (const xyNum of this.#currentDrawnCells) {
				const xy = numberToXY(xyNum);
				if (visibilityMap[xy.x - x0][xy.y - y0]) {
					this.draw(xy);
				} else {
					this.#display.draw(xy.x, xy.y, null, null, null);
				}
			}

			if (this.#lastDrawnCells.length > 0) {
				const mapped = Engine._mapCells(this.#currentDrawnCells, this.#lastDrawnCells);
				this._drawBlank(mapped);
			}
		}

		private static _mapCells(dest: number[], source: number[]) {
			return source.filter(key => !dest.includes(key));
		}

		private _drawBlank(cells: number[]) {
			for (const c of cells) {
				const xy = numberToXY(c);
				this.#display.draw(xy.x, xy.y, null, null, null);
			}
		}
		/** CALL BACKS **/

		private _drawUICB() {
			$(this.#elementSelector).append(this.#display.getContainer() as HTMLElement);
			this.#engine.start();
		}
	}
}
