namespace App {
	/**
	 * Simple class for managing the playing of music files and other sound effects in the game.
	 * @class AudioEngine
	 */
	export class Audio {
		#bgmTracks: TwineSugarCube.AudioList | null = null;
		#init = false;

		init(): void {
			if (this.#init == true) return;

			this.#bgmTracks = SimpleAudio.lists.get("bgm");
			assertIsDefined(this.#bgmTracks);
			this.#bgmTracks.loop(true);
			this.#init = true;
		}

		get bgmOn(): boolean {return this.#bgmTracks?.isPlaying() ?? false;}
		get bgmOff(): boolean {return !this.bgmOn;}

		/**
		 * Stop or pause the current playing bgm (also any other ones)
		 */
		private _stopAllBGM(pauseBGM = false) {
			if (pauseBGM) {
				this.#bgmTracks?.pause();
			} else {
				this.#bgmTracks?.stop();
			}
		}

		isPlaying(): boolean {
			return this.#bgmTracks?.isPlaying() ?? false;
		}

		/**
		 * Turn the BGM system on.
		 */
		startBGM(resumeBGM: boolean): void {
			if (this.isPlaying() == false && this.#init == true) {
				console.log("AudioEngine: BGM turned on.");
				if (!resumeBGM) {
					this.#bgmTracks?.stop();
				}
				this.#bgmTracks?.play();
			}
		}

		/**
		 * Turn the BGM system off.
		 */
		stopBGM(pauseBGM: boolean): void {
			console.log("AudioEngine: BGM turned off.");
			this._stopAllBGM(pauseBGM);
		}

		/**
		 * Called from Settings when game loads.
		 */
		volumeInit(): void {
			try {
				this.volumeAdjust();
			} catch (ex) {
				console.log("Waiting for audio system to be initialized…");
				setTimeout(this.volumeInit.bind(this), 1000);
			}
		}

		/**
		 * Called from Settings when the user changes the volume slider
		 */
		volumeAdjust(): void {
			const vol = settings.bgmVolume / 10; // turn into range between 0 and 1
			if (vol == 0 && this.bgmOn) {
				this.stopBGM(true);
			} else if (vol != 0 && this.bgmOff) {
				this.startBGM(true);
			}

			this.#bgmTracks?.volume(vol);
		}

		transition(_passage: string): void {
			if (settings.bgmVolume / 10 != 0 && this.bgmOff) {
				this.startBGM(true); // resume playing any bgm files on transition.
			}
		}
	}
}
