namespace App.Combat {
	/**
	 * This class is used for spectating and betting on fights.
	 * TODO: Allow the player to input the amount they want to bet.
	 * TODO: Calculate odds of match up and pay out on them.
	 */
	export class SpectatorEngine {
		// initialized by loadEncounter()
		#OP_A!: Combatant;
		#OP_B!: Combatant;
		#BET_A!: number;
		#BET_B!: number;
		#BetOn: string | null = null;
		#ChatLog: string[] = [];
		#LootBuffer = [];
		#MaxBet = 0;
		#Club!: string;
		#CurrentOpponent!:Combatant;

		get opponentA(): Combatant {return this.#OP_A;}
		get opponentB(): Combatant {return this.#OP_B;}
		get betOn(): string | null {return this.#BetOn;}
		get club(): string {return this.#Club;}
		get maxBet(): number {return this.#MaxBet;}
		get currentOpponent(): Combatant {return this.#CurrentOpponent;}
		get currentTarget(): Combatant {return this.currentOpponent.id == 'A' ? this.opponentB : this.opponentA;}
		get winner(): string {
			if (this.opponentA == null && this.opponentB == null) return "no one";
			return this?.opponentA?.isDead == true ? this.opponentB?.name : this.opponentA?.name;
		}

		get payoutStr(): HTMLSpanElement | null {
			if (this.betOn == null) return null;

			const res = document.createElement('span');
			if (this.didIWin) {
				res.appendTextNode("You win ");
			} else {
				res.appendTextNode("You lose ");
			}
			UI.appendFormattedFragment(res, {text: this.maxBet.toString(), style: 'item-money'});
			res.appendTextNode(" coins!");
			return res;
		}

		loadEncounter(club: string, maxBet: number): void {
			sessionStorage.setItem('QOS_FIGHTCLUB_NAME', club);
			sessionStorage.setItem('QOS_FIGHTCLUB_MAXBET', maxBet.toString());

			this.#BET_A = 0;
			this.#BET_B = 0;
			this.#ChatLog = [];
			this.#LootBuffer = [];
			this.#Club = club;
			this.#MaxBet = maxBet;

			this.#OP_A = this._addEnemy(App.Combat.clubBetData[club].randomElement());
			this.#OP_A.id = 'A';

			// Don't allow the same "name" to fight each other. Useful for unique encounters.
			while (true) {
				this.#OP_B = this._addEnemy(App.Combat.clubBetData[club].randomElement());
				this.#OP_B.id = 'B';
				if (this.opponentA?.name != this.opponentB?.name) break;
			}

			this.#CurrentOpponent = this.#OP_A;
		}

		drawUI(): void {
			if (this.club == null) { // Reload from session state if browser refreshed.
				this.loadEncounter(sessionStorage.getItem('QOS_FIGHTCLUB_NAME') as string,
					parseInt(sessionStorage.getItem('QOS_FIGHTCLUB_MAXBET') as string));
			}

			$(document).one(":passageend", this._drawUI.bind(this));
		}

		drawResults(): void{
			$(document).one(":passageend", this._drawWinLog.bind(this));
		}

		betLink(target: Combatant): HTMLAnchorElement {
			return UI.passageLink(`Bet ${this.maxBet} coins on  ${target.title}`, 'FightBetOverUI',
				() => {
					setup.world.nextPhase(1);
					setup.spectator.placeBet(target.id);
				});
		}

		watchLink(): HTMLAnchorElement {
			return UI.passageLink(`Just watch`, 'FightBetOverUI',
				() => {
					setup.world.nextPhase(1);
					setup.spectator.spectateFight();
				});
		}

		betALink(): HTMLAnchorElement {
			return this.betLink(this.opponentA);
		}

		betBLink(): HTMLAnchorElement {
			return this.betLink(this.opponentB);
		}

		placeBet(targetId: string): void {
			this.#BetOn = targetId;
			this._simulateCombat();
			this._doPayout();
		}

		spectateFight(): void {
			this.#BetOn = null;
			this._simulateCombat();
		}

		get didIWin(): boolean {
			const winner = this.opponentA?.isDead == true ? this.opponentB : this.opponentA;
			return (winner.id == this.betOn);
		}

		private _drawUI(): void {
			this._drawEnemyContainers();
		}

		private _drawEnemyContainers(): void {
			const root = $('#EnemyGUI');
			root.empty();
			SpectatorEngine._drawPortrait(this.opponentA);
			const vs = $('<div>').attr('id', 'versusTxt').addClass('EnemyContainer').text('VS');
			root.append(vs);
			SpectatorEngine._drawPortrait(this.opponentB);
		}

		private static _drawPortrait(fighter: Combatant): void {
			const root = $('#EnemyGUI');
			const container = $('<div>').attr('id', 'EnemyContainer' + fighter.id).addClass('EnemyContainer');
			container.append('<div>').addClass('EnemyTitle').text(fighter.title);
			const frame = $('<div>').attr('id', 'EnemyPortrait' + fighter.id).addClass('EnemyPortrait');
			frame.addClass(fighter.portrait);
			container.append(frame);
			root.append(container);
		}

		private _drawWinLog(): void {
			const root = $('#WinDiv');
			const log = $('<div>').attr('id', 'WinChatLog');
			root.append(log);
			for (let i = 0; i < this.#ChatLog.length; i++) {
				log.append("<P class='ChatLog'>" + this.#ChatLog[i] + "</P>");
			}

			log.scrollTop(log.prop("scrollHeight"));
		}

		/**
		 * Add an enemy object to the encounter.
		 * @param e enemy object data
		 */
		private _addEnemy(e: string): Combatant {
			const d = Combat.enemyData[e];
			const enemy = new Combat.Combatant(d,
				this._updateNPCStatusCB.bind(this),
				this._updatePlayerStatusCB.bind(this),
				this._chatLogCB.bind(this));
			return enemy;
		}

		private static _formatMessage(m: string, o: Combatant) {
			if (_.isNil(o)) {
				return m;
			}

			const npc = setup.world.npc("Dummy"); // Fake NPC
			npc.data.name = o.name; // Swapsie name
			// Custom replacer(s)
			/*
			m = m.replace(/(ENEMY_([0-9]+))/g, function (m, f, n) {
				return "<span style='color:cyan'>" + that.#enemies[n].Name + "</span>";
			}); */

			m = m.replace(/NPC_PRONOUN/g, () => o.gender == Gender.Male ? "his" : "her");

			m = PR.tokenizeString(setup.player, npc, m);
			return m;
		}

		/**
		 * Cheap and dirty hack to re-use combat messages.
		 */
		private _substituteYou(m: string, o: Combatant) {
			const target = (o.id === this.opponentA.id ? this.opponentB : this.opponentA);
			m = m.replace(/((\s)(you)([^a-zA-z]))/g, (_m, _p1, p2: string, _p3, p4: string) => p2 + target.name + p4);

			m = m.replace(/^You/g, target.name);
			return m;
		}

		private _writeMessage(m: string, o: Combatant) {
			m = this._substituteYou(m, o);
			m = SpectatorEngine._formatMessage(m, o);
			this.#ChatLog.push(m);
		}

		// Dummy callbacks

		private _chatLogCB(m: string, o: Combatant) {
			this._writeMessage(m, o);
		}

		private _updatePlayerStatusCB(_m: Combatant) {
			// no-op
		}

		private _updateNPCStatusCB(_npc: Combatant) {
			// no-op
		}

		private _nextRound() {
			this.#CurrentOpponent = this.currentOpponent.id == 'A' ? this.opponentB : this.opponentA;
		}

		private _simulateCombat() {
			while (this.opponentA?.isDead != true && this.opponentB?.isDead != true) {
				this.currentOpponent.startTurn();
				this.currentOpponent.doAI(this.currentTarget, this._chatLogCB.bind(this));
				this.currentOpponent.endTurn();
				this._nextRound();
			}
		}

		private _doPayout() {
			if (this.didIWin == true) {
				setup.player.earnMoney(this.maxBet, GameState.CommercialActivity.Betting);
			} else {
				setup.player.spendMoney(this.maxBet, GameState.CommercialActivity.Betting);
			}
		}
	}
}
