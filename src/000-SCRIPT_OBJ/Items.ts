namespace App {
	export namespace Items {
		export interface ConsumableItemTypeMap {
			[Category.Drugs]: Items.Consumable,
			[Category.Food]: Items.Consumable,
			[Category.Cosmetics]: Items.Cosmetics,
			[Category.MiscConsumable]: Items.Consumable,
			[Category.MiscLoot]: Items.Consumable,
			[Category.LootBox]: Items.Consumable,
		}
		export interface InventoryItemTypeMap extends ConsumableItemTypeMap {
			[Category.Quest]: Items.QuestItem,
			[Category.Reel]: Items.Reel,
		}

		export interface EquipmentItemTypeMap {
			[Category.Clothes]: Items.Clothing,
			[Category.Weapon]: Items.Clothing,
		}

		export interface ItemTypeMap extends InventoryItemTypeMap, EquipmentItemTypeMap {
		}

		export type CategoryConsumable = keyof ConsumableItemTypeMap;
		export type CategoryEquipment = keyof EquipmentItemTypeMap;
		export type CategoryAny = keyof ItemTypeMap;

		export function isEquipmentCategory(cat: Category): cat is CategoryEquipment {
			return cat === Category.Clothes || cat === Category.Weapon;
		}

		export function isInventoryCategory(cat: Category): cat is CategoryEquipment {
			return !isEquipmentCategory(cat);
		}

		export const enum ClothingType {
			Accessory = "accessory",
			Clothing = "clothing",
			OnePiece = "onePiece",
			Weapon = "weapon"
		}

		export type CategoryInventory = keyof InventoryItemTypeMap;

		/**
		 * Fetch the default charges from an items data record.
		 */
		export function getCharges(type: CategoryInventory, tag: string): number;
		export function getCharges(rec: Data.ConsumableItemDesc<Data.ItemTypeInventory>): number;
		export function getCharges(typeOrRec: CategoryInventory | Data.ConsumableItemDesc<Data.ItemTypeInventory>, tag?: string): number {
			return ((typeof typeOrRec === "string") ?
				fetchData(typeOrRec, tag!) as Data.ConsumableItemDesc<Data.ItemTypeInventory> : typeOrRec).charges ?? 1;
		}

		export type ItemDescDictionary<T extends keyof Data.ItemTypeDescMap> = Record<string, Data.ItemTypeDescMap[T]>;

		/**
		 * Return the appropriate data dictionary for the item in question. If item type is invalid, returns null.
		 */
		export function tryGetItemsDictionary<T extends keyof Data.ItemTypeDescMap>(type: T): ItemDescDictionary<T> {
			type R = ItemDescDictionary<T>;
			switch (type) {
				case Category.Drugs: return Data.drugs as R;
				case Category.Food: return Data.food as R;
				case Category.Cosmetics: return Data.cosmetics as R;
				case Category.MiscConsumable: return Data.misc as R;
				case Category.MiscLoot: return Data.miscLoot as R;
				case Category.Clothes:
				case Category.Weapon: return Data.clothes as R;
				case Category.Quest: return Data.questItems as R;
				case Category.LootBox: return Data.lootBoxes as R;
				case Category.Reel: return Data.slots as R;
			}
			throw "WTF, TS?!";
		}

		/**
		 * Return the appropriate data dictionary item for the item in question. If item type is invalid or
		 * item is not found, shows error message and throws exception
		 */
		export function fetchData<T extends keyof Data.ItemTypeDescMap>(type: T, name: string): Data.ItemTypeDescMap[T] {
			const itemsDictionary = tryGetItemsDictionary(type);
			if (itemsDictionary == null) {
				const errorMessage = `Invalid item type: ${type}`;
				alert(errorMessage);
				throw new Error(errorMessage);
			}

			const itemData = itemsDictionary[name];
			if (itemData === null || itemData === undefined) {
				const errorMessage = `Item with name "${name}" of type "${type}" not found`;
				alert(errorMessage);
				throw new Error(errorMessage);
			}
			return itemData;
		}

		export function isInventoryDesc(d: Data.ItemTypeDescMap[CategoryAny]): d is Data.InventoryItemDescAny {
			return d.hasOwnProperty("shortDesc");
		}

		export function isEquipmentDesc(d: Data.ItemTypeDescMap[CategoryAny]): d is Data.ClothingItemDesc {
			return d.hasOwnProperty("slot");
		}

		/**
		 * Automatically calculate the price of an item based on it's attributes.
		 * @param category the dictionary name
		 * @param tag the key of the item
		 * @returns the price in gold coins.
		 */
		export function calculateBasePrice(category: Category, tag: string): number {
			try {
				const item = factory(category, tag);
				const price = item.basePrice * (item.data.priceCoefficient ?? 1);
				return (price == 0) ? 100 : price;
			} catch (e) {
				// Whatever, I guess?
				return 100;
			}
		}

		function calculateEffectPrice(effectDict: Record<string, Data.EffectDescBase>, effects: string[]): number { // TODO Clothing effects
			if (effects.length == 0) return 0;
			let price = 0;
			if (effects.length > 1) {
				price += effects.reduce((accumulator, effect) => accumulator + effectDict[effect].value, 0);
			} else {
				price += effectDict[effects[0]].value;
			}

			return price;
		}

		/**
		 * Search and find clothing items by a variety of categories.
		 */
		export function listAllClothes(category: string, rank: string, slot: ClothingSlot):
			{tag: string; rank: string; slot: string; cat: string[];}[] {
			type R = {tag: string, rank: string, slot: string, cat: string[]};
			let clothes: R[] = [];

			for (const k in Data.clothes) {
				if (!Data.clothes.hasOwnProperty(k)) continue;
				const cat = Data.clothes[k].style;
				if (typeof cat !== 'undefined' && cat.length >= 1) {
					const item = {tag: k, rank: Data.clothes[k].rarity, slot: Data.clothes[k].slot, cat: cat};
					if (category != null && $.inArray(category, cat) != -1) {
						clothes.push(item);
					} else if (category == null) {
						clothes.push(item);
					}
				}
			}

			if (rank != null) clothes = clothes.filter(ob => ob.rank == rank);
			if (slot != null) clothes = clothes.filter(ob => ob.slot == slot);

			return clothes;
		}

		export type ItemListingRecord = {
			cat: Items.Category,
			tag: string, name: string,
			desc: string,
			price: number,
			style: string | null,
			category: string[] | null,
			useEffects: string[],
			wearEffects: string[],
			activeEffects: string[],
			meta: string[];
			inMarket: boolean;

		};

		/**
		 * Just a debug function
		 */
		export function listAllPrices(category: Category, filter = 0): ItemListingRecord[] {
			const out: ItemListingRecord[] = [];
			const d = tryGetItemsDictionary(category);
			/* eslint-disable @typescript-eslint/no-unsafe-assignment */
			/* eslint-disable @typescript-eslint/no-unsafe-member-access */
			/* eslint-disable @typescript-eslint/no-explicit-any */
			for (const k of Object.keys(d)) {
				const item = d[k];
				const res = {
					cat: category,
					tag: k,
					name: item.name,
					desc: (<any>item).shortDesc ?? "",
					price: calculateBasePrice(category, k),
					style: (<any>item).style ?? null,
					category: (<any>item).category ?? null,
					useEffects: (<any>item).effects ?? [],
					wearEffects: (<any>item).wearEffect ?? [],
					activeEffects: (<any>item).activeEffect ?? [],
					meta: (<any>item).meta ?? null,
					inMarket: item.inMarket ?? true,
				};
				/* eslint-enable @typescript-eslint/no-explicit-any */
				/* eslint-enable @typescript-eslint/no-unsafe-member-access */
				/* eslint-enable @typescript-eslint/no-unsafe-assignment */
				out.push(res);
			}

			out.sort((a, b) => a.price - b.price);
			return (filter) ? out.filter(a => a.price < filter) : out;
		}

		/**
		 * Fetch a random item based on price / category.
		 */
		export function pickItem(category: Category | Category[],
			filterOb: {price?: number, category?: string, metaKey?: string}): ItemListingRecord | null {
			let items: ItemListingRecord[] = [];
			console.debug(filterOb);
			if (Array.isArray(category)) {
				while (category.length > 0) {
					const tmpCategory = category.randomElement();
					category.splice(category.indexOf(tmpCategory), 1); // Pop the item off.
					items = items.concat(listAllPrices(tmpCategory, filterOb.price));
				}
			} else {
				items = listAllPrices(category, filterOb.price);
			}

			// Ignore items that we don't want showing up randomly.
			if (items.length > 0) items = items.filter(o => o.inMarket);
			// Filter on clothing category
			if (items.length > 0 && (filterOb.hasOwnProperty("category")))
				items = items.filter(o => o.category?.contains(filterOb.category));
			// Filter on meta data
			if (items.length > 0 && (filterOb.hasOwnProperty("metaKey")))
				items = items.filter(o => o.meta?.contains(filterOb.metaKey));
			console.debug(items);
			return items.length > 0 ? items.randomElement() : null;
		}

		function makeItem<T extends keyof ItemTypeMap>(type: T, tag: string, inventory: ItemTypeInventoryTypeMap[T]): ItemTypeMap[T] {
		// function makeItem<T extends keyof ItemTypeMap>(type: T, tag: string, inventory?: any): ItemTypeMap[T] {
			type R = ItemTypeMap[T];
			switch (type) {
				case Category.Clothes:
				case Category.Weapon:// might change this in the future, for now weapons are "clothing"
					return new Clothing(type, tag, inventory as ItemTypeInventoryTypeMap[Category.Clothes]) as R;
				case Category.Cosmetics:
					return new Cosmetics(tag, inventory as ItemTypeInventoryTypeMap[Category.Cosmetics]) as R;
				case Category.Drugs:
				case Category.Food:
				case Category.MiscConsumable:
				case Category.MiscLoot:
				case Category.LootBox:
					return new Consumable(type, tag, inventory as ItemTypeInventoryTypeMap[Category.Drugs]) as R;
				case Category.Quest:
					return new QuestItem(tag, inventory as ItemTypeInventoryTypeMap[Category.Quest]) as R;
				case Category.Reel:
					return new Reel(tag, inventory as ItemTypeInventoryTypeMap[Category.Reel]) as R;
				default:
					throw ""
			}
		}

		export function factory<T extends keyof ItemTypeMap>(
			type: T, tag: string, inventory?: ItemTypeInventoryTypeMap[T], count?: number): ItemTypeMap[T] {
			// @ts-expect-error we pass undefined inventpry object, which is a hack
			const o = makeItem(type, tag, inventory);
			// HACK: when an item is created in an "disconnected" state, without an inventory manager,
			// this probably mean we want to get a description from the item. Thus we discard charges
			// from the data record in that case.
			if (o instanceof Consumable && inventory !== undefined && count == undefined) {
				count = getCharges(o.data);
				if (count > 1) o.addCharges(count - 1);
			}

			return o;
		}

		/**
		 * Generate an item id
		 */
		export function makeId<T extends Category>(category: T, tag: string): Data.ItemNameTemplate<T> {
			return `${category}/${tag}`;
		}

		export function splitId(id: Data.ItemNameTemplateConsumable): {category: keyof ConsumableItemTypeMap, tag: string};
		export function splitId(id: Data.ItemNameTemplateInventory): {category: keyof InventoryItemTypeMap, tag: string};
		export function splitId(id: Data.ItemNameTemplateEquipment): {category: Category.Clothes | Category.Weapon, tag: string};
		export function splitId<TType extends Category>(id: Data.ItemNameTemplate<TType>): {category: TType, tag: string};
		export function splitId<TType extends Category>(id: Data.ItemNameTemplate<TType>): {category: TType, tag: string} {
			const t = id.split('/');
			return {category: t[0] as TType, tag: t[1]};
		}

		export function isEquipment(id: Data.ItemNameTemplateAny): id is Data.ItemNameTemplateEquipment {
			return isEquipmentCategory(splitId(id).category);
		}

		export function isInventory(id: Data.ItemNameTemplateAny): id is Data.ItemNameTemplateInventory {
			return isInventoryCategory(splitId(id).category);
		}

		// #region Item classes

		export class BaseItem {
			#tag: string;
			#itemClass: Category;
			#desc: Data.ItemDesc;

			constructor(itemClass: Category, tag: string, desc: Data.ItemDesc) {
				this.#itemClass = itemClass;
				this.#tag = tag;
				this.#desc = $.extend(true, {}, desc);
			}

			get tag(): string {
				return this.#tag;
			}

			get itemClass(): Category {
				return this.#itemClass;
			}

			get name(): string {
				return this.#desc.name;
			}

			protected get desc(): Data.ItemDesc {
				return this.#desc;
			}
		}
		export abstract class Item<T extends keyof ItemTypeInventoryTypeMap> extends BaseItem {
			private readonly _inventory: ItemTypeInventoryTypeMap[T];

			constructor(itemClass: T, tag: string, d: Data.ItemTypeDescMap[T],
				inventory: ItemTypeInventoryTypeMap[T]) {
				super(itemClass, tag, d);
				this._inventory = inventory;
			}

			get id(): Data.ItemNameTemplate<T> {
				return Items.makeId(this.itemClass as T, this.tag);
			}

			get inventory(): ItemTypeInventoryTypeMap[T] {
				return this._inventory;
			}

			get data(): Data.ItemTypeDescMap[T] {
				return super.desc as Data.ItemTypeDescMap[T];
			}

			abstract get description(): string;

			// eslint-disable-next-line class-methods-use-this
			get basePrice(): number {
				return 100; // Whatever, I guess?
			}
		}

		export class Clothing extends Item<Category.Clothes | Category.Weapon> {
			#knowledge: string[] = [];
			constructor(category: Category.Clothes | Category.Weapon, tag: string, inventoryObj: Entity.ClothingManager) {
				super(category, tag, fetchData(category, tag), inventoryObj);

				this.#knowledge = this.wearEffect.reduce(
					(acc, itm) => {return acc.concat(Data.effectLibClothingWear[itm].knowledge)}, this.#knowledge);
				this.#knowledge = this.activeEffect.reduce(
					(acc, itm) => {return acc.concat(Data.effectLibClothingActive[itm].knowledge)}, this.#knowledge);
			}

			/**
			 * Short description of item
			 */
			override get description(): string {
				let result = this.data.shortDesc;
				// if (result instanceof String) result = String(result);
				if (typeof (result) !== "string" || result === "") return this.name;
				result = result.replace("{COLOR}", String(this.data.color));

				if (this.isLocked) result += " <span style='color:red'>(Locked)</span>";

				return result;
			}

			/**
			 * Examine an item, relate detailed description and any knowledge.
			 */
			examine(player: Entity.Player, omitDescription = false): string {
				if (omitDescription == true) {
					let output = this.style.join(", ") + " " + this.rankDescription + "\n";
					const usages = player.getHistory("clothingEffectsKnown", this.tag);
					const max = Math.min(usages, this.getKnowledge().length);

					for (let i = 0; i < max; i++)
						output += PR.pEffectMeter(this.getKnowledge()[i], this) + " ";
					return output;
				}

				let output = this.data.longDesc;
				const usages = player.getHistory("clothingEffectsKnown", this.tag);

				output += "\n";

				output += "@@.state-neutral;Style Categories  @@ ";
				output += this.style.join(", ");
				output += "\n";
				output += "@@.state-neutral;Rank @@ " + this.rankDescription + "\n";
				const max = Math.min(usages, this.getKnowledge().length);

				for (let i = 0; i < max; i++)
					output += PR.pEffectMeter(this.getKnowledge()[i], this) + " ";
				return output;
			}

			printEffectsOnly(player: Entity.Player): string {
				let output = "";
				const usages = player.getHistory("clothingEffectsKnown", this.tag);
				const max = Math.min(usages, this.getKnowledge().length);

				for (let i = 0; i < max; i++)
					output += PR.pEffectMeter(this.getKnowledge()[i], this) + " ";
				return output;
			}

			/**
			 * Apply (worn) affects overnight like skill gains / body mods.
			 * Apply all effects of this clothing item, usually overnight.
			 */
			applyEffects(player: Entity.Player): void {
				for (const e of this.wearEffect) {
					if (setup.world.debugMode == true) {
						console.group("Applying effect: ", e);
					}
					Data.effectLibClothingWear[e].fun(player, this);
					if (setup.world.debugMode == true) {
						console.groupEnd();
					}
				}
			}

			/**
			 * Learn Knowledge sleeping…
			 */
			learnKnowledge(player: Entity.Player): string {
				const flag = (player.getHistory("clothingKnowledge", this.tag) > 0);
				const know = player.getHistory("clothingEffectsKnown", this.tag);
				if (flag && know < this.getKnowledge().length) {
					const output = "@@.state-neutral;You learn something… your " + this.name +
						" has an effect!@@ " + PR.pEffectMeter(this.getKnowledge()[know], this);
					player.addHistory("clothingEffectsKnown", this.tag, 1);
					player.removeHistory("clothingKnowledge", this.tag);
					return output;
				}
				return "";
			}

			/**
			 * List all categories this piece of clothing belongs to.
			 */
			get style(): Data.Fashion.Style[] {
				return this.data.style ?? [Data.Fashion.Style.Ordinary];
			}

			/**
			 * Return the style of the item if it fits within the category supplied.
			 */
			categoryBonus(cat: Data.Fashion.Style): number {
				return this.style.includes(cat) ? this.styleBonus : 0;
			}

			/**
			 * What slot this is worn in.
			 */
			get slot(): ClothingSlot {
				return this.data.slot;
			}

			/**
			 * Slots to disable when this is worn.
			 */
			get restrict(): ClothingSlot[] {
				return this.data.restrict ?? [this.data.slot];
			}

			/**
			 * Effects that happen when the item is worn, overnight.
			 */
			get wearEffect(): string[] {
				return this.data.wearEffect ?? [];
			}

			/**
			 * Effects that can be applied to active skill rolls.
			 */
			get activeEffect(): string[] {
				return this.data.activeEffect ?? [];
			}

			/**
			 * Color of item.
			 */
			get color(): string {
				return this.data.color;
			}

			// STYLE TABLE
			// TYPE         COMMON  UNCOMMON    RARE    LEGENDARY
			// ACCESSORY    3       6           9       12
			// CLOTHING     5       10          15      20
			// ONE PIECE    10      20          30      40
			/**
			 * Style bonus related to quality of clothing item.
			 */
			get styleBonus(): number {
				const bonus: Record<ClothingType, Record<Rarity, number>> = {
					[ClothingType.Accessory]: {[Rarity.Common]: 3, [Rarity.Uncommon]: 6, [Rarity.Rare]: 9, [Rarity.Legendary]: 12},
					[ClothingType.Weapon]: {[Rarity.Common]: 3, [Rarity.Uncommon]: 6, [Rarity.Rare]: 9, [Rarity.Legendary]: 12},
					[ClothingType.Clothing]: {[Rarity.Common]: 5, [Rarity.Uncommon]: 10, [Rarity.Rare]: 15, [Rarity.Legendary]: 20},
					[ClothingType.OnePiece]: {[Rarity.Common]: 10, [Rarity.Uncommon]: 20, [Rarity.Rare]: 30, [Rarity.Legendary]: 40}
				};

				return this.data.type === ClothingType.Weapon ? 0 : bonus[this.data.type][this.data.rarity];
			}

			/**
			 * Show stars relating to rank on clothing.
			 */
			get rankDescription(): string {
				switch (this.data.rarity) {
					case Items.Rarity.Common: return "<span style='color:gold'>&#9733;</span>";
					case Items.Rarity.Uncommon: return "<span style='color:gold'>&#9733;&#9733;</span>";
					case Items.Rarity.Rare: return "<span style='color:gold'>&#9733;&#9733;&#9733;</span>";
					case Items.Rarity.Legendary: return "<span style='color:gold'>&#9733;&#9733;&#9733;&#9733;</span>";
				}
			}

			get rankNumber(): number {
				switch (this.data.rarity) {
					case Items.Rarity.Common: return 1;
					case Items.Rarity.Uncommon: return 2;
					case Items.Rarity.Rare: return 3;
					case Items.Rarity.Legendary: return 4;
				}
			}

			get hairColor(): Data.HairColor {
				return this.data.color as Data.HairColor ?? "black";
			}

			get hairLength(): number {
				return this.data.slot === ClothingSlot.Wig ? (this.data as Data.WigItemDesc).hairLength ?? 0 : 0;
			}

			get hairStyle(): string {
				return this.data.slot === ClothingSlot.Wig ? (this.data as Data.WigItemDesc).hairStyle ?? "" : "";
			}

			get hairBonus(): number {
				return this.data.slot === ClothingSlot.Wig ? (this.data as Data.WigItemDesc).hairBonus ?? 0 : 0;
			}

			/**
			 * Locked items cannot be removed unless unlocked.
			 */
			get isLocked(): boolean {
				if (this.inventory?.isWorn(this.id, this.slot)) {
					return this.inventory.isLocked(this.slot);
				}
				return false;
			}

			/**
			 * Query the effects (if present) on this piece of clothing and return the bonus
			 */
			getBonus(skillName: Data.WearSkill): number {
				return this.activeEffect.reduce(
					(ac, ef) => ac + Data.effectLibClothingActive[ef].fun(skillName, this), 0);
			}

			/**
			 * Checks whether this item can be sold in shops
			 */
			get inMarket(): boolean {
				return this.data.inMarket ?? true;
			}

			getKnowledge(): string[] {return this.#knowledge.sort();}

			override get basePrice(): number {
				let price = 0;
				// Base price set on style rank.
				if (typeof this.data.rarity !== 'undefined') {
					switch (this.data.rarity) {
						case Rarity.Common: price = 50; break;
						case Rarity.Uncommon: price = 100; break;
						case Rarity.Rare: price = 250; break;
						case Rarity.Legendary: price = 600; break;
					}
				} else {
					price = 50;
				}

				// modify based on type
				switch (this.data.type) {
					case ClothingType.Accessory: price *= 1.4; break;
					case ClothingType.Clothing: price *= 1.1; break;
					case ClothingType.OnePiece: price *= 2.5; break;
					case ClothingType.Weapon: price *= 3; break;
				}

				// Wigs have a special stat on them.
				price += this.hairBonus ?? 0 * 2;

				// Check for Categories. Extra categories other than 1 increase price by 10% per category
				price *= 1 + ((this.data.style ?? ['']).length - 1) * 0.1;

				// Add values from worn effects
				const wearEffect = this.data.wearEffect;
				if (wearEffect) {
					price += calculateEffectPrice(Data.effectLibClothingWear, wearEffect);
				}

				// Add values from active effects
				const activeEffect = this.data.activeEffect;
				if (activeEffect) {
					price += calculateEffectPrice(Data.effectLibClothingActive, activeEffect);
				}

				// Round up
				return Math.ceil(price);
			}

			/**
			 * Comparer to sort clothing items by rank, highest ranked first
			 *
			 * Suitable for sorting items for the same slot
			 */
			static compareByRank(left: Clothing, right: Clothing): number {
				if (left.rankNumber !== right.rankNumber) {
					return right.rankNumber - left.rankNumber; // higher ranked on top
				}
				return left.name.localeCompare(right.name);
			}

			private static _itemType(t: ClothingType) {
				return t === ClothingType.Weapon ? Category.Weapon : Category.Clothes;
			}
		}

		export abstract class BaseInventoryItem<TType extends CategoryInventory> extends Item<TType>{
			constructor(category: TType, tag: string, d: Data.ItemTypeDescMap[TType], inventoryObj: Entity.InventoryManager) {
				super(category, tag, d, inventoryObj);
			}

			abstract get type(): Data.ItemType;

			override get itemClass(): CategoryInventory {
				return super.itemClass as CategoryInventory;
			}

			/**
			* Shows long description of item and any knowledge known about it.
			*/
			abstract examine(player: Entity.Player, omitDescription: boolean): string;

			abstract get charges(): number;
		}

		export class Consumable extends BaseInventoryItem<CategoryConsumable> {
			private _messageBuffer: string[] = [];
			#knowledge: string[] = [];

			constructor(category: CategoryConsumable, tag: string, inventoryObj: Entity.InventoryManager) {
				super(category, tag, fetchData(category, tag), inventoryObj);

				this.#knowledge = this.useEffect.map(e => Data.effectLib[e].knowledge).flat().sort();
			}

			override get type(): Data.ItemTypeConsumable | Data.CosmeticsType {
				return this.data.type;
			}

			/**
			 * Short description of item
			 */
			override get description(): string {
				return this.data.shortDesc;
			}

			/**
			 * Shows long description of item and any knowledge known about it.
			 */
			override examine(player: Entity.Player, omitDescription: boolean): string {
				let output = omitDescription ? "" : this.data.longDesc;
				const usages = player.getHistory("items", this.tag);

				if (usages == 0) return output;

				if (!omitDescription) output += "\n\n";
				const max = Math.min(usages, this.getKnowledge().length);

				for (let i = 0; i < max; i++)
					output += PR.pEffectMeter(this.getKnowledge()[i], this) + "  ";

				return output;
			}

			printEffectsOnly(player: Entity.Player): string {
				let output = "";
				const usages = player.getHistory("items", this.tag);
				const max = Math.min(usages, this.getKnowledge().length);

				for (let i = 0; i < max; i++)
					output += PR.pEffectMeter(this.getKnowledge()[i], this) + " ";
				return output;
			}

			get useEffect(): string[] {
				return this.data.effects ?? [];
			}

			override get basePrice(): number {
				if ([Category.Cosmetics, Category.MiscLoot, Category.MiscConsumable].includes(this.itemClass)) {
					return this.data.value ?? 500;
				}
				// Drugs and food have a price which is the sum of their effect values
				return calculateEffectPrice(Data.effectLib, this.useEffect);
			}

			/**
			 * Apply all effects of this consumable item.
			 */
			applyEffects(player: Entity.Player): void {
				const effects = this.data.effects;
				if (effects) {
					for (const e of effects) {
						if (setup.world.debugMode) {console.group("Applying effect: ", e);}
						const tmp = Data.effectLib[e].fun(player);
						if ((typeof tmp !== 'undefined') && (tmp != "")) this._messageBuffer.push(tmp);
						if (setup.world.debugMode) {console.groupEnd();}
					}
				}

				// Knowledge.
				const usages = player.getHistory("items", this.tag);

				if (usages <= this.getKnowledge().length)
					this._messageBuffer.push("\n\n@@.state-neutral;You learn something… this item has an effect!@@ " +
						PR.pEffectMeter(this.getKnowledge()[(usages - 1)], this));
			}

			/**
			 * This message is printed when a player uses an item
			 */
			message(player: Entity.Player): string {
				let output = this.data.message;
				if (this._messageBuffer.length > 0) output += this._messageBuffer.join("\n");
				this._messageBuffer = [];
				return output === undefined ? "" : PR.tokenizeString(player, undefined, output);
			}

			// /**
			//  * Price of the item
			//  * @todo unused
			//  */
			// get Price(): number {
			// 	return this.Data.Price;
			// }

			/**
			 * No. of uses
			 */
			override get charges(): number{
				return this.inventory.charges(this.itemClass, this.tag);
			}

			setCharges(n: number): number {
				return this.inventory.setCharges(this.itemClass, this.tag, n);
			}

			addCharges(n: number): number {
				return this.inventory.addCharges(this.itemClass, this.tag, n);
			}

			removeCharges(n: number): number {
				return this.inventory.addCharges(this.itemClass, this.tag, -n);
			}

			get isFull(): boolean {
				return this.charges == Entity.InventoryManager.MAX_ITEM_CHARGES;
			}

			getKnowledge(): string[] {return this.#knowledge;}

			get isFavorite(): boolean {
				return this.inventory.isFavorite(this.id);
			}

			setFavorite(value = true): boolean {
				if (value == true) {
					this.inventory.addFavorite(this.id);
				} else {
					this.inventory.deleteFavorite(this.id);
				}
				return value;
			}

			toggleFavorite(): boolean {
				return this.setFavorite(!this.isFavorite);
			}
		}

		export class Cosmetics extends Consumable {
			constructor(tag: string, inventoryObj: Entity.InventoryManager) {
				super(Category.Cosmetics, tag, inventoryObj);
			}

			/**
			 * Get the bonus for an item by key
			 * @param b - bonus type to find
			 */
			getBonus(b: Skills.Any): [number, number, number] {
				return (this.data as Data.CosmeticsItemDesc).skillBonus[b] ?? [0, 0, 0];
			}
		}

		export class QuestItem extends BaseInventoryItem<Category.Quest> {
			constructor(tag: string, inventoryObj: Entity.InventoryManager) {
				super(Category.Quest, tag, fetchData(Category.Quest, tag), inventoryObj);
			}

			// eslint-disable-next-line class-methods-use-this
			get type(): Data.ItemTypeSpecial.Quest {
				return Data.ItemTypeSpecial.Quest;
			}

			get description(): string {
				return this.data.shortDesc;
			}

			/**
			 * Long description of item. No knowledge on quest items.
			 */
			override examine(_player: Entity.Player, omitDescription: boolean): string {
				return omitDescription ? "" : this.data.longDesc;
			}

			/**
			 * Doesn't work. You can't have more than 1 of any quest item.
			 */
			// eslint-disable-next-line class-methods-use-this
			addCharges(): 1 {return 1;}

			removeCharges(): number {
				return this.inventory.addCharges(this.itemClass, this.tag, -1);
			}

			/**
			 * Fake: This makes it so that quest items are unique.
			 */
			// eslint-disable-next-line class-methods-use-this
			override get charges(): number {return 1;}

			// eslint-disable-next-line class-methods-use-this
			get isFavorite(): boolean {return false;}
		}

		export class Reel extends BaseInventoryItem<Category.Reel> {
			private static readonly _styleMap: Record<Rarity, string> = {
				[Rarity.Common]: "item-common",
				[Rarity.Uncommon]: "item-uncommon",
				[Rarity.Rare]: "item-rare",
				[Rarity.Legendary]: "item-legendary"
			};

			private readonly _totalWeight: number;

			constructor(tag: string, inventoryObj: Entity.InventoryManager) {
				super(Category.Reel, tag, fetchData(Category.Reel, tag), inventoryObj);
				this._totalWeight = _.sum(Object.values(this.data.data));
			}

			// eslint-disable-next-line class-methods-use-this
			override get type(): Data.ItemTypeSpecial.Reel {
				return Data.ItemTypeSpecial.Reel;
			}

			override get description(): string {
				return `@@.${Reel._styleMap[this.data.rank]};(${this.data.rank}) ${this.data.name}@@`;
			}

			override examine(_player: Entity.Player, omitDescription: boolean): string {
				const attrs: Data.ReelAction[] = ['ass', 'bj', 'hand', 'tits', 'fem', 'perv', 'beauty'];
				const text = ['Ass Fucking', 'Blowjobs', 'Handjobs', 'Tit Fucking', 'Femininity', 'Perversion', 'Beauty'];
				let output = omitDescription ? "" : "A slot reel used for whoring. It has the following attributes:\n";
				for (let x = 0; x < attrs.length; x++) {
					const percent = this.calcPercent(attrs[x]);
					if (percent <= 0) continue;
					output += `${text[x]} - ${percent}% `;
				}
				return output;
			}

			calcPercent(key: Data.ReelAction): number {
				const count = this.data.data[key] ?? 0;
				return Math.round(count / this._totalWeight * 100);
			}

			get rank(): Rarity {return this.data.rank;}

			get reels(): Partial<Record<Data.ReelAction, number>> {
				return this.data.data;
			}

			get reelsArray(): Data.ReelAction[] {
				const res: Data.ReelAction[] = [];
				for (const [a, c] of Object.entries(this.data.data)) {
					for (let i = 0; i < c; ++i) {
						res.push(a);
					}
				}
				return res;
			}

			get css(): string {
				return this.data.css;
			}

			/**
			 * Find a particular symbol at an array index
			 */
			symbol(index: number): Data.ReelAction {
				return this.reelsArray[index];
			}

			/**
			 * No. of uses
			 */
			get charges(): number {
				return this.inventory.charges(this.itemClass, this.tag);
			}

			override get basePrice(): number {
				return this.data.value ?? 500;
			}
		}

		export type InventoryItem = Consumable | QuestItem | Reel;
		export type AnyItem = Clothing | InventoryItem;
	}
}
