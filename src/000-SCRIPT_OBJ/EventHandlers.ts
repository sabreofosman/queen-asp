/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-return */
namespace App {
	export namespace EventHandlers {

		export function init(): void {
			console.log("Initializing Event Handlers…");
		}

		/**
		 * Do we have a player state and a player object?
		 */
		export function hasPlayerState(): boolean {
			return (exists("SugarCube.setup.player") == true && exists("SugarCube.State.variables.PlayerState") == true);
		}

		function exists(namespace: string): boolean {
			const tokens = namespace.split('.');
			return tokens.reduce((prev: any, curr) => (typeof prev == "undefined") ? prev : prev[curr], globalThis) != undefined;
		}

		export function onLoad(save: TwineSugarCube.SaveObject): void {
			console.debug("Loading save object");
			const minSupportedSaveVersion = 1;
			if (save.version < minSupportedSaveVersion) {
				console.error(`Attempted to load too old save version (${save.version}).`);
				console.log(`Minimal supported save data version is ${minSupportedSaveVersion}`);
				// Dialog.setup("Save loading error");
				// Dialog.wiki("Attempted to load a save file created by too old game version, which could not be converted to the current format.");
				// Dialog.wiki("This data in this save file can not be loaded. Please start a new game instead.");
				// Dialog.open();
				throw new Error("This save version is too old and is not supported. Please start a new game.");
			}

			if (save.version > Data.game.version.save) {
				console.error(`Version out of range on save. Loaded : ${save.version}, above expected:${App.Data.game.version.save}`);
				/* Invalidates saves outside of legal scope */
				throw new Error("The save you're attempting to load is incompatible with the current game. Please download the latest game version.");
			}

			save.id = Config.saves.id; // the game title includes version, hence this overwrite

			setup.world.saveLoaded();
			setup.avatar.queuePortraitDraw();
		}

		export function onSave(save: TwineSugarCube.SaveObject, details: TwineSugarCube.SaveDetails): void {
			const gameSate = save.state.history[0].variables as TwineSugarCube.SugarCubeStoryVariables;
			switch (details.type) {
				case "autosave":
					save.title = `Day ${gameSate.day}, ${PR.phaseName(gameSate.phase)}`;
					break;
				default:
					save.title = `${save.state.history[0].title}, day ${gameSate.day}, ${App.PR.phaseName(gameSate.phase)}`;
					break;
			}
		}
	}
}
