namespace App.Tasks {
	export namespace Requirements {
		export abstract class Base {
			abstract get status(): boolean;

			render(parent: ParentNode) {
				if (this.isVisible) {
					const res = UI.makeElement('div');
					this.doRender(res);
					parent.append(res);
				}
			}

			protected get isVisible(): boolean {
				return true;
			}

			protected abstract doRender(parent: HTMLDivElement): void;
		}

		abstract class Individual extends Base {
			#desc: string;

			constructor(desc: string) {
				super();
				this.#desc = desc;
			}

			protected get desc(): string {
				return this.#desc;
			}

			override get isVisible(): boolean {
				return this.#desc.length > 0;
			}

			protected renderCheckMark(parent: ParentNode): void {
				const span = UI.appendNewElement('span', parent);
				if (this.status) {
					span.innerHTML = '&#x2713; ';
					span.classList.add('state-positive');
				} else {
					span.innerHTML = '&#x2717; ';
					span.classList.add('state-negative');
				}
			}

			protected renderDesc(parent: HTMLDivElement) {
				$(parent).wiki(this.desc);
			}
		}

		class Boolean extends Individual {
			#done: boolean;
			constructor(isMet: boolean, desc: string) {
				super(desc);
				this.#done = isMet;
			}

			get status(): boolean {
				return this.#done;
			}

			doRender(parent: HTMLDivElement): void {
				this.renderCheckMark(parent);
				this.renderDesc(parent);
			}
		}

		class Numeric extends Individual {
			#value: number;
			#target: number;
			#condition: Data.Tasks.Condition;

			constructor(value: number, target: number, condition: Data.Tasks.Condition | undefined, desc: string) {
				super(desc);
				this.#value = value;
				this.#target = target;
				this.#condition = Numeric.substituteDefaultCondition(condition);
			}

			get status(): boolean {
				return cmp(this.#value, this.#target, this.#condition);
			}

			doRender(parent: HTMLDivElement): void {
				this.renderCheckMark(parent);
				const meter = UI.rMeter(this.#value, this.#target,
					{
						invertMeter: isInvertedCondition(this.#condition),
						showScore: settings.displayMeterNumber,
						showMax: settings.displayMeterNumber
					});
				meter.style.marginRight = '0.5em';
				meter.classList.add('fixed-font');
				parent.append(meter);
				this.renderDesc(parent);
			}

			static substituteDefaultCondition(c?: Data.Tasks.Condition): Data.Tasks.Condition {
				return c ?? ">="; // default condition for numeric requirements
			}
		}

		export abstract class RequirementGroup extends Base {
			protected items: Base[] = [];
			private readonly _groups: Record<string, RequirementGroup> = {};

			add(r: Base): void {
				this.items.push(r);
			}

			addGroup(name: string, group: RequirementGroup): void {
				this._groups[name] = group;
				this.add(group);
			}

			group(name: string): RequirementGroup | null {
				const res = this._groups[name];
				return res ? res : null;
			}
		}

		export class RequirementGroupAll extends RequirementGroup {
			get status(): boolean {
				return this.items.length === 0 || this.items.every(r => r.status);
			}

			doRender(parent: HTMLDivElement): void {
				this.items.forEach(item => item.render(parent));
			}
		}

		export class RequirementGroupAny extends RequirementGroup {
			get status(): boolean {
				return this.items.length === 0 || this.items.some(r => r.status);
			}

			doRender(parent: HTMLDivElement): void {
				const header = parent.appendNewElement('div', "Any of", ['task-any-group']);
				const list = parent.appendNewElement('div');
				this.items.forEach(item => item.render(list));
				header.classList.add(this.status ? 'task-req-group-done' : 'task-req-group-undone')
			}
		}

		class CoreStat extends Numeric {
			constructor(player: Entity.Player, r: Data.Tasks.Requirements.Stat) {
				super(player.getStatPercent(r.type, r.name), r.value, r.condition, r.altTitle ?? r.name)
			}
		}

		class BodyStat extends Numeric {
			constructor(player: Entity.Player, r: Data.Tasks.Requirements.Body) {
				super(player.getStatPercent(r.type, r.name), r.value, r.condition, r.altTitle ??
					PR.getLevelingProperty(r.type, r.name, "adjective", r.value, true) + ' '  +
					PR.getNoun(r.type, r.name, r.value, true));
			}
		}

		class SkillStat extends Numeric {
			constructor(player: Entity.Player, r: Data.Tasks.Requirements.Skill) {
				super(player.getStatPercent(r.type, r.name), r.value, r.condition,
					r.altTitle ?? Data.Lists.skillConfig[r.name].altName ?? r.name);
			}
		}

		class Meta extends Numeric {
			constructor(player: Entity.Player, r: Data.Tasks.Requirements.Meta) {
				super(Meta._playerValue(player, r), r.value, r.condition,
					isInvertedCondition(Numeric.substituteDefaultCondition(r.condition)) ? `Too much ${r.name}` : `Not enough ${r.name}`);
			}

			private static _playerValue(player: Entity.Player, r: Data.Tasks.Requirements.Meta): number {
				switch (r.name) {
					case "beauty":
						return player.beauty;
				}
			}
		}

		class DaysPassed extends Boolean {
			private static _descString(status: boolean, days: number, c: Data.Tasks.Condition): string {
				if (DaysPassed._waitConditions.includes(c)) {
					return `wait ${days} days`;
				}
				return status ? `${days} days left` : `expired ${0 - days} days ago`;
			}

			static create(player: Entity.Player, r: Data.Tasks.Requirements.DaysPassed): DaysPassed {
				const offset = r.name ? Quest.getFlag(player, r.name) as number : 0;
				const statusFlag = cmp(setup.world.day, r.value + offset, r.condition);
				const reqString = DaysPassed._descString(statusFlag, setup.world.day - r.value - offset, r.condition ?? '==')
				return new DaysPassed(statusFlag, reqString);
			}

			private static readonly _waitConditions: Data.Tasks.Condition[] = ['eq', 'gt', 'gte', '==', '>', ">="];
		}

		class Equipped extends Boolean {
			constructor(player: Entity.Player, r: Data.Tasks.Requirements.Equipped) {
				super(cmp(player.isEquipped(r.name), r.value ?? true),
					r.altTitle ?? (r.value ?? true) ? `Must have ${r.name} equipped` : `Must not have ${r.name} equipped`)
			}
		}

		class IsWearing extends Boolean {
			private static _makeDesc(r: Data.Tasks.Requirements.IsWearing, eqItem: Items.Clothing | null): string {
				if (r.altTitle) {
					return r.altTitle;
				}

				let desc = "";
				if (r.value === false) {
					desc = "''NOT'' ";
				}
				if (r.slot) {
					desc += `wearing in slot '${r.slot}'`;
				} else {
					const itemName = Items.splitId(r.name);
					desc += "wearing " + Items.factory(itemName.category, itemName.tag).description;
				}
				if (r.isLocked !== undefined && eqItem && (eqItem.isLocked == r.isLocked) !== r.value) {
					desc += ` and ${r.isLocked ? "locked" : "unlocked"}`;
				}
				return desc;
			}

			static create(player: Entity.Player, r: Data.Tasks.Requirements.IsWearing) {
				const eqItem = r.slot !== undefined ? player.getEquipmentInSlot(r.slot) : player.wardrobeItem(r.name);
				let status = cmp((eqItem != null), r.value, r.condition ?? '==');
				if (r.isLocked !== undefined && eqItem && (eqItem.isLocked == r.isLocked) !== r.value) {
					status = false;
				}
				return new IsWearing(status, IsWearing._makeDesc(r, eqItem));
			}
		}

		class SingleItem extends Boolean {
			#isHidden: boolean;

			constructor(status: boolean, desc: string, isHidden: boolean) {
				super(status, desc);
				this.#isHidden = isHidden;
			}

			static create(player: Entity.Player, r: Data.Tasks.Requirements.Item) {
				const hasItems = player.getItemById(r.name) ? 1 : 0;
				const itemName = Items.splitId(r.name);
				const pString = Items.factory(itemName.category, itemName.tag).description;
				const itemOrClothing = itemName.category === Items.Category.Clothes ? "clothing" : "item";
				return new SingleItem(cmp(hasItems, r.value, r.condition),
					r.altTitle ?? (r.value ? `Have ${pString}` : `Missing ${itemOrClothing}: ${pString}`), r.hidden ?? false);
			}

			override get isVisible() {
				return !this.#isHidden && super.isVisible;
			}
		}

		class MultipleItems extends Numeric {
			#isHidden: boolean;

			constructor(value: number, target: number, c: Data.Tasks.Condition | undefined, desc: string, isHidden: boolean) {
				super(value, target, c, desc);
				this.#isHidden = isHidden;
			}

			static create(player: Entity.Player, r: Data.Tasks.Requirements.Item) {
				const itm = player.getItemById(r.name);
				const hasItems = itm ? (itm instanceof Items.Clothing ? 1 : itm.charges) : 0;
				const itemName = Items.splitId(r.name);
				const desc = r.altTitle ?? Items.factory(itemName.category, itemName.tag).description + ` × ${r.value}`;
				return new MultipleItems(hasItems, r.value, r.condition, desc, r.hidden ?? false);
			}

			override get isVisible() {
				return !this.#isHidden && super.isVisible;
			}
		}

		class StyledNumeric extends Numeric {
			#styles: string[] | string;
			constructor(value: number, target: number, c: Data.Tasks.Condition | undefined, desc: string, style: string[]|string) {
				super(value, target, c, desc);
				this.#styles = style;
			}

			override renderDesc(parent: HTMLDivElement) {
				parent.appendFormattedText({text: this.desc, style: this.#styles});
			}
		}

		class NpcStat extends Numeric {
			static findNpc(r: Data.Tasks.Requirements.NPCStat, npc: Entity.NPC | string): Entity.NPC {
				if (r.option) {
					return setup.world.npc(r.option);
				}
				return typeof npc === "string" ? setup.world.npc(npc) : npc;
			}

			static create(r: Data.Tasks.Requirements.NPCStat, npc: Entity.NPC | string) {
				const npcEntity = NpcStat.findNpc(r, npc);
				return new NpcStat(npcEntity[r.name], r.value, r.condition, r.altTitle ?? npcEntity.name + "'s " + r.name);
			}
		}

		export function makeReq(player: Entity.Player, r: Data.Tasks.Requirements.Any, npc: Entity.NPC | string): Base {
			switch (r.type) {
				case Stat.Body:
					return new BodyStat(player, r);
				case Stat.Core:
					return new CoreStat(player, r);
				case Stat.Skill:
					return new SkillStat(player, r);
				case 'daysPassed':
					return DaysPassed.create(player, r);
				case 'equipped':
					return new Equipped(player, r);
				case 'hairColor':
					return new Boolean(((player.getHairColor() == r.name) == r.value), r.altTitle ?? `Hair color - ${r.name}`);
				case 'hairStyle':
					return new Boolean(((player.getHairStyle() == r.name) == r.value), r.altTitle ?? `Hair style - ${r.name}`);
				case 'inPort':
					return new Boolean(player.isInPort(r.value) == r.condition, r.altTitle ?? `In port - ${r.condition}`);
				case 'isWearing':
					return IsWearing.create(player, r);
				case 'item':
					return r.value > 1 ? MultipleItems.create(player, r) : SingleItem.create(player, r);
				case 'jobFlag':
					return new Boolean(cmpAny(player.jobFlags[r.name], r.value, r.condition ?? "eq"), r.altTitle ?? r.name);
				case 'meta':
					return new Meta(player, r);
				case 'money':
					return new StyledNumeric(player.money, r.value, r.condition, "Coins", 'item-money');
				case 'npcStat':
					return NpcStat.create(r, npc);
				case 'portName':
					return new Boolean(cmp(player.getShipLocation().title, r.value, r.condition), r.altTitle ?? `Be in port ${r.value}`);
				case 'quest':
					return new Boolean(cmpAny(Quest.byTag(r.name).state(player), r.value, r.condition),
						r.altTitle ?? `Quest '${Data.quests[r.name].title}' is ${r.value}`);
				case 'questFlag':
					return new Boolean(cmpAny(player.questFlags[r.name], r.value, r.condition), r.altTitle ?? r.name);
				case 'style':
					return new Numeric(player.style, r.value, r.condition, r.altTitle ?? "Style and Grooming");
				case 'styleCategory':
					return new Numeric(Math.clamp(player.getStyleSpecRating(Data.Fashion.Style[r.name]), 0, 100),
						r.value, r.condition, r.altTitle ?? r.name);
				case 'tokens':
					return new StyledNumeric(player.money, r.value, r.condition, "Courtesan tokens", 'item-courtesan-token');
				case 'trackCustomers':
					return new Numeric(Quest.getFlag(player, `track_${r.name}`) as number, r.value, r.condition, r.altTitle ?? "Satisfy Customers");
				case 'trackProgress':
					return new Numeric(Quest.getProgressValue(player, r.name), r.value, r.condition, r.altTitle ?? "Progress");
			}
		}
	}
}
