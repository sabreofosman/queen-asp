declare module "twine-sugarcube" {
	export interface SugarCubeSetupObject {
		world: App.Entity.World;
		player: App.Entity.Player;
		avatar: App.Entity.AvatarEngine;
		jobEngine: App.JobEngine;
		eventEngine: App.EventEngine;
		coffinGame: App.Gambling.Coffin;
		combat: App.Combat.CombatEngine;
		spectator: App.Combat.SpectatorEngine;
		notifications: App.Notifications.Engine;
		loot: App.Loot;
		audio: App.Audio;
	}

	export interface SugarCubeSettingVariables {
		displayAvatar: boolean;
		displayNPC: boolean;
		displayBodyScore: boolean;
		fastAnimations: boolean;
		bgmVolume: number;
		displayMeterNumber: boolean;
		units: 'Imperial' | 'Metric';
		inlineItemDetails: boolean;
		alternateControlForRogue: boolean;
		autosaveAtSafePlaces: boolean;
		theme: string;
	}

	interface LinkHandlerData {
		f: (...args: unknown[]) => void;
		args: unknown[];
	}
	export interface SugarCubeTemporaryVariables {
		[x: string]: any;
		linkHandlers: Record<number, LinkHandlerData>;
	}

	export interface SugarCubeStoryVariables extends App.GameState.IGame { }
}

export {}
