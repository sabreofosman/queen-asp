namespace App.PR {
    /**
     * Get a list of all effects, but print ????? for ones unlearned.
     */
	export function getAllEffects(type: Exclude<Items.Category, Items.Category.Quest>, tag: string): string {
		const o = Items.factory(type, tag);
		if (o instanceof Items.Reel) {
			return "";
		}
		const effects = o.getKnowledge();
		const uses = (type == Items.Category.Clothes) ? setup.player.getHistory("clothingEffectsKnown", tag) :
			setup.player.getHistory('items', tag);

		const output: string[] = [];

		for (let i = 0; i < effects.length; i++) {
			if (i < uses) {
				output.push(pEffectMeter(effects[i], o));
			} else {
				output.push("?????");
			}
		}

		return output.join(" ");
	}

	let numericalMeters = false;

	export function setNumericalMeters(v: boolean): void {
		numericalMeters = v;
	}

	/**
     * Shortcut
     * @see unitSystem.lengthString
	 */
	export function lengthString(x: number, compact = false): string {
		return unitSystem.lengthString(x, compact);
	}

	/**
     * Shortcut
     * @see unitSystem.lengthValue
	 */
	export function lengthValue(x: number): number {
		return unitSystem.lengthValue(x);
	}

    /**
     * Returns config object which describe naming of the given type
     * @param type Config type (StatType.Body)
     */
	function getNamingConfig(type: Stat) {
		if (type == Stat.Body) return Data.naming.bodyConfig;
		throw "Unexpected type";
	}

	function isFullRatedNoun(r: Data.RatingFinalValue | Data.SimpleRated | Data.FullRated): r is Data.FullRated {
		return r.hasOwnProperty("index");
	}

	// function isRatedFinalValue(r: Data.RatingFinalValue | Data.SimpleRated | Data.FullRated): r is Data.FullRated {
	// 	return Array.isArray(r) || typeof r === "string";
	// }

    /**
     * Fetch index names required to find leveled value for a leveling property
     * @param stat Stat type (SKILL, BODY, STAT)
     * @param property Property name ("Bust", "Lips", etc.)
     * @param aspect "noun", "adjective"
     */
	function getNamingIndices(stat: Stat.Body, property: BodyPart, aspect: Data.NamingAspect): string[] {
		const st = getNamingConfig(stat);
		switch (aspect) {
			case "adjective":
				const adj = st[property].adjective;
				return adj.index ?? [property];
			case "noun":
				const noun = st[property].noun;
				return isFullRatedNoun(noun) ? noun.index : [property];
		}
	}

    /**
     * @summary Fetch a leveled value from the given rating for given values.
     *
     * This function descends into the passed ratings object using supplied index
     * value array. The rating object is indexed as using the first element of the
     * index array. The result of this operation is passed to this function again with
     * sliced index array. This continues until index array is exhausted or indexing
     * returns a simple string or array of strings (a random element of which is returned then).
     *
     * @param Ratings object with ratings or the final string
     * @param Value array of rating index values or a single value
     */
	function getMultiIndexLevelingProperty(ratings: Data.RatingValue, value: number | number[]): string {
		if (typeof (ratings) == "string") return ratings; // value does not change

		let v = 0;
		let nextValues: number[] = [];
		if (Array.isArray(value)) {
			v = value[0];
			if (value.length > 1) nextValues = value.slice(1);
		} else {
			v = value;
		}
		let lastSmallerRating: number | undefined = undefined;
		for (const prop of Object.keys(ratings)) {
			const nProp = parseInt(prop);
			if (nProp > v) break;
			lastSmallerRating = nProp;
		}
		if (lastSmallerRating == undefined) return `Untyped rating: ${value.toString()}`;
		const res = ratings[lastSmallerRating];
		if (nextValues.length === 0) {
			assertIsFinalRating(res);
			if (Array.isArray(res)) {
				return res.randomElement();
			} else {
				return res;
			}
		}
		return getMultiIndexLevelingProperty(res, nextValues);
	}

	type RatingKey = Exclude<BodyStatStr, "hair" | "height"> | NpcStat |
		Exclude<CoreStatStr, "energy" | "femininity" | "futa" | "health" | "hormones" | "nutrition"
			| "perversion" | "toxicity" | "willpower"> |
		"beauty" | "clothing" | "fetish" | "style";

    /**
     * Fetch a rating for a statistic/value
     */
	export function getRating(type: RatingKey, value: number, colorize = false): string {
		const entries = Object.entries(Data.ratings[type]);
		let lastSmallerValue = entries[0][1];
		for (const e of entries) {
			if (parseInt(e[0]) > value) break;
			lastSmallerValue = e[1];
		}

		if (lastSmallerValue == undefined) return `Untyped rating: ${type},${value}`;
		if (colorize == true) {
			return colorizeString(value, lastSmallerValue);
		} else {
			return lastSmallerValue;
		}
	}

	function isFixedLeveling(ld: Data.LevelingDataAny): ld is Data.FixedStatLevelingData {
		return ld.hasOwnProperty("fixed");
	}

	function isNoneLeveling(ld: Data.LevelingDataAny): ld is Data.NoneStatLevelingData {
		return ld.hasOwnProperty("none");
	}

	function isExplicitLeveling(ld: Data.LevelingDataAny): ld is Data.ExplicitStatLevelingData {
		return !isFixedLeveling(ld) && !isNoneLeveling(ld) && Object.entries(ld)[0][1].hasOwnProperty("cost");
	}

	function isLevelingRecordWithAdjective(lr: Data.StatLevelingRecordAny): lr is Data.StatLevelingRecordAdjective {
		return lr.hasOwnProperty("adjective");
	}

	/**
     * Helper function. Checks relevant statistic config and returns the leveling record if one exists.
     */
	function getLevelingRecordImpl(ratings: Data.LevelingDataAny, value: number): Data.StatLevelingRecordAny {
		if (isNoneLeveling(ratings)) {
			return ratings.none;
		}
		if (isFixedLeveling(ratings)) {
			return ratings.fixed;
		}
		const entries = Object.entries(ratings);
		let lastSmallerValue = entries[0][1];
		for (const e of entries) {
			if (parseInt(e[0]) > value) break;
			lastSmallerValue = e[1];
		}
		return lastSmallerValue;
	}

    /**
     * Helper function. Checks relevant statistic config and returns a colorized Property value for use if one exists.
     */
	export function getLevelingProperty<T extends Stat>(
		type: T, stat: StatTypeMap[T], property: "adjective", value: number, colorize = false): string {
		const cfg = getStatConfig(type)[stat];
		const propValue = getLevelingRecordImpl(cfg.leveling, value);
		if (!isLevelingRecordWithAdjective(propValue)) {
			return "";
		}

		const str = propValue[property];
		if (str === "" || !colorize) return str;

		return `<span style='color:${UI.colorScale(value - cfg.min, cfg.max - cfg.min)}'>${str}</span>`;
	}

    /**
     * Helper function. Checks relevant statistic config and returns a NOUN (colorized) for use if one exists.
     */
	export function getNoun(type: Stat.Body, stat: BodyStatStr, value: number | number[], colorize = false): string {
		const nCfg = getNamingConfig(type);
		if (nCfg == undefined || !nCfg.hasOwnProperty(stat)) return "NO_NOUN_FOR_" + type + ":" + stat;

		const statCfg = nCfg[stat as BodyPart];
		const ratedNounStr = (cfg: Data.BodyPartConfig, v: number | number[]): string => {
			const nounRec = cfg.noun;
			if (typeof nounRec === "string") {
				return nounRec;
			}
			if (Array.isArray(nounRec)) {
				return nounRec.randomElement();
			}
			return getMultiIndexLevelingProperty(nounRec.leveling, v);
		};
		const str = ratedNounStr(statCfg, value);

		if (colorize) {
			const ratingValues = Array.isArray(value) ? value : [value];
			const ratingProps = statCfg.adjective.rating.map(r => r.split('/'));
			// console.assert(ratingValues.length === ratingProps.length,
			//	"Length are not equal (v: %d, p:%d) for %s/%s", ratingValues.length, ratingProps.length, type, stat);
			// we should compute the median value now, but all our indices are at most of length 2
			// thus it is the same as the mean
			const weightedValue: number[] = []
			for (let i = 0; i < ratingValues.length; ++i) {
				const cfg = getStatConfig(ratingProps[i][0] as Stat)[ratingProps[i][1] as BodyStat];
				weightedValue.push((ratingValues[i] - cfg.min) / (cfg.max - cfg.min));
			}

			return `<span style='color:${UI.colorScale(_.mean(weightedValue), 1)}'>${str}</span>`;
		}
		return str;
	}

	/**
	 * Returns array of applicable adjective values for a leveling noun
	 */
	function getNoneAdjectives(type: Stat.Body, stat: BodyStat, player: Entity.Player, colorize?: boolean): string[];
	function getNoneAdjectives(type: Stat.Body, stat: BodyStatStr, player: Entity.Player, colorize?: boolean): string[];
	function getNoneAdjectives(type: Stat.Body, stat: BodyStatStr, player: Entity.Player, colorize = false): string[] {
		const tCfg = getNamingConfig(type);
		if (!tCfg) {return [];}
		const sCfg = tCfg[stat as BodyPartStr];
		if (!sCfg) return [];
		const adjCfg = sCfg.adjective;
		if (adjCfg == undefined) return [];

		const adjectiveRatings = adjCfg.rating || [type + '/' + stat];
		const adjectiveApplicableLevels = adjCfg.applicableLevel ??
			adjectiveRatings.map(() => {return {min: 0, max: 100};});

		const statVal = player.getStatPercent(type, stat);
		const adjectives: string[] = [];
		for (let i = 0; i < adjectiveRatings.length; ++i) {
			if (statVal >= adjectiveApplicableLevels[i].min && statVal <= adjectiveApplicableLevels[i].max) {
				const r = adjectiveRatings[i].split('/');
				adjectives.push(getAdjective(r[0] as StatTypeStr, r[1] as any, statVal, colorize));
			}
		}

		return adjectives;
	}

	/**
	 * Returns leveling noun, optionally with applicable adjectives prepended
	 * @param type
	 * @param stat
	 * @param player
	 * @param adjectives Whether to prepend adjectives
	 * @param colorize colorize output
	 */
	function getPlayerNoun(type: Stat.Body, stat: BodyPart, player: Entity.Player,
		adjectives = false, colorize = false): string {
		const indexNames = getNamingIndices(type, stat, "noun");
		if (indexNames == undefined) return "NO_NOUN_FOR_" + type + ":" + stat;
		const indices = indexNames.map(s => s.includes('/') ? s.split('/') : [type, s]);
		const indexValues = indices.map(x => player.getStat(x[0] as any, x[1]));
		let str = getNoun(type, stat, indexValues, colorize);

		if (adjectives) {
			str = getNoneAdjectives(type, stat, player, colorize).join(' ') + ' ' + str;
		}
		return str;
	}

    /**
     * Helper function. Get total amount of XP points needed to raise from a to b
     */
	function getTotalXPPoints<T extends keyof StatTypeMap>(type: T, stat: StatTypeMap[T], valueA: number, valueB: number): number {
		const statCfg = getStatConfig(type)[stat];
		if (statCfg.levelingCost) {
			return statCfg.levelingCost(valueB) - statCfg.levelingCost(valueA);
		}

		const levelingCost = statCfg.leveling;

		if (isFixedLeveling(levelingCost)) {
			return levelingCost.fixed.cost * (valueB - valueA);
		}

		if (isNoneLeveling(levelingCost) || !isExplicitLeveling(levelingCost)) {
			return 0;
		}

		let lastLeveledTo = valueA;
		let res = 0;

		for (const e of Object.entries(levelingCost)) {
			const propV = parseInt(e[0]);
			if (propV <= valueA) continue;
			if (propV > valueB) break;

			res += (propV - lastLeveledTo) * e[1].cost;
			lastLeveledTo = propV;
		}
		res += (valueB - lastLeveledTo) * levelingCost[lastLeveledTo].cost;
		return res;
	}

    /**
     * Helper function. Checks relevant statistic config and returns an ADJECTIVE (colorized) for use if one exists.
     */
	function getAdjective<T extends keyof StatTypeMap>(type: T, stat: StatTypeMap[T], value: number, colorize?: boolean): string;
	function getAdjective<T extends keyof StatTypeStrMap>(type: T, stat: StatTypeStrMap[T], value: number, colorize?: boolean): string;
	function getAdjective<T extends keyof Data.StatConfigMap>(type: T, stat: StatTypeMap[T], value: number, colorize = false): string {
		return getLevelingProperty(type, stat, "adjective", value, colorize);
	}

	function tokenizeRating<T extends keyof StatTypeMap>(player: Entity.Player, type: T, stat: StatTypeMap[T], str: string): string
	function tokenizeRating<T extends keyof StatTypeStrMap>(player: Entity.Player, type: T, stat: StatTypeStrMap[T], str: string): string
	function tokenizeRating<T extends keyof StatTypeMap>(player: Entity.Player, type: T, stat: StatTypeMap[T], str: string): string {
		function nounReplacer(_match: string, delimiter: string) {
			return getPlayerNoun(Stat.Body, stat as BodyPart, player, false, true) + delimiter;
		}
		function adjReplace(_match: string, delimiter: string) {
			return getAdjective(type, stat, player.getStat(type, stat), true) + delimiter;
		}
		str = str.replace(/PLAYER_NAME/g, player.slaveName);
		if (type == Stat.Body) {
			str = str.replace(/LENGTH_C/g, lengthString(statToCm(player, stat as BodyStatStr), true).toString());
			str = str.replace(/LENGTH/g, lengthString(statToCm(player, stat as BodyStatStr), false).toString());
		}
		str = str.replace(/NOUN([^A-Za-z_|$])/g, nounReplacer);
		str = str.replace(/ADJECTIVE([^A-Za-z_|$])/g, adjReplace);
		return tokenizeString(player, undefined, str);
	}

    /**
     * Helper function for getting stat configurations.
     */
	export function getStatConfig<T extends keyof Data.StatConfigMap>(type: T): Record<StatTypeMap[T], Data.StatConfigMap[T]>;
	export function getStatConfig<T extends keyof Data.StatConfigStrMap>(type: T): Record<StatTypeStrMap[T], Data.StatConfigStrMap[T]>;
	export function getStatConfig(type: Stat) {
		switch (type) {
			case Stat.Core: return Data.Lists.coreConfig;
			case Stat.Skill: return Data.Lists.skillConfig;
			case Stat.Body: return Data.Lists.bodyConfig;
		}
	}

    /**
     * Colorizes and returns a string primitive
     */
	export function colorizeString(value: number, str: string, maxValue?: number): string {
		return `<span style='color:${UI.colorScale(value, maxValue)}'>${str}</span>`;
	}

    /**
     * Used to colorize a string with colors corresponding to the meter scheme.
     * @param n the value to rate the color on.
     * @param s the string to colorize
     * @param h HTML safe?
     */
	function colorizeMeter(n: number, s: string): string {
		return `<span style='color:${UI.meterColor(n, 100)}'>${s}</span>`;
	}

	export function debugColorScale(): string {
		let str = "";
		for (let i = 0; i < 16; i++)
			str += `<span style='color:${UI.colorScale(i, 16)}'>Index ${i} = ${UI.colorScale(i, 16)}</span><br/>`;
		return str;
	}

    /**
     * Prints out a 10 star meter surrounded with brackets.
     * @param score - Current stat/score
     * @param maxScore - Maximum stat/score
     * @param invertMeter - reverse direction of stars relative to score so that high scores are less stars.
     * @param HtmlSafe
     */
	function pMeter(score: number, maxScore: number, invertMeter = false) {
		const clampedScore = Math.clamp(score, 0, maxScore);
		const units = (maxScore / 10);
		const stars = Math.floor((clampedScore / units));
		let sMeter = "";
		const nMeter = invertMeter ? (100 - (10 * stars)) : (10 * stars);
		let i = 0;

		for (i = 0; i < stars; i++)
			sMeter += "&#9733;";
		sMeter = colorizeMeter(nMeter, sMeter);

		if ((10 - stars) != 0) {
			sMeter += "<span style='color:grey;'>";
			for (i = 0; i < (10 - stars); i++)
				sMeter += "&#9733;";
			sMeter += "</span>";
		}

		if (numericalMeters) {
			return `[${sMeter}] ${score}`;
		} else {
			return `[${sMeter}]`;
		}
	}

    /**
     * Simple calculator turns CM into Inches.
     */
	/*
	function cm2Inch(n: number): number {
		return Math.round(n * 0.393700);
	}
	*/

	/**
	 * Lookup a body part's configuration entry and figure out the current CM size of it for the player.
	 * @param value
	 * @param statName - body stat with numerical value, like hair, penis, waist, etc.
	 * @param adjust - Optional arg: adjust stat by this.
	 */
	export function statValueToCM(value: number, statName: BodyStat, adjust?: number): number;
	export function statValueToCM(value: number, statName: BodyStatStr, adjust?: number): number;
	export function statValueToCM(value: number, statName: BodyStatStr, adjust = 0): number {
		const stat = getStatConfig(Stat.Body)[statName];
		const cmScale = stat.cmMax - stat.cmMin;
		const statPercent = Math.floor(((value - stat.min) / (stat.max - stat.min)) * 100);
		return (cmScale * ((statPercent + adjust) / 100)) + stat.cmMin;
	}

    /**
     * Lookup a body part's configuration entry and figure out the current CM size of it for the player.
     * @param player
     * @param StatName - currently supported: Bust, Ass, Hips, Waist, Penis
     * @param Adjust - Optional arg: adjust stat by this and report figure.
     */
	export function statToCm(player: Entity.Player, satName: BodyStat, adjust?: number): number;
	export function statToCm(player: Entity.Player, statName: BodyStatStr, adjust?: number): number;
	export function statToCm(player: Entity.Player, statName: BodyStatStr, adjust = 0): number {
		if (typeof player.getStatConfig(Stat.Body)[statName] === 'undefined') return 0;

		const cmScale = player.getStatConfig(Stat.Body)[statName].cmMax
			- player.getStatConfig(Stat.Body)[statName].cmMin
		return (cmScale * ((player.getStatPercent(Stat.Body, statName) + adjust) / 100))
			+ player.getStatConfig(Stat.Body)[statName].cmMin;
	}

    /**
     * Convert Ass stat into CM
	 * @todo maybe unused
     */
	export function assInCM(player: Entity.Player): number {return statToCm(player, BodyPart.Ass);}

    /**
     * Convert Waist Stat into CM.
     */
	export function waistInCM(player: Entity.Player): number {return statToCm(player, BodyPart.Waist);}

    /**
     * Convert Hips Stat into CM.
     */
	export function hipsInCM(player: Entity.Player): number {return statToCm(player, BodyPart.Hips);}

    /**
     * Convert Bust Stat into CM.
     */
	export function busInCM(player: Entity.Player): number {return statToCm(player, BodyPart.Bust);}

    /**
     * Print out a 10 star colorized stat meter for a statistic.
     * @param statName
     * @param player
     * @param invert - reverse direction of stars relative to score so that high scores are less stars.
     */
	export function pStatMeter(statName: CoreStat, player: Entity.Player, invert = false): string {
		const statValue = player.getStat(Stat.Core, statName);

		if (statName == CoreStat.Hormones) {
			if (statValue > 100) // Return "Female" version of this meter.
				return pMeter((player.getStat(Stat.Core, statName) - 100), 100, invert);
			if (statValue <= 100)
				return pMeter((100 - statValue), 100, invert);
		}

		return pMeter(player.getStat(Stat.Core, statName),
			player.getMaxStat(Stat.Core, statName), invert);
	}

    /**
     * Return a string describing and coloring the effect
     */
	export function pEffectMeter(effect: string, item: Items.AnyItem): string {
		let output = "";
		const effectStr = effect.replace(/ /g, '&nbsp;');

		// Build color and arrow
		if (effect.indexOf('-') != -1) {
			output = "<span class='state-negative'>" + effectStr.replaceAll('-', '&dArr;') + "</span>";
		} else {
			if (effect.indexOf('+') != -1) {
				output = "<span class='state-positive'>" + effectStr.replaceAll('+', '&uArr;') + "</span>";
			} else {
				if (effect.indexOf('?') != -1) {
					output = "<span class='state-positive'>&uArr;" + effectStr + "</span>";
					if (item instanceof Items.Clothing) {
						output = output.replace(/RANK/g, "&uArr;".repeat(item.rankNumber));
					}
				} else {
					output = `<span class='state-neutral'>${effectStr}</span>`;
					output = output.replace(/RANK/g, "&uArr;");
				}
			}
		}
		return output;
	}
    /**
     * Print out a 10 star colorized stat meter for a skill.
     * @param statName
     * @param player
     * @param invert - reverse direction of stars relative to score so that high scores are less stars.
     */
	export function pSkillMeter(statName: Skills.Any, player: Entity.Player, invert?: boolean): string {
		return pMeter(player.getStat(Stat.Skill, statName), 100, invert);
	}

    /**
     * Finds and prints out the NPC quest dialog as a string.
     * @param questId - ID of the Quest.
     * @param stage - INTRO, MIDDLE, FINISH
     * @param player
     * @param npc - String, ID of the NPC in player.NPCs array.
     */
	export function pQuestDialog(questId: string, stage: QuestStage, player: Entity.Player, npc: Entity.NPC): string {
		return tokenizeString(player, npc, Data.quests[questId][stage]);
	}

    /**
     * Get ID for a reward choices radio group
     */
	function choiceRadioGroupId(scene: Scene): string {
		return "taskRewardChoices" + scene.id;
	}

    /**
     * Print out task rewards
     */
	export function pTaskRewards(task: Task): string[] {
		const output: string[] = [];

		let pay = task.pay;
		let tokens = task.tokens;
		const items: string[] = [];
		let slotUnlockCount = 0;

		for (const scene of task.scenes) {
			const reward = scene.rewardItems;
			if (reward.pay > 0) {pay += scene.rewardItems.pay;}
			if (reward.tokens > 0) {tokens += scene.rewardItems.tokens;}
			for (const ri of reward.items) {
				if (typeof ri === 'number') continue; // we had to put number here too in order to maintain order
				const n = Items.splitId(ri.name);
				const oItem = Items.factory(n.category, n.tag);
				items.push(`${oItem.description} x ${ri.value}`);
			}
			slotUnlockCount += reward.slotUnlockCount;
		}

		if (pay > 0) {
			output.push(`<span class='item-money'>${pay} coins</span>`);
		}

		if (tokens > 0) {
			output.push(`<span class='item-courtesan-token'>${tokens} courtesan tokens</span>`);
		}

		for (let i = 0; i < slotUnlockCount; ++i) {
			output.push("<span style='color:cyan'>A slot reel unlock!</span>");
		}
		output.push(...items);

		// we print item choices at the end, thus let's loop scenes one more time
		for (const scene of task.scenes) {
			const reward = scene.rewardItems;
			if (reward.itemChoices.length === 0) continue;
			const sceneId = scene.id;
			const radioName = choiceRadioGroupId(scene);
			let choiceSelection = "<ul id=\"ItemChoice" + sceneId + "\">\n";
			for (let i = 0; i < reward.itemChoices.length; ++i) {
				const ci = reward.itemChoices[i];
				const n = Items.splitId(ci.name);
				const oItem = Items.factory(n.category, n.tag);
				choiceSelection += `<li><input type="radio" id="${radioName}${i}" name="${radioName}" value="${i}"`;
				if (i === 0) {
					choiceSelection += " checked";
				}
				choiceSelection += `><label for="${radioName}${i}">${oItem.description} x ${ci.value}</label></li>`;
			}
			choiceSelection += "</ul>";
			output.push(choiceSelection);
		}
		return output;
	}

    /**
     * Set scene item choices basing on the user selection
     */
	export function setTaskRewardChoices(task: Task): void {
		for (const scene of task.scenes) {
			const reward = scene.rewardItems;
			if (reward.itemChoices.length === 0) continue;
			const selector = `input[name="${choiceRadioGroupId(scene)}"]:checked`
			const elem = document.querySelector(selector);
			if (elem) {
				reward.chosenItem = parseInt((elem as HTMLInputElement).value);
			} else {
				console.error(`Could not find reward element via selector: ${selector}`);
			}
		}
	}

    /**
     * Print the description of an item.
     */
	/*
	function pItemDesc(itemType: Items.Category, tag: string, amount: number, opt?: boolean): string {
		const oItem = Items.factory(itemType, tag);
		if (opt && amount > 1) return oItem.description + " x " + amount;
		return oItem.description;
	}
	*/

    /**
     * Print out a description of the players Ass statistic.
     */
	export function pAss(player: Entity.Player, brief = false): string {
		const aPercent = player.getStatPercent(Stat.Body, BodyPart.Ass);
		const fPercent = player.getStatPercent(Stat.Core, CoreStat.Fitness);

		if (brief) {
			return getAdjective(Stat.Body, BodyPart.Ass, aPercent, true) + ' '
				+ getAdjective(Stat.Body, DerivedBodyStat.AssFirmness, fPercent, true);
		}

		const hPercent = player.getStatPercent(Stat.Body, BodyPart.Hips);
		let output = tokenizeRating(player, Stat.Body, BodyPart.Ass, getRating(BodyPart.Ass, aPercent));

		if ((aPercent > 30) || (hPercent > 30)) {
			if (aPercent < (hPercent - 15)) {
				output += ` It is ${colorizeString(0.2, "disproportionately small", 1.0)} for your `;
			} else if (aPercent > (hPercent + 15)) {
				output += ` It is ${colorizeString(0.2, "disproportionately big", 1.0)} for your `;
			} else {
				output += ` It is ${colorizeString(0.4, "flattered", 1)} by your `;
			}
			output += getAdjective(Stat.Body, BodyPart.Hips, hPercent, true) + " hips.";
		}

		return output;
	}

    /**
     * Print out a description of the players Penis statistic.
     */
	export function pPenis(player: Entity.Player, onlyAdj = false): string {
		const pPercent = player.getStatPercent(Stat.Body, BodyPart.Penis);
		// var iLength = CMtoINCH(player.getStat(Stat.Body, BodyPart.Penis));
		if (onlyAdj) return getAdjective(Stat.Body, BodyPart.Penis, pPercent, true);
		return tokenizeRating(player, Stat.Body, BodyPart.Penis, getRating(BodyPart.Penis, pPercent));
	}

    /**
     * Print how does the futa state matches current body state
     */
	export function pFutaStatus(player: Entity.Player): string {
		const pFuta = player.getStatPercent(Stat.Core, CoreStat.Futa);
		const hormones = player.getStat(Stat.Core, CoreStat.Hormones);
		if ((hormones > 100)) {
			const pPenis = player.getStatPercent(Stat.Body, BodyPart.Penis);
			const dp = pFuta - pPenis;
			if (dp > 90) return "You crave for a bigger penis."
			if (dp > 60) return "You feel an urge to grow a bigger penis."
			if (dp > 30) return "You are pretty sure a bigger penis would be a good thing to get."
			if (dp > 5) return "You feel your penis could be a bit bigger."
			return "You consider your penis size to be about right for you."
		} else {
			const pBust = player.getStatPercent(Stat.Body,BodyPart.Bust);
			const db = pFuta - pBust;
			if (db > 90) return "You crave for bigger tits."
			if (db > 60) return "You feel an urge to grow your boobs."
			if (db > 30) return "You are pretty sure bigger tits would be a good thing to get."
			if (db > 5) return "You feel your bust could be a bit bigger."
			return "You consider your bust size to be about right for you."
		}
	}

    /**
     * Prints out a description of the player's height statistic.
     */
	export function pHeight(player: Entity.Player, brief = false): string {
		const pHeight = statToCm(player, BodyProperty.Height);
		return lengthString(pHeight, true) + (brief ? "" : " tall");
	}

    /**
     * Prints out a description of the player's fetish statistic.
     */
	export function pFetish(player: Entity.Player): string {
		return colorizeString(player.fetish, getRating("fetish", player.fetish));
	}

    /**
     * Prints out a description of the player's height statistic.
     */
	export function pBeauty(player: Entity.Player): string {
		return colorizeString(player.beauty, getRating("beauty", player.beauty));
	}

    /**
     * Print out a description of the player's Balls statistic.
     */
	export function pBalls(player: Entity.Player, brief = false): string {
		const bPercent = player.getStatPercent(Stat.Body, BodyPart.Balls);
		if (brief) return getAdjective(Stat.Body, BodyPart.Balls, bPercent, true);
		return tokenizeRating(player, Stat.Body, BodyPart.Balls, getRating(BodyPart.Balls, bPercent));
	}

    /**
     * Print out a description of the player's Waist statistic.
     */
	export function pWaist(player: Entity.Player, brief = false): string {
		const wPercent = player.getStatPercent(Stat.Body, BodyPart.Waist);
		// var iLength = CMtoINCH(WaistInCM(player));
		if (brief) return getAdjective(Stat.Body, BodyPart.Waist, wPercent, true);
		return tokenizeRating(player, Stat.Body, BodyPart.Waist, getRating(BodyPart.Waist, wPercent));
	}

    /**
     * Print out a description of the player's Bust statistic.
     */
	export function pBust(player: Entity.Player, brief = false): string {
		const bPercent = player.getStatPercent(Stat.Body, BodyPart.Bust);
		if (brief) {
			const fPercent = player.getStatPercent(Stat.Body, BodyProperty.BustFirmness);
			return getAdjective(Stat.Body, BodyPart.Bust, bPercent, true) + ' '
				+ getAdjective(Stat.Body, BodyProperty.BustFirmness, fPercent, true);
		}
		return tokenizeRating(player, Stat.Body, BodyPart.Bust, getRating(BodyPart.Bust, bPercent));
	}

    /**
     * Print out a description of the player's bust firmness statistic.
     */
	export function pBustFirmness(player: Entity.Player, brief = false): string {
		const fPercent = player.getStatPercent(Stat.Body, BodyProperty.BustFirmness);
		if (brief) {
			return getAdjective(Stat.Body, BodyProperty.BustFirmness, fPercent, true);
		}
		return tokenizeRating(player, Stat.Body, BodyProperty.BustFirmness, getRating(BodyProperty.BustFirmness, fPercent));
	}

    /**
     * Print out a description of the player's bust lactation statistic.
     */
	export function pLactation(player: Entity.Player, brief = false): string {
		const fPercent = player.getStatPercent(Stat.Body, BodyProperty.Lactation);
		if (brief) {
			return getAdjective(Stat.Body, BodyProperty.Lactation, fPercent, true);
		}
		return tokenizeRating(player, Stat.Body, BodyProperty.Lactation, getRating(BodyProperty.Lactation, fPercent));
	}

    /**
     * Print out a description of the player's Bust (CUP) statistic.
     */
	export function pCup(player: Entity.Player): string {
		const bustStatVal = player.getStat(Stat.Body, BodyPart.Bust);
		const cc = (getTotalXPPoints(Stat.Body, BodyPart.Bust, 0, bustStatVal) +
			player.getStatXP(Stat.Body, BodyPart.Bust)) / 3.23;
		return unitSystem.cupString(bustStatVal) + " cup (" + unitSystem.massString(cc, true, 1000) + " each)";
	}

    /**
     * Print out a description of the player's Lips statistic.
     */
	export function pLips(player: Entity.Player, brief = false): string {
		const lPercent = player.getStatPercent(Stat.Body, BodyPart.Lips);
		if (brief) return getAdjective(Stat.Body, BodyPart.Lips, lPercent, true);
		return tokenizeRating(player, Stat.Body, BodyPart.Lips, getRating(BodyPart.Lips, lPercent));
	}
    /**
     * Print out a description of the player's Hips statistic.
     */
	export function pHips(player: Entity.Player, brief = false): string {
		const hPercent = player.getStatPercent(Stat.Body, BodyPart.Hips);
		if (brief) return getAdjective(Stat.Body, BodyPart.Hips, hPercent, true);
		return tokenizeRating(player, Stat.Body, BodyPart.Hips, getRating(BodyPart.Hips, hPercent));
	}

    /**
     * Print out a description of the player's Eyes.
     */
	export function pEyes(player: Entity.Player): string {
		const lashes = player.getStatPercent(Stat.Core, CoreStat.Hormones) >= 75 ? "long" : "average length";
		return "You have " + lashes + " eyelashes and " + player.eyeColor + " colored eyes.";
	}
    /**
     * Print out a description of the player's Style.
     */
	export function pStyle(player: Entity.Player): string {
		return colorizeString(player.style, getRating("style", player.style));
	}

    /**
     * Print out a description of the player's Clothing.
     */
	export function pClothing(player: Entity.Player): string {
		return colorizeString(player.clothesRating, getRating("clothing", player.clothesRating));
	}

    /**
     * Print out a description of the player's Makeup.
     */
	export function pMakeup(player: Entity.Player): string {
		return player.makeupStyle === "plain faced" ? "You are plain faced and not wearing any makeup"
			: "You are wearing " + colorizeString(player.makeupRating, player.makeupStyle) + " makeup";
	}

    /**
     * Print out a description of the player's Face.
     */
	export function pFace(player: Entity.Player, brief = false): string {
		const fPercent = player.getStatPercent(Stat.Body, BodyPart.Face);
		if (brief) return getAdjective(Stat.Body, BodyPart.Face, fPercent, true);
		let output = tokenizeRating(player, Stat.Body, BodyPart.Face, getRating(BodyPart.Face, fPercent));
		if (player.makeupRating == 0) {
			output += " it is bare and devoid of cosmetics.";
		} else if (player.makeupRating < 40) {
			output += " it is poorly done up in " +
				colorizeString(player.makeupRating, player.makeupStyle) + " makeup.";
		} else if (player.makeupRating < 60) {
			output += " it is moderately well done up in " +
				colorizeString(player.makeupRating, player.makeupStyle) + " makeup, somewhat enhancing your appeal.";
		} else if (player.makeupRating < 80) {
			output += " it is expertly done up in " +
				colorizeString(player.makeupRating, player.makeupStyle) + " makeup, enhancing your appeal.";
		} else {
			output += " it is flawlessly painted in " +
				colorizeString(player.makeupRating, player.makeupStyle) + " makeup, greatly enhancing your appeal.";
		}
		return output;
	}

    /**
     * Print out a description of the player's Fitness.
     */
	export function pFitness(player: Entity.Player, brief = false): string {
		const fPercent = player.getStatPercent(Stat.Core, CoreStat.Fitness);
		if (brief) return getAdjective(Stat.Core, CoreStat.Fitness, fPercent, true);
		return tokenizeRating(player, Stat.Core, CoreStat.Fitness, getRating(CoreStat.Fitness, fPercent));
	}

    /**
     * Print out a description of the player's Hormones.
     */
	export function pHormones(player: Entity.Player): string {
		return getAdjective(Stat.Core, CoreStat.Hormones, player.getStat(Stat.Core, CoreStat.Hormones), true);
	}

    /**
     * Print out a description of the player's Hair.
     */
	export function pHair(player: Entity.Player, brief = false): string {
		const wig = player.getEquipmentInSlot(ClothingSlot.Wig);
		if (wig) {
			if (brief) {
				return `${wig.hairLength} ${wig.hairColor} wig`;
			}
			return "You are wearing a wig to hide your natural hair. It is " + wig.hairColor + " and " +
				lengthString(wig.hairLength) + " long, styled in " +
				colorizeString(wig.hairBonus, wig.hairStyle) + ".";
		}
		if (brief) {
			return lengthString(statToCm(player, BodyPart.Hair), false) + " " + player.hairColor;
		}
		return "Your hair is " + player.hairColor + " and " + lengthString(statToCm(player, BodyPart.Hair), false) +
			" long, styled in " + colorizeString(player.hairRating, player.hairStyle) + ".";
	}

    /**
     * Print out a description of the player's Figure.
     */
	export function pFigure(player: Entity.Player): string {
		const pBust = statToCm(player, BodyPart.Bust);
		const pWaist = statToCm(player, BodyPart.Waist);
		const pHips = statToCm(player, BodyPart.Hips);

		const statsStr = `${lengthValue(pBust)}-${lengthValue(pWaist)}-${lengthValue(pHips)} figure`;

		const rBustHips = pBust / pHips;

		// Cases for being fat (too much waist). Go on a diet.
		if (pWaist >= 120) return `a ${colorizeString(0, "morbidly obese", 1)} ${statsStr}`;
		if (pWaist >= 100) return `an ${colorizeString(0.05, "obese", 1)} ${statsStr}`;
		if (pWaist >= 90) return `a ${colorizeString(0.1, "fat", 1)} ${statsStr}`;

		// Boobs and hips are in proportion
		if (rBustHips <= 1.1 && rBustHips >= 0.9) {
			if (pBust >= 95 && pWaist <= 75) return `an ${colorizeString(1, "extreme hourglass", 1)} ${statsStr}r`;
			if (pBust >= 95) return `an ${colorizeString(0.9, "hourglass", 1)} ${statsStr}`;
			if (pBust >= 90) return `a ${colorizeString(0.8, "very curvy", 1)} ${statsStr}`;
			if (pBust >= 85) return `a ${colorizeString(0.7, "curvy", 1)}  ${statsStr}`;
			if (pBust >= 82) return `a ${colorizeString(0.6, "slightly curvy", 1)} ${statsStr}`;
			if (pWaist <= 55) return `a ${colorizeString(0.5, "petite", 1)} ${statsStr}`;
			if (pWaist <= 70) return `a ${colorizeString(0.4, "slender", 1)} ${statsStr}`;
			if (pWaist < 80) return `a ${colorizeString(0.3, "thin", 1)} ${statsStr}`;
			return `an ${colorizeString(0.2, "average", 1)} ${statsStr}`;
		}

		// Boobs are bigger than hips
		if (rBustHips >= 1.25 && pBust >= 90) return `a ${colorizeString(0.6, "top heavy", 1)} ${statsStr}`;

		if (rBustHips <= 0.75 && pHips >= 90) return `a ${colorizeString(0.6, "bottom heavy", 1)} ${statsStr}`;

		if (pWaist <= 55) return `a ${colorizeString(0.4, "petite", 1)} ${statsStr}`;
		if (pWaist <= 70) return `a ${colorizeString(0.4, "slender", 1)} ${statsStr}`;
		if (pWaist < 80) return `a ${colorizeString(0.4, "thin", 1)} ${statsStr}`;

		return `an ${colorizeString(0.2, "average", 1)} ${statsStr}`;
	}

    /**
     * Replace tokens in string with calculated/derived literals and return it.
     */
	export function tokenizeString(player: Entity.Player, npc: Entity.NPC | undefined, str: string): string {
		if (typeof npc !== 'undefined') {
			str = str.replace(/NPC_NAME's/g, "<span class='npc'>" + npc.name + "'s</span>");
			str = str.replace(/NPC_NAME/g, "<span class='npc'>" + npc.name + "</span>");
			str = str.replace(/NPC_([A-Z_]+!?)/g, (_m, c: string) => {
				const npcAny = npc as any;
				const prop = _.camelCase(c);
				const v = c.endsWith('!') ? npcAny[prop.slice(0, -1)]() : npcAny[prop];
				return v !== undefined ? `${v}` : `ERROR: NO VALUE FOR PROPERTY '${c}' FOR NPC '${npc.name}'`;
			});
		}

		function adjReplacer(_match: string, stat: string) {
			const statName = _.camelCase(stat) as BodyStatStr;
			return getAdjective(Stat.Body, statName, player.getStat(Stat.Body, statName), true);
		}
		function nounReplacer(_match: string, stat: string) {
			return getPlayerNoun(Stat.Body, _.camelCase(stat) as BodyPart, player, false, true);
		}
		function pReplacer(_match: string, prefix: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> StatName
			const statName = _.pascalCase(stat);
			const statFuncName = 'p' + statName;
			if (PR.hasOwnProperty(statFuncName))
				// @ts-expect-error
				return `${PR[statFuncName](player, true)}${delim}`;
			return prefix + stat + delim;
		}
		function nReplacer(_match: string, prefix: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> statName
			const statName = _.camelCase(stat) as BodyPart;
			if (Data.naming.bodyConfig.hasOwnProperty(statName))
				return getPlayerNoun(Stat.Body, statName, player, true, true) + delim;
			return prefix + stat + delim;
		}
		function vReplacer(_match: string, prefix: string, stat: string, delim: string) {
			// uppercase characters following underscore (which is removed)
			// STAT_NAME -> statName
			const statName = _.camelCase(stat);
			if (player.getStatObject(Stat.Body).hasOwnProperty(statName))
				return `${player.getStat(Stat.Body, statName as BodyStat)}${delim}`;
			return prefix + stat + delim;
		}

		// Usage: p(Slot1|Slot2|...|$optional default string)
		// Use with player.isEquipped(string|array,bool) for most cases.
		function equipReplacer(_match: string, part: string) {
			const slots = part.split("|");
			for (const s of slots) {
				if (s.startsWith('$')) return s.slice(1); // default string
				const equip = player.getEquipmentInSlot(s as ClothingSlot);
				if (equip != null) return equip.description;
			}
			return "<span style='color:red'>bug!</span>";
		}

		// Like pReplacer, but pass an argument instead of using the characters statistic.
		function pReplacer2(_match: string, _prefix: string, stat: string, num: string, delim: string) {
			const statName = _.camelCase(stat);
			const statType: Stat = player.coreStats.hasOwnProperty(statName) ? Stat.Core :
				player.skills.hasOwnProperty(statName) ? Stat.Skill : Stat.Body;

			return getAdjective(statType, statName as any, Number(num), true) + delim;
		}

		str = str.replace(/PLAYER_NAME/g, "<span class='state-feminity'>" + player.slaveName + "</span>");
		str = str.replace(/GF_NAME/g, "<span class='state-girlinness'>" + player.girlfriendName + "</span>");
		str = str.replace(/pCUP/g, pCup(player)); // needs special handling because it has only a single parameter
		str = str.replace(/NOUN_([A-Za-z_]+)/g, nounReplacer);
		str = str.replace(/ADJECTIVE_([A-Za-z_]+)/g, adjReplacer);
		str = str.replace(/pBLOWJOBS/g, getAdjective(Stat.Skill, Skills.Sexual.BlowJobs,
			player.getStat(Stat.Skill, Skills.Sexual.BlowJobs), true));
		str = str.replace(/pPHASE/g, setup.world.phaseName(false));
		str = str.replace(/pEQUIP\(([^\)]*)\)/g, equipReplacer);
		str = str.replace(/(p)([A-Z]+)_([0-9]+)([^0-9]|$)/g, pReplacer2);
		str = str.replace(/(q)([A-Z_]+)([^A-Za-z]|$)/g, pReplacer);
		str = str.replace(/(p)([A-Z_]+)([^A-Za-z]|$)/g, pReplacer);
		str = str.replace(/(n)([A-Z_]+)([^A-Za-z]|$)/g, nReplacer);
		str = str.replace(/(v)([A-Z_]+)([^A-Za-z]|$)/g, vReplacer);
		// Hack for highlighting NPC speech
		str = str.replace(/s\(([^\)]+)\)/g, (_m, p: string) => `<span class='npcText'>"${p}"</span>`);
		// Important! highlight NPC speech
		str = str.replace(/s\!\(([^\)]+)\)/g, (_m, p: string) => `<span class='impText'>"${p}"</span>`);
		// Highlighting PC speech
		str = str.replace(/sp\(([^\)]+)\)/g, (_m, p: string) => `<span class='pcText'>"${p}"</span>`);
		// Highlighting PC thoughts
		str = str.replace(/tp\(([^\)]+)\)/g, (_m, p: string) => `<span class='pcThought'>"${p}"</span>`);

		return str;
	}

	export function pSkillName(skill: Skills.Any): string {
		return Data.Lists.skillConfig[skill].altName ?? skill.capitalizeFirstLetter();
	}

	export function pShipMapIcon(index: number): void {
		if (index >= Data.Lists.shipRoute.length) index = 0; // Force reset.
		const top = Data.Lists.shipRoute[index].top;
		const left = Data.Lists.shipRoute[index].left;
		console.log(`Placing map icon at top=${top}, left=${left}`);
		$(document).one(":passageend", () => {$("#mapIcon").css({top: top, left: left});});
	}

	export function phaseName(phase: number): string {
		return phase >= 0 && phase <= 4 ?
			["morning", "afternoon", "evening", "night", "late night"][phase] : phase.toString();
	}

    /**
     * Get icon for marking favorite item in inventory or shop lists
     */
	export function getItemFavoriteIcon(isFavorite: boolean): string {
		return isFavorite ? "<span class='action-general'>&#9733;</span>" : "<span>&#9734;</span>";
	}

    /**
     * Get random item from inventory
     */
	export function getRandomItemId(player: Entity.Player): string {
		const itemTypeCount = player.inventoryItemsCount();
		let randomIndex = Math.floor(Math.random() * itemTypeCount);
		let id = "";
		player.inventoryManager.everyItemRecord(undefined, undefined,
			(n, tag, itemClass) => {
				if (randomIndex <= 0) {
					id = Items.makeId(itemClass, tag);
					return false;
				}
				randomIndex -= n;
				return true;
			}
		);
		return id;
	}

	export function refreshTwineMoney(): void {
		try {
			$("#Money").text(setup.player.money.toString());
		} catch (err) {

		}
	}

	/*
	function refreshTwineTokens(): void {
		try {
			$("#Tokens").text(setup.player.tokens.toString());
		} catch (err) {

		}
	}
	*/

	export function pHormoneSymbol(): string {
		const val = setup.player.coreStats.hormones;

		if (val < 78) {
			return "<span id='HormoneSymbol' class='state-masculinity'>♂</span>";
		} else if (val >= 144) {
			return "<span id='HormoneSymbol' class='state-feminity'>♀</span>";
		} else {
			return "<span id='HormoneSymbol' class='state-neutral'>⚥</span>";
		}
	}

	export function refreshTwineMeter(m: CoreStat): void {
		const invert = m === CoreStat.Toxicity;
		try {
			$("#" + m).html(pStatMeter(m, setup.player, invert));
			if (m === CoreStat.Hormones) {
				$("#HormoneSymbol").html(pHormoneSymbol());
			}
		} catch (err) {

		}
	}

	/*
	function refreshSlaveName() {
		try {
			$("#scoreSlaveName").text('"' + setup.player.slaveName + '"');
		} catch (err) {

		}
	}
	*/

	export function refreshTwineScore(): void {
		// Redraw Energy Bars
		try {
			refreshTwineMeter(CoreStat.Health);
			refreshTwineMeter(CoreStat.Energy);
			refreshTwineMeter(CoreStat.Willpower);
			refreshTwineMeter(CoreStat.Perversion);
			refreshTwineMeter(CoreStat.Nutrition);
			refreshTwineMeter(CoreStat.Femininity);
			refreshTwineMeter(CoreStat.Toxicity);
			refreshTwineMeter(CoreStat.Hormones);
		} catch (err) {
			// no-op
		}
	}

    /**
     * Prints item description for the inventory list
     */
	export function printItem(item: Items.Clothing | Items.Consumable | Items.QuestItem, player: Entity.Player): string {
		let res = "<span class='inventoryItem'>" + item.description;
		if (settings.inlineItemDetails) {
			res += '<span class="tooltip">' + item.examine(player, false) + '</span></span>';
			res += "<br><div class='inventoryItemDetails'>" + item.examine(player, true) + '</div>';
		} else {
			res += '</span>';
		}
		return res;
	}

    /**
     * Highlight active button in a tabbar
     *
     * Finds the active element and appends " active" to its style, removing " active" from all
     * other children of the tabbar.
     * @param tabbarId  Id of the tab bar element
     * @param activeButtonId Id of the button for the active tab
     * @param activeTabText Text to set for .activeTabCaption children of the tab bar
     *
     * @example <div id="tabbar">
     *  <span class="activeTabCaption">placeholder text</span>
     *  <span class="tablink" id="btn1"><button class="mybutton">Button1</button></span>
     *  <span class="tablink" id="btn2"><button class="mybutton">Button2</button></span>
     * </div>
     *
     * Then calling HighlightActiveTabButton("tabbar", "btn1", "Sample text")
     *
     * will replace "placeholder text" with "Sample text" and append " active" to btn1 class:
     * <span class="tablink" id="btn1"><button class="mybutton active">Button1</button></span>
     *
     * The next call HighlightActiveTabButton("tabbar", "btn2", "Sample text2") will result in:
     * <div id="tabbar">
     *  <span class="activeTabCaption">Sample text2</span>
     *  <span class="tablink" id="btn1"><button class="mybutton">Button1</button></span>
     *  <span class="tablink" id="btn2"><button class="mybutton active">Button2</button></span>
     * </div>
     */
	export function highlightActiveTabButton(tabbarId: string, activeButtonId: string, activeTabText: string): void {
		const tabBar = document.getElementById(tabbarId);
		if (!tabBar) return;
		const tabs = tabBar.getElementsByClassName("tablink");
		for (const e of tabs) {
			if (e.firstChild instanceof HTMLElement) {
				e.firstChild.className = e.firstChild.className.replace(" active", "");
			}
		}
		const linkElem = document.getElementById(activeButtonId);
		if (linkElem && linkElem.firstChild instanceof HTMLElement) linkElem.firstChild.className += " active";
		const activeTab = tabBar.getElementsByClassName("activeTabCaption");
		for (const e of activeTab) {
			if (e instanceof HTMLElement) {
				e.innerText = activeTabText;
			}
		}
	}

	export function risingDialog(element: string | HTMLElement, message: string, color?: string): void;
	export function risingDialog(element: any, message: string, color = "white"): void {
		const root = $(element);
		$('#WhoreDialogDiv2').remove();

		const div = $('<div>').addClass('WhoreDialog').attr('id', 'WhoreDialogDiv2');
		const header = $('<h1>').addClass('ml13').html(message);
		header.css('color', color);
		div.append(header);
		root.append(div)

		// Wrap every letter in a span
		$('.ml13').each(function () {
			$(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
		});

		anime.timeline({loop: false})
			.add({
				targets: '.ml13 .letter',
				translateY: [100, 0],
				translateZ: 0,
				opacity: [0, 1],
				easing: "easeOutExpo",
				duration: 1000,
				delay: function (_el, i) {
					return 300 + 30 * i;
				}
			}).add({
				targets: '.ml13 .letter',
				translateY: [0, -100],
				opacity: [1, 0],
				easing: "easeInExpo",
				duration: 1000,
				delay: function (_el, i) {
					return 100 + 30 * i;
				}
			});
	}

	export function dialogBox(element: HTMLElement, message: string, props?: Record<string, string | number>): void;
	export function dialogBox(element: string, message: string, props?: Record<string, string | number>): void;
	export function dialogBox(element: any, message: string, props: JQuery.PlainObject<string|number> = {}): void {
		const bgColor = props.color;
		const fgColor = props.fgColor;
		const lineProps: JQuery.PlainObject<string | number> = bgColor !== undefined ? {"background-color": bgColor} : {};
		const textProps: JQuery.PlainObject<string | number> = fgColor !== undefined ? {color: fgColor} : {};
		const root = $(element);
		$('#WhoreDialogDiv').remove();

		const div = $('<div>').addClass('WhoreDialog').attr('id', 'WhoreDialogDiv').css(props);
		const header = $('<h1>').addClass('ml1');
		const inner = $('<span>').addClass('text-wrapper');

		inner.append($('<span>').addClass('line line1').css(lineProps));
		inner.append($('<span>').addClass('letters').html(message).css(textProps));
		inner.append($('<span>').addClass('line line2').css(lineProps));

		header.append(inner);
		div.append(header);
		root.append(div);

		// Javascript animations.
		$('.ml1 .letters').each(function () {
			$(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
		});

		anime.timeline({loop: false})
			.add({
				targets: '.ml1 .letter',
				scale: [0.3, 1],
				opacity: [0, 1],
				translateZ: 0,
				easing: "easeOutExpo",
				duration: 600,
				delay: function (_el, i) {
					return 70 * (i + 1)
				}
			}).add({
				targets: '.ml1 .line',
				scaleX: [0, 1],
				opacity: [0.5, 1],
				easing: "easeOutExpo",
				duration: 700,
				offset: '-=875',
				delay: function (_el, i, l) {
					return 80 * (l - i);
				}
			}).add({
				targets: '.ml1',
				opacity: 0,
				duration: 1000,
				easing: "easeOutExpo",
				// delay: 1000
				// delay: 500
			});
	}

	// Stuff to support fight club

	export function fightClubFlag(club: string, flag?: string): string {
		const base = "FIGHTCLUB_TRACK_" + club.replace(/ /g, "_");
		return flag ? base + '_' + flag.toUpperCase() : base;
	}

	export function addFightClubResult(player: Entity.Player, club: string, victory: boolean): number {
		const key = victory ? fightClubFlag(club) + "_WINS" : fightClubFlag(club) + "_LOSSES";
		if (player.questFlags.hasOwnProperty(key)) {
			player.questFlags[key] = 1 + <number>player.questFlags[key];
		} else {
			player.questFlags[key] = 1;
		}

		return <number>player.questFlags[key];
	}

	export function playerStatChooser<T extends keyof StatTypeMap>(type: T, stat: StatTypeMap[T], steps: number| number[]): string {
		const res: string[] = [];

		let values: number[] = [];
		if (typeof steps === "number") {
			const cfg = getStatConfig(type)[stat];
			const step = (cfg.max - cfg.min) / steps;
			for (let v = cfg.min; v <= cfg.max; v += step) {
				values.push(v);
			}
		} else {
			values = steps;
		}

		function setStat(type: T, stat: StatTypeMap[T], value: number) {
			setup.player.setStat(type, stat, value);
		}

		for (const v of values) {
			const adj = getAdjective(type, stat, v, true);
			const strVal = adj !== "" ? adj : lengthString(statValueToCM(v, stat as BodyStatStr))
			res.push(UI.link(strVal, setStat, [type, stat, v]));
		}
		return res.join(" | ");
	}

	/**
 	 * Handler for the meters 'print numbers' setting
 	 */
	export function handleMetersNumberValueSettingChanged(): void {
		if (settings.displayMeterNumber) {
			// to accommodate longer meters
			$('#ui-bar').css("width","350px"); // seems to work better on most browsers
			PR.setNumericalMeters(true);
		} else {
			PR.setNumericalMeters(false);
			$('#ui-bar').css("width","330px");
		}

		PR.refreshTwineScore();
	}

	export function handleDisplayBodyScoreChanged(): void {
		if (!settings.displayBodyScore) {
			$('#bodyScoreContainer').empty();
		}
	}

	export function handleAutosaveChanged(): void {
		const safeLocationTag = "hideout";
		if (settings.autosaveAtSafePlaces) {
			if (Array.isArray(Config.saves.autosave)) {
				if (!Config.saves.autosave.contains(safeLocationTag)) {
					Config.saves.autosave.push(safeLocationTag);
				}
			} else {
				Config.saves.autosave = [safeLocationTag];
			}
		} else {
			if (Array.isArray(Config.saves.autosave)) {
				Config.saves.autosave = Config.saves.autosave.delete(safeLocationTag);
			} else {
				Config.saves.autosave = [];
			}
		}
	}

	/**
	 * Returns the phase icon for the current phase.
	 */
	export function phaseIcon(phase: Phase): string {
		switch (phase) {
			case Phase.Morning: return "@@color:yellow;&#9788;@@";
			case Phase.Afternoon: return "@@color:orange;&#9728;@@";
			case Phase.Evening: return "@@color:azure;&#9734;@@";
			case Phase.Night: return "@@color:cornflowerblue;&#9789;@@";
			case Phase.LateNight: return "@@color:DeepPink;&#9789;@@";
		}
	}

	export function phaseIconStrip(): string {
		let res = '';
		const phase = Entity.World.instance.state.phase;
		for (let i = 0; i < 5; ++i) {
			res += i === phase ? `[${phaseIcon(i)}]` : phaseIcon(i);
		}
		return res;
	}
}
