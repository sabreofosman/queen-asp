/*
 * Prints out an element to be filled later on by App.UI.rPostProcess()
 * <<NPC "<npc_name>">> or <<NPC "<npc_name>" "<custom_action>" "<action_passage>">>
 */
Macro.add("NPC", {
	handler() {
		const npcDiv = document.createElement("div");
		npcDiv.id = `npc-${this.args[0]}`
		npcDiv.setAttribute('data-npc-name', this.args[0] as string);
		if (this.args.length > 1) {
			npcDiv.setAttribute('data-npc-args', `${this.args[1]},${this.args[2]}`);
		}
		this.output.append(npcDiv);
	},
});

/*
* Adds a travel sprit with the link to continue travel Optional argument can be used to pass custom  link text ("Continue" is the default)
*/
Macro.add('eventContinue', {
	handler() {
		this.output.append(App.UI.wTravels(setup.player, [{text: this.args[0] as string ?? "Continue", destination: setup.eventEngine.toPassage ?? "error"}]));
	},
});

/*
 * Adds a travel link for the given destination(s). If destination is of <str1>|<str2>, str1 is the text, str2 is passage name
 */
Macro.add('travel', {
	handler() {
		if (!this.args.length) {
			return this.error("No travel destination probided");
		}
		const destinations: App.Data.Travel.Destination[] = this.args
			.map((s) => {
				if (typeof s !== "string") {
					throw "Invalid argument to <<travel>>: only strings are allowed";
				}
				if (s.startsWith('[[')) {
					const pl = App.parseSCPassageLink(s);
					return {text: pl.text, destination: pl.link};
				} else {
					const parts = s.split('|');
					return parts.length > 1 ? {text: parts[0], destination: parts[1]} : s;
				}
			});
		if (destinations.length) {
			this.output.append(App.UI.wTravels(setup.player, destinations as App.Data.Travel.Destinations));
		}
	},
});

/*
	<<includeDOM element>>
	Simply inserts a given DOM element.
 */
Macro.add("includeDOM", {
	handler() {
		try {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
			this.output.append(Scripting.evalJavaScript(this.args.full));
		} catch (ex) {
			if (ex instanceof Error) {
				return this.error(`error evaluating ${this.args.full}: ${ex.name}: ${ex.message}`);
			} else {
				// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
				return this.error(`error evaluating ${this.args.full}: ${ex}`);
			}
		}
	},
});
