namespace App.UI {

	export abstract class DOMPassage {
		constructor(nameOrNames: string | string[]) {
			if (Array.isArray(nameOrNames)) {
				for (const n of nameOrNames) {
					PassageRenderManager.instance.register(n, this);
				}
			} else {
				PassageRenderManager.instance.register(nameOrNames, this);
			}
		}

		public abstract render(passageName: string, content: HTMLElement): HTMLElement | DocumentFragment | null;
	}

	type PassageRenderHandler = (passageName: string, content: HTMLElement) => HTMLElement | DocumentFragment | null;

	export class PassageRenderManager {
		private static _instance: PassageRenderManager | null = null;

		static get instance(): PassageRenderManager {
			if (PassageRenderManager._instance === null) {
				PassageRenderManager._instance = new PassageRenderManager();
			}
			return PassageRenderManager._instance;
		}

		constructor() {
			$(document).on(":passagerender", (ev) => {
				const renderer = this._passages.get(ev.passage.title);
				if (renderer) {
					const newContent = renderer.render(ev.passage.title, ev.content);
					if (newContent) {
						ev.content.append(newContent);
					}
				}

				for (const h of this._handlers) {
					const newContent = h(ev.passage.title, ev.content);
					if (newContent) {
						ev.content.append(newContent);
					}
				}
			});

			$(document).on(":passagedisplay", () => {
				const pn = "StoryCaption";
				const renderer = this._passages.get(pn);
				if (renderer && !variables().hasOwnProperty('InIntro')) {
					const elem = document.querySelector("#my-story-caption") as HTMLElement;
					const newContent = renderer.render(pn, elem);
					if (newContent) {
						App.UI.replace(elem, newContent);
					}
				}
			});
		}

		register(passageName: string, passage: DOMPassage): void {
			console.assert(!this._passages.has(passageName), "DOM passage named %s is already registered", passageName);
			this._passages.set(passageName, passage);
		}

		registerHandler(handler: PassageRenderHandler): void {
			this._handlers.push(handler);
		}

		unregister(passageName: string): void {
			this._passages.delete(passageName);
		}

		private readonly _passages: Map<string, DOMPassage> = new Map();
		private readonly _handlers: PassageRenderHandler[] = [];
	}
}
