/* eslint-disable @typescript-eslint/naming-convention */
App.registerPassageDisplayNames({
	Bazaar: "The Bazaar",
	GI_Apothecary: "Apothecary",
	GI_BackStage: "Backstage",
	GI_CheapBrothel: "The Saucy Slattern",
	GI_Clothiers: "Bella's Boudoir",
	GI_DanceHall: "Minxy's Dance Hall",
	GI_GeneralGoods: "Golden Goods",
	GI_GovernorsMansion: "The Governor's Mansion",
	GI_GovernorsMansionInside: "Enter the Mansion",
	GI_GuardStation: "Guard Station",
	GI_Jail: "Jail House",
	GI_Salon: "Madame Yvonne's Salon",
	GoldenIsle: "The Docks",
	GoldenIsleFightClub: "Exhibition Yard",
	HighHill: "High Hill District",
	Portside: "Portside District",
	SmugglersDen: "Smuggler's Den",
});

App.registerTravelDestination({
	GoldenIsle: [
		{
			text: "Back to The Mermaid",
			destination: "Deck"
		},
		"SmugglersDen", "Portside"
	],
	SmugglersDen: ["GoldenIsle"],
	Portside: ["GoldenIsle", "GI_GeneralGoods", "GI_CheapBrothel", "Bazaar", "GI_GuardStation"],
	Bazaar: ["Portside", "GI_Apothecary", "GoldenIsleFightClub"],
	GI_GeneralGoods: ["Portside"],
	GI_CheapBrothel: ["Portside"],
	GI_GuardStation: [
		"Portside", "GI_Jail",
		{
			available: player => !App.Quest.isCompleted(player, "JUSTUS_WHORING"),
			text: "High Hill District",
			note: `You move forward toward's the entrance to the @@.location-name;Hill Hill District@@, but the Guard Captain steps
			forward and blocks your way.

			@@.npc;Justus@@ says, <span class="npcText">"There's no way I'll let a vagrant such as yourself pass
			through these gates. Be gone slut, ply your trade somewhere else!"</span>

			//Surely there can be some way to convince him to let you pass?//`
		},
		{
			available: player => App.Quest.isCompleted(player, "JUSTUS_WHORING"),
			destination: "HighHill"
		}
	],
	GI_Jail: ["GI_GuardStation"],
	GI_Apothecary: ["Bazaar"],
	GoldenIsleFightClub: ["Bazaar"],
	HighHill: ["GI_GuardStation", "GI_Salon", "GI_Clothiers", "GI_DanceHall", "GI_GovernorsMansion"],
	GI_Salon: ["HighHill"],
	GI_Clothiers: ["HighHill"],
	GI_DanceHall: ["HighHill", "GI_BackStage"],
	GI_StudyCrowd: ["GI_BackStage"],
	GI_GovernorsMansion: [
		"HighHill",
		{
			available: player => !App.Quest.isCompleted(player, "ROYAL_ENTRANCE"),
			text: "Enter the Mansion",
			note: `As you approach the entrance to the @@.location-name;Governor's Mansion@@ a huge giant of a man stop you.

			@@.npc;Uticus@@ says, <span class="npcText">"Oi yee can't be entering there without my say so!"</span>

			You take one look at his giant frame and equally impressive cutlass and decide that proceeding would be a very unwise idea.`

		},
		{
			available: player => App.Quest.isCompleted(player, "ROYAL_ENTRANCE") &&
				setup.world.npc("Uticus").mood < 70 && (player.jobFlags["UTICUS_ANAL_LastCompleted"] ?? Number.POSITIVE_INFINITY) < setup.world.day,
			text: "Enter the Mansion",
			note: player => `As you approach the entrance to the @@.location-name;Governor's Mansion@@, @@c.npc;Uticus@@ stops you.

			<span class="npcText">"Hey @@state-feminity;${player.slaveName}@@, if you want to be entering \
			don't you think you have to take care of sumthin first?"</span>

			He makes a motion to his trousers and his barely contained monsterous cock.`
		},
		{
			available: player => App.Quest.isCompleted(player, "ROYAL_ENTRANCE") &&
				setup.world.npc("Uticus").mood < 70 && (player.jobFlags["UTICUS_ANAL_LastCompleted"] ?? Number.NEGATIVE_INFINITY) >= setup.world.day,
			text: "Enter the Mansion",
			note: player => `As you approach the entrance to the @@.location-name;Governor's Mansion@@, @@.npc;Uticus@@ stops you.

			<span class="npcText">"Hey @@.state-feminity;${player.slaveName}@@, why don't you come back \
			later and we'll have another round o' it? Maybe then I'll be satisified enough with your whore ass to \
			let ya see the Boss."</span>

			It seems that @@.npc;Uticus@@ won't be satisified with just a fuck or two, you'll have to keep letting \
			him molest you with his monsterous black cock until he says he's had enough.`
		},
		{
			available: player => App.Quest.isCompleted(player, "ROYAL_ENTRANCE") &&
				setup.world.npc("Uticus").mood >= 70 && (player.jobFlags["UTICUS_ANAL_LastCompleted"] ?? Number.NEGATIVE_INFINITY) >= setup.world.day,
			destination: "GI_GovernorsMansionInside",
			text: "Enter the Mansion"
		}
	],
	GI_GovernorsMansionInside: [
		{
			text: "Exit",
			destination: "GI_GovernorsMansion"
		}
	]
});

/* eslint-enable @typescript-eslint/naming-convention */
