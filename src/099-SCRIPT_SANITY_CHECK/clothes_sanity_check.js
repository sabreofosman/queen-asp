(function () {
	if (App == null) {
		alert("App == null!");
		return;
	}
	if (App.Data == null) {
		alert("App.Data == null!");
		return;
	}
	if (App.Data.clothes == null) {
		alert("App.Data.Clothes == null!");
		return;
	}

	for (var prop in App.Data.clothes) {
		if (App.Data.clothes.hasOwnProperty(prop)) {
			var clothingItem = App.Data.clothes[prop];
			if (clothingItem == null) {
				alert("Clothing item \"" + prop + "\" is null!");
			}
			if (clothingItem.name !== prop) {
				alert("Clothing item \"" + prop + "\" has a different name: \"" + clothingItem.name + "\"!");
			}
		}
	}
})();
