/* eslint-disable @typescript-eslint/naming-convention */
App.registerPassageDisplayNames({
	GirlfriendRoom: player => player.girlfriendName + "'s Room",
	GovernorStudy: player => App.Quest.isCompleted(player, "DADDYSGIRL") ? "@@.state-girlinness;Daddy's room@@" : "Study",
	IslaFarm: "Farmlands",
	IslaHarbor: "The Docks",
	IslaMansion: "Governors Mansion",
	IslaStore: "General Store",
	IslaTavern: "Tavern",
	IslaTown: "Town Square",
	IslaVillageLane: "Village Lane",
});

App.registerTravelDestination({
	IslaHarbor: [
		{
			destination: "Deck",
			text: "Back to The Mermaid"
		},
		"IslaTown", "IslaTavern"
	],
	IslaTavern: ["IslaHarbor"],
	IslaTown: [
		"IslaHarbor", "IslaVillageLane",
		{
			destination: "IslaStore",
			enabled: () => setup.world.phase < App.Phase.Night
		},
		{
			destination: "IslaMansion",
			enabled: player => setup.world.phase < App.Phase.Night || App.Quest.isCompleted(player, "THEBACKDOOR")
		}
	],
	IslaVillageLane: ["IslaTown", "IslaFarm"],
	IslaFarm: [
		"IslaVillageLane",
		{
			destination: "",
			text: "It's too embarassing to go home",
			enabled: () => false
		}
	],
	IslaStore: ["IslaTown"],
	IslaMansion: [
		"IslaTown",
		{
			// guarded until night
			available: player => setup.world.phase < App.Phase.Night && !App.Quest.isCompleted(player, "DADDYSGIRL"),
			text: player => player.girlfriendName + "'s Room",
			note: player => `As you attempt to gain entry into @@.state-girlinness;${player.girlfriendName}'s Room@@` +
				"the steward steps forward and blocks your way.\n\n\
			@@.npc;Jarvis@@ says, <span class=\"npcText\">\"Excuse me, but I can't allow you to enter any further than this. \
			The young miss isn't about but I dare say she wouldn't like you rifling through her things.</span>\n\n\
			//Perhaps if there was some way to distract him…//"
		},
		{
			// guarded until night
			available: player => setup.world.phase < App.Phase.Night && !App.Quest.isCompleted(player, "DADDYSGIRL"),
			text: "Study",
			note: `You move forward toward's the @@.location-name;Governor's Study@@, but the steward steps forward and blocks your way.

			@@.npc;Jarvis@@ says, <span class="npcText">"Sorry miss, but the Governor isn't taking any visitors now. \
			He's quite distraught over the disappearance of his daughter. Please come back some other time… or don't."</span>

			//How can you convince him to let you through without exposing your embarassing identity?//`
		},
		{
			available: player => setup.world.phase >= App.Phase.Night || App.Quest.isCompleted(player, "DADDYSGIRL"),
			destination: "GovernorStudy"
		},
		{
			available: player => setup.world.phase >= App.Phase.Night || App.Quest.isCompleted(player, "DADDYSGIRL"),
			destination: "GirlfriendRoom"
		}
	],
	GovernorStudy: ["IslaMansion"],
	GirlfriendRoom: ["IslaMansion"]
});

/* eslint-enable @typescript-eslint/naming-convention */
