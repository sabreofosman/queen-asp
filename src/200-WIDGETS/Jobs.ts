Macro.add("JobsWidget", {
	handler() {
		const pl = setup.player;
		const je = setup.jobEngine;

		this.output.append(App.UI.Widgets.rJobList(pl, setup.world.npc(this.args[1] as string),
			je.getAvailableLocationJobs(pl, this.args[0] as string), je.getUnavailableLocationJobs(pl, this.args[0] as string)));
	}
});
