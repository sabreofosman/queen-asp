Macro.add("printJobResult", {
	handler() {
		this.output.append(App.UI.rJobResults(setup.player, temporary()['MPC'] as App.Entity.NPC, this.args[0] as App.Job))
	}
});
