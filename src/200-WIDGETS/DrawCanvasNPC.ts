Macro.add("DrawCanvasNPC", {
	handler() {
		const div = App.UI.appendNewElement('div', this.output);
		div.id = 'npcUI';
		div.style.display = 'inline-block';
		setup.avatar.drawCanvasNPC(this.args[0] as string, div.id, 560, 266);
	}
});
