namespace App.UI.Widgets {
	export function storeInventory(player: Entity.Player, npc: Entity.NPC): HTMLTableElement {
		const store = App.StoreEngine.openStore(player, npc);

		const restockingText = store.ownerMood >= 80 ?
			(store.daysUntilRestocking() > 0 ? `Restocking in ${store.daysUntilRestocking()} days` : "Fresh supplies arrived this morning!")
			: null;

		const missingItemsForQuests = Quest.allMissingItems(player);

		const inventoryTable = makeElement('table', undefined, ['store-inventory-table']);
		const colgroup = appendNewElement('colgroup', inventoryTable);
		appendNewElement('col', colgroup, undefined, ['name']);
		appendNewElement('col', colgroup, undefined, ['quantity']);
		appendNewElement('col', colgroup, undefined, ['price']);
		appendNewElement('col', colgroup, undefined, ['buy']);
		appendNewElement('col', colgroup, undefined, ['examine']);

		const tHeader = appendNewElement('thead', inventoryTable);
		const header = appendNewElement('tr', tHeader);
		const caption = appendNewElement('td', header);
		caption.colSpan = 3;
		caption.style.textAlign = "center";
		const shopName = appendNewElement('span', caption, store.name, ['shopName']);
		if (restockingText) {
			if (settings.inlineItemDetails) {
				appendNewElement('td', header, restockingText).colSpan = 2;
			} else {
				appendNewElement('span', shopName, restockingText, ['tooltip']);
			}
		}

		const makeStoreBuyButtons = (item: Data.StoreInventoryItem) => {
			const buyButton = makeElement('button', "Buy", ['store-button']);
			buyButton.onclick = () => {
				store.buyItems(item, 1);
				Engine.play("Shop"); // FIXME
			};
			let noBuyAllButton = false;
			const price = store.getPrice(item);
			if (item.qty < 1) {
				buyButton.classList.add('disabled-store-button');
				noBuyAllButton = true;
			}
			if (player.maxItemCapacity(item)) {
				buyButton.classList.add('disabled-store-button');
				buyButton.textContent = "Full";
				noBuyAllButton = true;
			}
			if (player.money < price) {
				buyButton.classList.add('disabled-store-button');
				noBuyAllButton = true;
			}
			if (item.type === Items.Category.Clothes || item.type === Items.Category.Weapon) { // no "buy all" button
				if (player.ownsWardrobeItem(item.tag)) {
					buyButton.textContent = "Owned";
					buyButton.classList.add('disabled-store-button');
					noBuyAllButton = true;
				}
			}

			if (item.qty < 2 || player.money < price * 2) {
				noBuyAllButton = true;
			}

			if (noBuyAllButton) {
				return buyButton;
			}

			const buyAllButton = makeElement('button', "Buy all", ['store-button']);
			buyAllButton.onclick = () => {
				store.buyItems(item);
				Engine.play("Shop"); // FIXME
			}

			const res = new DocumentFragment();
			res.append(buyButton);
			res.append(" ");
			res.append(buyAllButton);
			return res;
		};

		const makeStoreExamineButton = (item: Data.StoreInventoryItem) => {
			const button = makeElement('button', "Examine", ['store-button']);
			button.onclick = () => {
				UI.replace('#storeExamine', store.printItemLong(item));
			}
			return button;
		};

		const appendSection = (container: HTMLTableSectionElement, name: string, styleName: string, inv: Data.StoreInventoryItem[]): void => {
			const headerRow = appendNewElement('tr', container, undefined, ['section-header']);
			const nameCell = appendNewElement('td', headerRow, undefined, ['name']);
			appendNewElement('span', nameCell, name, [styleName]);
			appendNewElement('td', headerRow, "Quantity", ['quantity']);
			appendNewElement('td', headerRow, "Price", ['price']);
			appendNewElement('td', headerRow, undefined, ['buy']);
			appendNewElement('td', headerRow, undefined, ['examine']);

			if (inv.length === 0) {
				const row = appendNewElement('tr', container);
				const note = appendNewElement('td', row);
				note.colSpan = 5;
				appendNewElement('span', note, "Nothing for sale!", ['attention']);
				return;
			}

			for (const itm of inv) {
				const row = appendNewElement('tr', container);
				const n = appendNewElement('td', row);
				if (missingItemsForQuests.hasOwnProperty(Items.makeId(itm.type, itm.tag))) {
					appendNewElement('span', n, "(!) ", ['item-required-for-quest']);
				}
				$(n).wiki(store.printItem(itm));
				const q = appendNewElement('td', row, `${itm.qty}`);
				q.style.textAlign = 'center';
				const p = appendNewElement('td', row);
				p.style.textAlign = 'right';
				$(p).wiki(`${store.getPrice(itm)}`);
				const b = appendNewElement('td', row);
				b.style.textAlign = 'left';
				b.append(makeStoreBuyButtons(itm));
				const e = appendNewElement('td', row);
				e.style.textAlign = 'center';
				e.append(makeStoreExamineButton(itm));
			}
		}

		const inventory = appendNewElement('tbody', inventoryTable);
		appendSection(inventory, "General Items", 'item-category-general', store.getCommonInventory());
		appendSection(inventory, "Rare Items", 'item-category-rare', store.getRareInventory());
		const footer = appendNewElement('tfoot', inventoryTable);
		const fRow = appendNewElement('tr', footer);
		const cExamine = appendNewElement('td', fRow);
		cExamine.colSpan = 5;
		const examine = appendNewElement('span', cExamine);
		examine.id = "storeExamine";

		return inventoryTable;
	}
}

Macro.add("storeInventory", {
	handler() {
		this.output.append(App.UI.Widgets.storeInventory(setup.player, variables().menuAction as App.Entity.NPC));
	}
});
