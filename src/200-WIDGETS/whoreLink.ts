Macro.add("whoreLink", {
	handler() {
		if (setup.world.whoreTutorial) {
			this.output.append(App.UI.passageLink("Whore", "Whore", () => {
				setup.player.adjustStat(App.CoreStat.Energy, -1);
				App.SlotEngine.instance.loadScene(this.args[0] as string, passage());
			}));
		} else {
			this.output.append(App.UI.passageLink("Whore", "WhoreTutorial", () => {
				setup.world.whoreTutorial = true;
				variables().whore.link = this.args[0] as string;
				variables().whore.passage = passage();
			}));
		}
	}
});
