namespace App.UI.Widgets.Wardrobe {

	function toggleCollapsible(this: HTMLButtonElement) {
		this.classList.toggle("wardrove-desc-cover-active");
		const content = this.nextElementSibling as HTMLDivElement;
		if (content.style.maxHeight) {
			content.style.maxHeight = "";
		} else {
			content.style.maxHeight = `${content.scrollHeight}px`;
		}
	}

	class SlotPanel {
		#table: WardrobeTable;
		#slot: ClothingSlot;

		constructor(table: WardrobeTable, slot: ClothingSlot) {
			this.#table = table;
			this.#slot = slot;
		}

		render(): HTMLTableRowElement {
			const eqInSlot = this.#table.player.equipment[this.#slot];
			const res = document.createElement('tr');
			appendNewElement('td', res, this.#slot);
			const descCell = appendNewElement('td', res);
			const desc = appendNewElement('button', descCell);
			const descContent = appendNewElement('div', descCell, undefined, ['wardrove-item-details']);
			desc.classList.add('wardrove-desc-cover');
			desc.addEventListener("click", toggleCollapsible.bind(desc));
			if (eqInSlot) {
				$(desc).wiki(eqInSlot.description);
			} else {
				appendNewElement('span', desc, 'Nothing', ['state-disabled']);
			}
			// details and options
			if (eqInSlot) {
				descContent.append(this._makeRemoveCard(eqInSlot)); // will handle locked items
			}
			if (!eqInSlot || !eqInSlot.isLocked) {
				const items = this.#table.player.wardrobeItemsBySlot(this.#slot);
				items.sort(Items.Clothing.compareByRank);
				for (const itm of items) {
					descContent.append(this._makeWearCard(itm));
				}
			}

			return res;
		}

		private _makeOptionCard(item: Items.Clothing, actionCaption: string, action?: () => void) {
			const element = makeElement('div', undefined, ['wardrove-change-option']);
			const btnAction = appendNewElement("button", element, actionCaption, ["wardrobe"]);
			if (action) {
				btnAction.onclick = action;
			} else {
				btnAction.classList.add('disabled-wardrobe');
			}
			const title = appendNewElement("span", element, undefined, ['wardrobe-option-caption']);
			$(title).wiki(item.description);
			const desc = appendNewElement("div", element, undefined, ['wardrobe-item-description']);
			$(desc).wiki(item.examine(this.#table.player));
			return element;
		}

		private _wear(item: Items.Clothing) {
			this.#table.player.wear(item);
			this.#table.parent.update();
		}

		private _remove(item: Items.Clothing) {
			this.#table.player.remove(item);
			this.#table.parent.update();
		}

		private _makeWearCard(item: Items.Clothing) {
			return this._makeOptionCard(item, "Wear", this._wear.bind(this, item));
		}

		private _makeRemoveCard(item: Items.Clothing) {
			if (item.isLocked) {
				return this._makeOptionCard(item, "Locked");
			} else {
				return this._makeOptionCard(item, "Remove", this._remove.bind(this, item));
			}
		}
	}

	class WardrobeTable extends SelfUpdatingTable {
		#player: Entity.Player;
		#slotPanels: SlotPanel[] = [];
		#parent: WardrobeUI;

		constructor(player: Entity.Player, parent: WardrobeUI) {
			super();
			this.#parent = parent;

			const footer = appendNewElement('tfoot', this.table);
			const footerRow = appendNewElement('tr', footer);
			appendNewElement('td', footerRow);
			const footerCell = appendNewElement('td', footerRow);
			const getNakedLink = App.UI.appendNewElement('a', footerCell, "Get naked", ['link-internal']);
			getNakedLink.onclick = () => {
				setup.player.strip();
				this.#parent.update();
			};
			this.#player = player;

			this.table.classList.add('wardrobe-table');

			const orderedSlots: ClothingSlot[] = [
				ClothingSlot.Wig, ClothingSlot.Hat, ClothingSlot.Neck,
				ClothingSlot.Bra, ClothingSlot.Nipples,
				ClothingSlot.Corset, ClothingSlot.Panty, ClothingSlot.Stockings,
				ClothingSlot.Shirt, ClothingSlot.Pants, ClothingSlot.Dress, ClothingSlot.Costume,
				ClothingSlot.Shoes, ClothingSlot.Butt, ClothingSlot.Penis, ClothingSlot.Weapon
			];

			for (const slot of orderedSlots) {
				this.#slotPanels.push(new SlotPanel(this, slot));
			}

			this.header.append(WardrobeTable.wardrobeTableHeader())
		}

		protected static wardrobeTableHeader(): HTMLTableRowElement {
			const res = document.createElement('tr');
			const slotCol = appendNewElement('th', res, "Slot");
			slotCol.style.width = "12em";
			const descCol = appendNewElement('th', res, "Item");
			descCol.style.width = "100%";
			return res;
		}

		get player() {
			return this.#player;
		}

		get parent() {
			return this.#parent;
		}

		update() {
			const rows = this.#slotPanels.map(p => p.render());
			replace(this.body, ...rows);
		}
	}

	class OutfitAutoStyleTable extends SelfUpdatingTable {
		#player: Entity.Player;
		#parent: OutfitStylePanel;

		constructor(player: Entity.Player, parent: OutfitStylePanel) {
			super();
			this.#player = player;
			this.#parent = parent;
		}

		update() {
			replace(this.body, this._makeBody());
		}

		private _makeBody(): DocumentFragment {
			const rows = new DocumentFragment();
			const style = Data.Fashion.Style;
			rows.append(this._styleRow([style.Ordinary, style.PirateSlut, style.Bimbo]));
			rows.append(this._styleRow([style.SissyLolita, style.GothicLolita, style.Bdsm]));
			rows.append(this._styleRow([style.DaddyGirl, style.NaughtyNun, style.PetGirl]));
			rows.append(this._styleRow([style.HighClassWhore, style.SluttyLady, style.SexyDancer]));
			return rows;
		}

		private _styleItem(styleName: Data.Fashion.Style): [HTMLTableCellElement, HTMLTableCellElement] {
			const nameCell = document.createElement('td');
			if (styleName !== Data.Fashion.Style.Ordinary) {
				const link = appendNewElement('a', nameCell, styleName, ['link-internal']);
				link.onclick = this._wearStyle.bind(this, styleName);
				nameCell.append(':');
			} else {
				nameCell.textContent = "Ordinary:";
			}
			const meterCell = document.createElement('td');
			const meterSpan = appendNewElement('span', meterCell, undefined, ['fixed-font']);
			meterSpan.append(rMeter(setup.player.getStyleSpecRating(styleName), 100, {showScore: settings.displayMeterNumber}));
			return [nameCell, meterCell];
		}

		private _styleRow(styles: Data.Fashion.Style[]) {
			const row = document.createElement('tr');
			for (const st of styles) {
				row.append(...this._styleItem(st));
			}
			return row;
		}

		private _wearStyle(cat: Data.Fashion.Style) {
			this.#player.autoWearStyle(cat);
			this.#parent.styleChanged();
		}
	}

	class OutfitStylePanel {
		#styleDesc: HTMLSpanElement;
		#stylingChoices: OutfitAutoStyleTable;
		#player: Entity.Player;
		#parent: WardrobeUI;
		#element: HTMLDivElement;

		constructor(player: Entity.Player, parent: WardrobeUI) {
			this.#player = player;
			this.#parent = parent;
			this.#element = document.createElement('div');
			const caption = appendNewElement('button', this.#element, "The style and fashion of your clothing is ");
			this.#styleDesc = appendNewElement('span', caption);
			const content = appendNewElement('div', this.#element, undefined, ['wardrove-item-details']);
			caption.classList.add('wardrove-desc-cover');
			caption.addEventListener("click", toggleCollapsible.bind(caption));
			this.#stylingChoices = new OutfitAutoStyleTable(player, this);
			content.append("Current Fashion Breakdown (click a name to automatically dress that way)");
			content.append(this.#stylingChoices.table);
			this.#stylingChoices.render();
			this.update();
		}

		get element() {
			return this.#element;
		}

		update() {
			replace(this.#styleDesc, PR.pClothing(this.#player));
			this.#stylingChoices.update();
		}

		styleChanged() {
			this.#parent.update();
		}
	}

	class EquipmentSetsPanel {
		constructor(player: Entity.Player, parent: WardrobeUI) {
			this.#player = player;
			this.#parent = parent;
			this.#element = document.createElement('div');
			const caption = appendNewElement('button', this.#element, "Saved equipment sets");
			const content = appendNewElement('div', this.#element, undefined, ['wardrove-item-details']);
			caption.classList.add('wardrove-desc-cover');
			caption.addEventListener("click", toggleCollapsible.bind(caption));
			const selectorId = "equipment-set-selector";

			const wearActionsDiv = content.appendNewElement('div', undefined, ['spaced-horizontal-container']);
			wearActionsDiv.appendNewElement('label', "Select and apply a saved set: ").htmlFor = selectorId;
			this.#setSelector = wearActionsDiv.appendNewElement("select");
			this.#setSelector.id = selectorId;
			this.#setSelector.onchange = this._onSetSelected.bind(this);
			this.#wear = wearActionsDiv.appendNewElement('button', "Wear", ["wardrobe"]);
			this.#wear.onclick = () => {
				this.#player.clothing.wearSet(this._selectedSetIndex());
				this.#parent.update();
			};

			const editActionsDiv = content.appendNewElement('div', undefined, ['spaced-horizontal-container']);
			this.#name = editActionsDiv.appendNewElement('input');
			this.#name.required = true;
			this.#save = editActionsDiv.appendNewElement('button', "Save", ["wardrobe"]);
			tippy(this.#save, {content: "Save currently worn equipment as a named set"});
			this.#save.onclick = this._saveSet.bind(this);
			this.#remove = editActionsDiv.appendNewElement('button', "Delete", ["wardrobe"]);
			this.#remove.onclick = () => {
				this.#player.clothing.deleteSet(this._selectedSetIndex());
				this.update();
			};

			this.update();
		}

		update(): void {
			this._refreshSavedSets();
			this._onSetSelected();
		}

		get element() {
			return this.#element;
		}

		private _refreshSavedSets() {
			const sets = this.#player.clothing.storedSets.map((s, i) => {return {index: i, set: s};});
			sets.sort((a, b) => a.set.name < b.set.name ? -1 : (a.set.name === b.set.name ? 0 : 1));
			const options: HTMLOptionElement[] = [];
			sets.forEach((s) => {
				const option = document.createElement('option');
				option.value = s.index.toString();
				option.append(s.set.name);
				options.push(option);
			});
			this.#setSelector.replaceChildren(...options);
		}

		private _onSetSelected() {
			const setIndex = this._selectedSetIndex();
			if (setIndex >= 0) {
				const set = this.#player.clothing.storedSets[setIndex];
				const tooltip = new DocumentFragment;
				tooltip.append(`Wear selected set '${set.name}', which consists of:`);
				const slotList = appendNewElement('ul', tooltip);
				for (const [s, it] of Object.entries(set.items)) {
					slotList.appendNewElement('li', `${s}: ${Items.splitId(it).tag}`)
				}
				tippy(this.#wear, {content: tooltip});
				this.#name.value = set.name;
			} else {
				tippy(this.#wear);
				this.#name.value = "";
			}
		}

		private _saveSet() {
			const name = this.#name.value;
			if (!name.length) {
				alert("Please provide a name for the set");
				return;
			}
			const existing = this.#player.clothing.storedSets.find(s => s.name === name);
			if (existing) {
				alert(`Set named '${name}' already exists. Please choose a different name.`);
				return;
			}
			this.#player.clothing.saveCurrentSet(this.#name.value);
			this.update();
		}

		private _selectedSetIndex(): number {
			if (!this.#setSelector.selectedOptions.length) {
				return -1;
			}
			return parseInt(this.#setSelector.selectedOptions[0].value);
		}

		#player: Entity.Player;
		#parent: WardrobeUI;
		#element: HTMLDivElement;
		#setSelector: HTMLSelectElement;
		#name: HTMLInputElement;
		#wear: HTMLButtonElement;
		#save: HTMLButtonElement;
		#remove: HTMLButtonElement;
	}

	export class WardrobeUI {
		#element: HTMLDivElement;
		#slotsTable: WardrobeTable;
		#stylePanel: OutfitStylePanel;
		#setsPanel: EquipmentSetsPanel;

		constructor(player: Entity.Player) {
			this.#element = document.createElement('div');
			this.#stylePanel = new OutfitStylePanel(player, this);
			this.#slotsTable = new WardrobeTable(player, this);
			this.#setsPanel = new EquipmentSetsPanel(player, this);

			this.#element.append(this.#stylePanel.element);
			this.#element.append(this.#setsPanel.element);
			const header = App.UI.appendNewElement('div', this.#element);
			header.style.display = "flex";
			header.style.flexWrap = "nowrap";

			const captionDiv = App.UI.appendNewElement('div', header);
			captionDiv.style.width = "100%";
			const captionH = App.UI.appendNewElement('h3', captionDiv);
			App.UI.appendNewElement('span', captionH, "Worn Items", ['item-category-general']);
			this.#element.append(this.#slotsTable.render());
		}

		get element(): HTMLElement {
			return this.#element;
		}

		update(): void {
			this.#stylePanel.update();
			this.#setsPanel.update();
			this.#slotsTable.update();
		}
	}
}

Macro.add("WardrobeUI", {
	handler() {
		this.output.append(new App.UI.Widgets.Wardrobe.WardrobeUI(this.args[0] as App.Entity.Player).element);
	}
});
