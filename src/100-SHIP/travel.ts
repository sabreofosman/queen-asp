/* eslint-disable @typescript-eslint/naming-convention */
App.registerPassageDisplayNames({
	FirstMate: "Ships Helm",
	Cargo: "Cargo Hold",
	Captain: "Captain's Quarters"
});

App.registerTravelDestination({
	Cabin: ["Deck"],
	Captain: ["Deck"],
	Cargo: ["Deck"],
	Deck: [
		"Cabin",
		{
			destination: "FirstMate",
			enabled: App.Data.Travel.disableAtLateNight
		},
		{
			destination: "Galley",
			enabled: App.Data.Travel.disableAtLateNight
		},
		{
			destination: "Cargo",
			enabled: App.Data.Travel.disableAtLateNight
		},
		{
			destination: "Captain",
			enabled: App.Data.Travel.disableAtLateNight
		}
	],
	FirstMate: ["Deck"],
	Galley: ["Deck"],
	MermaidFightClub: ["Deck"]
});
